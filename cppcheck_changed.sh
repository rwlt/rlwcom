#!/bin/sh

if git rev-parse --verify HEAD >/dev/null 2>&1
then
	against=HEAD
else
	# Initial commit: diff against an empty tree object
	against=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi

# We should pass only added or modified C/C++ source files to cppcheck.
changed_files=$(git diff-index --cached $against | \
	grep -E '[MA]	.*\.(c|cpp|cc|cxx)$' | cut -d'	' -f 2)

#echo $changed_files

if [ -n "$changed_files" ]; then
	cppcheck --std=c11 --enable=style --template='gcc' --error-exitcode=1 $changed_files
	exit $?
fi
