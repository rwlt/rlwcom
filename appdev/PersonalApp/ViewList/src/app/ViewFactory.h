#ifndef VIEWFACTORY_H_
#define VIEWFACTORY_H_


#include "module/AbstractViewFactory.h"
#include "module/ViewIdentity.h"
#include "MainWindowView.h"
#include "PersonView.h"
#include "LoginView.h"

namespace Module {
    class Controller;
    class MainWindowInterface;
    class PersonWindowInterface;
    class DBLoginWindowInterface;
}

namespace App
{

/// Creating Views
/** ViewFactory for creating views required from module personal presenter
 *
 * \sa Module::AbstractViewFactory
 */
class ViewFactory: public Module::AbstractViewFactory
{
public:
    /// Constructor
    ViewFactory ( );
    /// Deconstructor
    ~ViewFactory();
    /// Create instance of the Module Presenter View
    /**
     * Create the View interfaces used by the presenter to be able to load the data from presenter.
     *
     * The main view will always have parent view as empty so the main view is known. Can only be one
     * main view from a Module presenter.
     *
     * \param controller Module controller pointer reference
     * \param viewIdentity Module presenter view identityInstance
     * \param parentViewIdentity A parent view identity (default is empty parent)
     *
     * \sa mainInterface(), personInterface() and Module::ViewIdentity()
     */
    virtual void createInstance ( Module::Controller* controller,
        Module::ViewIdentity viewIdentity, Module::ViewIdentity parentViewIdentity = Module::ViewIdentity::NullView );
    /// Destroy instance of a Module Presenter View
    /**
     * \param viewIdentity Module presenter view identityInstance
     *
     * \sa Module::View() and Module::ViewIdentity()
     */
    virtual void destroyInstance ( Module::ViewIdentity viewIdentity );
    /// Retrieve main window view
    /**
     * \return pointer to Module::MainWindowInterface()
     */
    App::MainWindowView *mainWindow();
    /// Retrieve main window view
    /**
     * \return pointer to Module::MainWindowInterface()
     */
    Module::MainWindowInterface *mainInterface();
    /// Retrieve instance of person view
    /**
     * \return pointer to Module::PersonWindowInterface()
     */
    Module::PersonWindowInterface *personInterface ();
    /// Retrieve instance of login view
    /**
     * \return pointer to Module::DBLoginWindowInterface()
     */
    Module::DBLoginWindowInterface *loginInterface ();
    /// Number of views created and open
    /**
     * \return int of number of created and open views
     */
    int countViews();

private:
    ViewFactory ( const ViewFactory &old ); // no copying allowed
    ViewFactory operator = ( const ViewFactory &old ); // no assignment allowed

    Module::View *moduleView[Module::ViewIdentity::TotalViews];
    Module::ViewIdentity m_activeViewIdentity;

    MainWindowView view_main;
    PersonView     view_person;
    LoginView      view_login;
    
};

}
#endif /* VIEWFACTORY_H_ */
