/*
 * PersonView.cpp
 */

#include "PersonView.h"
#include "module/Controller.h"
#include <sstream>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/window.h>
#include <gtkmm/grid.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/paned.h>
#include <gtkmm/frame.h>
#include <gtkmm/buttonbox.h>
#define GTKMM_DISABLE_DEPRECATED
#include <vector>

namespace App
{


//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
//
PersonView::PersonView():
    status(componentStatusBar),
    m_firstNameValidateLabel("Firstname"),
    m_surnameValidateLabel("Surname"),
    addButton("_Add", true),
    changeButton("C_hange", true),
    cancelButton("_Cancel", true),
	m_personEdit(this),
	m_validationList(this),
	m_editState(this)
{ }
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
PersonView::~PersonView()
{
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
int PersonView::createView(std::unique_ptr<Module::PersonPresenter> prsntr)
{
    presenter = std::move(prsntr);

    //set_default_size(380, 230);
    //WindowFramed::add(componentPanel);
    Gtk::Window::add(componentPanel);
    // Menu actions - use SimpleActionGroup
    m_refActionGroup = Gio::SimpleActionGroup::create();
    m_refActionGroup->add_action("quit", sigc::mem_fun(*this, &PersonView::on_cancel_click));
    insert_action_group("personview", m_refActionGroup);
    // Menu bar
    const char *ui_info =
        "<interface>"
        "  <menu id='menubar'>"
        "    <submenu id='editmenu'>"
        "      <section>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>_Quit</attribute>"
        "          <attribute name='action'>personview.quit</attribute>"
//        "          <attribute name='accel'>&lt;Primary&gt;q</attribute>"
        "        </item>"
        "      </section>"
        "    </submenu>"
        "  </menu>"
        "<interface>";
    componentMenu.createControl(ui_info);
    componentMenu.addMenu("_Edit", "editmenu");
    componentPanel.add(componentMenu);

    // Status bar
    componentPanel.add(componentStatusBar);

    // Person edit boxes
    m_firstNameEdit.createControl(3);
    m_surnameEdit.createControl(4);
    m_firstNameValidateLabel.createControl(5);
    m_surnameValidateLabel.createControl(6);
    m_firstNameEdit.set_width_chars(42);
    m_surnameEdit.set_max_width_chars(50);
    m_surnameEdit.set_width_chars(42);
    m_surnameEdit.set_max_width_chars(50);
    Gtk::Label *l1 = Gtk::manage(new Gtk::Label("First name:", Gtk::ALIGN_START));
    Gtk::Label *l2 = Gtk::manage(new Gtk::Label("Surname:", Gtk::ALIGN_START));

    Gtk::Grid *gridPn = Gtk::manage(new Gtk::Grid);
    gridPn->set_border_width(8);
    gridPn->set_row_spacing(2);
    gridPn->set_column_spacing(6);
    gridPn->attach(m_firstNameEdit, 0, 0, 5, 1);
    gridPn->attach(m_surnameEdit, 0, 2, 5, 1);
    gridPn->attach_next_to(*l1, m_firstNameEdit, Gtk::POS_TOP, 2, 1);
    gridPn->attach_next_to(*l2, m_surnameEdit, Gtk::POS_TOP, 2, 1);
    gridPn->attach_next_to(m_firstNameValidateLabel, *l1, Gtk::POS_RIGHT, 3, 1);
    gridPn->attach_next_to(m_surnameValidateLabel, *l2, Gtk::POS_RIGHT, 3, 1);

    addButton.signal_clicked().connect(sigc::mem_fun(*this, &PersonView::on_add_click));
    changeButton.signal_clicked().connect(sigc::mem_fun(*this, &PersonView::on_change_click));
    cancelButton.signal_clicked().connect(sigc::mem_fun(*this, &PersonView::on_cancel_click));

    Gtk::ButtonBox *button_Box = Gtk::manage(new Gtk::ButtonBox(Gtk::ORIENTATION_VERTICAL));
    button_Box->set_border_width(5);
    button_Box->set_layout(Gtk::BUTTONBOX_START);
    button_Box->set_spacing(10);
    button_Box->pack_start(addButton);
    button_Box->pack_start(changeButton);
    button_Box->pack_start(cancelButton);
    //button_Box->set_focus_chain(std::vector<Gtk::Widget*>());
    addButton.set_can_default(true); //n.set_can_default();
    cancelButton.set_can_default(true); //n.set_can_default();
    button_Box->set_focus_child(addButton);    

    Gtk::Box *allbox = Gtk::manage(new Gtk::Box());
    allbox->set_border_width(10);
    allbox->pack_start(*gridPn, Gtk::PACK_EXPAND_WIDGET);
    allbox->pack_start(*button_Box, Gtk::PACK_SHRINK);

    Gtk::Frame *allframe = Gtk::manage(new Gtk::Frame());
    allframe->set_shadow_type(Gtk::SHADOW_ETCHED_IN);
    allframe->set_border_width(8);
    allframe->add(*allbox);
    componentPanel.pack_start(*allframe, Gtk::PACK_EXPAND_WIDGET);

//    addWindowClient(&m_firstNameEdit);
//    addWindowClient(&m_surnameEdit);
//    addWindowClient(&m_firstNameValidateLabel);
//    addWindowClient(&m_surnameValidateLabel);

    componentPanel.show_all();
    presenter->initialize();
    return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void PersonView::readEditControl()
{
	m_personEdit.getItem();
}
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void PersonView::showView(const Module::Message &message)
{
    static int shown = 0;
    ++shown;
    std::stringstream title;
    title << "PERSON EDIT shown " << std::to_string(shown) << " times.";
    set_title(title.str());
    present();

}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void PersonView::closeView()
{
    hide();
}

void PersonView::on_add_click()
{
    componentStatusBar.updateText("Add Pressed!");
    presenter->addNewPerson();
}

void PersonView::on_change_click()
{
    componentStatusBar.updateText("Change Pressed!");
    presenter->changePerson();
}

void PersonView::on_cancel_click()
{
    hide();
}

} /* namespace App */

