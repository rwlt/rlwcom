/*
 * MainWindowView.cpp
 */

#include "MainWindowView.h"
#include "module/Message.h"
#include "module/Controller.h"
#include <giomm-2.4/giomm/simpleactiongroup.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/window.h>
#include <gtkmm/grid.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/paned.h>
#include <gtkmm/frame.h>
#include <gtkmm/messagedialog.h>
#include <sstream>
#define GTKMM_DISABLE_DEPRECATED
#include <iostream>

namespace App
{

//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
//
MainWindowView::MainWindowView(): Module::MainWindowInterface(true) {}
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
MainWindowView::~MainWindowView() {}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
int MainWindowView::createView(std::unique_ptr<Module::MainPresenter> prsntr)
{
    presenter = std::move(prsntr);
    set_default_size(780, 520);
    //WindowFramed::add(componentPanel);
    Gtk::Window::add(componentPanel);

    // Menu actions - use SimpleActionGroup
    Glib::RefPtr<Gio::SimpleActionGroup> actionGroup;
    actionGroup = Gio::SimpleActionGroup::create();
    actionGroup->add_action("new",
                            sigc::mem_fun(*this, &MainWindowView::on_file_new));
    actionGroup->add_action("open",
                            sigc::mem_fun(*this, &MainWindowView::on_file_open));
    actionGroup->add_action("save",
                            sigc::mem_fun(*this, &MainWindowView::on_file_save));

    actionGroup->add_action("undo",
                            sigc::mem_fun(*this, &MainWindowView::on_edit_undo));
    actionGroup->add_action("redo",
                            sigc::mem_fun(*this, &MainWindowView::on_edit_redo));

    actionGroup->add_action("login",
                            sigc::mem_fun(*this, &MainWindowView::on_login_click));

    actionGroup->add_action("quit",
                            sigc::mem_fun(*this, &MainWindowView::on_quit_click));

    actionGroup->add_action("popupedit", sigc::bind<Glib::ustring>(
                                sigc::mem_fun(*this, &MainWindowView::on_person_list_popup_generic), "edit"));
    actionGroup->add_action("popupremove", sigc::bind<Glib::ustring>(
                                sigc::mem_fun(*this, &MainWindowView::on_person_list_popup_generic), "remove"));
    // Insert mainview so the GtkBuilder can use the actions.
    insert_action_group("mainview", actionGroup);

    // Use GtkBuilder to make the menu in Menu object - pass this to it to make those menus
    // Menu bar
    const char *ui_info2 =
        "<interface>"
        "  <menu id='menubar'>"
        "    <submenu id='filemenu'>"
        "      <section>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>_New</attribute>"
        "          <attribute name='action'>mainview.new</attribute>"
        "          <attribute name='accel'>&lt;Primary&gt;n</attribute>"
        "        </item>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>_Open</attribute>"
        "          <attribute name='action'>mainview.open</attribute>"
        "          <attribute name='accel'>&lt;Primary&gt;o</attribute>"
        "        </item>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>_Save</attribute>"
        "          <attribute name='action'>mainview.save</attribute>"
        "          <attribute name='accel'>&lt;Primary&gt;s</attribute>"
        "        </item>"
        "      </section>"
        "      <section>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>_Login</attribute>"
        "          <attribute name='action'>mainview.login</attribute>"
        "          <attribute name='accel'>&lt;Primary&gt;l</attribute>"
        "        </item>"
        "      </section>"
        "      <section>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>_Quit</attribute>"
        "          <attribute name='action'>mainview.quit</attribute>"
        "          <attribute name='accel'>&lt;Primary&gt;q</attribute>"
        "        </item>"
        "      </section>"
        "    </submenu>"
        "    <submenu id='editmenu'>"
        "      <section>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>_Undo</attribute>"
        "          <attribute name='action'>mainview.undo</attribute>"
        "          <attribute name='accel'>&lt;Primary&gt;u</attribute>"
        "        </item>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>_Redo</attribute>"
        "          <attribute name='action'>mainview.redo</attribute>"
        "          <attribute name='accel'>&lt;Primary&gt;r</attribute>"
        "        </item>"
        "      </section>"
        "    </submenu>"
        "  </menu>"
        "  <menu id='popupmenu'>"
        "      <section>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>_Edit</attribute>"
        "          <attribute name='action'>mainview.popupedit</attribute>"
        "          <attribute name='accel'>&lt;Primary&gt;e</attribute>"
        "        </item>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>_Remove</attribute>"
        "          <attribute name='action'>mainview.popupremove</attribute>"
        "          <attribute name='accel'>&lt;Primary&gt;m</attribute>"
        "        </item>"
        "      </section>"
        "   </menu>"
        "<interface>";

    componentMenu.createControl(ui_info2);
    componentMenu.addMenu("_File", "filemenu");
    componentMenu.addMenu("_Edit", "editmenu");
    componentPanel.add(componentMenu);

    componentListPopupMenu.createControl(ui_info2);
    componentListPopupMenu.addPopupMenuItem("popupmenu");
    componentListPopupMenu.accelerate(*this);

    // Status bar
    componentPanel.add(componentStatusBar);

    componentPersonDBTreeList.addColumn("PersonID", "Person ID");
    componentPersonDBTreeList.addColumn("Firstname", "First Name");
    componentPersonDBTreeList.addColumn("Surname", "Surname");
    componentPersonDBTreeList.addColumn("ChestID", "Chest ID");
    componentPersonDBTreeList.addSortByColumn("Surname");
    componentPersonDBTreeList.createControl();
    componentPersonDBTreeList.get_tree_selection()->signal_changed().connect(
        sigc::mem_fun(*this, &MainWindowView::on_person_list_selection_changed));

    componentPersonDBTreeList.add(componentListPopupMenu);
    componentPersonDBTreeList.set_hexpand(true);
    componentPersonDBTreeList.set_vexpand(true);
    componentPersonDBTreeList.show();

    componentSystemList.addColumn("String", "System Name");
    //componentSystemList.addSortByColumn("String");
    componentSystemList.createControl();
    componentSystemList.get_tree_selection()->signal_changed().connect(
        sigc::mem_fun(*this, &MainWindowView::on_system_list_selection_changed));
    componentSystemList.set_hexpand(true);
    componentSystemList.set_vexpand(true);
    componentSystemList.show();

    componentCategoryList.addColumn("String", "Category Name");
    //componentCategoryList.addSortByColumn("Index");
    componentCategoryList.createControl();
    componentCategoryList.get_tree_selection()->signal_changed().connect(
        sigc::mem_fun(*this, &MainWindowView::on_category_list_selection_changed));
    componentCategoryList.set_hexpand(true);
    componentCategoryList.set_vexpand(true);
    componentCategoryList.show();

    Gtk::ScrolledWindow *textScroll = Gtk::manage(new Gtk::ScrolledWindow());
    textScroll->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);
    textScroll->add(m_TextView);

    Gtk::Grid *gridPn = Gtk::manage(new Gtk::Grid);
    gridPn->set_border_width(4);
    gridPn->set_row_spacing(2);
    gridPn->set_column_spacing(2);
    gridPn->attach(m_ProgressBar, 0, 0, 1, 1);
    gridPn->attach(*textScroll, 0, 1, 2, 2);

    Gtk::Button *b1 = Gtk::manage(new Gtk::Button("Add"));
    b1->signal_clicked().connect(sigc::mem_fun(*this, &MainWindowView::on_add_click));
    Gtk::Button *b2 = Gtk::manage(new Gtk::Button("Report"));
    b2->signal_clicked().connect(sigc::mem_fun(*this, &MainWindowView::on_report_click));
    m_startButton.signal_clicked().connect(sigc::mem_fun(*this, &MainWindowView::on_start_process_click));
    m_stopButton.signal_clicked().connect(sigc::mem_fun(*this, &MainWindowView::on_stop_process_click));

    Gtk::Box *buttonBox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
    buttonBox->set_border_width(12);
    buttonBox->set_spacing(4);
    buttonBox->pack_start(*b1, Gtk::PACK_SHRINK, 4);
    buttonBox->pack_start(*b2, Gtk::PACK_SHRINK, 4);
    buttonBox->pack_start(m_startButton, Gtk::PACK_SHRINK, 4);
    buttonBox->pack_start(m_stopButton, Gtk::PACK_SHRINK, 4);

    Gtk::Box *allbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL, 0));
    allbox->pack_start(*buttonBox, Gtk::PACK_SHRINK);
    allbox->pack_start(*gridPn, Gtk::PACK_EXPAND_WIDGET);

    Gtk::ScrolledWindow *treeScroll2 = Gtk::manage(new Gtk::ScrolledWindow());
    treeScroll2->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);
    treeScroll2->add(componentPersonDBTreeList);

    Gtk::ScrolledWindow *treeScroll3 = Gtk::manage(new Gtk::ScrolledWindow());
    treeScroll3->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);
    treeScroll3->add(componentSystemList);

    Gtk::ScrolledWindow *treeScroll4 = Gtk::manage(new Gtk::ScrolledWindow());
    treeScroll4->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_ALWAYS);
    treeScroll4->add(componentCategoryList);

    allbox->pack_start(*treeScroll3, Gtk::PACK_EXPAND_WIDGET);
    allbox->pack_start(*treeScroll4, Gtk::PACK_EXPAND_WIDGET);
    
    Gtk::Paned *panedLists = Gtk::manage(new Gtk::Paned(Gtk::ORIENTATION_HORIZONTAL));
    panedLists->pack1(*treeScroll2, true, false);

    Gtk::Paned *paned = Gtk::manage(new Gtk::Paned(Gtk::ORIENTATION_VERTICAL));
    paned->pack1(*panedLists, true, false);
    paned->pack2(*allbox, false, true);
    paned->set_position(250);

    componentPanel.pack_start(*paned, Gtk::PACK_EXPAND_WIDGET);

    // Worker thread setup
    m_ProgressBar.set_text("Fraction done");
    m_ProgressBar.set_show_text();
    m_TextView.set_editable(false);
    // Create a text buffer mark for use in update_widgets().
    auto buffer = m_TextView.get_buffer();
    buffer->create_mark("last_line", buffer->end(), /* left_gravity= */ true);

    // Connect the handler to the dispatcher.
//    m_Dispatcher.connect(sigc::mem_fun(*this, &MainWindowView::on_notification_from_worker_thread));
    m_DBDispatcher.connect(sigc::mem_fun(*this, &MainWindowView::on_notification_from_dbworker_thread));
    
    update_start_stop_buttons();
    // end worker thread init.

    presenter->initialize();
    return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void MainWindowView::readEditControl() {}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showError(std::string message)
{
    Gtk::MessageDialog dlg(message, false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK, true);
    dlg.set_title("Main View Error");
    dlg.set_transient_for(*this);
    dlg.set_modal();
    dlg.set_decorated(false);
    dlg.run();
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showView(const Module::Message &message)
{
    static int shown = 0;
    ++shown;
    std::stringstream title;
    title << "PERSON LIST  shown " << std::to_string(shown) << " times.";
    set_title(title.str());

    componentPanel.show_all();

}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void MainWindowView::on_file_new()
{
    componentStatusBar.updateText("File New menu item opened");
    //presenter->newFile();
}
//
void MainWindowView::on_file_open()
{
    Gtk::FileChooserDialog dialog("Please choose a file",
                                  Gtk::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_transient_for(*this);

    //Add response buttons the the dialog:
    dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    dialog.add_button("_Open", Gtk::RESPONSE_OK);

    //Add filters, so that only certain file types can be selected:
    auto filter_any = Gtk::FileFilter::create();
    filter_any->set_name("Any files");
    filter_any->add_pattern("*");
    dialog.add_filter(filter_any);

    auto filter_text = Gtk::FileFilter::create();
    filter_text->set_name("Text files");
    filter_text->add_mime_type("text/plain");
    dialog.add_filter(filter_text);

    auto filter_cpp = Gtk::FileFilter::create();
    filter_cpp->set_name("C/C++ files");
    filter_cpp->add_mime_type("text/x-c");
    filter_cpp->add_mime_type("text/x-c++");
    filter_cpp->add_mime_type("text/x-c-header");
    dialog.add_filter(filter_cpp);

    //Show the dialog and wait for a user response:
    int result = dialog.run();

    //Handle the response:
    switch (result) {
    case (Gtk::RESPONSE_OK): {
        //Notice that this is a std::string, not a Glib::ustring.
        std::string filename = dialog.get_filename();
        //presenter->openFilename(filename);
        componentStatusBar.updateText("File selected: " + filename);
        break;
    }
    case (Gtk::RESPONSE_CANCEL): {
        componentStatusBar.updateText("Cancel clicked. ");
        break;
    }
    default: {
        break;
    }
    }

}
//
void MainWindowView::on_file_save()
{
    componentStatusBar.updateText("File Save menu item opened");
    //presenter->saveFilename();
}
//
void MainWindowView::on_edit_undo()
{
    //presenter->undo();
    presenter->undo();
    componentStatusBar.updateText("Edit Undo menu item opened");
}
//
void MainWindowView::on_edit_redo()
{
    //presenter->redo();
    presenter->redo();
    componentStatusBar.updateText("Edit Redo menu item opened");
}
//
void MainWindowView::on_add_click()
{
     presenter->addNewPerson();
     //presenter->addNewPerson();
}

void MainWindowView::on_login_click()
{
    presenter->loginUser();
    //presenter->loginUser();
}
//
void MainWindowView::on_report_click()
{
//    presenter->reportChanged();
}
//
void MainWindowView::on_change_click()
{
    componentStatusBar.updateText("Change Person clicked.");
}
//
void MainWindowView::on_quit_click()
{
    closeView();    // Close the modules view
    hide();
}
//
void MainWindowView::on_person_list_popup_generic(Glib::ustring msg)
{
    int selected = componentPersonDBTreeList.get_selected_row();
    if (selected > 0) { //
        personDBList.setPosition(selected);
        if (msg == "remove") {
            presenter->removePerson();
        }
        if (msg == "edit") {
            presenter->changePerson();
        }
    }

}
//
void MainWindowView::closeView()
{
    if (presenter->worker_has_stopped() == false) {
        presenter->stop_work(); // will wait until stopped
    }
}
// notify() is called from ExampleWorker::do_work(). It is executed in the worker
// thread. It triggers a call to on_notification_from_worker_thread(), which is
// executed in the GUI thread.
//void MainWindowView::worker_notify()
//{
//    m_Dispatcher.emit();
//}
// notify() is called from ExampleWorker::do_work(). It is executed in the worker
// thread. It triggers a call to on_notification_from_worker_thread(), which is
// executed in the GUI thread.
////void MainWindowView::task_notify()
////{
////    m_Dispatcher.emit();
////}
//void MainWindowView::worker_complete()
//{
//    m_DBDispatcher.emit();
//}
//
void MainWindowView::on_start_process_click()
{
    //presenter->start_work();
    update_start_stop_buttons();
}
//
void MainWindowView::on_stop_process_click()
{
    //presenter->ctrl()->stop_work();
    update_start_stop_buttons();
}
//
void MainWindowView::update_start_stop_buttons()
{
    const bool thread_is_running = (presenter->worker_has_stopped() == false);
    m_startButton.set_sensitive(!thread_is_running);
    m_stopButton.set_sensitive(thread_is_running);
}
//
void MainWindowView::on_notification_from_worker_thread()
{
    if (presenter->worker_has_stopped() == true) {
        update_start_stop_buttons();
    }
    update_widgets();
}
//
void MainWindowView::on_notification_from_dbworker_thread()
{
    if (presenter->worker_has_stopped() == true) {
		std::cout << "on_notification_from_dbworker_thread\n";
		presenter->complete_dbworker();
    }
    update_widgets();
}
//
void MainWindowView::update_widgets()
{
    double fraction_done;
    std::string message_from_worker_thread;
    presenter->get_data(&fraction_done, &message_from_worker_thread);

    m_ProgressBar.set_fraction(fraction_done);
    if (message_from_worker_thread != m_TextView.get_buffer()->get_text()) {
        auto buffer = m_TextView.get_buffer();
        buffer->set_text(message_from_worker_thread);

        // Scroll the last inserted line into view. That's somewhat complicated.
        Gtk::TextIter iter = buffer->end();
        iter.set_line_offset(0); // Beginning of last line
        auto mark = buffer->get_mark("last_line");
        buffer->move_mark(mark, iter);
        m_TextView.scroll_to(mark);
        // TextView::scroll_to(iter) is not perfect.
        // We do need a TextMark to always get the last line into view.
    }
    if (fraction_done == 1.0) {
		int pos = personDBList.getPosition() - 1;
		std::cout << "Pos to focus: " << pos  << "\n";
		componentPersonDBTreeList.set_selected_row(pos);
    }
}
//
//--------------------------------------------------------------------------------
// Privatesignal handlers
//-------------------------------------------------------------------------------
//
void MainWindowView::on_person_list_selection_changed() {}
//
void MainWindowView::on_system_list_selection_changed()
{
    int selected = componentSystemList.get_selected_row();
    if (selected > 0) { // 
        /*Module::Message viewMessage;
        Module::Parameter param;
        param.kind = Module::ParameterKind::TWO_ARG;
        param.args.twoArgs.valueA = selected;
        param.args.twoArgs.valueB = Module::ViewIdentity::PersonEdit;
        viewMessage.setParameter(param);*/
//        systemList.setPosition(selected);
        //presenter->activate_system();
    }
    
}
//
void MainWindowView::on_category_list_selection_changed()
{
    int selected = componentCategoryList.get_selected_row();
    if (selected > 0) { // 
//        categoryList.setPosition(selected);
//        presenter->activate_system();
    }
}


} /* namespace App */

