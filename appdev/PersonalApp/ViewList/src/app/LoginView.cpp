/*
 * LoginView.cpp
 */

#include "LoginView.h"
#include "module/Controller.h"
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/window.h>
#include <gtkmm/grid.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/messagedialog.h>
#include <gtkmm/paned.h>
#include <gtkmm/frame.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/alignment.h>
#define GTKMM_DISABLE_DEPRECATED
#include <sstream>
#include <vector>
#include <iostream>

namespace App
{


//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
//
LoginView::LoginView():
    loginButton("_Login", true),
    logoutButton("L_ogout", true),
    cancelButton("_Cancel", true),
	m_validationList(this),
 	m_loggedState(this)
{
}
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
LoginView::~LoginView()
{
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
int LoginView::createView(std::unique_ptr<Module::DBLoginPresenter> prsntr)
{
    presenter = std::move(prsntr);

    //WindowFramed::add(componentPanel);
    Gtk::Window::add(componentPanel);

    // Menu actions - use SimpleActionGroup
    m_refActionGroup = Gio::SimpleActionGroup::create();
    m_refActionGroup->add_action("quit", sigc::mem_fun(*this, &LoginView::on_cancel_click));
    insert_action_group("loginview", m_refActionGroup);

    // Use GtkBuilder to make the menu in Menu object - pass this to it to make those menus
    // Menu bar
    const char *ui_info =
        "<interface>"
        "  <menu id='menubar'>"
        "    <submenu id='loginmenu'>"
        "      <section>"
        "        <item>"
        "          <attribute name='label' translatable='yes'>Close</attribute>"
        "          <attribute name='action'>loginview.quit</attribute>"
//        "          <attribute name='accel'>&lt;Primary&gt;q</attribute>"
        "        </item>"
        "      </section>"
        "    </submenu>"
        "  </menu>"
        "<interface>";

    componentMenu.createControl(ui_info);
    componentMenu.addMenu("Login", "loginmenu");
    componentPanel.add(componentMenu);

    m_username.createControl(3);
    m_password.createControl(4);
    m_usernameValidateLabel.createControl(5);
    m_passwordValidateLabel.createControl(6);
    //m_username.set_width_chars(42);
    m_username.set_max_width_chars(30);
    m_username.set_max_length(30);
    //m_password.set_width_chars(42);
    m_password.set_max_width_chars(30);
    m_password.set_visibility(false);
    m_password.set_input_purpose(Gtk::INPUT_PURPOSE_PASSWORD);
    m_password.set_max_length(30);
    Gtk::Label *l1 = Gtk::manage(new Gtk::Label("User name:", Gtk::ALIGN_START));
    Gtk::Label *l2 = Gtk::manage(new Gtk::Label("Password:", Gtk::ALIGN_START));

    Gtk::Grid *gridPn = Gtk::manage(new Gtk::Grid);
    gridPn->set_border_width(8);
    gridPn->set_row_spacing(2);
    gridPn->set_column_spacing(6);
    gridPn->attach(m_username, 0, 0, 5, 1);
    gridPn->attach(m_password, 0, 2, 5, 1);
    gridPn->attach_next_to(*l1, m_username, Gtk::POS_TOP, 2, 1);
    gridPn->attach_next_to(*l2, m_password, Gtk::POS_TOP, 2, 1);
    gridPn->attach_next_to(m_usernameValidateLabel, *l1, Gtk::POS_TOP, 3, 1);
//    gridPn->attach_next_to(m_usernameValidateLabel, *l1, Gtk::POS_RIGHT, 3, 1);
//    gridPn->attach_next_to(m_passwordValidateLabel, *l2, Gtk::POS_RIGHT, 3, 1);

    loginButton.signal_clicked().connect(sigc::mem_fun(*this, &LoginView::on_login_click));
    logoutButton.signal_clicked().connect(sigc::mem_fun(*this, &LoginView::on_logout_click));
    cancelButton.signal_clicked().connect(sigc::mem_fun(*this, &LoginView::on_cancel_click));

    Gtk::ButtonBox *button_Box = Gtk::manage(new Gtk::ButtonBox(Gtk::ORIENTATION_HORIZONTAL));
    button_Box->set_border_width(5);
    button_Box->set_layout(Gtk::BUTTONBOX_CENTER);
    button_Box->set_spacing(10);
    button_Box->pack_start(loginButton);
    button_Box->pack_start(logoutButton);
    button_Box->pack_start(cancelButton);
/*    loginButton.set_can_default(true); //n.set_can_default();
    logoutButton.set_can_default(true); //n.set_can_default();
    cancelButton.set_can_default(true); //n.set_can_default();
    button_Box->set_focus_child(cancelButton);*/

    Gtk::Box *allbox = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
    allbox->set_spacing(3);
    allbox->pack_start(*gridPn, Gtk::PACK_EXPAND_WIDGET);
    allbox->pack_start(*button_Box, Gtk::PACK_EXPAND_WIDGET);

    Gtk::Alignment *align1 = Gtk::manage(new Gtk::Alignment(0.5, 0.5, 0, 0));
    align1->add(*allbox);

    Gtk::Label *framelabel = Gtk::manage(new Gtk::Label());
    framelabel->set_use_markup(true);
    framelabel->set_markup("<b><u><span foreground=\"black\" underline=\"single\">Login Module</span></u></b>");
    framelabel->set_margin_left(4);
    Gtk::Frame *allframe = Gtk::manage(new Gtk::Frame());
    allframe->set_label_widget(*framelabel);
    allframe->set_label_align(0.0, 1.0);
    allframe->set_shadow_type(Gtk::SHADOW_ETCHED_OUT);
    allframe->set_border_width(8);
    allframe->add(*align1);


    componentPanel.pack_start(*allframe, Gtk::PACK_EXPAND_WIDGET);

//    m_userRoleSource.addBind(&m_userRoleBind);

    componentPanel.show_all();

    presenter->initialize();
    return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void LoginView::readEditControl()
{
    // Componenet View Items
 //   m_userRoleSource.doUpdate();
}
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void LoginView::showView(const Module::Message &message)
{
    present();
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
void LoginView::closeView()
{
    hide();
}

void LoginView::on_login_click()
{
    presenter->loginUser();
}

void LoginView::on_logout_click()
{
    presenter->logoutUser();
}

void LoginView::on_cancel_click()
{
    hide();
}

} /* namespace App */

