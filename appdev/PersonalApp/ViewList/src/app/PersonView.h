#ifndef D_PERSONVIEW_H_
#define D_PERSONVIEW_H_

#include "components/PanelBox.h"
#include "components/Menu.h"
#include "components/StatusBar.h"
#include "components/Edit.h"
#include "components/ValidationLabel.h"
#include "components/TreeView.h"
#include "module/PersonWindowInterface.h"
#include "module/PersonPresenter.h"
#include "module/Person.h"
#include "ComponentViewItem.h"
#include "ComponentViewList.h"
#include "AppViewList.h"
#include "AppViewItem.h"
#include "gtkmm/window.h"
#include <giomm-2.4/giomm/simpleactiongroup.h>
#include <memory>

namespace App
{

/**
 *
 * \page personview View Person details popup window (modal)
 *
 * Uses ViewBind to do most transfer of data from the main presenter module.
 * Uses a person validation to check data.
 * 
 * \sa App::ViewBind()
 */

/// PersonView is a gtkmm vertical box (PanelBox)
/**
 * \sa WindowFramed, PersonEditInterface and PersonEditPresenter
 */
class PersonView:
    public Gtk::Window,
    public Module::PersonWindowInterface
{
    std::unique_ptr<Module::PersonPresenter> presenter;   // Personal module presenter
    App::Component::PanelBox componentPanel;            // PanelBox is the one panel on a main window
    App::Component::Menu componentMenu;
    App::Component::StatusBar componentStatusBar;
    Glib::RefPtr<Gio::SimpleActionGroup> m_refActionGroup;
    App::Component::Edit m_firstNameEdit;
    App::Component::Edit m_surnameEdit;
    App::Component::ValidationLabel m_firstNameValidateLabel;
    App::Component::ValidationLabel m_surnameValidateLabel;
    App::ComponentViewItem<std::string> status;
    Gtk::Button addButton;
    Gtk::Button changeButton;
    Gtk::Button cancelButton;
    
public:
    /// Constructor
    PersonView();
    /// Virtual deconstructor
    virtual ~PersonView();
    /// Create the view
    /**
     * Personal module presenter created with main window
     * \param[in] prsntr main presenter as smart pointer in main view
     * \return int return value
     *
     */
    int createView ( std::unique_ptr<Module::PersonPresenter> prsntr ); 
    /// Main view interface method to get a person
    /**
     * \return ViewItem<Module::Person>
     */
    Module::ViewItem<Module::Person> &getPerson() {
        return m_personEdit;
    }
    /// Main view interface method to get a person validation
    /**
     * \return ViewList<Validate::ValidationError>
     */
    Module::ViewList<Validate::ValidationError> &getValidationError() {
        return m_validationList;
    }
    /// Main view interface method to get presenter status
    /**
     * \return ViewItem<std::string>
     */
    Module::ViewItem<std::string> &getStatus() {
        return  status ;
    }
    /// Main view interface method to read the edit control
    void readEditControl();
    /// Main view interface method
    /**
     * \return ViewItem<Module::ViewEditState> return value
     */
    virtual Module::ViewItem<Module::ViewEditState> &getState (){
        return m_editState;
    };
    
    /// Main view interface method
    /**
     * \param[in] message message from presenter
     */
    void showView ( const Module::Message &message );
    /// Main view interface method
    /**
     */
    void closeView ();
    void worker_notify(){};

protected:
    void on_add_click();                 ///< slots for add button
    void on_change_click();              ///< slots for change button
    void on_cancel_click();                ///< slots for quit

private:

    class PersonEdit: public Module::ViewItem<Module::Person>
    {
    	PersonView *m_view;
    	Module::Person m_item_container;    ///< container is a BindSource of DataType
        App::ViewBind<Module::Person> firstNameBind;   // Generic std::string firstname
        App::ViewBind<Module::Person> surnameBind;     // Generic std::string surname

    public:
        /// Constructor
        /**
         * Source where the item is added at. A ViewItem will only contain one item
         * in the reference container BindSource
         * \param item reference the Control::Item() where the view item is added
         * \sa BindSource and ViewItem
         */
    	PersonEdit ( PersonView *view ) :m_view ( view ),
	    firstNameBind("Firstname", &m_view->m_firstNameEdit),
	    surnameBind("Surname", &m_view->m_surnameEdit)
		{
        }
        /// Virtual deconstructor
        virtual ~PersonEdit() {
        }
        /// Set item
        /**
         * \param item const reference to the item
         *
         */
        virtual void setItem ( const Module::Person& item ) {
            m_item_container = item;
            firstNameBind.doBind(item);
            surnameBind.doBind(item);
        }
        /// Get item
        /**
         * \return item reference to the item
         *
         */
        virtual Module::Person& getItem() {
            firstNameBind.doUpdate(m_item_container, 0);
            surnameBind.doUpdate(m_item_container, 0);
            return ( m_item_container );
        }

    };
    PersonEdit m_personEdit;

    // Where the presenters data is contained - a vector of Module::Person for just one person
    // This bindsource is handy for a group of items which could be put into a dialog
    // or when just one item is needed. Can be used with lists although componentList can
    // hold the data in its own model
    class ValidationList: public Module::ViewList<Validate::ValidationError>
    {
    	PersonView *m_view;
        App::ViewBind<Validate::ValidationError, App::Component::ValidationLabel> firstNameValidateBind;   // Generic std::string firstname
        App::ViewBind<Validate::ValidationError, App::Component::ValidationLabel> surnameValidateBind;     // Generic std::string surname
    	int m_position{1};
        std::vector<Validate::ValidationError> m_container; ///< containers list
    public:
        /// Virtual deconstructor
        ValidationList( PersonView *view ) :m_view ( view ),
		firstNameValidateBind("Validate::ValidationError", &m_view->m_firstNameValidateLabel),
		surnameValidateBind("Validate::ValidationError", &m_view->m_surnameValidateLabel)
		{}
        /// Virtual deconstructor
        virtual ~ValidationList() {}
        /// Add item
        /**
         * \param item const reference to the item
         *
         */
        virtual void add ( Validate::ValidationError item ) {
            m_container.push_back(item);
            firstNameValidateBind.doBind(item);
            surnameValidateBind.doBind(item);
        }
        /// Clear the list
        virtual void clear() {
        	if (m_container.size() > 0){
        	Validate::ValidationError& e = m_container.at(0);
        	int index = 0;
        	for (const auto item : m_container) {
                firstNameValidateBind.doBindClear(item, index);
                surnameValidateBind.doBindClear(item, index);
                ++index;
        	}
            m_container.clear();
            std::cout << m_container.size();
        	}
        }
        /// Size of the list
        virtual unsigned int size() {
            return ( m_container.size() );
        }
        /// Set position of selected item
        virtual void setPosition ( int value ) {
            m_position = value;
        }
        /// Get position of selected item
        virtual int getPosition() {
            return ( m_position);
        }
        /// Get selected item
        /**
         * \return DataType reference to item
         */
        virtual Validate::ValidationError& getSelectItem() {
            return ( m_container.at(m_position-1));
        }

        /// Define the iterator typedefs
        typedef typename std::vector<Validate::ValidationError>::iterator iterator;
        /// Define the const iterator typedefs
        typedef typename std::vector<Validate::ValidationError>::const_iterator const_iterator;
        /// Begin iterator
        iterator begin() {
            return ( m_container.begin() );
        }
        /// End iterator
        iterator end() {
            return ( m_container.end() );
        }
        /// Begin const iterator
        /**
         * \return const iterator
         */
        const_iterator begin() const {
            return ( m_container.begin() );
        }
        /// End const iterator
        /**
         * \return const iterator
         */
        const_iterator end() const {
            return ( m_container.end() );
        }

    };
    ValidationList m_validationList;


    class EditState: public Module::ViewItem<Module::ViewEditState>
    {
    	PersonView *m_view;
        Module::ViewEditState m_item_container{Module::ViewEditState::AddNew};    ///< container is a BindSource of DataType
    public:
        /// Constructor
        /**
         * Source where the item is added at. A ViewItem will only contain one item
         * in the reference container BindSource
         * \param item reference the Control::Item() where the view item is added
         * \sa BindSource and ViewItemApp
         */
        EditState  ( PersonView *view ) :m_view ( view )
    	{  }
        /// Virtual deconstructor
        virtual ~EditState() {
        }
        /// Set item
        /**
         * \param item const reference to the item
         *
         */
        virtual void setItem ( const Module::ViewEditState& item ) {
            m_item_container = item;

            m_view->m_firstNameEdit.set_can_focus();
            m_view->m_firstNameEdit.grab_focus();
            if (item==Module::ViewEditState::AddNew) {
                m_view->addButton.show();
                m_view->changeButton.hide();
            } else {
                m_view->addButton.hide();
                m_view->changeButton.show();
            }
        }
        /// Get item
        /**
         * \return item reference to the item
         *
         */
        virtual Module::ViewEditState& getItem() {
            return ( m_item_container );
        }

    };
    EditState m_editState;


};

} /* namespace App */

#endif /* D_PERSONVIEW_H_ */
