#ifndef MAINWINDOWVIEW_H_
#define MAINWINDOWVIEW_H_

#include "components/PanelBox.h"
#include "components/Menu.h"
#include "components/StatusBar.h"
#include "components/Edit.h"
#include "components/TreeView.h"
#include "module/MainWindowInterface.h"
#include "module/MainPresenter.h"
#include "module/Person.h"
#include "module/Entity.h"
#include "ComponentViewItem.h"
#include "ComponentViewList.h"
#include "AppViewList.h"
#include "AppViewItem.h"
#include "gtkmm/progressbar.h"
#include "gtkmm/textview.h"
#include <glibmm-2.4/glibmm/dispatcher.h>
#include "gtkmm/window.h"
#include <memory>

/** \mainpage An application using View and Model (MVP design)
 *
 * The transfer of the model to the view is through BindSource and Bind types.
 * The main presenter is really the main window logic.
 * The is a controller which is designed to work on a module and is the first object
 * to use in an application to start the modules application logic.
 *
 * When the controller needs to have a new view, the application has to have
 * an implementation of the view using the modules view interfaces. This is so the
 * underlying presenter knows how to give the view the data and how to validate it.
 *
 * - \subpage bindmodule
 * - \subpage controller
 * - \subpage viewlist
 * - \subpage personview
 * - \subpage filestorage
 * - \subpage validation
 *
 */

/// The top namespace
/** App is the top namespace
 */
namespace App
{
/**
 *
 * \page viewlist View List application
 *
 * Uses ViewBind to do most transfer of data from the main presenter module.
 * \sa App::ViewBind()
 */

/// MainWindow is a gtkmm vertical box (PanelBox) where the menu, a central grid, and status bar are contained
/**
 * \sa WindowFramed, MainWindowInterface and MainPresenter
 */
class MainWindowView:  public Gtk::Window, public Module::MainWindowInterface
{
    std::unique_ptr<Module::MainPresenter> presenter;   // Personal module presenter
    App::Component::PanelBox componentPanel;            // PanelBox is the one panel on a main window
    App::Component::Menu componentMenu;
    App::Component::Menu componentListPopupMenu;
    App::Component::StatusBar componentStatusBar;
    App::Component::TreeView componentPersonDBTreeList;
    App::Component::TreeView componentSystemList;
    App::Component::TreeView componentCategoryList;
    App::ComponentViewList<Module::Person, App::Component::TreeView> personDBList{componentPersonDBTreeList};
    App::ComponentViewItem<std::string> status{componentStatusBar};

    Gtk::ProgressBar m_ProgressBar{};
    Gtk::Button m_startButton{"Start Process"};
    Gtk::Button m_stopButton{"Stop Process"};
    Gtk::TextView m_TextView{};
    Glib::Dispatcher m_Dispatcher{};    
    Glib::Dispatcher m_DBDispatcher{};
    
public:
    /// Constructor
    MainWindowView();
    /// Virtual deconstructor
    virtual ~MainWindowView();
    /// Create the view
    /**
     * Personal module presenter created with main window
     * \param[in] prsntr main presenter as smart pointer in main view
     * \return int return value
     *
     */
    int createView ( std::unique_ptr<Module::MainPresenter> prsntr );
    /// Main view interface method to get person list
    /**
     * \return ViewList<Module::Person>
     */
    Module::ViewBindList<Module::Person> &getPersonList() override {
        return personDBList;
    };
    /// Main view interface method to get a person
    /**
     * \return ViewItem<Module::Person>
     */
//    Module::ViewItem<Module::Person> &getPerson() override {
//        return person;
//    }
    /// Main view interface method to get presenter status
    /**
     * \return ViewItem<std::string>
     */
    Module::ViewItem<std::string> &getStatus() override {
        return status ;
    }
    /// Main view interface method to read the edit control
    void readEditControl() override;
    /// Main view interface method
    /**
     * \param[in] message message from presenter
     */
    void showError ( std::string message ) override;
    /// Main view interface method
    /**
     * \param[in] message message from presenter
     */
    void showView ( const Module::Message &message ) override;
    /// Main view interface method
    void closeView () override;
    /// View interface method
    void worker_notify() override {
        m_DBDispatcher.emit();
    };
    
private:
    void on_file_new();                  ///< slots for file new
    void on_file_open();                 ///< slots for file open
    void on_file_save();                 ///< slots for file save
    void on_edit_undo();                 ///< slots for edit undo
    void on_edit_redo();                 ///< slots for edit redo
    void on_add_click();                 ///< slots for add
    void on_report_click();              ///< slots for report
    void on_change_click();              ///< slots for change
    void on_login_click();               ///< slots for DB login
    void on_quit_click();                ///< slots for quit
    void on_person_list_popup_generic ( Glib::ustring msg ); ///< slots for person list popup
    void on_person_list_selection_changed();  ///< slots for person list on selection changed
    void on_system_list_selection_changed();  ///< slots for system list on selection changed
    void on_category_list_selection_changed(); ///< slots for category list on selection changed
    
    void on_start_process_click();             ///< slots for process
    void on_stop_process_click();             ///< slots for process
    void update_widgets();
    // Dispatcher handler.
    void on_notification_from_worker_thread();
    void on_notification_from_dbworker_thread();
    void update_start_stop_buttons();
};

} /* namespace App */

#endif /* MAINWINDOWVIEW_H_ */
