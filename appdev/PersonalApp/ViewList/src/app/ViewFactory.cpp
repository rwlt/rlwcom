/*
 * ViewFactory.cpp
 */

#include "ViewFactory.h"
#include "module/MainPresenter.h"
#include "module/PersonPresenter.h"
#include "module/DBLoginPresenter.h"
#include <iostream>
#include <sstream>
#include <cstring>
#include <memory>

namespace App
{

ViewFactory::ViewFactory():
    m_activeViewIdentity(Module::ViewIdentity::MainWindow)
{
    //strncpy(s,d)
    std::memset(moduleView, '\0', sizeof(moduleView));
}

ViewFactory::~ViewFactory()
{
    for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index) {
        if (moduleView[index] != 0) {
            moduleView[index] = 0;
        }
    }
}
//
//----------------------------------------------------------------------------------
// identityInstance
// Use Module ViewIdentity to get View Implementations instance
//----------------------------------------------------------------------------------
//
Module::MainWindowInterface *ViewFactory::mainInterface () {
    // We find the view here
    if ( moduleView[Module::ViewIdentity::MainWindow] != 0 ) {
        return &view_main;
    }
    return ( nullptr );
};
//
//----------------------------------------------------------------------------------
// identityInstance
// Use Module ViewIdentity to get View Implementations instance
//----------------------------------------------------------------------------------
//
App::MainWindowView *ViewFactory::mainWindow () {
    // We find the view here
    if ( moduleView[Module::ViewIdentity::MainWindow] != 0 ) {
        return &view_main;
    }
    return ( nullptr );
};
//
//----------------------------------------------------------------------------------
// identityInstance
// Use Module ViewIdentity to get View Implementations instance
//----------------------------------------------------------------------------------
//
Module::PersonWindowInterface *ViewFactory::personInterface () {
    if ( moduleView[Module::ViewIdentity::PersonEdit] != 0 ) {
        return &view_person;
    }
    return ( nullptr );
};
//
//----------------------------------------------------------------------------------
// identityInstance
// Use Module ViewIdentity to get View Implementations instance
//----------------------------------------------------------------------------------
//
Module::DBLoginWindowInterface *ViewFactory::loginInterface () {
    if ( moduleView[Module::ViewIdentity::LoginUser] != 0 ) {
        return &view_login;
    }
    return ( nullptr );
};
//
//----------------------------------------------------------------------------------
// activeViewIdentity
// This know which view identity is active from the module controller
//----------------------------------------------------------------------------------
//
/*Module::ViewIdentity ViewFactory::activeViewIdentity()
{
    return m_activeViewIdentity;
}*/
//
//----------------------------------------------------------------------------------
// createInstance
// Use Module ViewIdentity and the total views available to keep
// an array of views to return to caller
//----------------------------------------------------------------------------------
//
void ViewFactory::createInstance(Module::Controller* controller,
    Module::ViewIdentity viewIdentity,
    Module::ViewIdentity parentViewIdentity)
{
    
    // We find the view here
    if (moduleView[viewIdentity] == 0) {
        // If not found create and add to the moduleViews in array index
        // If parentViewIdentity a view then get the views HWND for parent usage for a child view
        if (parentViewIdentity != Module::ViewIdentity::NullView) {
            if (moduleView[parentViewIdentity] == 0) {
                std::cout << "From view identity -- Implementation required for Module::ViewIdentity. Enumerator index is "
                          << viewIdentity << " " << std::endl;
                //PostQuitMessage(WM_QUIT);
            }
        }

        std::unique_ptr<Module::MainPresenter> mainPresenter;
        std::unique_ptr<Module::PersonPresenter> personPresenter;
        std::unique_ptr<Module::DBLoginPresenter> loginPresenter;
        switch (viewIdentity) {
        case Module::ViewIdentity::MainWindow:
            // The main window is made at beginning for GTk Apps
            mainPresenter = std::unique_ptr<Module::MainPresenter>(new Module::MainPresenter(&view_main, controller));
            moduleView[viewIdentity] = &view_main;
            view_main.createView(std::move(mainPresenter)); //change ownership of presenter to the view
            break;

        case Module::ViewIdentity::PersonEdit:
            personPresenter = std::unique_ptr<Module::PersonPresenter>(new Module::PersonPresenter(&view_person, controller));
            moduleView[viewIdentity] = &view_person;
            view_person.createView(std::move(personPresenter)); //change ownership of presenter to the view
            view_person.set_transient_for(view_main);
            view_person.set_modal();
            view_person.set_decorated(false);
            break;

        case Module::ViewIdentity::LoginUser:
            loginPresenter = std::unique_ptr<Module::DBLoginPresenter>(new Module::DBLoginPresenter(&view_login, controller));
            moduleView[viewIdentity] = &view_login;
            view_login.createView(std::move(loginPresenter)); //change ownership of presenter to the view
            view_login.set_transient_for(view_main);
            view_login.set_modal();
            view_login.set_decorated(false);
            break;

        default:
            std::cout << "A View Identity not setup in factory: " << viewIdentity << std::endl;
            break;
        }
    }
}
//
//----------------------------------------------------------------------------------
// destroyInstance by Module::ViewIdentity
//----------------------------------------------------------------------------------
//
void ViewFactory::destroyInstance(Module::ViewIdentity viewIdentity)
{
    // Do nothing we just hide the views
/*    if (moduleView[viewIdentity] != 0) {
        moduleView[viewIdentity] = 0;
        std::cout << "Number of Views left created = " << countViews() << "\n";
    }*/
}
//
// -------------------------------------------------------------------------
// count views
// -------------------------------------------------------------------------
//
int ViewFactory::countViews() {
    int count = 0;
    for ( int index = 0; index < Module::ViewIdentity::TotalViews; index++ ) {
        if ( moduleView[index] != 0 ) {
            count++;
        }
    }
    return count;
}


} // end namespace App


