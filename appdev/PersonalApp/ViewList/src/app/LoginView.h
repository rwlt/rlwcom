#ifndef D_LOGINVIEW_H_
#define D_LOGINVIEW_H_

#include "components/PanelBox.h"
#include "module/DBLoginWindowInterface.h"
#include "module/DBLoginPresenter.h"
#include "module/ViewItem.h"
#include "module/Message.h"
#include "components/StatusBar.h"
#include "components/Edit.h"
#include "components/ValidationLabel.h"
#include "EditBoxItem.h"
#include "ComponentViewItem.h"
#include "ComponentViewList.h"
#include "AppViewList.h"
#include "AppViewItem.h"
#include <giomm-2.4/giomm/simpleactiongroup.h>
#include "gtkmm/window.h"
#include <memory>
#include <iostream>
#include <sstream>

namespace App
{

/// LoginView is a gtkmm vertical box (PanelBox) modal window with main view as parent
/**
 * \sa WindowFramed
 */
class LoginView:
    public Gtk::Window,
    public Module::DBLoginWindowInterface
{
    std::unique_ptr<Module::DBLoginPresenter> presenter;   // Personal module presenter
    App::Component::PanelBox componentPanel;            // PanelBox is the one panel on a main window
    App::Component::Menu componentMenu;

    Glib::RefPtr<Gtk::Builder> m_refBuilder;
    Glib::RefPtr<Gio::SimpleActionGroup> m_refActionGroup;

    App::Component::Edit m_username;
    App::Component::Edit m_password;
    Module::UserRole m_userRoleSource{Module::UserRole::GUEST};
    App::Component::ValidationLabel m_usernameValidateLabel{"username"};
    App::Component::ValidationLabel m_passwordValidateLabel{"password"};
    App::EditBoxItem username{m_username};
    App::EditBoxItem password{m_password};
    App::AppViewItem<Module::UserRole> role{m_userRoleSource};

    Gtk::Button loginButton;
    Gtk::Button logoutButton;
    Gtk::Button cancelButton;

public:
    /// Constructor
    LoginView();
    /// Virtual deconstructor
    virtual ~LoginView();
    /// Create the view
    /**
     * Login presenter created with main window
     * \param[in] prsntr login presenter as smart pointer in main view
     * \return int return value
     *
     */
    int createView ( std::unique_ptr<Module::DBLoginPresenter> prsntr );

    /// Interface method to get a Username
    /**
     * \return ViewItem<std::string>
     */
    virtual EditBoxItem &getUsername() {
        return username;
    };
    /// Interface method to get a Username
    /**
     * \return ViewItem<std::string>
     */
    virtual EditBoxItem &getPassword() {
        return password;
    };
    /// get view edit state to set state
    /**
     * \return ViewItem of User role enum
     * \sa Module::UserRole
     */
    virtual Module::ViewItem<Module::UserRole> &getRole () {
        return role;
    };
    /// get state of presenter with logged user or not
    /**
     * \return ViewItem of logged state enum
     * \sa Module::LoggedState
     */
    virtual Module::ViewItem<Module::LoggedState> &getLoggedState () {
        return m_loggedState;
    };
    /// Interface method to get the view validation errors ViewList
    /**
     * \return ViewList<Validate::ValidationError>
     */
    Module::ViewList<Validate::ValidationError> &getValidationError() {
        return m_validationList;
    }
    /// Main view interface method to read the edit control
    void readEditControl();

    /// View interface method
    /**
     * \param[in] message message from presenter
     */
    void showView ( const Module::Message &message );
    /// View interface method
    /**
     */
    void closeView ();
    void worker_notify(){};
protected:
    void on_login_click();    ///< slot for login
    void on_logout_click();    ///< slot for login
    void on_cancel_click();   ///< slot for cancel

private:

    // Where the presenters data is contained - a vector of Module::Person for just one person
    // This bindsource is handy for a group of items which could be put into a dialog
    // or when just one item is needed. Can be used with lists although componentList can
    // hold the data in its own model
    class ValidationList: public Module::ViewList<Validate::ValidationError>
    {
        LoginView *m_view;
        App::ViewBind<Validate::ValidationError, App::Component::ValidationLabel> usernameValidateBind;
        App::ViewBind<Validate::ValidationError, App::Component::ValidationLabel> passwordValidateBind;
    	int m_position{1};
        std::vector<Validate::ValidationError> m_container; ///< containers list
    public:
        /// Virtual deconstructor
        ValidationList( LoginView *view ) :m_view ( view ),
		usernameValidateBind("Validate::ValidationError", &m_view->m_usernameValidateLabel),
		passwordValidateBind("Validate::ValidationError", &m_view->m_passwordValidateLabel)
		{}
        /// Virtual deconstructor
        virtual ~ValidationList() {}
        /// Add item
        /**
         * \param item const reference to the item
         *
         */
        virtual void add ( Validate::ValidationError item ) {
            m_container.push_back(item);
            usernameValidateBind.doBind(item);
            passwordValidateBind.doBind(item);
        }
        /// Clear the list
        virtual void clear() {
        	int index = 0;
        	for (auto v: m_container) {
				usernameValidateBind.doBindClear(Validate::ValidationError(), index);
				passwordValidateBind.doBindClear(Validate::ValidationError(), index);
				++index;
        	}
            m_container.clear();
        }
        /// Size of the list
        virtual unsigned int size() {
            return ( m_container.size() );
        }
        /// Set position of selected item
        virtual void setPosition ( int value ) {
            m_position = value;
        }
        /// Get position of selected item
        virtual int getPosition() {
            return ( m_position);
        }
        /// Get selected item
        /**
         * \return DataType reference to item
         */
        virtual Validate::ValidationError& getSelectItem() {
            return ( m_container.at(m_position-1));
        }

        /// Define the iterator typedefs
        typedef typename std::vector<Validate::ValidationError>::iterator iterator;
        /// Define the const iterator typedefs
        typedef typename std::vector<Validate::ValidationError>::const_iterator const_iterator;
        /// Begin iterator
        iterator begin() {
            return ( m_container.begin() );
        }
        /// End iterator
        iterator end() {
            return ( m_container.end() );
        }
        /// Begin const iterator
        /**
         * \return const iterator
         */
        const_iterator begin() const {
            return ( m_container.begin() );
        }
        /// End const iterator
        /**
         * \return const iterator
         */
        const_iterator end() const {
            return ( m_container.end() );
        }

    };
    ValidationList m_validationList;



    class LoggedState: public Module::ViewItem<Module::LoggedState>
    {
        LoginView *m_view;
        Module::LoggedState m_item_container{Module::LoggedState::NOTLOGGED};    ///< container is a BindSource of DataType
    public:
        /// Constructor
        /**
         * Source where the item is added at. A ViewItem will only contain one item
         * in the reference container BindSource
         * \param item reference the Control::Item() where the view item is added
         * \sa BindSource and ViewItemApp
         */
    	LoggedState  ( LoginView *view ) :m_view ( view )
    	{  }
        /// Virtual deconstructor
        virtual ~LoggedState() {
        }
        /// Set item
        /**
         * \param item const reference to the item
         *
         */
        virtual void setItem ( const Module::LoggedState& item ) {
            m_item_container = item;
            if ( item==Module::LoggedState::NOTLOGGED ) {
                std::cout << "LoggedStateBind NOTLOGGED" << std::endl;
                m_view->loginButton.show();
                m_view->logoutButton.hide();
                m_view->m_username.set_sensitive(true);
                m_view->m_username.set_can_focus();
                m_view->m_username.grab_focus();
                m_view->m_password.set_sensitive(true);
            } else { // is logged user so show logout and login detials
                std::cout << "LoggedStateBind A LOGGED USER" << std::endl;
                m_view->loginButton.hide();
                m_view->logoutButton.show();
                m_view->m_username.set_sensitive(false);
                m_view->m_password.set_sensitive(false);
            }
        }
        /// Get item
        /**
         * \return item reference to the item
         *
         */
        virtual Module::LoggedState& getItem() {
            return ( m_item_container );
        }

    };
    LoggedState m_loggedState;



};

} /* namespace App */

#endif /* D_LOGINVIEW_H_ */
