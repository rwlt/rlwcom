//============================================================================

#include "app/ViewFactory.h"
#include "module/Controller.h"
#include <gtkmm/application.h>
#include "app/MainWindowView.h"

int main(int argc, char **argv)
{

    Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv, "org.gtkmm.example");
    app->set_accel_for_action("mainview.save", "<Primary>s");
    app->set_accel_for_action("mainview.new", "<Primary>n");
    app->set_accel_for_action("mainview.open", "<Primary>o");
    app->set_accel_for_action("mainview.quit", "<Primary>q");
    app->set_accel_for_action("mainview.login", "<Primary>l");
    app->set_accel_for_action("mainview.undo", "<Primary>u");
    app->set_accel_for_action("mainview.redo", "<Primary>r");
    app->set_accel_for_action("mainview.popupedit", "<Primary>e");
    app->set_accel_for_action("mainview.popupremove", "<Primary>m");
    app->set_accel_for_action("personview.quit", "<Primary>c");

    App::ViewFactory viewFactory;                // This applications Views factory
    Module::Controller controller(&viewFactory); // The module controller - from the PersonModule library

    controller.run();                            // The MainWindowView is made now (Module::ViewIdentity::MainWindow)
    App::MainWindowView *mainView = viewFactory.mainWindow();
    int ret = app->run(*mainView);          // Now use GTKmm UI loop
    return (ret);
}
