/*
**  parse date
**
**  class definitions for a textual content lexical analyzer
**
*/
#define __DEBUG

#include "ParseDate.h"
#include <cstring>
#include <iostream>
#define __DEBUG2

ParseDate::ParseDate(const std::string &infilename, bool in_string): Scanner(infilename, in_string),
    read_year(-1),
    month_first(false),
    month_range(false)
{
    read_short_month[0] = '\0';

    days_in_month.clear();
    map_month_days.clear();

    // initialize the symbols stack
    ss.push(TS_EOS);
    ss.push(NTS_G);

    // setup the parsing table
    table[NTS_G][TS_DAY] = 1;
    table[NTS_G][TS_MONTH] = 1;
    table[NTS_G][TS_EOS] = 25;
    table[NTS_G1][TS_DAY] = 2;
    table[NTS_G1][TS_MONTH] = 2;
    table[NTS_G1][TS_EOS] = 26;
    table[NTS_M0][TS_DAY] = 3;
	table[NTS_M0][TS_MONTH] = 28;
    table[NTS_D][TS_DAY] = 4;
    table[NTS_D1][TS_DASH] = 5;
	table[NTS_D1][TS_DAY] = 6;
    table[NTS_D1][TS_MONTH] = 7;
    table[NTS_D1][TS_YEAR] = 7;
    table[NTS_D2][TS_DAY] = 8;
    table[NTS_D2][TS_MONTH] = 31;
    table[NTS_D3][TS_DAY] = 9;
    table[NTS_D3][TS_MONTH] = 10;
    table[NTS_M][TS_MONTH] = 11;
    table[NTS_M1][TS_DAY] = 12;
    table[NTS_M1][TS_MONTH] = 12;
    table[NTS_M1][TS_DASH] = 13;
    table[NTS_M1][TS_YEAR] = 14;
    table[NTS_Y][TS_YEAR] = 15;
    table[NTS_Y][TS_EOS] = 27;
    table[NTS_Y1][TS_DAY] = 16;
    table[NTS_Y1][TS_DASH] = 17;
    table[NTS_Y1][TS_EOS] = 23;
    table[NTS_Y2][TS_DAY] = 18;
    table[NTS_Y2][TS_MONTH] = 29;
    table[NTS_Y3][TS_DAY] = 19;
    table[NTS_M2][TS_DAY] = 20;
    table[NTS_M2][TS_MONTH] = 30;
    table[NTS_M3][TS_DAY] = 21;
    table[NTS_M3][TS_YEAR] = 22;
    table[NTS_Y3][TS_EOS] = 24;

}

ParseDate::~ParseDate()
{
}

/********************************************************************
 *  showtoken - will output the token

    ERR_TOKEN,
    END_OF_FILE,
    DAY_TOKEN,
    YEAR_TOKEN,
    WORD_TOKEN
*******************************************************************/

std::string ParseDate::showtoken(LLTOKEN_CODE token)
{
    std::string message;
    switch (lexer(token)) {
    case TS_MONTH:
        message = "a word month in the date context";
        break;
    case TS_DAY:
        message = "a day number in the date context";
        break;
	case TS_YEAR:
		message = "a year number in the date context";
		break;
	case TS_DASH:
        message = "a dash for a date range in the date context";
        break;
	case TS_SPACE:
		message = "a space in the date context";
		break;
	case TS_EOS:
        message = "end of file in the date context";
        break;
    default: //do nothing
        break;
    }
    return message;
}

/********************************************************************
 *  accept - will check if next token read is as expected
 *******************************************************************/
void ParseDate::accept(LLTOKEN_CODE accept_token)
{
    if (code == accept_token) {
        get_token();
    } else {
        std::cout << "accept: Expecting " << showtoken(accept_token) << " on line " << on_line << " " << on_col << ". \n";
    }
}

/*---------------------------------------------------------------------------*
**  "lexer" Converts a valid token to the corresponding terminal symbol
**
**  returns:  Symbols enum of terminal symbol
*/
enum Symbols ParseDate::lexer(LLTOKEN_CODE token)
{
	std::stringstream str;
	int number;
	switch (token) {
    case L_NUMBER_TOKEN:
		str << lexeme;
		str >> number;
		if (number > 1000) {
			return TS_YEAR;
		} else {
			return TS_DAY;
		}
    case L_WORD_TOKEN:
		return TS_MONTH;
	case L_SPACE_TOKEN:
		return TS_SPACE;
	case L_PUNCT_TOKEN:
		if (lexeme[0] == '-') {
			return TS_DASH;
		}
		else {
			return TS_SPACE; // all other punctuation treated as whitespace
		}
    case L_END_OF_FILE:
        return TS_EOS; // end of stack: the $ terminal symbol
    case L_ERR_TOKEN:
        return TS_INVALID; // end of stack: the $ terminal symbol
    default:
        return TS_INVALID;
    }
}

/*---------------------------------------------------------------------------*
**  "get_parsedate" extracts the next mailing label from the source file
**
**  returns:  nothing
*/
dateitems ParseDate::get_dateitems()
{

    month_first = false; // set it now it gets set in routeMonthOrDay

    get_token();

    while (!ss.empty()) {
        if (lexer(code) == ss.top()) {
            bool dash = false;
			bool can_accept = true;
			switch (lexer(code)) {
            case TS_DASH:
                dash = true;
                break;
			case TS_DAY:
				can_accept = acceptDay();
				break;
			case TS_MONTH:
				can_accept = acceptMonth();
				break;
			case TS_YEAR:
				can_accept = acceptYear();
				if (can_accept)
					assembleDates();
				break;
            }

			if (can_accept) {
				accept(code);
				ss.pop();

				if (!ss.empty() && ss.top() == NTS_M2 && dash == true) {
					month_range = true;
				}
				if (!ss.empty() && ss.top() == NTS_D2 && dash == true) {
					month_range = true;
				}
			} else {
				// Cause the token to show invalid symbol and let it continue to next token
				ss.pop(); // Remove top token and replace with Invalid to let default show its not accepted
				ss.push(TS_INVALID);
			}

		} else if (lexer(code) == TS_SPACE) { 
			accept(code); // ignore TS_SPACE
		} else {
            switch (table[ss.top()][lexer(code)]) {
            case 1: // 1. Dates Grammer
                ss.pop();
                ss.push(NTS_G1);
                break;

            case 2: // 2. Date Item
                ss.pop();
                ss.push(NTS_Y);    // Year
                ss.push(NTS_M0);     // DaysMonth
                break;

            case 3: // 3. DaysMonth
                month_first = false;
                ss.pop();
                ss.push(NTS_M1); // NextMonth
                ss.push(NTS_M);  // Month
				ss.push(NTS_D);  // Day
                break;

            case 4: // 4. DaysMonth
                ss.pop();
                ss.push(NTS_D1);  // Pre Range Day
				//ss.push(TS_SPACE); 
				ss.push(TS_DAY);  // Terminal Symbol Day
                break;

            case 5: // 5. Pre Range Day
                ss.pop();
                ss.push(NTS_D2);  // Pre Range Day
                ss.push(TS_DASH);  // Terminal Symbol Day
                break;

            case 6: // 6. Next Day
                ss.pop();
                ss.push(NTS_D);  // Day
				break;

            case 7: // 7. Month
                ss.pop(); // Left deviation back to Day NTS_D and follow to NTS_M
                break;

            case 8: // 8. Range Day
                ss.pop();
                ss.push(NTS_D3);
                ss.push(TS_DAY);  // Day
                if (month_first == true)
                    // Set 0X8 bit mask to 1 to show this is a ranged day
                {
                    map_month_days[read_short_month].back() |= 1 << 8;
                } else
                    // put the day into the read_short_month days vector now
                {
#ifdef DEBUG2
                    std::cout << "day range true change: " << lexeme << " " << days_in_month.back() << std::endl;
#endif
                    days_in_month.back() |= 1 << 8;
#ifdef DEBUG2
                    std::cout << "day range true change: " << lexeme << " " << days_in_month.back() << std::endl;
#endif
                }
                break;

            case 9: // 9. Next Day
                ss.pop();
                ss.push(NTS_D);
                break;

            case 10: // 10. Month
                ss.pop(); // Left deviation back to Day NTS_D and follow to NTS_M
                break;

            case 11: // 11. Month
                ss.pop();
                //ss.push(TS_SPACE);
				ss.push(TS_MONTH);
				break;

            case 12: // 12. DayMonth
                ss.pop();
                ss.push(NTS_M0);
                break;

            case 13: // 13. PreMonthRange
                ss.pop();
                ss.push(NTS_M2);
                ss.push(TS_DASH);
                break;

            case 14: // 14. Follow through to Year
                ss.pop();
                break;

            case 15: // 15. Year
                ss.pop();
                ss.push(NTS_Y1);
				//ss.push(TS_SPACE);
				ss.push(TS_YEAR);
                break;

            case 16: // 16. Next Year
                ss.pop();
                ss.push(NTS_G1);
                break;

            case 17: // 17. Pre Range Year
                ss.pop();
                ss.push(NTS_Y2);
                ss.push(TS_DASH);
                break;

            case 18: // 18. Range Year
                ss.pop();
                ss.push(NTS_Y3);
                ss.push(NTS_Y);
                ss.push(NTS_M);
                ss.push(NTS_D);
                break;

            case 19: // 19. Range Year
                ss.pop();
                ss.push(NTS_G1);
                break;

            case 20: // 20. Range Month
                month_first = false;
                ss.pop();
                ss.push(NTS_M3);
                ss.push(NTS_M);
                ss.push(NTS_D);
                break;

            case 21: // 21. Post Range Month to Day Month
                ss.pop();
                ss.push(NTS_M0);
                break;

            case 22: // 22. Post Range Month Follow through to Year
                ss.pop();
                break;

            case 23: // 23. Post Year and can be End of Stream EOF
            case 24: // 24 PostYear Range and EOF
            case 25: // 25 PostYear Range and EOF
            case 26: // 26 Dateitem and EOF
            case 27: // 27 Year and EOF
                ss.pop();
                break;

            case 28: // 28. MonthDay
                month_first = true;
                ss.pop();
                ss.push(NTS_M1);
                ss.push(NTS_D);
                ss.push(NTS_M);
                break;

            case 29: // 29. MonthDay
                month_first = true;
                ss.pop();
                ss.push(NTS_Y3);
                ss.push(NTS_Y);
                ss.push(NTS_D);
                ss.push(NTS_M);
                break;

            case 30: //
                month_first = true;
                ss.pop();
                ss.push(NTS_M3);
                ss.push(NTS_D);
                ss.push(NTS_M);
                break;

            case 31: //
                month_first = true;
                ss.pop();
                ss.push(NTS_D);
                ss.push(TS_MONTH);
                break;

            default:
                std::cout << "Parse warning: " << showtoken(code) <<
                          " on line " << on_line << " column " << on_col << std::endl;
                ss.pop();
                if (ss.top() == TS_EOS) {
                    ss.push(NTS_G);
                }
                get_token();
                break;
            }
        }
    }

    return vec_dateitems;
}


/********************************************************************
 *  acceptDay
 *
 *******************************************************************/
bool ParseDate::acceptDay()
{
    unsigned short int read_short_day;           // Day as a number
    std::stringstream str;
    str << lexeme;
    str >> read_short_day;
#ifdef DEBUG2
    std::cout << "acceptday: " << lexeme << " " << read_short_day << std::endl;
#endif

    if (month_range == true) {
        // mark read_short_day with a bit to show its a month range and its the to day in the range
        // this will be the first day for the month - mark a new bit to show its month range
        // not day range
#ifdef DEBUG2
        std::cout << "month range true change: " << lexeme << " " << read_short_day << std::endl;
#endif
        read_short_day |= 1 << 7;
#ifdef DEBUG2
        std::cout << "month range true change: " << lexeme << " " << read_short_day << std::endl;
#endif
        month_range = false;  // the range to is done turn of month_range

    }


    if (month_first == true) {
#ifdef DEBUG2
        std::cout << "acceptDay:  month is first " << read_short_month << '\n';
#endif
        map_month_days[read_short_month].push_back(read_short_day);

    } else {
        /*        if (month_range == true) {
                    // mark read_short_day with a bit to show its a month range and its the to day in the range
                    // this will be the first day for the month - mark a new bit to show its month range
                    // not day range
                    //read_short_day |= 1 << 7;
                    #ifdef DEBUG2
                        std::cout << "month range true change: " << lexeme << " " << days_in_month.back() << std::endl;
                    #endif
                    if (!days_in_month.empty()) {
                       days_in_month.back() |= 1 << 7;
                    }
                    #ifdef DEBUG2
                    std::cout << "month range true change: " << lexeme << " " << days_in_month.back() << std::endl;
                    #endif
                    month_range = false;  // the range to is done turn of month_range

                }*/
        // put the day into the read_short_month days vector now
        days_in_month.push_back(read_short_day);
#ifdef DEBUG2
        std::cout << "acceptDay: days are first and number of days occurred = " << days_in_month.size() <<
                  ". Day found is: " << read_short_day <<
                  " " << read_short_month << std::endl;
#endif
    }
    return true;
}

/********************************************************************
 *  acceptMonth
 *
 *******************************************************************/
bool ParseDate::acceptMonth()
{
    // Use the scanner Word to find the Month
    if (strlen(lexeme) < 2) {
        return false;

    } else {

        char check_short_month[SHORT_MONTH_LENGTH]; // string for the map lookup
        strncpy(check_short_month, lexeme, SHORT_MONTH_LENGTH - 1);
        check_short_month[SHORT_MONTH_LENGTH - 1] = '\0';
        for (int i = 0; check_short_month[i]; i++) {
            check_short_month[i] = tolower(check_short_month[i]);
        }
        month_names::iterator index_loc;
        index_loc = map_month_names.find(check_short_month);

#ifdef DEBUG2
        std::cout << "check month: " << check_short_month << '\n';
#endif
        if (index_loc == map_month_names.end()) {
#ifdef DEBUG2
            std::cout << "not found as month lexeme missing word out: " << check_short_month << '\n';
#endif
            return false;

        } else {

            strncpy(read_short_month, check_short_month, SHORT_MONTH_LENGTH - 1);
            read_short_month[SHORT_MONTH_LENGTH - 1] = '\0';
#ifdef DEBUG2
            std::cout << "short month: " << read_short_month << '\n';
#endif

            if (month_first == true) {
#ifdef DEBUG2
                std::cout << "acceptMonth: read month is added to month_days map " << index_loc->second << '\n';
#endif
                // we make the map entry for the month and a new days vector
                days new_days; // empty days vector
                month_days_pair pair(read_short_month, new_days);
                map_month_days.insert(pair);

            } else {
                // we copy the days_in_month to this read_short_month
#ifdef DEBUG2
                std::cout << "acceptMonth: read month gets the days found added to month_days map " << index_loc->second << " " << read_short_month << '\n';
#endif
                days new_days; // empty days vector
                month_days_pair pair(read_short_month, new_days);
                map_month_days.insert(pair);

                days::const_iterator cur_day;
                for (cur_day = days_in_month.begin(); cur_day != days_in_month.end(); ++cur_day) {
#ifdef DEBUG2
                    std::cout << "acceptMonth: " << *cur_day << " " << read_short_month << '\n';
#endif
                    map_month_days[read_short_month].push_back(*cur_day);
                }
                days_in_month.clear();

            }
            return true; // do accept the lexeme
        }
    }

}

/********************************************************************
 *  acceptYear
 *
 *******************************************************************/
bool ParseDate::acceptYear()
{
    std::stringstream str;
    str << lexeme;
    str >> read_year;
	bool accepted = false;
	accepted = (read_year > 1000);


#ifdef DEBUG2
    if (month_first == true) {
        std::cout << "acceptYear:  month is first, year is " << read_year << '\n';
    } else {
        std::cout << "acceptYear:  days are first, year is " << read_year << '\n';

    }
#endif
    return accepted;
}

/********************************************************************
 *  assembleDates
 * have read any date items then make the date values and strings for each days dates or ranges
 *
 *******************************************************************/
void ParseDate::assembleDates()
{
    if (read_year > 0) {
        // Now create the found date values/ or dateranges
        month_names::iterator cur_index;    // index to loop
        month_days::iterator tmp_index_loc = map_month_days.end();
        month_days::iterator index_loc;
        days::iterator cur_day;

        // Loop over the order of months .. jan feb... dec
        for (cur_index = map_month_names.orderbegin(); cur_index != map_month_names.orderend(); ++cur_index) {

#ifdef DEBUG2
            std::cout << "***********************\n " ;
            std::cout << map_month_names[cur_index->second] << '\n' ;
            std::cout << "size:" << map_month_days[cur_index->second].size() << '\n';
            std::cout << "month:" << cur_index->second << '\n';
#endif

            index_loc = map_month_days.find(cur_index->second);
            if (index_loc != map_month_days.end()) {

                // Look at this months days to find by value the date range required to produce
                days days_in_month = index_loc->second;
                std::string month_name = map_month_names[index_loc->first];
                std::string month_short_name = index_loc->first;

                for (cur_day = days_in_month.begin(); cur_day != days_in_month.end(); ++cur_day) {

                    DateItem new_dateitem;
                    unsigned short int print_day;

                    strncpy(read_short_month, month_short_name.c_str(), SHORT_MONTH_LENGTH - 1);
                    read_short_month[SHORT_MONTH_LENGTH - 1] = '\0';

                    print_day = *cur_day;
                    new_dateitem.SetDate(print_day, read_short_month, month_name.c_str(), read_year);

                    if ((print_day & (1 << 8)) != 0) {
                        // A day or month range
                        // Find the cur_day real day
                        print_day &= ~(1 << 8);
                        new_dateitem.SetShortDay(print_day);

                        // date item day will have range go to next day
                        ++cur_day;
                        if (cur_day != days_in_month.end()) {

                            // If a month range is found
                            if ((*cur_day & (1 << 7)) != 0) {

                                *cur_day &= ~(1 << 7);
                                new_dateitem.SetRangeToDate(*cur_day, read_short_month, month_name.c_str(), read_year, DR_DAY);
#ifdef DEBUG2
                                std::cout << "************\n " ;
                                std::cout << "A day and month range:\n " ;
                                std::cout << "Date: " << new_dateitem << '\n' ;
                                std::cout << "************\n " ;
#endif

                            } else {
                                new_dateitem.SetRangeToDate(*cur_day, read_short_month, month_name.c_str(), read_year, DR_DAY);
#ifdef DEBUG2
                                std::cout << "************\n " ;
                                std::cout << "A day range:\n " ;
                                std::cout << "Date: " << new_dateitem << '\n' ;
                                std::cout << "************\n " ;
#endif
                            }
                        }
                        vec_dateitems.push_back(new_dateitem);

                    }
                    // Test if a range month day (the to day in the range) is found
                    else if ((print_day & (1 << 7)) != 0) {
                        // Find the cur_day real day
                        print_day &= ~(1 << 7);
						if (tmp_index_loc != map_month_days.end()) {
							days first_days_in_month = tmp_index_loc->second;
							unsigned short int first_print_day = first_days_in_month.back();

							std::string first_month_name = map_month_names[tmp_index_loc->first];
							std::string first_month_short_name = tmp_index_loc->first;
							strncpy(read_short_month, first_month_short_name.c_str(), SHORT_MONTH_LENGTH - 1);
							read_short_month[SHORT_MONTH_LENGTH - 1] = '\0';

							new_dateitem.SetDate(first_print_day, read_short_month, first_month_name.c_str(), read_year);

							// date item has a range date item set
							strncpy(read_short_month, month_short_name.c_str(), SHORT_MONTH_LENGTH - 1);
							read_short_month[SHORT_MONTH_LENGTH - 1] = '\0';
							new_dateitem.SetRangeToDate(print_day, read_short_month, month_name.c_str(), read_year, DR_MONTH);
#ifdef DEBUG2
							std::cout << "************\n ";
							std::cout << "A month range:\n ";
							std::cout << "Date: " << new_dateitem << '\n';
							std::cout << "************\n ";
#endif
							vec_dateitems.pop_back(); // remove the last one as it is altered to add this month range to
							vec_dateitems.push_back(new_dateitem);
						}
                    } else {
                        new_dateitem.SetShortDay(*cur_day);
                        vec_dateitems.push_back(new_dateitem);

                    }

                } // for-each loop over months days

                // remember what the last month is with days found
                tmp_index_loc = index_loc;

            } // index_loc != map_month_days.end()
        }

        // set a new days
    }
    days_in_month.clear();
    map_month_days.clear();
    read_year = -1;

}
