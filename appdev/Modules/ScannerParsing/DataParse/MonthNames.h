/*
**  MonthNames.h
**
*/

#ifndef MONTHNAMES_H
#define MONTHNAMES_H

#include <map>

typedef std::map<std::string, std::string> month_names;      // month names
typedef std::pair<std::string, std::string> month_name_pair; // and pair type

class MonthNames
{
public:
    MonthNames();
    virtual ~MonthNames();

private:
    month_names map_month_names;        // month names
    month_names map_month_order;        // month names

public:
    month_names::iterator find ( char *find );
    month_names::iterator orderbegin();
    month_names::iterator orderend();
    month_names::iterator begin();
    month_names::iterator end();
    std::string operator[] ( std::string key );
};


#endif /* MONTHNAMES_H */



