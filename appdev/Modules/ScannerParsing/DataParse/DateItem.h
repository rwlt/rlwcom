/******************************************************************************
 * class dateitem -  dateitem details                                         *
 *                                                                            *
 *****************************************************************************/
#ifndef DATEITEM_H
#define DATEITEM_H

#include "../Common/constants.h"
#include <sstream>

enum DATE_RANGE {
    DR_NONE = 0,
    DR_DAY = 1,
    DR_MONTH = 2
};

class DateItem
{
    unsigned short int short_day;           // Short day
    unsigned int year;                      // Year
    char short_month[SHORT_MONTH_LENGTH];   // Short month
    char long_month[20];                    // Long month name
                                            // Date range to details
    unsigned short int to_short_day;        // Short day
    unsigned int to_year;                   // Year
    char to_short_month[SHORT_MONTH_LENGTH];// Short month
    char to_long_month[20];                 // Long month name

    DATE_RANGE date_range;                  // Type of date range to
    
public:
    /** Default constructor */
    DateItem();
    DateItem ( unsigned short int new_short_day,
               const char *new_short_month,
               const char *new_long_month,
               unsigned int new_year );

    /** Default destructor */
    virtual ~DateItem();

    /** Copy constructor
     *  \param other Object to copy from
     */
    DateItem ( const DateItem &rhs );

    /** Assignment operator
     *  \param other Object to assign from
     *  \return A reference to this
     */
    DateItem &operator= ( const DateItem &rhs );

    bool SetShortDay ( unsigned short int new_short_day );
    bool SetDate ( unsigned short int day,
                   const char *month,
                   const char *long_month,
                   unsigned int year );
    bool SetRangeToDate ( unsigned short int day,
                          const char *month,
                          const char *long_month,
                          unsigned int year,
                          DATE_RANGE new_date_range );
    const DATE_RANGE GetDateRange();
    friend std::ostream &operator << ( std::ostream &out_stream, const DateItem &date_item );

};

inline std::ostream &operator << ( std::ostream &out_stream, const DateItem &date_item )
{
    out_stream << std::left;
    switch ( date_item.date_range ) {
    case DR_DAY:
        out_stream << date_item.short_day << '-' << date_item.to_short_day;
        out_stream << ' ' << date_item.long_month;
        out_stream << ' ' << date_item.year;
        break;

    case DR_MONTH:
        out_stream << date_item.short_day << ' ' << date_item.long_month << '-';
        out_stream << date_item.to_short_day;
        out_stream << ' ' << date_item.to_long_month;
        out_stream << ' ' << date_item.year;
        break;

    case DR_NONE:
        out_stream << date_item.short_day;
        out_stream << ' ' << date_item.long_month;
        out_stream << ' ' << date_item.year;
        break;

    default:
        // Do nothing
        break;
    }

    return ( out_stream );
}



#endif // DATEITEM_H
