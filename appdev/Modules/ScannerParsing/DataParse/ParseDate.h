/*
**  llparsedate.h
**
**  class definitions for a textual content mailing label
**
*/

#ifndef LLPARSEDATE_H 
#define LLPARSEDATE_H 
 
#include "../Common/constants.h"
#include "../Scanner/scanner.h"
#include "MonthNames.h"
#include "DateItem.h"
#include <vector>
#include <map>

enum Symbols {
    // the symbols:
    // Terminal symbols:
    TS_DAY,         // 1 to 2 digit day ( from scanner lexical analyser )
    TS_MONTH,       // 3 letter month short name ( from scanner lexical analyser )
    TS_YEAR,        // 4 digits ( from TOKEN in scanner lexical analyser )
    TS_DASH,        // -
	TS_SPACE,      
	TS_EOS,         // $, in this case corresponds to '\0' i.e. 0 eqauls no data from
    TS_INVALID,     // invalid token

    // Non-terminal symbols:
    NTS_G,          // Dates Grammer
    NTS_G1,         // Date Item
    NTS_M0,         // Day Months
    NTS_D,          // Day
    NTS_D1,         // Pre Range Day
    NTS_D2,         // Range Day
    NTS_D3,         // Post Range Day
    NTS_M,          // Month
    NTS_M1,         // Next Month
    NTS_M2,         // Range Month
    NTS_M3,         // Post Range Month
    NTS_Y,          // Year
    NTS_Y1,         // Pre Range Year
    NTS_Y2,         // Range Year
    NTS_Y3          // Post Range Year
};

typedef std::map< enum Symbols, std::map<enum Symbols, int> > symbol_table; 
typedef std::stack<enum Symbols>     symbol_stack;          // symbol stack
typedef std::vector<DateItem> dateitems;                     // dateitems
typedef std::vector<unsigned int> days;                      // days
typedef std::map<std::string, days> month_days;              // month names
typedef std::pair<std::string, days> month_days_pair;        // and pair type

class ParseDate: public Scanner 
{
public:
    ParseDate ( const std::string &infilename, bool in_string = false );
    virtual ~ParseDate();
    dateitems get_dateitems();                   // Start finding and checking the date(s) in input source

private:
    int read_year;                               // The year 
    bool month_first;                            // Is the style month first or day first. Year is always last.
    bool month_range;                            // Can have a range going over to another month
    char read_short_month[SHORT_MONTH_LENGTH];   // Short month (3 letter lowercase) for a key
    days days_in_month;                          // Days are kept on a vector
    month_days map_month_days;                   // Months found get a map of found days
    symbol_stack ss;                             // Parse stack sing grammer symbols
    symbol_table table;                          // Grammer table for finding dates
    
    MonthNames map_month_names;                  // month names
    dateitems vec_dateitems;                     // complete list of date items found in the input file
    
    /*
    Converts a valid token to the corresponding terminal symbol
    */
    enum Symbols lexer ( LLTOKEN_CODE token );
    std::string showtoken ( LLTOKEN_CODE token );
    void accept ( LLTOKEN_CODE accept_token );
    bool acceptDay();
    bool acceptMonth();
    bool acceptYear();
    void assembleDates();

};

#endif /* LLPARSEDATE_H */



