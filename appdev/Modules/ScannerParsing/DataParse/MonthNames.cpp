/*
**  MonthNames.cpp
**
*/
#include "MonthNames.h"

MonthNames::MonthNames()
{
    month_name_pair month_pair;
    month_pair.first = std::string("jan");
    month_pair.second = std::string("January");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("feb");
    month_pair.second = std::string("February");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("mar");
    month_pair.second = std::string("March");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("apr");
    month_pair.second = std::string("April");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("may");
    month_pair.second = std::string("May");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("jun");
    month_pair.second = std::string("June");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("jul");
    month_pair.second = std::string("July");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("aug");
    month_pair.second = std::string("August");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("sep");
    month_pair.second = std::string("September");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("oct");
    month_pair.second = std::string("October");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("nov");
    month_pair.second = std::string("November");
    map_month_names.insert(month_pair);
    month_pair.first = std::string("dec");
    month_pair.second = std::string("December");
    map_month_names.insert(month_pair);

    month_pair.first = std::string("01");
    month_pair.second = std::string("jan");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("02");
    month_pair.second = std::string("feb");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("03");
    month_pair.second = std::string("mar");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("04");
    month_pair.second = std::string("apr");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("05");
    month_pair.second = std::string("may");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("06");
    month_pair.second = std::string("jun");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("07");
    month_pair.second = std::string("jul");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("08");
    month_pair.second = std::string("aug");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("09");
    month_pair.second = std::string("sep");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("10");
    month_pair.second = std::string("oct");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("11");
    month_pair.second = std::string("nov");
    map_month_order.insert(month_pair);
    month_pair.first = std::string("12");
    month_pair.second = std::string("dec");
    map_month_order.insert(month_pair);

}

MonthNames::~MonthNames()
{
    
}

month_names::iterator MonthNames::find(char* find) {
	return map_month_names.find(find);
}

month_names::iterator MonthNames::orderbegin() {
	return map_month_order.begin();
}

month_names::iterator MonthNames::orderend() {
	return map_month_order.end();
}

month_names::iterator MonthNames::end() {
	return map_month_names.end();
}

std::string MonthNames::operator[](std::string key) {
	return map_month_names[key];
}