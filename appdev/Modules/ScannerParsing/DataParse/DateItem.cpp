/*
 * *  dateitem
 **
 **  class definitions for a dateitem details. A date range with to date is available
 **
 */

#include "DateItem.h"
#include <cstring>
#include <iostream>

DateItem::DateItem():
    short_day(0), year(0),
    to_short_day(0), to_year(0),
    date_range(DR_NONE)
{
    short_month[0] = '\0';
    long_month[0] = '\0';
    to_short_month[0] = '\0';
    to_long_month[0] = '\0';
}

DateItem::DateItem(unsigned short int new_short_day,
                   const char *new_short_month,
                   const char *new_long_month,
                   unsigned int new_year):
    short_day(new_short_day), year(new_year),
    to_short_day(0), to_year(0),
    date_range(DR_NONE)
{
    short_month[0] = '\0';
    long_month[0] = '\0';
    strcpy(short_month, new_short_month);
    strcpy(long_month, new_long_month);
    to_short_month[0] = '\0';
    to_long_month[0] = '\0';
}

DateItem::~DateItem()
{
}

DateItem::DateItem(const DateItem &other):
    short_day(other.short_day), year(other.year),
    to_short_day(other.to_short_day), to_year(other.to_year),
    date_range(other.date_range)
{
    strcpy(short_month, other.short_month);
    strcpy(long_month, other.long_month);
    strcpy(to_short_month, other.to_short_month);
    strcpy(to_long_month, other.to_long_month);
}


DateItem &DateItem::operator=(const DateItem &rhs)
{
    if (this == &rhs) {
        return *this;    // handle self assignment
    }
    short_day = rhs.short_day;
    year = rhs.year;
    to_short_day = rhs.to_short_day;
    to_year = rhs.to_year;
    date_range = rhs.date_range;
    strcpy(short_month, rhs.short_month);
    strcpy(long_month, rhs.long_month);
    strcpy(to_short_month, rhs.to_short_month);
    strcpy(to_long_month, rhs.to_long_month);
    return *this;
}
//
//------------------------------------------------------------
// Public accessors
//------------------------------------------------------------
//
bool DateItem::SetShortDay(unsigned short int new_short_day)
{
    short_day = new_short_day;
    return true;
}

bool DateItem::SetDate(unsigned short int day,
                       const char *month,
                       const char *the_long_month,
                       unsigned int the_year)
{
    if (strlen(month) == 0) {
        return false;
    }
    if (strlen(month) > (int)SHORT_MONTH_LENGTH - 1) {
        return false;
    }
    if (strlen(the_long_month) == 0) {
        return false;
    }
    if (strlen(the_long_month) > (int)sizeof(long_month)) {
        return false;
    }

    short_day = day;
    strcpy(short_month, month);
    strcpy(long_month, the_long_month);
    year = the_year;

    return true;
}

bool DateItem::SetRangeToDate(unsigned short int day,
                              const char *month,
                              const char *long_month,
                              unsigned int year,
                              DATE_RANGE new_date_range)
{
    if (strlen(month) == 0) {
        return false;
    }
    if (strlen(month) > (int)SHORT_MONTH_LENGTH - 1) {
        return false;
    }
    if (strlen(long_month) == 0) {
        return false;
    }
    if (strlen(long_month) > (int)sizeof(to_long_month)) {
        return false;
    }

    to_short_day = day;
    strcpy(to_short_month, month);
    strcpy(to_long_month, long_month);
    to_year = year;
    date_range = new_date_range;

    return true;
}

const DATE_RANGE DateItem::GetDateRange()
{
    return date_range;
}
