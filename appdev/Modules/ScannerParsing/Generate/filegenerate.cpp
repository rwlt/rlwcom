/*
**  filegenerate.cpp
**
**  class definitions for a textual content lexical analyzer
**
**  filegenerate - new constructor to prepare the i/o system called file to read
** ~filegenerate - destructor to release filename
**  advance - used a unbuffered I/O to buffer the input into the filegenerate
*/

#include "filegenerate.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef _WINDOWS
#include <io.h>
#else /* _WINDOWS */
#include <unistd.h>
#endif /* _WINDOWS */

#include <cctype>
#include <cstdio>
#include <iostream>
#include <cstring>
#include <stdexcept>

filegenerate::filegenerate(const std::string &arg_create_filename):
    create_filename(arg_create_filename), file_output(0), write_size_buffer(0)
{
    std::cout << "CREATE filename " << create_filename << '\n';

    write_size_buffer = 0;                   // set 0 written text to output file
    memset(buffer, '\0', BUFFER_SIZE);       // set buffer empty to strcat
    file_output = open(arg_create_filename.c_str(), O_WRONLY | O_TRUNC | O_CREAT, 0666);
    if (file_output < 0) {
        throw std::runtime_error("file created failure");
    }
}

filegenerate::~filegenerate()
{
    // Write the end bffer to output from the scanner to file_output
    if (file_output >= 0) {
        write(file_output, buffer, (unsigned int) write_size_buffer);
        close(file_output);
    }
}

/*---------------------------------------------------------------------------*
**  "generate" reads the scanner input and writes into file_output
**
*/
void filegenerate::write_text(const char *text)
{
    int size;        // size characters of text
    size = strlen(text);
    if (file_output >= 0) {
        if ((size + write_size_buffer) >= BUFFER_SIZE) {
            write(file_output, buffer, (unsigned int) write_size_buffer);
            memset(buffer, '\0', BUFFER_SIZE);
            write_size_buffer = 0;
        }
        strcat(buffer, text);
        write_size_buffer += size;
    }

}

