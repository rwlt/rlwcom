#ifndef D_PARSEDATEFIXTURE_H
#define D_PARSEDATEFIXTURE_H

#include "DataParse/ParseDate.h"
///////////////////////////////////////////////////////////////////////////////
//
//  ParseDateFixture:
//
///////////////////////////////////////////////////////////////////////////////

class ParseDateFixture
{
public:
    explicit ParseDateFixture() :
    index0(0),
    index1(1)
    {
    };
    virtual ~ParseDateFixture(){};
    const int index0;
    const int index1;
    
    /********************************************************************
     *  showtoken - will output the token
     * 
     *    ERR_TOKEN,
     *    END_OF_FILE,
     *    DAY_TOKEN,
     *    YEAR_TOKEN,
     *    WORD_TOKEN
     *******************************************************************/
    std::string showtoken(LLTOKEN_CODE token)
    {
        std::string message("");
        switch (token) {
            case L_WORD_TOKEN:
                message = "a word in the date context";
                break;
//            case L_MONTH_TOKEN:
//                message = "a month in the date context";
//                break;
            case L_DAY_TOKEN:
                message = "a day number in the date context";
                break;
            case L_YEAR_TOKEN:
                message = "a year in the date context";
                break;
            case L_DASH_TOKEN:
                message = "a dash for a date range in the date context";
                break;
            case L_START_OF_FILE:
                message = "end of file";
                break;
            case L_END_OF_FILE:
                message = "end of file";
                break;
            case L_ERR_TOKEN:
                message = "error in file";
                break;
            default: //do nothing
                break;
        }
        return message;
    }
    
    
private:
    
    ParseDateFixture ( const ParseDateFixture& );
    ParseDateFixture& operator= ( const ParseDateFixture& );
    
};

#endif  // D_PARSEDATEFIXTURE_H
