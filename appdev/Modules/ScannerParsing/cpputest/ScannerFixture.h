#ifndef D_SCANNERFIXTURE_H
#define D_SCANNERFIXTURE_H

#include "Scanner/scanner.h"
///////////////////////////////////////////////////////////////////////////////
//
//  ProcessFixture:
//
///////////////////////////////////////////////////////////////////////////////

class ScannerFixture
{
public:
    explicit ScannerFixture() :
    index0(0),
    index1(1)
    {
    };
    virtual ~ScannerFixture(){};
    const int index0;
    const int index1;

    /********************************************************************
     *  showtoken - will output the token
     * 
     *    ERR_TOKEN,
     *    END_OF_FILE,
     *    DAY_TOKEN,
     *    YEAR_TOKEN,
     *    WORD_TOKEN
     *******************************************************************/
    std::string showtoken(LLTOKEN_CODE token)
    {
        std::string message("");
        switch (token) {
            case L_WORD_TOKEN:
                message = "a word in the date context";
                break;
//            case L_MONTH_TOKEN:
//                message = "a month in the date context";
//                break;
            case L_DAY_TOKEN:
                message = "a day number in the date context";
                break;
            case L_YEAR_TOKEN:
                message = "a year in the date context";
                break;
            case L_DASH_TOKEN:
                message = "a dash for a date range in the date context";
                break;
            case L_START_OF_FILE:
                message = "end of file";
                break;
            case L_END_OF_FILE:
                message = "end of file";
                break;
            case L_ERR_TOKEN:
                message = "error in file";
                break;
            default: //do nothing
                break;
        }
        return message;
    }
    
    
private:

    ScannerFixture ( const ScannerFixture& );
    ScannerFixture& operator= ( const ScannerFixture& );

};

#endif  // D_SCANNERFIXTURE_H
