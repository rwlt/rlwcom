#include "../DataParse/DateItem.h"
#include <iostream>
#include <sstream>
//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// DateItem test group
//-------------------------------------------------------
//
TEST_GROUP(DateItemAccessor)
{
//    ParseDateFixture fixture;
};

TEST(DateItemAccessor, DayRangeAccessor)
{
    std::string date("10-12 November 2016");
    std::stringstream the_date;
    unsigned short int print_day;
    DateItem new_dateitem(10, "nov", "November", 2016);

    the_date.str(std::string());
    the_date << new_dateitem;
    STRCMP_EQUAL(std::string("10 November 2016").c_str(), the_date.str().c_str());
    LONGS_EQUAL(DR_NONE, new_dateitem.GetDateRange());
    
    // 10-12 November 2016
    // read a day and added to month
    // Test if a range day is found
    print_day = 10;
    print_day |= 1 << 8;
    // Test if a range day is found
    if ((print_day & (1 << 8)) != 0) {
        // Find the cur_day real day
        unsigned short int to_day;
        print_day &= ~(1 << 8);
        new_dateitem.SetShortDay(print_day);

        // date item has a range date item set
        to_day = 12;
        new_dateitem.SetRangeToDate(to_day, "nov", "November", 2016, DR_DAY);

    }

    the_date.str(std::string());
    the_date << new_dateitem;
    STRCMP_EQUAL(date.c_str(), the_date.str().c_str());
    LONGS_EQUAL(DR_DAY, new_dateitem.GetDateRange());
    
}

TEST(DateItemAccessor, DayAndMonthRangeAccessor)
{
    std::string date("10-12 November 2016");
    //10-12 November 2016
    std::stringstream the_date;
    unsigned short int print_day;
    DateItem new_dateitem(10, "nov", "November", 2016);
    
    the_date.str(std::string());
    the_date << new_dateitem;
    STRCMP_EQUAL(std::string("10 November 2016").c_str(), the_date.str().c_str());
    LONGS_EQUAL(DR_NONE, new_dateitem.GetDateRange());
    
    // read a day and added to month
    // Test if a range day is found
    print_day = 10;
    print_day |= 1 << 7;
    print_day |= 1 << 8;
    
    new_dateitem.SetShortDay(print_day);
    // Test if a range day is found
    if (((print_day & (1 << 8)) != 0) && ((print_day & (1 << 7)) != 0)) {
        // Find the cur_day real day
        unsigned short int to_day;
        print_day &= ~(1 << 8);
        print_day &= ~(1 << 7);
        new_dateitem.SetShortDay(print_day);
        
        // date item has a range date item set
        to_day = 12;
        new_dateitem.SetRangeToDate(to_day, "nov", "November", 2016, DR_DAY);
        
    }
    
    the_date.str(std::string());
    the_date << new_dateitem;
    STRCMP_EQUAL(date.c_str(), the_date.str().c_str());
    LONGS_EQUAL(DR_DAY, new_dateitem.GetDateRange());
    
}

TEST(DateItemAccessor, MonthRangeAccessor)
{
    std::string date("29 November-10 December 2016");
    std::stringstream the_date;
    unsigned short int print_day;
    DateItem new_dateitem(29, "nov", "November", 2016);
    
    the_date.str(std::string());
    the_date << new_dateitem;
    STRCMP_EQUAL(std::string("29 November 2016").c_str(), the_date.str().c_str());
    LONGS_EQUAL(DR_NONE, new_dateitem.GetDateRange());
    
    print_day = 29;
    print_day |= 1 << 7;
    
    new_dateitem.SetShortDay(print_day);
    if ((print_day & (1 << 7)) != 0) {
        unsigned short int to_day;
        
        print_day &= ~(1 << 7);
        new_dateitem.SetShortDay(print_day);
        
        // date item has a range date item set
        to_day = 10;
        new_dateitem.SetRangeToDate(to_day, "dec", "December", 2016, DR_MONTH);
        
    }
    
    the_date.str(std::string());
    the_date << new_dateitem;
    STRCMP_EQUAL(date.c_str(), the_date.str().c_str());
    LONGS_EQUAL(DR_MONTH, new_dateitem.GetDateRange());
    
}

