#include "ParseDateFixture.h"

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// ParseDate test group
//-------------------------------------------------------
//
TEST_GROUP(ParseDateAccessor)
{
    ParseDateFixture fixture;
};

TEST(ParseDateAccessor, ScanDateMonthRangeAccessor)
{
    dateitems vec_dateitems;
    std::stringstream the_date;
    
    LONGS_EQUAL(0, vec_dateitems.size());
    
    // Septembe will be found as Sep so the correct spelling of September is the dateitem details
    std::string date("15,18 August-2 Septembe 2013");
    ParseDate parsedate(date, true); 
    
    vec_dateitems = parsedate.get_dateitems();
     
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(parsedate.code).c_str());
    LONGS_EQUAL(2, vec_dateitems.size());
    
    the_date.str(std::string());
    the_date << vec_dateitems.at(0); 
    STRCMP_EQUAL(std::string("15 August 2013").c_str(), the_date.str().c_str());
    the_date.str(std::string());
    the_date << vec_dateitems.at(1);
    STRCMP_EQUAL(std::string("18 August-2 September 2013").c_str(), the_date.str().c_str());
    
}

TEST(ParseDateAccessor, ScanDateDayRangeAccessor)
{
    dateitems vec_dateitems;
    std::stringstream the_date; 
     
    LONGS_EQUAL(0, vec_dateitems.size());  
    
    // Septembe will be found as Sep so the correct spelling of September is the dateitem details
    std::string date("24-26, 28 February 2013");
    ParseDate parsedate(date, true); 
    
    vec_dateitems = parsedate.get_dateitems();
    
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(parsedate.code).c_str());
    LONGS_EQUAL(2, vec_dateitems.size());
    
    the_date.str(std::string());
    the_date << vec_dateitems.at(0);
    /// 23-153 February 2013   -
    STRCMP_EQUAL(std::string("24-26 February 2013").c_str(), the_date.str().c_str());

    //the_date.str(std::string());
    //the_date << vec_dateitems.at(1);
    //STRCMP_EQUAL(std::string("18 August-2 September 2013").c_str(), the_date.str().c_str());
    
}


TEST(ParseDateAccessor, OpenNoOpenFileAccessor)
{
    ParseDate parsedate("nofile.txt");
    STRCMP_EQUAL(std::string("nofile.txt is not able to be read.").c_str(), parsedate.lexeme);
    
    parsedate.get_dateitems();
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(parsedate.code).c_str());
    
}

TEST(ParseDateAccessor, OpenEmptyFileAccessor)
{
    ParseDate parsedate("empty.txt");
    parsedate.get_dateitems();
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(parsedate.code).c_str());
    
}

TEST(ParseDateAccessor, ScanDatesAccessor) 
{
    dateitems vec_dateitems;
    std::stringstream the_date;
    ParseDate parsedate("date_file.txt");

    vec_dateitems = parsedate.get_dateitems();
    
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(parsedate.code).c_str());
    LONGS_EQUAL(12, vec_dateitems.size());
    
    the_date.str(std::string());
    the_date << vec_dateitems.at(0);
    STRCMP_EQUAL(std::string("22 January-15 February 1980").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(1);
    STRCMP_EQUAL(std::string("23-25 February 1980").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(2);
    STRCMP_EQUAL(std::string("27 February 1980").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(3);
    STRCMP_EQUAL(std::string("17 July 1980").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(4);
    STRCMP_EQUAL(std::string("20 July 1980").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(5);
    STRCMP_EQUAL(std::string("13 April-5 May 1985").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(6);
    STRCMP_EQUAL(std::string("3 June 1985").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(7);
    STRCMP_EQUAL(std::string("28 April-2 May 1999").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(8);
    STRCMP_EQUAL(std::string("3 December 2000").c_str(), the_date.str().c_str());
    
    the_date.str(std::string());
    the_date << vec_dateitems.at(9);
    STRCMP_EQUAL(std::string("4 December 2000").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(10);
    STRCMP_EQUAL(std::string("23 November 2013").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(11);
    STRCMP_EQUAL(std::string("24 November 2013").c_str(), the_date.str().c_str());
    
}

TEST(ParseDateAccessor, ScanDatesInvalidAccessor)
{
    dateitems vec_dateitems;
    std::stringstream the_date;
    ParseDate parsedate("date_file_invalid.txt");
    
    vec_dateitems = parsedate.get_dateitems();

    LONGS_EQUAL(5, vec_dateitems.size());
    
    the_date.str(std::string());
    the_date << vec_dateitems.at(0);
    STRCMP_EQUAL(std::string("39 December 2000").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(1);
    STRCMP_EQUAL(std::string("9 December 2000").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(2);
    STRCMP_EQUAL(std::string("9 December 2000").c_str(), the_date.str().c_str());
    
    the_date.str(std::string());
    the_date << vec_dateitems.at(3);
    STRCMP_EQUAL(std::string("9 December 2000").c_str(), the_date.str().c_str());

    the_date.str(std::string());
    the_date << vec_dateitems.at(4);
    STRCMP_EQUAL(std::string("9 December 2000").c_str(), the_date.str().c_str());

    STRCMP_EQUAL(std::string("").c_str(), parsedate.lexeme);
    LONGS_EQUAL(293, parsedate.on_col);
    LONGS_EQUAL(1, parsedate.on_line);
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(parsedate.code).c_str());
}
