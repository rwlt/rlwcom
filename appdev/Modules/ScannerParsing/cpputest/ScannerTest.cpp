#include "ScannerFixture.h"

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// Scanner test group
//-------------------------------------------------------
//
TEST_GROUP(ScannerAccessor)
{
    ScannerFixture fixture;
};

TEST(ScannerAccessor, ScanDatesWithStringAccessor)
{

    std::string date("15,18 August-2 Septembe 2013");
    Scanner scanner(date, true);

    while (scanner.code != L_END_OF_FILE) {
        scanner.get_token();
    }
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("a year in the date context").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("").c_str(), scanner.lexeme);

    LONGS_EQUAL(28, scanner.on_col);
    LONGS_EQUAL(1, scanner.on_line);
}


TEST(ScannerAccessor, OpenNoOpenFileAccessor)
{
    Scanner scanner("nofile.txt");
    STRCMP_EQUAL(std::string("nofile.txt is not able to be read.").c_str(), scanner.lexeme);

    scanner.get_token();
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("error in file").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("nofile.txt is not able to be read.").c_str(), scanner.lexeme);
    LONGS_EQUAL(0, scanner.on_col);
    LONGS_EQUAL(0, scanner.on_line);

}

TEST(ScannerAccessor, OpenEmptyFileAccessor)
{
    Scanner scanner("empty.txt");
    scanner.get_token();
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("").c_str(), scanner.lexeme);
    LONGS_EQUAL(0, scanner.on_col);
    LONGS_EQUAL(1, scanner.on_line);

}


TEST(ScannerAccessor, ScanTokenAccessor)
{
    Scanner scanner("date3_file.txt");

    /*while (scanner.code != L_END_OF_FILE) {
        scanner.get_token();
    }*/
    scanner.get_token();
    //a day number in the date context
    STRCMP_EQUAL(std::string("a day number in the date context").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("26").c_str(), scanner.lexeme);
    LONGS_EQUAL(1, scanner.on_col);
    LONGS_EQUAL(1, scanner.on_line);

    scanner.get_token();
    STRCMP_EQUAL(std::string("a word in the date context").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("a day number in the date context").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("December").c_str(), scanner.lexeme);
    LONGS_EQUAL(4, scanner.on_col);
    LONGS_EQUAL(1, scanner.on_line);

    scanner.get_token();
    STRCMP_EQUAL(std::string("a year in the date context").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("a word in the date context").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("2007").c_str(), scanner.lexeme);
    LONGS_EQUAL(13, scanner.on_col);
    LONGS_EQUAL(1, scanner.on_line);

    scanner.get_token();
    STRCMP_EQUAL(std::string("a dash for a date range in the date context").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("a year in the date context").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("-").c_str(), scanner.lexeme);
    LONGS_EQUAL(17, scanner.on_col);
    LONGS_EQUAL(1, scanner.on_line);

    scanner.get_token();
    STRCMP_EQUAL(std::string("a day number in the date context").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("a dash for a date range in the date context").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("12").c_str(), scanner.lexeme);
    LONGS_EQUAL(18, scanner.on_col);
    LONGS_EQUAL(1, scanner.on_line);

    scanner.get_token();
    STRCMP_EQUAL(std::string("a word in the date context").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("a day number in the date context").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("January").c_str(), scanner.lexeme);
    LONGS_EQUAL(21, scanner.on_col);
    LONGS_EQUAL(1, scanner.on_line);

    scanner.get_token();
    STRCMP_EQUAL(std::string("a year in the date context").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("a word in the date context").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("2008").c_str(), scanner.lexeme);
    LONGS_EQUAL(29, scanner.on_col);
    LONGS_EQUAL(1, scanner.on_line);

    scanner.get_token();
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("a year in the date context").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("").c_str(), scanner.lexeme);
    LONGS_EQUAL(0, scanner.on_col);
    LONGS_EQUAL(4, scanner.on_line);

}

TEST(ScannerAccessor, ScanDatesAccessor)
{
    Scanner scanner("date_file.txt");

    while (scanner.code != L_END_OF_FILE) {
        scanner.get_token();
    }
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("a year in the date context").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("").c_str(), scanner.lexeme);
    LONGS_EQUAL(0, scanner.on_col);
    LONGS_EQUAL(7, scanner.on_line);
}

TEST(ScannerAccessor, ScanDatesInvalidAccessor)
{
    Scanner scanner("date_file_invalid.txt");

    while (scanner.code != L_END_OF_FILE) {
        scanner.get_token();
    }
    STRCMP_EQUAL(std::string("end of file").c_str(), fixture.showtoken(scanner.code).c_str());
    STRCMP_EQUAL(std::string("a year in the date context").c_str(), fixture.showtoken(scanner.previous).c_str());
    STRCMP_EQUAL(std::string("").c_str(), scanner.lexeme);
    LONGS_EQUAL(293, scanner.on_col);
    LONGS_EQUAL(1, scanner.on_line);
}
