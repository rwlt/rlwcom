#ifndef D_CONSTANTS_H_
#define D_CONSTANTS_H_

const int BUFFER_SIZE = (4 * 1024);  // use 4k buffers
//const int BUFFER_SIZE = (8);  // use 32k buffers
const int SHORT_MONTH_LENGTH = 4;

#endif /* D_CONSTANTS_H_ */