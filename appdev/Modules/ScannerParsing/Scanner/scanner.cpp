/*
**  scanner.cpp
**
**  class definitions for a textual content lexical analyzer
**
**  scanner - new constructor to prepare the i/o system called file to read
** ~scanner - destructor to release filename
**  advance - used a unbuffered I/O to buffer the input into the scanner
*/
#define __DEBUG2
#define __DEBUG

#include "scanner.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef _WINDOWS
#include <io.h>
#else /* _WINDOWS */
#include <unistd.h>
#endif /* _WINDOWS */

#include <cstring>
#include <iostream>
#include <string>

Scanner::Scanner(const std::string &infilename, bool in_string):
    code(L_START_OF_FILE), previous(L_END_OF_FILE), on_line(0), on_col(0),
    i_src(0), i_lex(0), read_size_buffer(0), file_scan(-1), line_count(0),
    col_pos_count(0), curr_ch(0), next_ch(0), input_string(in_string),
    filename(infilename)
{
    lexeme[0] = '\0';
    buffer[0] = '\0';

    if (input_string == true) {
        // Get buffer from string in constructor
        i_src = 0;            // set no data read yet
        line_count = 1;       // line number on 1
        col_pos_count = 1;

        // Read the buffer ready for scanning
#ifdef DEBUG2
        std::cout << "String Buffer size: " << BUFFER_SIZE << std::endl;
#endif
        // Check the input string is not going to larger than our buffer.
        // Only using a sensible size so wont be in large sizes.
        // If oversized it will be truncated and not added on later the same as file contents are.
        // Only using one buffer for string inputs
        std::size_t slen = infilename.length();
        read_size_buffer = 0;
        if (slen > BUFFER_SIZE) {
            read_size_buffer = BUFFER_SIZE;
        } else {
            read_size_buffer = slen;
        }
        strncpy(buffer, infilename.c_str(), read_size_buffer );
        buffer[read_size_buffer] = '\0';
#ifdef DEBUG2
        std::cout << "String amount read " << slen << std::endl;
        std::cout << "buffer             " << buffer << std::endl;
        std::cout << "buffer length      " << strlen(buffer) << std::endl;
#endif
        initialize_table();
        advance(); // begin

    } else {
        // Get file handle and buffer from the file
        file_scan = open(infilename.c_str(), O_RDONLY);

        if (file_scan < 0) {
            code = L_ERR_TOKEN;
            std::string msg(infilename + " is not able to be read.");
            strncpy(lexeme, msg.c_str(), msg.length());
            lexeme[msg.length()] = '\0';

        } else {
            i_src = 0;            // set no data read yet
            line_count = 1;       // line number on 1
            col_pos_count = 1;

            // Read the buffer ready for scanning
#ifdef DEBUG2
            std::cout << "Buffer size: " << sizeof(buffer) << std::endl;
#endif
            read_size_buffer = read(file_scan, buffer, sizeof(buffer));
#ifdef DEBUG2
            std::cout << "amount read " << read_size_buffer << std::endl;
#endif

            initialize_table();
            advance(); // begin
        }
    }
}

Scanner::~Scanner()
{
    // Make sure read file is close with file handle
    if (file_scan >= 0) {
#ifdef DEBUG
        std::cout << "file h : " << file_scan << " closed" << std::endl;
#endif
        close(file_scan);
    }
}
/*---------------------------------------------------------------------------*
**  "advance" sets curr_ch to the next character in the source file.
**
**  returns:  the current character
*/
char Scanner::advance()
{
    // file_scan is read to get a buufer when empty input found
    int forward = i_src + 1;
#ifdef DEBUG2
    std::cout << "forward check =  " << forward << std::endl;
#endif

    if (read_size_buffer == 0) {
        curr_ch = 0;
        next_ch = 0;
        return 0; // EOF
    }

    curr_ch = buffer[i_src];

    if (input_string == true) {  // only when reading a file do it in buffers
        if (forward == read_size_buffer) {
            next_ch = 0;
            i_src = 0;
            read_size_buffer = 0; // No more buffers to be read so zero read set
        } else {
            next_ch = buffer[forward];
            ++i_src;
        }

    } else {
        if (forward == read_size_buffer) {     // end of a buffer for forward
#ifdef DEBUG2
            std::cout << "Read forward buffer size: " << sizeof(buffer) << std::endl;
#endif
            read_size_buffer = read(file_scan, buffer, sizeof(buffer));
            next_ch = 0;
#ifdef DEBUG2
            std::cout << "amount read " << read_size_buffer << std::endl;
#endif

            if (read_size_buffer == 0) {
                next_ch = 0;
            } else {
                next_ch = buffer[0];
            }

            i_src = 0;
        } else {
            next_ch = buffer[forward];
            ++i_src;
        }
    }

    if (curr_ch == '\n') {
        line_count++;
        col_pos_count = 1;
    } else {
        ++col_pos_count;
    }

    return curr_ch;
}
/*---------------------------------------------------------------------------*
 * *  "initialize_table"
 **
 */
void Scanner::initialize_table()
{
    // initialize the symbols stack
    ss.push(STS_EOS);
    ss.push(SNTS_S);

    // setup the lexical scanner parsing table
    table[SNTS_S][STS_ALPHA] = 1;
    table[SNTS_S][STS_DIGIT] = 1;
    table[SNTS_S][STS_PUNCT] = 1;
    table[SNTS_S][STS_SPACE] = 1;
    table[SNTS_S][STS_EOS] = 1;
    table[SNTS_S][STS_INVALID] = 1;

    table[SNTS_S1][STS_ALPHA] = 2;
    table[SNTS_S1][STS_DIGIT] = 2;
    table[SNTS_S1][STS_PUNCT] = 2;
    table[SNTS_S1][STS_SPACE] = 2;
    table[SNTS_S1][STS_EOS] = 2;
    table[SNTS_S1][STS_INVALID] = 2;

    table[SNTS_S2][STS_ALPHA] = 4;
    table[SNTS_S2][STS_DIGIT] = 4;
    table[SNTS_S2][STS_PUNCT] = 4;
    table[SNTS_S2][STS_SPACE] = 4;
    table[SNTS_S2][STS_EOS] = 4;
    table[SNTS_S2][STS_INVALID] = 4;

    table[SNTS_T][STS_ALPHA] = 9;
    table[SNTS_T][STS_DIGIT] = 6;
    table[SNTS_T][STS_PUNCT] = 8;
    table[SNTS_T][STS_SPACE] = 7;
    table[SNTS_T][STS_EOS] = 11;
    table[SNTS_T][STS_INVALID] = 28;

    table[SNTS_D1][STS_ALPHA] = 15;
    table[SNTS_D1][STS_DIGIT] = 14;
    table[SNTS_D1][STS_PUNCT] = 15;
    table[SNTS_D1][STS_SPACE] = 15;
    table[SNTS_D1][STS_EOS] = 15;
    table[SNTS_D1][STS_INVALID] = 29;

    table[SNTS_A1][STS_ALPHA] = 22;
    table[SNTS_A1][STS_DIGIT] = 24;
    table[SNTS_A1][STS_PUNCT] = 23;
    table[SNTS_A1][STS_SPACE] = 23;
    table[SNTS_A1][STS_EOS] = 23;
    table[SNTS_A1][STS_INVALID] = 31;

	table[SNTS_W1][STS_ALPHA] = 33;
	table[SNTS_W1][STS_DIGIT] = 33;
	table[SNTS_W1][STS_PUNCT] = 33;
	table[SNTS_W1][STS_SPACE] = 32;
	table[SNTS_W1][STS_EOS] = 33;
	table[SNTS_W1][STS_INVALID] = 34;

}
/********************************************************************
 *  showtoken - will output the token
 *******************************************************************/

std::string Scanner::showsymbol(ScannerSymbols symbol)
{

    std::string message;
    switch (symbol) {
    case STS_ALPHA:
        message = "a word alpha in the context";
        break;
    case STS_DIGIT:
        message = "a digit in the context";
        break;
    case STS_SPACE:
        message = " a space in the context";
        break;
    case STS_PUNCT:
        message = " a punctution in the context";
        break;
    case STS_INVALID:
        message = " an unknown character in the context";
        break;
    case SNTS_S:
        message = " SNTS_S";
        break;
    case SNTS_S1:
        message = " SNTS_S1";
        break;
    case SNTS_S2:
        message = " SNTS_S2";
        break;
    case SNTS_T:
        message = " SNTS_T";
        break;
    case SNTS_D1:
        message = " SNTS_D1";
        break;
    case SNTS_A1:
        message = " SNTS_A1";
        break;
    case STS_EOS:
        message = " STS_EOS";
        break;
    default: //do nothing
        message = " default";
        break;
    }

    return message;
}

/*---------------------------------------------------------------------------*
**  "symbolslexer" The scanner general symbol found by characters
**
**  returns:  Symbols enum of terminal symbol
*/
enum ScannerSymbols Scanner::symbolslexer()
{
    if (curr_ch >= 0x00 && curr_ch <= 0xff) {   // only char values
        if (isspace(curr_ch)) {
            return STS_SPACE;
        } else if (ispunct(curr_ch)) {
            return STS_PUNCT;
        } else if (isalpha(curr_ch)) {
            return STS_ALPHA;
        } else if (isdigit(curr_ch)) {
            return STS_DIGIT;
        } else if (curr_ch == 0) {
            return STS_EOS;
        } else {
            return STS_INVALID;
        }
    } else {
        return STS_INVALID;
    }
}

/*---------------------------------------------------------------------------*
**  "lexer" Converts a valid token to the corresponding terminal symbol
**
**  returns:  Symbols enum of terminal symbol
*/
LLTOKEN_CODE Scanner::tokenlexer(ScannerSymbols symbol)
{
    switch (symbol) {
    case SNTS_A1:
        return L_WORD_TOKEN; 
    case SNTS_D1:
        return L_NUMBER_TOKEN;
    case STS_PUNCT:
        return L_PUNCT_TOKEN;
	case SNTS_W1:
		return L_SPACE_TOKEN;
	case STS_EOS:
        return L_END_OF_FILE; // end of stack: the $ terminal symbol
    case STS_INVALID:
        return L_ERR_TOKEN; // end of stack: the $ terminal symbol
    default:
        return L_ERR_TOKEN;
    }
}

/*---------------------------------------------------------------------------*
**  "get_token" extracts the next token from the source file
**
**  returns:  nothing
*/
void Scanner::get_token()
{
    previous = code;

    while (!ss.empty()/*.size() > 0*/) {
        ScannerSymbols curr_symbol = symbolslexer();
#ifdef DEBUG
        std::cout << "Matched symbols: " << showsymbol(curr_symbol) << std::endl;
#endif
        if (curr_symbol == ss.top()) { // lexicals lexeme builder
            on_line = line_count;
            on_col = col_pos_count - (i_lex + 1);

            if (curr_symbol != STS_INVALID) { // No unknown put into lexeme
                lexeme[i_lex] = curr_ch;
            }
            advance();
            i_lex++;
            ss.pop();

        } else {
            switch (table[ss.top()][curr_symbol]) {
            case 1: // 1. LexicalSymbol Grammer
                ss.pop();
                ss.push(SNTS_S1);
                break;

            case 2: // Every tokens start start
                ss.pop();
                ss.push(SNTS_T);

                i_lex = 0;

                break;

            case 4: // Token accept state
                // S2 is where a code token it made

                ss.pop();
                ss.push(SNTS_S1);

                lexeme[i_lex] = 0;  // End of lexeme make

                // Return with the found lexeme - the string value
                // The LLTOKEN code
#ifdef DEBUG
                std::cout << "Token lexeme: " << lexeme << '\n' << std::endl;
#endif
                return;

            case 6: // Digits start
                ss.pop();
                ss.push(SNTS_S2);
                ss.push(SNTS_D1);
                ss.push(STS_DIGIT);
				code = tokenlexer(SNTS_D1);
				break;

            case 7: // Spaces
                ss.pop();
                ss.push(SNTS_S2);
				ss.push(SNTS_W1);
				ss.push(STS_SPACE);
				code = tokenlexer(SNTS_W1);
				break;

            case 8: // Punctuation tokens
                ss.pop();
				ss.push(SNTS_S2);
				ss.push(STS_PUNCT);
				code = tokenlexer(STS_PUNCT);
				break;

            case 9: // Start of a word ( starts with alpha only )
                ss.pop();
                ss.push(SNTS_S2);
                ss.push(SNTS_A1);
                // Map the SNTS_A1 to the token ID being created
                // STS S2 is where the lexeme is finally sent with
                // the token ID
                code = tokenlexer(SNTS_A1);
                ss.push(STS_ALPHA);
                break;

            case 11: // E move for EOS at start token
                ss.pop();
                break;

            case 14: // Start of a digit token - either 1 - 2 days or 4 days are found
                // as a token here (Maybe i should just find whole digits and do the
                // 2 or 4 length in theparser???)
                ss.pop();
                ss.push(SNTS_D1);
                ss.push(STS_DIGIT);
                break;

            case 15: // E move when non-digit found in digit tokenise
                // To aavoid this i could back track with a fail to try a new year token
                // (4 digit) path
                ss.pop();
                break;

            case 22: // The word token find
                ss.pop();
                ss.push(SNTS_A1);
                ss.push(STS_ALPHA);
                break;

            case 23:// E move if not a digit or alpha char for the word token
                ss.pop();
                break;

            case 24: // A digit is an alphanumeric following alpha in the word token
                ss.pop();
                ss.push(SNTS_A1);
                ss.push(STS_DIGIT);
                break;

            case 27: // Eos found
                ss.pop();
                ss.push(SNTS_S);
                break;

            case 28: // Unknown found
                ss.pop();
                ss.push(SNTS_S1);
                ss.push(STS_INVALID);
                break;

            case 29: // Unknown found
                ss.pop();
                ss.push(STS_INVALID);
                break;

            case 31: // Unknown found
                ss.pop();
                ss.push(STS_INVALID);
                break;

			case 32: // Spaces and whitespace
				ss.pop();
				ss.push(SNTS_W1);
				ss.push(STS_SPACE);
				break;

			case 33: // E move when non-digit found in digit tokenise
				ss.pop();
				break;

			case 34: // Unknown found
				ss.pop();
				ss.push(STS_INVALID);
				break;

			default: // should not get here if above is correct
                std::cout << "tokenizer parsing table defaulted: Expecting " << showsymbol(curr_symbol) <<
                          " on line " << on_line << " column " << on_col << std::endl;
                ss.pop();
                advance();
                break;
            }
        }
    }

    code = L_END_OF_FILE;

}
