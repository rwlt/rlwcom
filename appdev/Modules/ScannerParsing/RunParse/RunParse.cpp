//
//--------------------------------------------------------------------
//
// RunParse.cpp : Defines the entry point for the console application.
//
//--------------------------------------------------------------------
//
#include "../Utilities/Utilities.h"
#include "../Common/constants.h"

#include "../Scanner/scanner.h"

#include "../DataParse/ParseDate.h"
#include "../DataParse/DateItem.h"
#include "../Generate/filegenerate.h"

#include <iostream>
#include <stdexcept>
#include <sstream>

int verbose = 0;
int string_input = 0;
std::stringstream text_input;
char out_file[40];
const char *out_file_ptr = "print.out";
char *program_name;
dateitems vec_dateitems;

void print_dateitems(filegenerate *out_file);
void usage();
void do_file(const char *const name);


//
//------------------------------------------------------------------------
// Usage
//------------------------------------------------------------------------
//
int main(int argc, char *argv[])
{

    program_name = argv[0];

    while ((argc > 1) && (argv[1][0] == '-')) {

        switch (argv[1][1]) {
        case 'v':
            verbose = 1;
            break;
        case 'o':
            out_file_ptr = &argv[1][2];
            break;
        case 's':
            string_input = 1;
            text_input << &argv[1][2];
            break;
        default:
            std::cerr << "Bad option " << argv[1] << std::endl;
            usage();
        }

        ++argv;
        --argc;
    }

    if (argc == 1) {
        do_file("print.in");
    } else {
        while (argc > 1) {
            do_file(argv[1]);
            ++argv;
            --argc;
        }
    }
    return 0;
}
//
//------------------------------------------------------------------------
// Usage
//------------------------------------------------------------------------
//
void do_file(const char *const name)
{
    CUtilities utilities;
    double duration;
    std::string output_file(out_file_ptr);
    
    std::cout << 
              " Verbose " << verbose <<
              " Input text used: " << string_input << 
              " String text: " << text_input.str() << 
              " Output " << out_file_ptr << std::endl;
    std::cout << "Requested file to scan is " << name << "\n";;
    std::cout << "Requested file to generate is " << output_file << "\n";;


    // Timer of the procesrs
    utilities.start();

    try {
        // Setup parameters for ParseDate ctr
        bool in_string = false;
        std::string in_file_name(name);
        if (string_input ){
            in_string = true;
            in_file_name = text_input.str();
        }
        ParseDate parse(in_file_name, in_string);
        vec_dateitems = parse.get_dateitems();

        filegenerate file_out(output_file);
        print_dateitems(&file_out);

    } catch (std::runtime_error &err) {
        std::cout << "Error: # " << err.what() << "\n";
    }

    // Show how long it took
    duration = utilities.stop(); // end stop watch
    std::cout << "The generation of file took " << duration << " seconds.\n";
	std::string wait;
	std::getline(std::cin, wait);
}
//
//------------------------------------------------------------------------
// Usage
//------------------------------------------------------------------------
//
void usage()
{
    std::cerr << "Usage is " << program_name <<
              " [options] [file-list]" << std::endl;
    std::cerr << "Options" << std::endl;
    std::cerr << "  -v              verbose" << std::endl;
    std::cerr << "  -s<quote text>  Use string in quotes (\"\") as input text. No file-list is read." << std::endl;
    std::cerr << "  -o<name>        Set output file name" << std::endl;
    exit(8);
}
//
//------------------------------------------------------------
// Print out 
//------------------------------------------------------------
//
void print_dateitems(filegenerate *out_file)
{
    std::cout << "Print dataitems" << " " << '\n';
    out_file->write_text(std::string("Date items\n").c_str());
    dateitems::iterator cur_index;
    std::string date_text;
    for (cur_index = vec_dateitems.begin(); cur_index != vec_dateitems.end(); ++cur_index) {
        
        std::stringstream ostr;
        ostr << *cur_index;
        date_text = ostr.str();
        out_file->write_text(date_text.c_str());
        out_file->write_text("\n");
        std::cout << date_text << " " << '\n';
    }
    
}
