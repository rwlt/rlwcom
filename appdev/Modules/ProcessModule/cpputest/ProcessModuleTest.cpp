
#include "processtools/ProcessDetail.h"
#include "gtest/gtest.h"

namespace {

	TEST(ProcessAccessor, Constructor) {
		ProcessDetail clsProcess("<process id=\"strg1\" name=\"Storage Module\" default=\"yes\">"
			"  <system id=\"createpkgs\" name=\"Package Implementations\" default=\"yes\">"
			"   <config>"
			"      <command id=\"strgmod1\" name=\"StorageModule 1.0 Unit tests\" type=\"folder\">"
			"        <commandfolder folder=\"/home/rodney/app_config/process/create_command\"/>"
			"        <args>"
			"          <arg value=\"makecheck\" /> <!-- name of the shell command -->"
			"          <arg use=\"fullpath\" />  <!-- use attribute on arg tells to use the variable from -->"
			"          <arg use=\"filename\" />  <!-- working over a selectlist folder and files          -->"
			"          <arg use=\"ext\" />  <!-- ext is the filename extension to filter within the folder -->"
			"        </args>"
			"      </command>"
			"    </config>"
			"    <select>"
			"      <item id=\"crd1\" name=\"Package 1 to Package 5\">"
			"        <input folder=\"/home/rodney/app_config/input_rd1\" ext=\"dd\"/>"
			"      </item>"
			"      <item id=\"crd2\" name=\"Package 6 to Package 12\">"
			"         <input folder=\"/home/rodney/app_config/input_ rd1\" ext=\"dd\"/>"
			"      </item>"
			"    </select>"
			"  </system>"
			"  <system id=\"deploypkgs\" name=\"Package Installations\" default=\"no\">"
			"   <config>"
			"      <command id=\"strgmod2\" name=\"StorageModule 1.0 Deploy tests\" type=\"folder\">"
			"        <commandfolder folder=\"/home/rodney/app_config/process/create_command\"/>"
			"        <args>"
			"          <arg value=\"deploycheck\" />"
			"          <arg use=\"fullpath\" />  <!-- use attribute on arg tells to use the variable from -->"
			"          <arg use=\"filename\" />  <!-- working over a selectlist folder and files          -->"
			"          <arg use=\"ext\" />  <!-- ext is the filename extension to filter within the folder -->"
			"        </args>"
			"      </command>"
			"    </config>"
			"    <select>"
			"      <item id=\"drd1\" name=\"Package 1 to Package 5\">"
			"        <input folder=\"/home/rodney/app_config/install_rd1\" ext=\"dd\"/>"
			"      </item>"
			"      <item id=\"drd2\" name=\"Package 6 to Package 12\">"
			"         <input folder=\"/home/rodney/app_config/install_ rd1\" ext=\"dd\"/>"
			"      </item>"
			"    </select>"
			"  </system>"
			"</process>");
		EXPECT_EQ(clsProcess.XmlError(), "");

		EXPECT_EQ(clsProcess.GetID(), "strg1");
		EXPECT_EQ(clsProcess.GetName(), "Storage Module");

		auto active = clsProcess.GetActiveSystem();
		ASSERT_EQ(std::string("createpkgs"), active) << "Process ID not found as default system.";

		SystemProcess &system = clsProcess.GetSystemProcess("deploypkgs");
		EXPECT_EQ(system.GetID(), "deploypkgs");
		EXPECT_EQ(system.GetName(), "Package Installations");

		ListBase<SystemProcess> &list = clsProcess.GetListSystem();
		EXPECT_EQ(2, list.GetSize());
	}

	TEST(ProcessAccessor, ConstructorWithXmlError) {
		ProcessDetail clsProcessWithXmlError("<process id=\"strg1\" name=\"Storage Module\">"
			"  <system id=\"createpkgs\" name=\"Package Implementations\" default=\"yes\">"
			"   <config>"
			"      <command id=\"strgmod1\" name=\"StorageModule 1.0 Unit tests\" type=\"folder\">"
			"        <commandfolder folder=\"/home/rodney/app_config/process/create_command\"/>"
			"        <args>"
			"          <arg value=\"makecheck\" /> <!-- name of the shell command -->"
			"          <arg use=\"fullpath\" />  <!-- use attribute on arg tells to use the variable from -->"
			"          <arg use=\"filename\" />  <!-- working over a selectlist folder and files          -->"
			"          <arg use=\"ext\" />  <!-- ext is the filename extension to filter within the folder -->"
			"        </args>"
			"      </command>"
			"         item id=\"crd1\" name=\"Package 1 to Package 5\">"
			"        <input folder=\"/home/rodney/app_config/input_rd1\" ext=\"dd\"/>"
			"      </item>"
			"      <item id=\"crd2\" name=\"Package 6 to Package 12\">"
			"         <input folder=\"/home/rodney/app_config/input_ rd1\" ext=\"dd\"/>"
			"      </item>"
			"    </select>"
			"  </system>"
			"  <system id=\"deploypkgs\" name=\"Package Installations\" default=\"no\">"
			"   <config>"
			"      <command name=\"StorageModule 1.0 Deploy tests\" type=\"folder\">"
			"        <commandfolder folder=\"/home/rodney/app_config/process/create_command\"/>"
			"        <args>"
			"          <arg value=\"deploycheck\" />"
			"          <arg use=\"fullpath\" />  <!-- use attribute on arg tells to use the variable from -->"
			"          <arg use=\"filename\" />  <!-- working over a selectlist folder and files          -->"
			"          <arg use=\"ext\" />  <!-- ext is the filename extension to filter within the folder -->"
			"        </args>"
			"      </command>"
			"    </config>"
			"    <select>"
			"      <item id=\"drd1\" name=\"Package 1 to Package 5\">"
			"        <input folder=\"/home/rodney/app_config/install_rd1\" ext=\"dd\"/>"
			"      </item>"
			"      <item id=\"drd2\" name=\"Package 6 to Package 12\">"
			"         <input folder=\"/home/rodney/app_config/install_ rd1\" ext=\"dd\"/>"
			"      </item>"
			"    </select>"
			"  </system>"
			"</process>");

		EXPECT_TRUE(!clsProcessWithXmlError.XmlError().empty());

		EXPECT_EQ(clsProcessWithXmlError.GetID(), "");
		EXPECT_EQ(clsProcessWithXmlError.GetName(), "");

		auto active = clsProcessWithXmlError.GetActiveSystem();
		ASSERT_EQ(std::string(""), active);

		ListBase<SystemProcess> &list = clsProcessWithXmlError.GetListSystem();
		EXPECT_EQ(0, list.GetSize());
	}

	TEST(ProcessAccessor, ConstructorWithXmlErrorMissingAttrs) {
		ProcessDetail clsProcessWithXmlErrorMissingAttrs("<process id=\"strg1\" name=\"Storage Module\">"
			"  <system id=\"createpkgs\" name=\"Package Implementations\" default=\"yes\">"
			"   <config>"
			"      <command name=\"StorageModule 1.0 Unit tests\" type=\"folder\">"
			"        <commandfolder folder=\"/home/rodney/app_config/process/create_command\"/>"
			"        <args>"
			"          <arg value=\"makecheck\" /> <!-- name of the shell command -->"
			"          <arg use=\"fullpath\" />  <!-- use attribute on arg tells to use the variable from -->"
			"          <arg use=\"filename\" />  <!-- working over a selectlist folder and files          -->"
			"          <arg use=\"ext\" />  <!-- ext is the filename extension to filter within the folder -->"
			"        </args>"
			"      </command>"
			"    </config>"
			"    <select>"
			"      <item id=\"crd1\">"
			"        <input folder=\"/home/rodney/app_config/input_rd1\" ext=\"dd\"/>"
			"      </item>"
			"      <item id=\"crd2\" name=\"Package 6 to Package 12\">"
			"         <input folder=\"/home/rodney/app_config/input_ rd1\" ext=\"dd\"/>"
			"      </item>"
			"    </select>"
			"  </system>"
			"  <system id=\"deploypkgs\" name=\"Package Installations\" default=\"no\">"
			"   <config>"
			"      <command id=\"strgmod2\" name=\"StorageModule 1.0 Deploy tests\">"
			"        <commandfolder folder=\"/home/rodney/app_config/process/create_command\"/>"
			"        <args>"
			"          <arg value=\"deploycheck\" />"
			"          <arg use=\"fullpath\" />  <!-- use attribute on arg tells to use the variable from -->"
			"          <arg use=\"filename\" />  <!-- working over a selectlist folder and files          -->"
			"          <arg use=\"ext\" />  <!-- ext is the filename extension to filter within the folder -->"
			"        </args>"
			"      </command>"
			"    </config>"
			"    <select>"
			"      <item id=\"drd1\" name=\"Package 1 to Package 5\">"
			"        <input folder=\"/home/rodney/app_config/install_rd1\" ext=\"dd\"/>"
			"      </item>"
			"      <item id=\"drd2\" name=\"Package 6 to Package 12\">"
			"         <input folder=\"/home/rodney/app_config/install_ rd1\" ext=\"dd\"/>"
			"      </item>"
			"    </select>"
			"  </system>"
			"</process>", "strg1");
		EXPECT_EQ(clsProcessWithXmlErrorMissingAttrs.XmlError(), "Attribute id attribute empty\nAttribute type attribute empty\n");

		EXPECT_EQ(clsProcessWithXmlErrorMissingAttrs.GetID(), "strg1");
		EXPECT_EQ(clsProcessWithXmlErrorMissingAttrs.GetName(), "Storage Module");

		auto active = clsProcessWithXmlErrorMissingAttrs.GetActiveSystem();
		ASSERT_EQ(std::string("createpkgs"), active);
		SystemProcess &system = clsProcessWithXmlErrorMissingAttrs.GetSystemProcess("createpkgs");
		ListBase<CommandCfg> cmds = system.GetCommandCfgList();
		EXPECT_EQ(0, cmds.GetSize()); // The xml errors had a missing id attr

		ListBase<SystemProcess> &list = clsProcessWithXmlErrorMissingAttrs.GetListSystem();
		EXPECT_EQ(2, list.GetSize());
	}

	TEST(ProcessAccessor, StartJobAccessorProcessOneSystem) {
		ProcessDetail clsProcess("<process id=\"strg1\" name=\"Storage Module\">"
			"  <system id=\"createpkgs\" name=\"Package Implementations\" default=\"yes\">"
			"   <config>"
			"      <command id=\"strgmod1\" name=\"StorageModule 1.0 Unit tests\" type=\"folder\">"
			"        <commandfolder folder=\"/home/rodney/app_config/process/create_command\"/>"
			"        <args>"
			"          <arg value=\"makecheck\" /> <!-- name of the shell command -->"
			"          <arg use=\"fullpath\" />  <!-- use attribute on arg tells to use the variable from -->"
			"          <arg use=\"filename\" />  <!-- working over a selectlist folder and files          -->"
			"          <arg use=\"ext\" />  <!-- ext is the filename extension to filter within the folder -->"
			"        </args>"
			"      </command>"
			"    </config>"
			"    <select>"
			"      <item id=\"crd1\" name=\"Package 1 to Package 5\">"
			"        <input folder=\"/home/rodney/app_config/input_rd1\" ext=\"dd\"/>"
			"      </item>"
			"      <item id=\"crd2\" name=\"Package 6 to Package 12\">"
			"         <input folder=\"/home/rodney/app_config/input_ rd1\" ext=\"dd\"/>"
			"      </item>"
			"    </select>"
			"  </system>"
			"  <system id=\"deploypkgs\" name=\"Package Installations\" default=\"no\">"
			"   <config>"
			"      <command id=\"strgmod2\" name=\"StorageModule 1.0 Deploy tests\" type=\"folder\">"
			"        <commandfolder folder=\"/home/rodney/app_config/process/create_command\"/>"
			"        <args>"
			"          <arg value=\"deploycheck\" />"
			"          <arg use=\"fullpath\" />  <!-- use attribute on arg tells to use the variable from -->"
			"          <arg use=\"filename\" />  <!-- working over a selectlist folder and files          -->"
			"          <arg use=\"ext\" />  <!-- ext is the filename extension to filter within the folder -->"
			"        </args>"
			"      </command>"
			"    </config>"
			"    <select>"
			"      <item id=\"drd1\" name=\"Package 1 to Package 5\">"
			"        <input folder=\"/home/rodney/app_config/install_rd1\" ext=\"dd\"/>"
			"      </item>"
			"      <item id=\"drd2\" name=\"Package 6 to Package 12\">"
			"         <input folder=\"/home/rodney/app_config/install_ rd1\" ext=\"dd\"/>"
			"      </item>"
			"    </select>"
			"  </system>"
			"</process>", "strg1");

		ListBase<SystemProcess> &list = clsProcess.GetListSystem();
		EXPECT_EQ(2, list.GetSize());
		

		clsProcess.SetActiveSystem("deploypkgs");
		clsProcess.SetCurrentPage("1");
		clsProcess.SetCategoryList(); // Do this to prepare the selected items to process

		EXPECT_EQ("deploypkgs", clsProcess.GetActiveSystem());
		EXPECT_EQ(1, clsProcess.GetCurrentPage());

		SystemProcess &system = clsProcess.GetSystemProcess("deploypkgs");
		EXPECT_EQ(system.GetID(), "deploypkgs");
		EXPECT_EQ(system.GetName(), "Package Installations");

		system.SetupCommand();
		ListBase<CommandCfg> cmds = system.GetCommandCfgList();
		EXPECT_EQ(1, cmds.GetSize()); 

		ListBase<SelectItem> select_list = system.GetSelectItemList();
		EXPECT_EQ(2, select_list.GetSize());

		// This does set the active system using "deploypkgs"
		// and SetCategoryList for item selections
		clsProcess.StartJob("deploypkgs", "drd1"); // Correct values used here 
		
	}

	//TEST(ProcessAccessor, CategoryListAccessor)
	//{
	//
	//    fixture.clsProcess.SetActiveSystem("playclipscc");
	//    STRCMP_EQUAL(std::string("playclipscc").c_str(), fixture.clsProcess.GetActiveSystem().c_str());
	//
	//    fixture.clsProcess.SetCategoryList();
	//    // Will not clear active system when trying to find categories for active system.
	//    STRCMP_EQUAL(std::string("playclipscc").c_str(), fixture.clsProcess.GetActiveSystem().c_str());
	//
	//    fixture.clsProcess.SetActiveSystem("playclips");
	//    STRCMP_EQUAL(std::string("playclips").c_str(), fixture.clsProcess.GetActiveSystem().c_str());
	//
	//    fixture.clsProcess.SetCategoryList();
	//}

}

GTEST_API_ int main(int argc, char **argv) {
	printf("Running main() from gtest_main.cc\n");
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}



//    try {
//        fixture.clsProcess.SetActiveSystem("createpkgs");
//        STRCMP_EQUAL(std::string("createpkgs").c_str(), fixture.clsProcess.GetActiveSystem().c_str());
//        fixture.clsProcess.SetCurrentPage("1");  //??? Not sure what current page is for
//        LONGS_EQUAL(1, fixture.clsProcess.GetCurrentPage());
//        
//        fixture.clsProcess.SetCategoryList();//This reads a new file for the system on the select list for categories
//        SystemProcess &system1 = fixture.clsProcess.GetSystemProcess("createpkgs");
//        LONGS_EQUAL(2, system1.GetSelectItemList().GetSize());
//        SystemProcess &system2 = fixture.clsProcess.GetSystemProcess("deploypkgs");
//        LONGS_EQUAL(2, system1.GetSelectItemList().GetSize());
//        LONGS_EQUAL(0, system2.GetSelectItemList().GetSize()); // As need to set active system first
//        fixture.clsProcess.SetActiveSystem("deploypkgs");
//        fixture.clsProcess.SetCategoryList();//This reads a new file for the system on the select list for categories
//        LONGS_EQUAL(2, system2.GetSelectItemList().GetSize()); // As need to set active system first
//        
//        system1.SetActiveSelect("crd1"); // the systems categories are read in the clsProcess config when systems were read
//        STRCMP_EQUAL(std::string("crd1").c_str(), system1.GetActiveSelect().c_str());
//        SelectItem &category = system1.GetSelectItem("crd1");
//        STRCMP_EQUAL(std::string("crd1").c_str(), category.GetID().c_str());
//        
////        STRCMP_EQUAL(std::string("/home/rodney/app_config/createpkgs.xml").c_str(), system1.GetConfigFile().getFullPath().c_str());
//        system1.SetupCommand();
//        LONGS_EQUAL(1, system1.GetCommandCfgList().GetSize()); // As need to set active system first
//        CommandCfg &cmdcfg = system1.GetCommandItem("strgmod1");
//        STRCMP_EQUAL(std::string("strgmod1").c_str(), cmdcfg.GetID().c_str());
//        STRCMP_EQUAL(std::string("StorageModule 1.0 Unit tests").c_str(), cmdcfg.GetName().c_str());
//        
//    } catch (std::exception &e) {
//        LONGS_EQUAL(1,0);
//    }

//}

