
#include "FileIn.h"
#include <iostream>
#include <fstream>

namespace Resource
{
//
// Constructor
//
FileIn::FileIn(): filename("")
{
}
//
// Destructor
//
FileIn::~FileIn()
{
    data_file.close();
}
//
// Public method
//
void FileIn::openFile(const std::string &name)
{
    if (data_file.is_open()) {
        data_file.close();
    }
    filename = name;
    data_file.open(name);
    if (!data_file.good()) {
        std::cerr << "Error: not good with opening file " << filename << std::endl;
    }
}
//
// Public method
//
std::string FileIn::readLine()
{
    std::string line;
    if (!data_file.is_open()) {
        return ("");
    }
    std::getline(data_file, line);
    if (!data_file.eof() && !data_file.good()) {
        std::cerr << "Error: could not read line on file " << filename << std::endl;
    }
    return (line);
}
//
// Public method
//
bool FileIn::eof()
{
    return (!data_file.good() || data_file.eof());
}


} /* namespace resource */
