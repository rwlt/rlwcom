
#include "SystemTask.h"
#include <cstdio>
#include <sstream>
#include <fstream>
#include "../fileutil/fileutil.h"


/// Constructor
SystemTask::SystemTask(TaskNotify *task_nofity) :
    m_task_notify(task_nofity),
    m_worker_thread(),
    m_Mutex(),
    m_shall_stop(false),
    m_has_stopped(true),
    m_fraction_done(0.0),
    m_message()
{}

/// Deconstructor
SystemTask::~SystemTask()
{
    std::lock_guard<std::mutex> lock(m_Mutex);
    if (m_has_stopped == false) {
        m_worker_thread.detach();
    } else  {
        if (m_worker_thread.joinable()) {
            m_worker_thread.join();
        }
    }
}

//
//---------------------------------------------------------
// Public method
//---------------------------------------------------------
//
void SystemTask::DoTask(const SystemProcess &system_process, const SelectItem &select_item)
{
    std::lock_guard<std::mutex> lock(m_Mutex);
    if (m_has_stopped == true) {
        m_worker_thread = std::thread(&SystemTask::do_work, this, system_process, select_item);
        m_has_stopped = false;
        m_shall_stop = false;
    }

}

//
//---------------------------------------------------------
// Public method
//---------------------------------------------------------
//
// End the task - terminates any running process and finishes the task
void SystemTask::EndTask()
{
    std::lock_guard<std::mutex> lock(m_Mutex);
    m_shall_stop = true;
    // Update JobTransactions tsble in database with the temporay job file information
//    _tzset();
//    _time64(&m_lstoptime);*/
}

void SystemTask::JoinDetach()
{
    if (m_worker_thread.joinable()) {
        m_worker_thread.join();
    }
}

bool SystemTask::HasStopped() const
{
    std::lock_guard<std::mutex> lock(m_Mutex);
    return m_has_stopped;
}
// Accesses to these data are synchronized by a mutex.
// Some microseconds can be saved by getting all data at once, instead of having
// separate get_fraction_done() and get_message() methods.
void SystemTask::GetData(double *fraction_done, std::string *message) const
{
    std::lock_guard<std::mutex> lock(m_Mutex);

    if (fraction_done) {
        *fraction_done = m_fraction_done;
    }

    if (message) {
        *message = m_message;
    }
}

//
// Private function
//

void SystemTask::do_work(SystemProcess system_process, SelectItem select_item)
{
    {
        std::lock_guard<std::mutex> lock(m_Mutex);
        m_has_stopped = false;
        m_fraction_done = 0.0;
        m_message = "";
    } // The mutex is unlocked here by lock's destructor.

    std::cout << "Do_Work "  << system_process.GetID() << " " << select_item.GetName() << std::endl;
    std::list<FileDetail>::iterator selectfiles_Iter;
    std::list<FileDetail> selectfiles;
    std::stringstream templatetask;
    std::stringstream sfpath;
    std::stringstream select_list_filename;

    templatetask << '~' << system_process.GetID() << '/' << select_item.GetID();
    sfpath << '~' << system_process.GetID();
    //mkdir(sfpath);

    // Read the file list for this tasks commands
    select_list_filename << templatetask.str() << "_filelist.asc";

    FileDetail fdetail("abc");
    selectfiles.push_front(fdetail);
    FileDetail fdetail1("def");
    selectfiles.push_front(fdetail1);
    FileDetail fdetail2("hij");
    selectfiles.push_front(fdetail2);
    FileDetail fdetail3("klm");
    selectfiles.push_front(fdetail3);
    FileDetail fdetail4("nop");
    selectfiles.push_front(fdetail4);
    FileDetail workfolder;

    // For each commandcfg in system member lstComandCfg
    // create the call to the command and its args
    // Use selectitem (category) to get the file list to process using the command
    // Makes a batch file with each line a call to the command
    ListBase<CommandCfg> cmdcfgList = system_process.GetCommandCfgList();
    std::cout << "cmd cfg count "  << cmdcfgList.GetSize() << std::endl;
    if (cmdcfgList.GetSize() > 0) {
        for (const CommandCfg & cmdcfg : cmdcfgList) {
            std::cout << "cmd cfg "  << cmdcfg.GetName() << std::endl;
            workfolder = cmdcfg.GetWorkFolder();
            std::stringstream batchfilename;
            batchfilename << templatetask.str() << '_' << cmdcfg.GetID() << "_sh";
            workfolder.getFullPath();
            try {

                for (selectfiles_Iter = selectfiles.begin();
                        selectfiles_Iter != selectfiles.end();
                        ++selectfiles_Iter) {
                    FileDetail filedetail = *selectfiles_Iter;
                    filedetail.getFullPath();

                    // The order the args is important as it is read from the config file for the
                    // command.
                    for (int argcount = 0; argcount < cmdcfg.ArgCount();
                            argcount++) {
                        const CMDARG &cmdarg = cmdcfg.GetCommandArg(argcount);
                        switch (cmdarg.type) {
                        case Fullpath:
                            break;
                        case Filename:
                            break;
                        case Ext:
                            break;
                        default:
                            break;
                        }
                    }
                }


                // Log the access token info
                // Run the new batch file in a window shell
                select_item.GetFolder();

                // Start the child process.
                //if (!CreateProcess() {            }
            } catch (SystemFilesException &e) {
            }

            // Lock and manage thread variables
            {
                std::lock_guard<std::mutex> lock(m_Mutex);
                m_fraction_done += 0.5;

                // if (i % 4 == 3) {
                std::ostringstream ostr;
                ostr << (m_fraction_done * 100.0) << "% done\n";
                m_message += ostr.str();
                // }

                if (m_fraction_done >= 1.0) {
                    m_message += "Finished";
                    break;
                }

                if (m_shall_stop) {
                    m_message += "Stopped";
                    break;
                }
            }
            m_task_notify->task_notify();
        }

    }
    // Lock and manage thread variables
    {
        std::lock_guard<std::mutex> lock(m_Mutex);
		m_fraction_done = 1.0;
		m_message += "Finished";
		m_shall_stop = false;
        m_has_stopped = true;
    }
    m_task_notify->task_notify();

}

