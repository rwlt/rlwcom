#ifndef D_SYSTEMTASK_H
#define D_SYSTEMTASK_H


#include "../fileutil/fileutil.h"
#include "../processtools/SystemProcess.h"
//#include "time.h"
#include <thread>
#include <mutex>

/// TaskNotify interface class
/* System Task is constructed with an instance of this interface to notify the caller
 */
class TaskNotify
{
public:
    /// Notify from a worker thread 
    virtual void task_notify(){};
};

/// TaskBase
class TaskBase
{
public:
    /// Constructor
    TaskBase() {
        //tzset();
        //_time64 ( &m_lstarttime );
    }
    /// Deconstructor
    virtual ~TaskBase() {
    }
    /// DoTask
    /**
     * \param system SystemProcess
     * \param select_item SelectItem
     */
    virtual void DoTask ( const SystemProcess &system_process, const SelectItem &select_item ) = 0;
    /// EndTask
    virtual void EndTask() = 0;
    /// has work stopped
    virtual bool HasStopped() const = 0;
    /// join completed work (or detach)
    virtual void JoinDetach() = 0;
public:
    // Start time of task
    //__time64_t m_lstarttime, m_lstoptime;
};

/// SystemTask
class SystemTask: public TaskBase
{
    TaskNotify *m_task_notify;
//    SystemProcess *m_system_process;
//    SelectItem *m_select_item;

    std::thread m_worker_thread;
    mutable std::mutex m_Mutex; // Synchronizes access to member data.
    
    // Data used by both TaskNofify instance(GUI) thread and this classes worker thread.
    bool m_shall_stop;
    bool m_has_stopped;

    double m_fraction_done;
    std::string m_message;
    
public:
    /// Constructor
    /**
     * \param task_notify interface with callback
     */
    SystemTask(TaskNotify* task_notify);
    /// Deconstructor
    ~SystemTask();
    /// DoTask
    /**
     * \param system SystemProcess
     * \param select_item SelectItem
     */
    void DoTask (const SystemProcess &system_process, const SelectItem &select_item );
    /// EndTask
    void EndTask();
    /// has work stopped
    bool HasStopped() const;
    /// join completed work (or detach)
    void JoinDetach();
    //void LogLastError();
    /// get data thread safe
    /**
     * \param fraction_done double
     * \param message string
     */
    void GetData ( double *fraction_done, std::string *message ) const;
    
private:
    // Hide the assignment and copy constructors (No value copying allowed)
    SystemTask ( const SystemTask& oldTask );
    SystemTask operator = ( const SystemTask& oldTask );

    // Thread function.
    void do_work (SystemProcess system_process, SelectItem select_item);
    
    /// template to read the size of data from stream
    template<typename T>
    std::istream &binary_read ( std::istream &stream, T &value ) {
        return stream.read ( reinterpret_cast<char *> ( &value ), sizeof ( T ) );
    };
    
};

#endif /* D_SYSTEMTASK_H */
