/*
 * FileIn.h
 */

#ifndef D_FILEIN_H
#define D_FILEIN_H

#include <fstream>
#include <string>

namespace Resource
{

class FileIn
{
public:
    FileIn();
    virtual ~FileIn();

    void openFile ( const std::string &name );
    std::string readLine();
    bool eof();

private:
    std::string filename;
    std::ifstream data_file;
};

} /* namespace resource */

#endif /* DFILEIN_H */
