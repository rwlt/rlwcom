#ifndef FILEUTIL_H
#define FILEUTIL_H

#include <cstdlib>
#include <iostream>
#include <list>
#include <string>
#include <cstring>

#define   OUT_BUFF_SIZE 512
#define   READ_HANDLE 0
#define   WRITE_HANDLE 1
#define   BEEP_CHAR 7
//#define LOGGER_4
//#define _MAX_PATH 255
//#define _MAX_DIR 155
//#define _MAX_DRIVE 25
//#define _MAX_EXT 25


//
//----------------------------------------------------------------
// Class: SystemFilesException
//----------------------------------------------------------------
// This exception is throwed by the on any error condition. The text
// contains the description of the problem.
//
class SystemFilesException
{
    std::string text;

public:

    SystemFilesException ( const char *cstr ) :text ( cstr ) {}
    std::string getText() const {
        return text;
    }

};
//
//----------------------------------------------------------------
// Class: FileDetail
//----------------------------------------------------------------
//
class FileDetail
{
    std::string fullpath;
    std::string filename;
    std::string drive;
    std::string dir;

public:

    FileDetail():fullpath ( "" ), filename ( "" ), drive ( "" ), dir ( "" ) {
    }

    FileDetail ( const std::string &fpath ):fullpath(fpath), filename(""), drive(""), dir("") {
    }

    ~FileDetail() {
    }

public:

    std::string getFullPath() {
        return fullpath;
    }

    inline void setFullPath ( const std::string &sfpath ) {
        fullpath = sfpath;
    }

    friend std::ostream& operator << ( std::ostream& outFile, const FileDetail& fileDetail );

};

inline std::ostream& operator << ( std::ostream& outFile, const FileDetail& fileDetail )
{
    outFile << fileDetail.fullpath;
    return ( outFile );
}

//
//-------------------------------------------------------------
// Class: SystemFiles
//-------------------------------------------------------------
//
class SystemFiles
{
//    SystemFiles(const SystemFiles &copy);
//    SystemFiles& operator = (const SystemFiles &copy);

    std::list<FileDetail>::iterator processFileList_Iter;
    std::list<FileDetail> processFileList;
    std::list<std::string>::iterator fileSpec_Iter;
    std::list<std::string> fileSpec;
    std::string szExt;

public:

    SystemFiles();
    ~SystemFiles();
    int fileSpecification ( const std::string &szExt );
    int readDirectory ( const std::string &szDrive, const std::string &szDirectory, int iOption );
    int processFiles ( long ( *function ) ( FileDetail* fdetail ) );
};

#endif /* FILEUTIL_H */
