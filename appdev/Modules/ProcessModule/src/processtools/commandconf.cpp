#include "commandconf.h"

#include <cstring>
#include <iostream>

//
//-----------------------------------------------------------
// Constructor
//-----------------------------------------------------------
//
CommandCfg::CommandCfg() :
    cmdArgSize(0)
{
    CMDARG cmdarg;
    cmdarg.value = "";
    cmdarg.type = Value;
    for (int index = 0; index < 10; ++index) {
        m_argv[index] = cmdarg;
    }
}
//
//-----------------------------------------------------------
// Destructor
//-----------------------------------------------------------
//
CommandCfg::~CommandCfg()
{
}
//
//-----------------------------------------------------------
// Public method
//-----------------------------------------------------------
//
/* Add an argument to the command
   Returns the number of arguments for the command after this argument is added */
int CommandCfg::AddCommandArg(const std::string &szarg, ArgType argtype)
{
    CMDARG cmdarg;
    cmdarg.value = szarg;
    cmdarg.type = argtype;
    m_argv[cmdArgSize] = cmdarg;
    ++cmdArgSize;
    return (cmdArgSize - 1);
}
//
//-----------------------------------------------------------
// Public method
//-----------------------------------------------------------
//
// The number of arguments in m_argv
int CommandCfg::ArgCount() const
{
    return (cmdArgSize - 1);
}
//
//-----------------------------------------------------------
// Public method
//-----------------------------------------------------------
//
const CMDARG &CommandCfg::GetCommandArg(int argitem) const
{
    return m_argv[argitem];
}
//
//-----------------------------------------------------------
// Public method
//-----------------------------------------------------------
//

void CommandCfg::writeCommandItem() const
{
    std::cout <<  "      >>>>>>>>"
              << std::endl << "      ID:          " << GetID()
              << std::endl << "      Name:        " << GetName()
              << std::endl << "      Work Folder: " << GetWorkFolder()
              << std::endl;
    //CmdArgs
    for (int index = 0; index <= ArgCount(); ++index) {
        const CMDARG &arg = GetCommandArg(index);
        std::cout
                << "      CMDARG    Value:    " << arg.value << std::endl;
    }

}
