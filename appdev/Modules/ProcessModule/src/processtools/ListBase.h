#ifndef D_LISTBASE_H
#define D_LISTBASE_H


#include <list>
#include <algorithm>
#include <type_traits>
#include "IDNamePair.h"
#include <iostream>
#include <stdexcept>

template <typename T>
class ListBase
{
    static_assert(std::is_base_of<IDNamePair, T>::value, "T must derive from IDNamePair");
	std::list<T> m_lst;

public:
	ListBase() :m_lst{} {
    }
    virtual ~ListBase () {
    }
    //
    //-------------------------------------------------------------------------
    // Define the iterator typedefs
    //-------------------------------------------------------------------------
    //
    typedef typename std::list<T>::iterator iterator;
    typedef typename std::list<T>::const_iterator const_iterator;
    //
    //-------------------------------------------------------------------------
    // Define the iterator
    //-------------------------------------------------------------------------
    //
    virtual iterator begin() {
        return m_lst.begin();
    }
    virtual iterator end() {
        return m_lst.end();
    }
    virtual const_iterator begin() const {
        return m_lst.begin();
    }
    virtual const_iterator end() const {
        return m_lst.end();
    }

public:
    T &GetItem ( const std::string &findName ) {
        std::string name = findName;
        iterator iter;
        iter = std::find_if ( m_lst.begin(), m_lst.end(), [name] ( const T& t ) -> bool { 
            return t.GetID() == name;
            
        } );
        if ( iter != m_lst.end() ) {
            return *iter;
        }
        std::string message = findName + " not found";
        throw std::out_of_range(message);
    }

    void AddItem ( T item ) {
        iterator iter;
        static_assert ( std::is_base_of<IDNamePair, T>::value, "T must derive from IDNamePair" );
        IDNamePair* namePair = dynamic_cast<IDNamePair*> ( &item );
        iter = std::find_if ( m_lst.begin(), m_lst.end(), [namePair] ( const T& t ) -> bool { return t.GetID() == namePair->GetID();} );
        if ( iter != m_lst.end() ) {
            return;
        }
        m_lst.push_back ( item );
    }

    int GetSize() const {
        return ( int ) m_lst.size( );
    }

    void Clear() {
        m_lst.clear();
    }
private:
    //ListBase ( const ListBase &oldListBase );
    //ListBase operator = ( const ListBase &oldListBase );

};

#endif /* D_LISTBASE_H */
