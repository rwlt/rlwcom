#ifndef D_COMMANDCONF_H
#define D_COMMANDCONF_H



#include "../fileutil/fileutil.h"
#include "IDNamePair.h"

/// ArgType
enum ArgType {
    Value,     ///< Value
    Fullpath,  ///< FullPath
    Filename,  ///< Filename
    Ext        ///< Ext
};

/// CMDARG
struct CMDARG {
    ArgType type;       ///< type
    std::string value; ///< value
};

/// CommandCfg
class CommandCfg: public IDNamePair
{
public:
    /// Constructor
    CommandCfg();
    /// Deconstructor
    virtual ~CommandCfg();

    /// Add an argument to the command
    int AddCommandArg ( const std::string &szarg, ArgType argtype );
    /// The number of arguments in m_argv
    int ArgCount() const;
    /// Get Command Arg
    const CMDARG &GetCommandArg ( int argitem ) const;

    /// Print command details
    void writeCommandItem() const;

    /// Set work folder
    int SetWorkFolder ( const std::string &fpath ) {
        m_commandworkfolder.setFullPath ( fpath );
        return 0;
    }

    /// Get work folder
    FileDetail GetWorkFolder() const {
        return m_commandworkfolder;
    }

private:
    // The arguments for the command to use
    FileDetail m_commandworkfolder;
    int cmdArgSize;
    CMDARG m_argv[10];

};

#endif /* D_COMMANDCONF_H */
