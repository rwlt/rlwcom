#ifndef D_SELECTITEM_H
#define D_SELECTITEM_H

#include "../fileutil/fileutil.h"
#include "IDNamePair.h"
#include <string>

/// SelectItem
class SelectItem: public IDNamePair
{ 
    std::string m_folder;
    std::string m_ext;
    FileDetail m_pathdetail;

public:
    /// Constructor
    SelectItem();
    /// Deconstructor
    virtual ~SelectItem();

    /// Print selectitem details
    void writeSelectItem() const;

    /// Set Folder
    void SetFolder ( const std::string &dpath ) {
        m_pathdetail.setFullPath ( dpath );
        m_folder = dpath;
    }
    /// Get Folder
    const std::string GetFolder() const {
        return m_folder;
    }
    /// Set Ext
    void SetExt ( const std::string &fext ) {
        m_ext = fext;
    }
    /// Get Ext
    const std::string GetExt () const {
        return m_ext;
    }

};

#endif /* D_SELECTITEM_H */