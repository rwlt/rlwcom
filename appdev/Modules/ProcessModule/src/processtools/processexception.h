#ifndef D_PROCESSEXCEPTION_H
#define D_PROCESSEXCEPTION_H


#include <string>
#include <stdexcept>

/// ProcessException
/**
 * This exception is throwed by the on any error condition. The text
 * contains the description of the problem.
 */
class ProcessException
{
    std::string reasonstring;
    std::string descriptionstring;
public:
    /// Constructor
    /**
     * \param reason string
     * \param desc string
     */
    ProcessException ( const char *reason, const char *desc )
        : reasonstring ( reason ), descriptionstring ( desc )
    {};
    /// Reason
    std::string getReason() const {
        return reasonstring;
    };
    /// Reason
    std::string getDescription() const {
        return descriptionstring;
    }
};

#endif /* D_PROCESSEXCEPTION_H */
