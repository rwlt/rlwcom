#include "SelectItem.h"
#include <iostream>


SelectItem::SelectItem(): m_folder(""), m_ext("")
{
}

SelectItem::~SelectItem()
{
}

void SelectItem::writeSelectItem() const
{
    std::cout <<  "      >>>>>>>>"
              << std::endl << "      ID:          " << GetID()
              << std::endl << "      Name:        " << GetName()
              << std::endl << "      Folder:      " << GetFolder()
              << std::endl << "      Extension:   " << GetExt()
              << std::endl;
}
