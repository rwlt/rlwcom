#include "ProcessDetail.h"

#include <list>
#include <string>

#include "../fileutil/fileutil.h"
#include "../work/SystemTask.h"
#include "processexception.h"
#include <stdexcept>
#include "pugixml.hpp"
//
//-------------------------------------------------------------
// Constructor
//-------------------------------------------------------------
//
ProcessDetail::ProcessDetail(const char *xml, const std::string &id) : m_xml(xml), m_activeSystem(""), m_currentPage(1)
{
	m_lstSystem.Clear();

	using namespace pugi;
	using namespace std;
	xml_document doc;
	xml_parse_result result = doc.load_string(m_xml);
	std::string upperID = toUppercase(id.c_str());
	if (result.status != 0)
	{
		m_xmlerror << "Error description: " << result.description() << "\n";
		m_xmlerror << "Error offset: " << result.offset << " (error at [..." << (m_xml + result.offset) << "]\n\n";
	} else {
		xml_node process = doc.child("process");
		map_attrs process_attrs{ { m_a_id,{ "", true } },{ m_a_name,{ "", true } }, { m_a_default,{ "", false } } };
		if (readValues(process_attrs, process)) {
			if ( (!upperID.empty() && toUppercase(std::get<0>(process_attrs[m_a_id]).c_str()).compare(upperID) == 0) ||
				 (upperID.empty() && toUppercase(std::get<0>(process_attrs[m_a_default]).c_str()).compare("YES") == 0 ) ) {

				SetID(std::get<0>(process_attrs[m_a_id]));
				SetName(std::get<0>(process_attrs[m_a_name]));
				for (xml_node system : process.children("system"))
				{
					map_attrs system_attrs{ { m_a_id,{ "", true } }, { m_a_icon_src,{ "", false } }, { m_a_name,{ "", true } }, { m_a_default,{ "", true } } };
					if (!readValues(system_attrs, system))
						continue;
					SystemProcess systemProcess = system_details(std::get<0>(system_attrs[m_a_id]), std::get<0>(system_attrs[m_a_name]), system);
					if (toUppercase(std::get<0>(system_attrs[m_a_default]).c_str()).compare("YES") == 0) {
						SetActiveSystem(std::get<0>(system_attrs[m_a_id]));
					}
					AddListItem(systemProcess);
				}
			}
		}
	}
}
//
//-------------------------------------------------------------
// Destructor
//-------------------------------------------------------------
//
ProcessDetail::~ProcessDetail()
{
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
std::string ProcessDetail::XmlError() {
	return m_xmlerror.str();
}

//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
SystemProcess &ProcessDetail::GetSystemProcess(const std::string &name)
{
    return m_lstSystem.GetItem(name);
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
ListBase<SystemProcess> &ProcessDetail::GetListSystem()
{
    return (m_lstSystem);
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
void ProcessDetail::SetCategoryList()
{
    if (m_lstSystem.GetSize() <= 0) {
        return;
    }
    try {
        SystemProcess &systemProcess = m_lstSystem.GetItem(m_activeSystem);
		using namespace pugi;
		using namespace std;
		m_xmlerror.str("");
		xml_document doc;
		xml_parse_result result = doc.load_string(m_xml);
		if (result.status != 0)
		{
			m_xmlerror << "Error description: " << result.description() << "\n";
			m_xmlerror << "Error offset: " << result.offset << " (error at [..." << (m_xml + result.offset) << "]\n\n";
		}
		else {
			xml_node process = doc.child("process");
			for (xml_node system : process.children("system"))
			{
				map_attrs system_attrs{ { m_a_id,{ "", true } },{ m_a_icon_src,{ "", false } },{ m_a_name,{ "", true } },{ m_a_default,{ "", true } } };
				if (!readValues(system_attrs, system))
					continue;
				auto c1 = std::get<0>(system_attrs[m_a_id]);
				auto c2 = systemProcess.GetID();
				if (c1.compare(c2) == 0) {
					systemProcess.ClearSelectList();
					select_item_detail(systemProcess, system);
					break;
				}
			}
		}
    } catch (const std::exception &e) {
        std::cout << "Cound not read " << m_activeSystem << std::endl;
        return;
    }

}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
void ProcessDetail::SetActiveSystem(const std::string &system)
{
    m_activeSystem = system;
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
std::string ProcessDetail::GetActiveSystem()
{
    return m_activeSystem;
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
void ProcessDetail::SetCurrentPage(const std::string &page)
{
    int intPage = atoi(page.c_str());
    m_currentPage = intPage;
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
int ProcessDetail::GetCurrentPage()
{
    return (m_currentPage);
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
void ProcessDetail::AddListItem(SystemProcess item)
{
    return m_lstSystem.AddItem(item);
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
void ProcessDetail::StartJob(const std::string &szSystem,
                             const std::string &szCategory)
{
    // Read systems from process file data
    ListBase<SystemProcess> &list = GetListSystem();
    if (list.GetSize() <= 0) { // Only if systems have been found from process config file
        return;
    }
    SetActiveSystem(szSystem);
    try {

        SystemProcess &system = GetSystemProcess(m_activeSystem);
        SetCategoryList();
        SetCurrentPage("1");
        //const char* pstrPage = "3";
        system.SetActiveSelect(szCategory);
        SelectItem &category = system.GetSelectItem(szCategory);
        
        std::cout
                << "<<<<<<<<<<<<<<< System Process >>>>>>>>>>>>" << std::endl;
        std::cout << "Process:         " << GetName() << " " << std::endl
                  << "Active system:   " << m_activeSystem << " " << std::endl
                  << "Active category: " << system.GetActiveSelect() << " " << std::endl
                  << "Active page:     " << m_currentPage << std::endl;

        system.SetupCommand();

		TaskNotify notifier;
        SystemTask task(&notifier);
        task.DoTask(system, category);
		std::string message{ "" };
		double fraction_done = 0.0;
		while (fraction_done < 1.0) {
			std::this_thread::sleep_for(std::chrono::milliseconds(50));
			task.GetData(&fraction_done, &message);
		}
        std::cout << "Finished OK" << std::endl;
        
    } catch (const std::exception &e) {
        for (auto system : list) {
            system.writeSystem();
        }
        std::cout << "Problem with job selection: " << e.what() << std::endl;
        std::cout << "See the list available above for names available." << std::endl;
    }

}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
void ProcessDetail::CancelJob(const std::string &szSystem,
                              const std::string &szCategory)
{
}

//
//-------------------------------------------------------------
// Private method
//-------------------------------------------------------------
//
bool ProcessDetail::readValues(map_attrs &attrs, const pugi::xml_node &node) {
	for (auto fld : attrs) {
		std::get<0>(attrs[fld.first]) = node.attribute(fld.first.c_str()).value();
	}
	std::string error{ "" };
	if (find_attributes(attrs, error) == false) {
		m_xmlerror << error;
		return false;
	}
	return true;
}

//
//-------------------------------------------------------------
// Private method
//-------------------------------------------------------------
//
SystemProcess ProcessDetail::system_details(const std::string &id, const std::string &name, const pugi::xml_node &system) {
	SystemProcess systemProcess;
	systemProcess.SetID(id);
	systemProcess.SetName(name);
	for (auto command : system.child("config").children("command"))
	{
		map_attrs command_attrs{ { m_a_id,{ "", true } },{ m_a_name,{ "", true } },{ m_a_type,{ "", true } } };
		if (!readValues(command_attrs, command))
			continue;

		CommandCfg cfg = command_details(std::get<0>(command_attrs[m_a_id]), std::get<0>(command_attrs[m_a_name]), command);
		systemProcess.AddCommandItem(cfg);
	}
	return systemProcess;
}

//
//-------------------------------------------------------------
// Private method
//-------------------------------------------------------------
//
CommandCfg ProcessDetail::command_details(const std::string &id, const std::string &name, const pugi::xml_node &command) {
	CommandCfg cfg;
	cfg.SetID(id);
	cfg.SetName(name);
	pugi::xml_node command_folder = command.child("commandfolder");
	map_attrs command_folder_attrs{ { m_a_folder,{ "", true } } };
	if (readValues(command_folder_attrs, command_folder))
	{
		cfg.SetWorkFolder(std::get<0>(command_folder_attrs[m_a_folder]));
		arg_detail(cfg, command);
	}
	return cfg;
}

//
//-------------------------------------------------------------
// Private method
//-------------------------------------------------------------
//
void ProcessDetail::select_item_detail(SystemProcess &systemProcess, const pugi::xml_node &system) {
	for (pugi::xml_node item : system.child("select").children("item"))
	{
		map_attrs item_attrs{ { m_a_id,{ "", true } },{ m_a_name,{ "", true } } };
		if (!readValues(item_attrs, item))
			continue;

		pugi::xml_node input = item.child("input");
		map_attrs input_attrs{ { m_a_folder,{ "", true } },{ m_a_ext,{ "", true } } };
		if (!readValues(input_attrs, input))
			continue;

		SelectItem selectItem;
		selectItem.SetID(std::get<0>(item_attrs[m_a_id]));
		selectItem.SetName(std::get<0>(item_attrs[m_a_name]));
		selectItem.SetFolder(std::get<0>(input_attrs[m_a_folder]));
		selectItem.SetExt(std::get<0>(input_attrs[m_a_ext]));
		systemProcess.AddSelectItem(selectItem);
	}

}

//
//-------------------------------------------------------------
// Private method
//-------------------------------------------------------------
//
void ProcessDetail::arg_detail(CommandCfg &cfg, const pugi::xml_node &command) {
	for (pugi::xml_node arg : command.child("args").children("arg"))
	{
		map_attrs arg_attrs{ { m_a_use,{ "", false } },{ m_a_value,{ "", false } } };
		if (!readValues(arg_attrs, arg))
			continue;
		auto use = std::get<0>(arg_attrs[m_a_use]);
		auto value = std::get<0>(arg_attrs[m_a_value]);
		if (!use.empty()) {
			auto usearg = toUppercase(use.c_str());
			int index = 0;
			CMDARG arg;
			if (usearg.compare("FULLPATH") == 0) {
				index = cfg.AddCommandArg(usearg, Fullpath);
				arg = cfg.GetCommandArg(index);
			}
			else if (usearg.compare("FILENAME") == 0) {
				index = cfg.AddCommandArg(usearg, Filename);
				arg = cfg.GetCommandArg(index);
			}
			else if (usearg.compare("EXT") == 0) {
				index = cfg.AddCommandArg(usearg, Ext);
				arg = cfg.GetCommandArg(index);
			}
		}
		else if (!value.empty()) {
			cfg.AddCommandArg(value, Value);
		}
	}
}