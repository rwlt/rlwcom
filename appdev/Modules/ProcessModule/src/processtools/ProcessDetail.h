#ifndef D_PROCESSDETAIL_H
#define D_PROCESSDETAIL_H

#include <iostream>
#include "IDNamePair.h"
#include "ListBase.h"
#include "SelectItem.h"
#include "SystemProcess.h"
#include "processexception.h"
#include <sstream>

class ProcessDetail;
namespace pugi {
	class xml_node;
}

/// Process Detail
class ProcessDetail: public IDNamePair
{
    // Private data members
    const char *m_xml;
    std::string m_activeSystem;          // What current system is active
    int m_currentPage;                   // What current page is active
    ListBase<SystemProcess> m_lstSystem; // Keep SystemProcess list here in Process detail
	std::stringstream m_xmlerror{};   ///< what was xml parser problem
	std::string m_a_id{ "id" }, m_a_name{ "name" };
	std::string m_a_icon_src{ "iconsrc" }, m_a_default{ "default" };
	std::string m_a_type{ "type" }, m_a_folder{ "folder" };
	std::string m_a_use{ "use" }, m_a_value{ "value" };
	std::string m_a_ext{ "ext" };

public:
    /// Constructor
    /**
     * \param xml char*
     */
    ProcessDetail ( const char *xml, const std::string &id="" );
    /// Deconstructor
    ~ProcessDetail ();

	/// Get XmlError by string
	std::string XmlError();

	/// Get System Process by name
    SystemProcess &GetSystemProcess ( const std::string &name );
    /// List System Processes
    ListBase<SystemProcess> &GetListSystem();
    /// Set SetCategoryList
    void SetCategoryList();
    /// Set Active System by name
    /**
     * \param system string
     */
    void SetActiveSystem ( const std::string &system );
    /// Get active system
    std::string GetActiveSystem();
    /// Set page
    void SetCurrentPage ( const std::string &page );
    /// Get page
    int GetCurrentPage();
    /// Add SystemProcess to list
    /**
     * \param item SystemProcess
     */
    void AddListItem ( SystemProcess item );
    /// Start Job
    /**
     * \param systemName
     * \param categoryName
     */
    void StartJob ( const std::string &systemName, const std::string &categoryName );
    /// Cancel Job
    /**
     * \param systemName
     * \param categoryName
     */
    void CancelJob ( const std::string &systemName, const std::string &categoryName );

private:
	bool readValues(map_attrs &attrs, const pugi::xml_node &node);
	SystemProcess system_details(const std::string &id, const std::string &name, const pugi::xml_node &system);
	CommandCfg command_details(const std::string &id, const std::string &name, const pugi::xml_node &node);
	void select_item_detail(SystemProcess &systemProcess, const pugi::xml_node &system);
	void arg_detail(CommandCfg &cfg, const pugi::xml_node &command);
};

#endif /* D_PROCESSDETAIL_H */
