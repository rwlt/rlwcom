#include "SystemProcess.h"

//
//-------------------------------------------------------------
// Constructor
//-------------------------------------------------------------
//
SystemProcess::SystemProcess():
    m_activeSelect("")
{
}
//
//-------------------------------------------------------------
// Destructor
//-------------------------------------------------------------
//
SystemProcess::~SystemProcess()
{
    m_lstSelect.Clear();
    m_lstCommand.Clear();
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
int SystemProcess::SetupCommand()
{
    return 0;
}

//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
SelectItem &SystemProcess::GetSelectItem(const std::string &name)
{
    return m_lstSelect.GetItem(name);
}
void SystemProcess::ClearSelectList() {
	m_lstSelect.Clear();
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
void SystemProcess::AddSelectItem(SelectItem item)
{
    return m_lstSelect.AddItem(item);
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
CommandCfg &SystemProcess::GetCommandItem(const std::string &name)
{
    return m_lstCommand.GetItem(name);
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
void SystemProcess::AddCommandItem(CommandCfg item)
{
    return m_lstCommand.AddItem(item);
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
int SystemProcess::SetActiveSelect(const std::string &select)
{
    m_activeSelect = select;
    return 0;
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
std::string SystemProcess::GetActiveSelect() const
{
    return m_activeSelect;
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
const ListBase<CommandCfg> &SystemProcess::GetCommandCfgList() const
{
    return (m_lstCommand);
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
const ListBase<SelectItem> &SystemProcess::GetSelectItemList() const
{
    return (m_lstSelect);
}
//
//-------------------------------------------------------------
// Public method
//-------------------------------------------------------------
//
void SystemProcess::writeSystem() const
{
    std::cout << std::endl
              << "System:          " << GetID() << std::endl
              << "Name:            " << GetName() << std::endl
              << "Active Category: " << (GetActiveSelect() == "" ? "[empty]" : GetActiveSelect()) << '\n' << std::endl;

    const ListBase<SelectItem> &listSelect = GetSelectItemList();
    if (listSelect.GetSize() > 0) {
        std::cout << "   Category - Select Item(s)" << std::endl;
        for (const SelectItem & selItem : listSelect) {
            selItem.writeSelectItem();
        }
    }

    const ListBase<CommandCfg> &listCommand = GetCommandCfgList();
    if (listCommand.GetSize() > 0) {
        std::cout << "   Command Item(s)" << std::endl;
        for (const CommandCfg & cmd : listCommand) {
            cmd.writeCommandItem();
        }
    }
    std::cout << std::endl;
}
