#ifndef D_IDNAMEPAIR_H
#define D_IDNAMEPAIR_H

#include <string>
#include <map>

typedef std::map<std::string, std::tuple<std::string, bool>> map_attrs;

/// IDNamePair
class IDNamePair
{
    std::string m_ID;
    std::string m_name;

public:
    /// Constructor
    IDNamePair();
    /// Deconstructor
    virtual ~IDNamePair();

    /// Set ID
    void SetID ( const std::string &value );
    /// Get ID
    const std::string GetID() const;
    /// Set Name
    void SetName ( const std::string &name );
    /// Get Name
    const std::string GetName() const;

	bool find_attributes(map_attrs name_value, std::string& error);

	std::string toUppercase(const char *name);

};

#endif /* D_IDNAMEPAIR_H */
