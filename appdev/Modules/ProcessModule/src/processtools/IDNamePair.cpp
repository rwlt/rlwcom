#include "IDNamePair.h"
#include <sstream>

IDNamePair::IDNamePair(): m_ID(""), m_name(""){}

IDNamePair::~IDNamePair(){}

void IDNamePair::SetID ( const std::string &value ) {
    m_ID = value;
}

const std::string IDNamePair::GetID() const {
    return m_ID;
}

void IDNamePair::SetName ( const std::string &name ) {
    m_name = name;
}

const std::string IDNamePair::GetName() const {
    return m_name;
}

bool IDNamePair::find_attributes(map_attrs name_value, std::string& error) {
	std::stringstream ss;
	for (auto pair : name_value) {
		if (std::get<0>(pair.second).empty() && std::get<1>(pair.second) == true) {
			ss << "Attribute " << pair.first << " attribute empty\n";
		}
	}
	error = ss.str();
	if (error.empty()) {
		return true;
	}
	else {
		return false;
	}
}

std::string IDNamePair::toUppercase(const char *name) {
	int counter = 0;
	char upd[40];
	char *p_upd = upd;
	while (name[counter]) {
		*p_upd = toupper(name[counter]);
		p_upd++;
		counter++;
	}
	*p_upd = '\0';
	return std::string(upd);
}
