#ifndef D_SYSTEMPROCESS_H
#define D_SYSTEMPROCESS_H

#include "IDNamePair.h"
#include "ListBase.h"
#include "commandconf.h"
#include "SelectItem.h"

class SystemProcess;

/// System Process
class SystemProcess: public IDNamePair
{
    std::string m_activeSelect;
	ListBase<SelectItem> m_lstSelect{};
	ListBase<CommandCfg> m_lstCommand{};

public:
    /// Constructor
    SystemProcess();
    /// Deconstructor
    virtual ~SystemProcess();

    /// Select Item by name
    /**
     * \param name string
     * \return SelectItem
     */
    SelectItem &GetSelectItem ( const std::string &name );
	/// Clear Select List 
	/**
	* \return string
	*/
	void ClearSelectList();
	/// Add a Select Item
    /**
     * \param item SelectItem
     */
    void AddSelectItem ( SelectItem item );
    /// Command Cfg by name
    /**
     * \param name string
     * \return CommandCfg
     */
    CommandCfg &GetCommandItem ( const std::string &name );
    /// Add Command Cfg
    /**
     * \param item CommandCfg
     */
    void AddCommandItem ( CommandCfg item );
    /// Set Active Select item
    /**
     * \param select string
     * \return int
     */
    int SetActiveSelect ( const std::string &select );
    /// Get Active Select
    /**
     * \return string
     */
    std::string GetActiveSelect() const;
	/// Setup command
    int SetupCommand();
    /// Get CommandCfg List
    const ListBase<CommandCfg> &GetCommandCfgList() const;
    /// Get SelectItem List
    const ListBase<SelectItem> &GetSelectItemList() const;
    /// Print system details
    void writeSystem() const;
    /// Size of select item list
    /**
     * \return int
     */
    int GetSize() {
        return m_lstSelect.GetSize();
    }

};

#endif /* D_SYSTEMPROCESS_H */
