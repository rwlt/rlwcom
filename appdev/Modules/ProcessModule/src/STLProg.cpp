
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <set>
#include <string>

#include "FileIn.h"
#include "processtools/IDNamePair.h"
#include "processtools/ProcessDetail.h"
#include "data/tree.h"
#include <random>
#include <stdexcept>

#define LOGGER

void pointer_data_struct_tree(); // A pointer structure for a binary tree implentation
/*
 *     std::mt19937 rng;
 *    rng.seed(std::random_device()());
 *    std::uniform_int_distribution<std::mt19937::result_type> dist40(1, 40); // distribution range
 *
 *    pointer_data_struct_tree(); // Function where tree.h is used for printing trees.
 */
//
//----------------------------------------------------------------------
// Main Program
//----------------------------------------------------------------------
//
int main(int argc, char **argv)
{
    // The process configuration in Xml files reader and runs a set of proc (shell procs).
    // It uses expat xml parsing where the configuration is read and creates
    // the Classes to perform the process to do shell proc's. Useful if thats what you need ;)
    //     D:\\app\\process.xml
    if (argc == 2 || argc == 3 || argc == 4) {
        std::string system;
        std::string category;
        if (argc == 2) {
            system = "";
            category = "";
        }
        if (argc == 3) {
            system = std::string(argv[2]);
            category = "";
        }
        if (argc == 4) {
            system = std::string(argv[2]);
            category = std::string(argv[3]);
        }
        std::string filename(argv[1]);
        ProcessDetail clsProcess(filename.c_str());
        clsProcess.SetID("PROCESS1");
        clsProcess.SetName("Process Clips");
        try {
            clsProcess.StartJob(system, category);
        } catch (const std::exception &e) {
            std::cout << "Problem with process. Try using a system and category listed." << std::endl;
        }
    }
    return (0);
}
//
//---------------------------------------------------
// Public function
//---------------------------------------------------
//
void pointer_data_struct_tree()
{
    Resource::FileIn fileIn; // Will open/close resource file at end of scope
    data::tree tree1;

    fileIn.openFile("/home/rodney/app_config/numbers.txt");
    while (!(fileIn.eof())) {
        tree1.enterItem(atoi(fileIn.readLine().c_str()));
    }
    tree1.printLevelOrder();
    tree1.levelOrder();

}
