/*
 * tree.cpp
 */

#include "tree.h"
#include <iostream>
#include <sstream>
#include <queue>
#include <stack>
namespace data
{
//
//-------------------------------------------------
// Deconstructor
//-------------------------------------------------
//
tree::~tree()
{
    if (root != nullptr) {
        deleteRecsPostOrderItem(root);
    }
}
//
//-------------------------------------------------
// Public method
//-------------------------------------------------
//
void tree::enterItem(const int item)
{
    enterTreeItem(root, item);
}
//
//-------------------------------------------------
// Public method
//-------------------------------------------------
//
void tree::print()
{
    std::cout << "Height " << height(root) << std::endl;
    printItem(root);
}
//
//-------------------------------------------------
// Public method
//-------------------------------------------------
//
void tree::printPostOrder()
{
    std::cout << "Height " << height(root) << std::endl;
    printPostOrderItem(root);
}
//
//-------------------------------------------------
// Public method
//-------------------------------------------------
//
/* Function to print level order traversal a tree*/
void tree::printLevelOrder()
{
    std::cout << "Print level plain." << std::endl;
    int h = height(root);
    std::cout << "Level Order Total Height " << height(root) << std::endl;
    int i;
    for (i = 1; i <= h; i++)
        printGivenLevel(root, i);
    std::cout << std::endl;
}
//
//-------------------------------------------------
// Public method
//-------------------------------------------------
//
void tree::levelOrder()
{
    std::cout << "Print level order by queue and in a pretty tree (Max 5 depth)." << std::endl;
    levelQueueOrder(root);
    std::cout << std::endl;
}
//
//-------------------------------------------------
// Public method
//-------------------------------------------------
//
void tree::levelQueueNonRecOrder()
{
    printNonRecspostorder(root);
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::enterTreeItem(node *&treeNode, const int item)
{

    if (treeNode == nullptr) {
        treeNode = new node();
        treeNode->left = nullptr;
        treeNode->right = nullptr;
        treeNode->data = item;
    }
    if (treeNode->data == item)
        return;
    if (treeNode->data < item)
        enterTreeItem(treeNode->right, item);
    if (treeNode->data > item)
        enterTreeItem(treeNode->left, item);
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::printItem(node *top)
{
    if (top == nullptr)
        return;
    printItem(top->left);
    std::cout << top->data << " ";
    printItem(top->right);

}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::printPostOrderItem(node *top)
{
    if (top == nullptr)
        return;
    printPostOrderItem(top->left);
    printPostOrderItem(top->right);
    std::cout << top->data << " ";

}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::deleteRecsPostOrderItem(node *top)
{
    if (top == nullptr)
        return;
    deleteRecsPostOrderItem(top->left);
    deleteRecsPostOrderItem(top->right);
//std::cout << top->data << " ";
    delete top;

}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
/* Print nodes at a given level */
void tree::printGivenLevel(node *top, int level)
{
    if (top == nullptr)
        return;
    if (level == 1)
        std::cout << top->data << " ";
    else if (level > 1) {
        printGivenLevel(top->left, level - 1);
        printGivenLevel(top->right, level - 1);
    }
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
tree::node *tree::placeHolderNode()
{
    node *holder = new node();
    holder->data = -1;
    holder->left = nullptr;
    holder->right = nullptr;
    return holder;
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
int tree::nodeAmountByHeight(int treeHeight)
{
    int totalNodeAllowed = 1;
    for (int c = 1; c < treeHeight; c++) {
        totalNodeAllowed = totalNodeAllowed * 2;
        //std::cout << "TT " << totalNodeAllowed << std::endl;
    }
    return totalNodeAllowed;
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::drawRepeatString(int lengthSpace, const std::string &charPrint)
{
    std::stringstream text;
    for (int cnt = 0; cnt < lengthSpace; ++cnt) {
        text << charPrint;
    }
    std::cout << text.str();
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::drawLavelSpaces(int nodeCount, int halfSpaceNode,
                           int lengthSpace)
{
    if (nodeCount == halfSpaceNode) {
        drawRepeatString(lengthSpace / 2, " ");
    } else {
        drawRepeatString(lengthSpace, " ");
    }
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::drawFullBranch(int lengthSpace)
{
    drawRepeatString(lengthSpace, spacePrint);
    drawRepeatString(lengthSpace - 2, linePrint);
    std::cout << leftBranch << "  " << rightBranch;
    drawRepeatString(lengthSpace - 2, linePrint);
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::drawRightBranch(int lengthSpace)
{
    drawRepeatString(lengthSpace, spacePrint);
    drawRepeatString(lengthSpace - 2, spacePrint);
    std::cout << spacePrint << "  " << rightBranch;
    drawRepeatString(lengthSpace - 2, linePrint);
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::drawLeftBranch(int lengthSpace)
{
    drawRepeatString(lengthSpace, spacePrint);
    drawRepeatString(lengthSpace - 2, linePrint);
    std::cout << leftBranch << "  " << spacePrint;
    drawRepeatString(lengthSpace - 2, spacePrint);
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::drawNoBranch(int lengthSpace)
{
    drawRepeatString(lengthSpace, spacePrint);
    drawRepeatString(lengthSpace - 2, spacePrint);
    std::cout << spacePrint << "  " << spacePrint;
    drawRepeatString(lengthSpace - 2, spacePrint);
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::drawFourNode(const std::vector<std::string> &drawitems, int col,
                        int lengthSpace)
{
    drawLeftRightBranches(drawitems, col, lengthSpace);
    drawRepeatString((lengthSpace), " ");
    drawLeftRightBranches(drawitems, col + 2, lengthSpace);
    drawRepeatString((lengthSpace), " ");
    drawLeftRightBranches(drawitems, col + 4, lengthSpace);
    drawRepeatString((lengthSpace), " ");
    drawLeftRightBranches(drawitems, col + 6, lengthSpace);
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::drawItemInTree(const std::string &item, const int itemSpace,
                          const int itemGaps)
{
    if (item.compare("##") == 0) {
        drawRepeatString(itemSpace, " ");
        drawRepeatString(itemGaps, " ");
    } else {
        // Draw the item centered as possible
        int over = itemSpace - item.length();
        if (over > 0) {
            int halfover = (over / 2) + 1;
            drawRepeatString(halfover, " ");
            std::cout << item;
            drawRepeatString(over - halfover, " ");
        } else {
            std::cout << item;
        }
        drawRepeatString(itemGaps, " ");
    }
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::drawLeftRightBranches(const std::vector<std::string> &drawitems,
                                 int col, int lengthSpace)
{
    unsigned check = col + 1;
    if (drawitems.size() > check) {
        const std::string *left;
        const std::string *right;
        left = &drawitems.at(col);
        right = &drawitems.at(col + 1);
        if (left->compare("##") == 0 && right->compare("##") == 0) {
            drawNoBranch(lengthSpace);
        } else if (left->compare("##") == 0) {
            drawRightBranch(lengthSpace);
        } else if (right->compare("##") == 0) {
            drawLeftBranch(lengthSpace);
        } else {
            drawFullBranch(lengthSpace);
        }
    } else {
        drawNoBranch(lengthSpace);
    }
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::drawCompleteTreeByLevelOrder(int treeHeight,
                                        const std::vector<std::string> &drawitems)
{
// Draw the tree
// All nodes of a complete binary tree are to print out
// Maximum level printed is 6 levels.
// Depending on the current level the items are spaced to fit upto a level 6 tree.
// Calculate the rows number of grid place holders according to the tree height
// lvl1 = height
    const int itemSpace = 5;
    const int itemGaps = 1;
    int totalNodeAllowed = nodeAmountByHeight(6);//treeHeight); // Need the total nodes full a full tree
    std::cout << totalNodeAllowed << " " << treeHeight << std::endl;
    int basemax = (totalNodeAllowed / 2) * (itemSpace + itemGaps);
    int nodeDone = 0;
    for (std::string item : drawitems) {
//  if (nodeDone > totalNodeAllowed)
//   break;
//  std::cout << "&&& " << item << " " << nodeDone << std::endl;

        if (nodeDone == 0) {
            // Top node
            int lengthSpace = (basemax) - (itemSpace + itemGaps);
            drawLavelSpaces(nodeDone, 0, lengthSpace);
            drawItemInTree(item, itemSpace, itemGaps);
        } else if (nodeDone > 0 && nodeDone <= 2) {
            // Level 2
            int lengthSpace = (basemax / 2) - (itemSpace + itemGaps);
            drawLavelSpaces(nodeDone, 1, lengthSpace);
            drawItemInTree(item, itemSpace, itemGaps);
        } else if (nodeDone > 2 && nodeDone <= 6) {
            // Level 3
            int lengthSpace = (basemax / 4) - (itemSpace + itemGaps);
            drawLavelSpaces(nodeDone, 3, lengthSpace);
            drawItemInTree(item, itemSpace, itemGaps);
        } else if (nodeDone > 6 && nodeDone <= 14) {
            // Level 4
            int lengthSpace = (basemax / 8) - (itemSpace + itemGaps);
            drawLavelSpaces(nodeDone, 7, lengthSpace);
            drawItemInTree(item, itemSpace, itemGaps);
        } else if (nodeDone > 14 && nodeDone <= 30) {
            // Level 5
            int lengthSpace = (basemax / 16) - (itemSpace + itemGaps);
            // 128   3
            //std::cout << lengthSpace << " " << basemax << " " << itemSpace << std::endl;
            drawLavelSpaces(nodeDone, 15, lengthSpace);
            drawItemInTree(item, itemSpace, itemGaps);
        }

        // End of line print. Print line branches on next line
        if (nodeDone == 0) {
            // Level 2 branches from level 1
            std::cout << std::endl;
            int lengthSpace = (basemax / 4);
            drawLeftRightBranches(drawitems, 1, lengthSpace);
            std::cout << std::endl;
        }
        if (nodeDone == 2) {
            // Level 3 branches from level 2
            std::cout << std::endl;
            int lengthSpace = (basemax / 8);
            drawLeftRightBranches(drawitems, 3, lengthSpace);
            drawRepeatString((lengthSpace), " ");
            drawLeftRightBranches(drawitems, 5, lengthSpace);
            std::cout << std::endl;
        }
        if (nodeDone == 6) {
            // Level 4 branches from level 3
            std::cout << std::endl;
            int lengthSpace = (basemax / 16);
            drawFourNode(drawitems, 7, lengthSpace);
            std::cout << std::endl;
        }

        if (nodeDone == 14) {
            // Level 5 branches from level 4
            std::cout << std::endl;
            int lengthSpace = (basemax / 32);
            drawFourNode(drawitems, 15, lengthSpace);
            drawRepeatString((lengthSpace), " ");
            drawFourNode(drawitems, 23, lengthSpace);
            std::cout << std::endl;
        }

//  if (nodeDone == 30)
//  {   // Level 6 branches from level 5
//   std::cout << std::endl;
//   int lengthSpace = (basemax / 32);
//   drawFourNode(drawitems, 31, lengthSpace);
//   drawRepeatString((lengthSpace), " ");
//   drawFourNode(drawitems, 39, lengthSpace);
//   std::cout << std::endl;
//  }

//  std::cout << "&&&& " << item << " " << nodeDone << std::endl;
        ++nodeDone; // go to the next node in draw item list
    }
    std::cout << "ALL DONE" << std::endl;
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
// Print the tree level-order assisted by queue
// Uses twice memory than the tree for this operation
void tree::levelQueueOrder(node *n)
{
// Create a queue
    std::queue<node *> q;
// Keep a record of placeholder nodes
    std::vector<node *> holders;
// Create the nodes of a complete tree upto 6 levels.
    std::vector<std::string> drawitems;

    int treeHeight = height(root); // What is the current height.
    std::cout << "Level Order Total Height " << treeHeight << std::endl;
    if (treeHeight <= 0)
        return;
    if (treeHeight > 6)
        treeHeight = 6;

    int totalNodeAllowed = nodeAmountByHeight(6);//treeHeight); // Need totals nodes full a full tree
    int nodeCount = 0;

// Push the root
    q.push(n); //FIFO

    while (!q.empty()) {
        // Dequeue a node from front
        node *v = q.front();
        ++nodeCount;
        if (nodeCount > totalNodeAllowed) // Full number of nodes found
            break;

        if (v->data == -1) {
            // place holder of empty node
            drawitems.push_back("##");
        } else {
            std::stringstream value;
            value << v->data;
            drawitems.push_back(value.str());
        }

        // Enqueue the left children
        if (v->left != nullptr) {
            q.push(v->left);
        } else { // otherwise create a holder node for the empty leaf
            node *holder = placeHolderNode();
            holders.push_back(holder);
            q.push(holder);
        }

        // Enqueue the right children
        if (v->right != nullptr) {
            q.push(v->right);
        } else { // otherwise create a holder node for the empty leaf
            node *holder = placeHolderNode();
            holders.push_back(holder);
            q.push(holder);
        }

        // Pop the visited node
        q.pop();
    }
// Remove place holders as not required after draw items is made for a complete tree
    for (node * holder : holders) {
        delete holder;
    }

// Draw the tree
    drawCompleteTreeByLevelOrder(treeHeight, drawitems);

}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
void tree::printNonRecspostorder(node *head)
{
    if (head == nullptr) {
        return;
    }
    std::stack<node *> stack;
    stack.push(head);

    while (!stack.empty()) {
        node *next = stack.top();

        bool finishedSubtrees = (next->right == head || next->left == head);
        bool isLeaf = (next->left == nullptr && next->right == nullptr);
        if (finishedSubtrees || isLeaf) {
            stack.pop();
            std::cout << next->data << " ";
            head = next;
        } else {
            if (next->right != nullptr) {
                stack.push(next->right);
            }
            if (next->left != nullptr) {
                stack.push(next->left);
            }
        }
    }
}
//
//-------------------------------------------------
// Private method
//-------------------------------------------------
//
/* Compute the "height" of a tree -- the number of
 nodes along the longest path from the root node
 down to the farthest leaf node.*/
int tree::height(node *node)
{
    if (node == nullptr)
        return 0;
    else {
        /* compute the height of each subtree */
        int lheight = height(node->left);
        int rheight = height(node->right);

        /* use the larger one */
        if (lheight > rheight)
            return (lheight + 1);
        else
            return (rheight + 1);
    }
}

} /* namespace data */
