/*
 * LButton.h
 *
 *  Created on: Mar 28, 2015
 *      Author: Rodney Woollett
 */

#ifndef LBUTTON_H_
#define LBUTTON_H_

#include "LButtonTexture.h"

class LButton
{
public:
	//Initializes variables
	LButton(LButtonTexture* buttonTexture);

	// Use default copy constructor LButton(const LButton& oldLButton);

	//Deallocates memory
	virtual ~LButton();

	//Sets top left position void
	void setPosition(int x, int y);
	//Handles mouse event
	void handleEvent(SDL_Event* e);
	//Shows button sprite
	void render();

private:
	//Top left position
	SDL_Point mPosition;
	//Currently used global sprite
	LButtonSprite mCurrentSprite;
	LButtonTexture* mButtonTexture;

};

#endif /* LBUTTON_H_ */
