/*
 * LButtonTexture.h
 *
 *  Created on: Mar 28, 2015
 *      Author: Rodney Woollett
 */

#ifndef LBUTTONTEXTURE_H_
#define LBUTTONTEXTURE_H_


#include "LTexture.h"

//Button constants
const int BUTTON_WIDTH = 91;
const int BUTTON_HEIGHT = 25;

enum LButtonSprite
{
	BUTTON_SPRITE_MOUSE_OUT = 0,
	BUTTON_SPRITE_MOUSE_OVER_MOTION = 1,
	BUTTON_SPRITE_MOUSE_DOWN = 2,
	BUTTON_SPRITE_MOUSE_UP = 3,
	BUTTON_SPRITE_TOTAL = 4
};

class LButtonTexture: public LTexture
{
private:
	const int TOTAL_SPRITES;
	SDL_Rect spriteClips[4];
	LButtonSprite mCurrentSprite;

public:
	LButtonTexture(SDL_Renderer* renderer);
	// Use default copy constructor LButtonTexture(const LButtonTexture& oldLButtonTexture);

	//Deallocates memory
	~LButtonTexture();

	void setButtonSprite(LButtonSprite sprite);
	void render( int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE );
};

#endif /* LBUTTONTEXTURE_H_ */
