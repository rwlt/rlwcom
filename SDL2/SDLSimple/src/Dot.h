/*
 * LButton.h
 *
 *  Created on: Mar 28, 2015
 *      Author: Rodney Woollett
 */

#ifndef DOT_H_
#define DOT_H_

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

#include "LTexture.h"

class Dot
{
public:
	//The dimensions of the dot
	static const int DOT_WIDTH = 16;
	static const int DOT_HEIGHT = 15;
	//Maximum axis velocity of the dot static
	const int DOT_VEL = 10;

	//Initializes variables
	Dot(LTexture* dotTexture);

	// Use default copy constructor LButton(const LButton& oldLButton);

	//Deallocates memory
	virtual ~Dot();

	//Handles mouse event
	void handleEvent(SDL_Event* e);
	//Moves the dot
	void move();
	//Shows button sprite
	void render();

private:
	//The X and Y offsets of the dot
	int mPosX, mPosY;
	//The velocity of the dot
	int mVelX, mVelY;

	//Currently used global sprite
	LTexture* mTexture;

};

#endif /* DOT_H_ */
