/*
 * Timer.cpp
 *
 *      Author: Lazy Foo(lazyfoo.net)
 */

#include "Timer.h"
#include "SDL.h"
EngineLayer::Timer::Timer()
{

	//Initialize the variables
	startTicks = 0;
	pausedTicks = 0;
	paused = false;
	started = false;
}

void EngineLayer::Timer::start()
{
	//Start the Timer
	started = true;

	//Unpause the Timer
	paused = false;

	//Get the current clock time
	startTicks = SDL_GetTicks();
}

void EngineLayer::Timer::stop()
{
	//Stop the Timer
	started = false;

	//Unpause the Timer
	paused = false;
	//Get the current clock time
	startTicks = SDL_GetTicks();
}

void EngineLayer::Timer::pause()
{
	//If the Timer is running and isn't already paused
	if ((started == true) && (paused == false))
	{
		//Pause the Timer
		paused = true;

		//Calculate the paused ticks
		pausedTicks = SDL_GetTicks() - startTicks;
	}
}

void EngineLayer::Timer::unpause()
{
	//If the Timer is paused
	if (paused == true)
	{
		//Unpause the Timer
		paused = false;

		//Reset the starting ticks
		startTicks = SDL_GetTicks() - pausedTicks;

		//Reset the paused ticks
		pausedTicks = 0;
	}
}

int EngineLayer::Timer::get_ticks()
{
	//If the Timer is running
	if (started == true)
	{
		//If the Timer is paused
		if (paused == true)
		{
			//Return the number of ticks when the Timer was paused
			return (pausedTicks);
		}
		else
		{
			//Return the current time minus the start time
			return (SDL_GetTicks() - startTicks);
		}
	}

	//If the Timer isn't running
	return (0);
}

bool EngineLayer::Timer::is_started()
{
	return (started);
}

bool EngineLayer::Timer::is_paused()
{
	return (paused);
}

