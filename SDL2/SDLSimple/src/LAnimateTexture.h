/*
 * LAnimateTexture.h
 *
 *  Created on: Mar 27, 2015
 *      Author: Rodney Woollett
 */

#ifndef LANIMATETEXTURE_H_
#define LANIMATETEXTURE_H_

#include "LTexture.h"

class LAnimateTexture: public LTexture
{
private:
	const int WALKING_ANIMATION_FRAMES;
	SDL_Rect spriteClips[4];
	//Current animation frame
	int frame;

public:
    LAnimateTexture(SDL_Renderer* renderer);
	// Use default copy constructor LAnimateTexture(const LAnimateTexture& oldLAnimateTexture);

	//Deallocates memory
	~LAnimateTexture();

    //void render( int x, int y, SDL_Rect* clip = NULL);
	void render( int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE );
};

#endif /* LANIMATETEXTURE_H_ */
