/*
 * Dot.cpp
 *
 *  Created on: Mar 28, 2015
 *      Author: Rodney Woollett
 */

#include <SDL.h>
#include "Dot.h"

Dot::Dot(LTexture* dotTexture)
{
	//Initialize the offsets
	mPosX = 0;
	mPosY = 0;
	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
	// Init the texture to render
	mTexture = dotTexture;
}
Dot::~Dot()
{
	//Deallocate
}

void Dot::handleEvent(SDL_Event* e)
{
	// Lesson try to base the velocity on per second rather than 10 each frame
	//in this tutorial we're basing the velocity as amount moved per frame.
	//In most games, the velocity is done per second. The reason were doing it per
   //frame is that it is easier, but if you know physics it shouldn't be hard to
	//update the dots position based on time. Need know physics.
	//If a key was pressed
	if (e->type == SDL_KEYDOWN && e->key.repeat == 0)
	{
		//Adjust the velocity
		switch (e->key.keysym.sym)
		{
		case SDLK_UP:
			mVelY -= DOT_VEL;
			break;
		case SDLK_DOWN:
			mVelY += DOT_VEL;
			break;
		case SDLK_LEFT:
			mVelX -= DOT_VEL;
			break;
		case SDLK_RIGHT:
			mVelX += DOT_VEL;
			break;
		}
	}
	//If a key was released else
	if (e->type == SDL_KEYUP && e->key.repeat == 0)
	{
		//Adjust the velocity
		switch (e->key.keysym.sym)
		{
		case SDLK_UP:
			mVelY += DOT_VEL;
			break;
		case SDLK_DOWN:
			mVelY -= DOT_VEL;
			break;
		case SDLK_LEFT:
			mVelX += DOT_VEL;
			break;
		case SDLK_RIGHT:
			mVelX -= DOT_VEL;
			break;
		}
	}
}

void Dot::move()
{
	//Move the dot left or right
	mPosX += mVelX;
	//If the dot went too far to the left or right
	if ((mPosX < 0) || (mPosX + DOT_WIDTH > SCREEN_WIDTH))
	{
		//Move back
		mPosX -= mVelX;
	}
	//Move the dot up or down
	mPosY += mVelY;
	//If the dot went too far up or down
	if ((mPosY < 0) || (mPosY + DOT_HEIGHT > SCREEN_HEIGHT))
	{
		//Move back
		mPosY -= mVelY;
	}
}

void Dot::render()
{
	mTexture->render(mPosX, mPosY);
}

