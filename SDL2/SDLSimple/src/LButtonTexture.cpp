/*
 * LButtonTexture.cpp
 *
 *  Created on: Mar 28, 2015
 *      Author: Rodney Woollett
 */

#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include "LButtonTexture.h"

LButtonTexture::LButtonTexture(SDL_Renderer* renderer) :
		LTexture(renderer), TOTAL_SPRITES(4)
{
	//Initialize button texture default sprite
	mCurrentSprite = BUTTON_SPRITE_MOUSE_OUT;
	//Set sprite clips
	spriteClips[ 0 ].x = 0; spriteClips[ 0 ].y = 0; spriteClips[ 0 ].w = 91; spriteClips[ 0 ].h = 25;
	spriteClips[ 1 ].x = 0; spriteClips[ 1 ].y = 25; spriteClips[ 1 ].w = 91; spriteClips[ 1 ].h = 25;
	spriteClips[ 2 ].x = 0; spriteClips[ 2 ].y = 50; spriteClips[ 2 ].w = 91; spriteClips[ 2 ].h = 25;
	spriteClips[ 3 ].x = 0; spriteClips[ 3 ].y = 75; spriteClips[ 3 ].w = 91; spriteClips[ 3 ].h = 25;

}

LButtonTexture::~LButtonTexture()
{
}

void LButtonTexture::setButtonSprite(LButtonSprite sprite)
{
	mCurrentSprite = sprite;
}

void LButtonTexture::render( int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip )
{
	//Render current frame
	SDL_Rect* currentClip = &spriteClips[ mCurrentSprite ];
	LTexture::render(x, y, currentClip, angle, center, flip);

}





