/*
 * LAnimateTexture.cpp
 *
 *  Created on: Mar 27, 2015
 *      Author: Rodney Woollett
 */

#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include "LAnimateTexture.h"

LAnimateTexture::LAnimateTexture(SDL_Renderer* renderer) :
		LTexture(renderer), WALKING_ANIMATION_FRAMES(4)
{
	//Initialize animation frame
	frame = 0;
	//Set sprite clips
	spriteClips[ 0 ].x = 0; spriteClips[ 0 ].y = 0; spriteClips[ 0 ].w = 64; spriteClips[ 0 ].h = 205;
	spriteClips[ 1 ].x = 64; spriteClips[ 1 ].y = 0; spriteClips[ 1 ].w = 64; spriteClips[ 1 ].h = 205;
	spriteClips[ 2 ].x = 128; spriteClips[ 2 ].y = 0; spriteClips[ 2 ].w = 64; spriteClips[ 2 ].h = 205;
	spriteClips[ 3 ].x = 196; spriteClips[ 3 ].y = 0; spriteClips[ 3 ].w = 64; spriteClips[ 3 ].h = 205;

}

LAnimateTexture::~LAnimateTexture()
{
}

//void LAnimateTexture::render( int x, int y, SDL_Rect* /* clip */)
void LAnimateTexture::render( int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip )
{
	//Render current frame
	SDL_Rect* currentClip = &spriteClips[ frame / 4 ];
	//gSpriteSheetTexture.render( ( SCREEN_WIDTH - currentClip->w ) / 2, ( SCREEN_HEIGHT - currentClip->h ) / 2, currentClip );
	LTexture::render(x, y, currentClip, angle, center, flip);

	//Go to next frame
	++frame;
	//Cycle animation
	if( frame / 4 >= WALKING_ANIMATION_FRAMES )
	{
		frame = 0;
	}
}


