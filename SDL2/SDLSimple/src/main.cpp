/*
 * main.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: Rodney Woollett
 */

//Using SDL and standard IO
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <cassert>
#include <cstdio>
#include <string>
#include <sstream>
#include <cmath>
#include "LTexture.h"
#include "LAnimateTexture.h"
#include "LButtonTexture.h"
#include "LButton.h"
#include "Timer.h"
#include "Dot.h"

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;
const int SCREEN_TICKS_PER_FRAME = 1000 / SCREEN_FPS;

//Key press surfaces constants
enum KeyPressSurfaces
{
	KEY_PRESS_SURFACE_DEFAULT,
	KEY_PRESS_SURFACE_UP,
	KEY_PRESS_SURFACE_DOWN,
	KEY_PRESS_SURFACE_LEFT,
	KEY_PRESS_SURFACE_RIGHT,
	KEY_PRESS_SURFACE_TOTAL
};
enum DrawMethod
{
	SURFACES, TEXTURES, GEOMETRY, VIEWPORT, COLORKEY, OPENGL
};
// Function prototypes
// Starts up SDL and create window
bool init();
// Loads media
bool loadMedia();
//Frees media and shuts down SDL
void close();

//Loads individual image
SDL_Surface* loadPNGSurface(std::string path);

//The window we'll be rendering to
SDL_Window* gWindow = NULL;
//The window renderer
SDL_Renderer* gRenderer = NULL;
//Globally used font
TTF_Font *gFont = NULL;
//Scene textures
LTexture* gFooTexture = NULL;
LTexture* gBackgroundTexture = NULL;
//Rendered texture
LTexture* gTextTexture = NULL;
LTexture* gSoundPromptTexture = NULL;
LTexture* gTimerPromptTexture = NULL;
LTexture* gTimeTextTexture = NULL;
LTexture* gFPSTextTexture = NULL;
//Scene sprites
LTexture* gSpriteSheetTexture = NULL; // LAnimateTexture - with 4 sprite images
//Current displayed texture
LTexture* gTexture = NULL;
//Button texture with sprites
LTexture* gButtonSpriteSheetTexture = NULL;
//Dot
LTexture* gDotTexture = NULL;
//The surface contained by the window
SDL_Surface* gScreenSurface = NULL;
//The images that correspond to a keypress
SDL_Surface* gKeyPressSurfaces[KEY_PRESS_SURFACE_TOTAL];
//Current displayed image
SDL_Surface* gCurrentSurface = NULL;

//The music that will be played
Mix_Music *gMusic = NULL;
//The sound effects that will be used
Mix_Chunk *gScratch = NULL;
Mix_Chunk *gHigh = NULL;
Mix_Chunk *gMedium = NULL;
Mix_Chunk *gLow = NULL;

// Buttons
const int TOTAL_BUTTONS = 4;
LButton* gButtons[TOTAL_BUTTONS];

// Main
int main(int, char **)
{

	DrawMethod drawMethod = COLORKEY;

	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Load media
		if (!loadMedia())
		{
			printf("Failed to load media!\n");
		}
		else
		{

			//Main loop flag
			bool quit = false;
			//Event handler
			SDL_Event e;

			//Modulation components
			Uint8 r = 255;
			Uint8 g = 255;
			Uint8 b = 255;
			//Alpha Modulation component
			Uint8 a = 255;

			//Angle of rotation
			double degrees = 0;
			//Flip type
			SDL_RendererFlip flipType = SDL_FLIP_NONE;

			//Set default current surface
			gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT];
			//Current rendered texture
			SDL_Texture* currentTexture = NULL;
			// A Dot
			Dot dot(gDotTexture);

			//Set text color as red
			SDL_Color textColor =
			{ 175, 0, 0, 255 };
			//The application timer
			EngineLayer::Timer timer;
			//In memory text stream
			std::stringstream timeText;

			EngineLayer::Timer fpsTimer;
			EngineLayer::Timer capTimer;
			//Start counting frames per second
			int countedFrames = 0;
			fpsTimer.start();

			//While application is running
			while (!quit)
			{
				//Start cap timer
				capTimer.start();

				//Handle events on queue
				while (SDL_PollEvent(&e) != 0)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
					//User presses return key
					else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN)
					{
						//NIY
					}
					//User presses a key
					else if (e.type == SDL_KEYDOWN)
					{
						//Select surfaces based on key press
						switch (e.key.keysym.sym)
						{
						//Play high sound effect
						case SDLK_1:
							Mix_PlayChannel(-1, gHigh, 0);
							break;
							//Play medium sound effect
						case SDLK_2:
							Mix_PlayChannel(-1, gMedium, 0);
							break;
							//Play low sound effect
						case SDLK_3:
							Mix_PlayChannel(-1, gLow, 0);
							break;
							//Play scratch sound effect
						case SDLK_4:
							Mix_PlayChannel(-1, gScratch, 0);
							break;

						case SDLK_9:
							//If there is no music playing
							if (Mix_PlayingMusic() == 0)
							{
								//Play the music
								Mix_PlayMusic(gMusic, -1);
							}
							//If music is being played
							else
							{
								//If the music is paused
								if (Mix_PausedMusic() == 1)
								{
									//Resume the music
									Mix_ResumeMusic();
								}
								//If the music is playing
								else
								{
									//Pause the music
									Mix_PauseMusic();
								}
							}
							break;
						case SDLK_0:
							//Stop the music
							Mix_HaltMusic();
							break;

						case SDLK_UP:
							gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_UP];
							break;
						case SDLK_DOWN:
							gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN];
							break;
						case SDLK_LEFT:
							gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT];
							break;
						case SDLK_RIGHT:
							gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_RIGHT];
							break;

							//Increase red
						case SDLK_q:
							r += 32;
							break;
							//Increase green
						case SDLK_w:
							g += 32;
							break;
							//Increase blue
						case SDLK_e:
							b += 32;
							break;
							//Decrease red
						case SDLK_a:
							r -= 32;
							break;
							//Decrease green
						case SDLK_s:
							g -= 32;

							// Timer start/stop
							if (timer.is_started())
							{
								timer.stop();
							}
							else
							{
								timer.start();
							}
							break;
							//Decrease blue
						case SDLK_d:
							b -= 32;
							// Pause/unpause
							if (timer.is_paused())
							{
								timer.unpause();
							}
							else
							{
								timer.pause();
							}
							break;

							//Surfaces render
						case SDLK_y:
							drawMethod = SURFACES;
							break;

							//Textures render
						case SDLK_u:
							drawMethod = TEXTURES;
							break;

							//Geometry render
						case SDLK_i:
							drawMethod = GEOMETRY;
							break;

							//View port render
						case SDLK_o:
							drawMethod = VIEWPORT;
							break;

							//Color key render
						case SDLK_p:
							drawMethod = COLORKEY;

							break;

							//Increase alpha on z
						case SDLK_z:
							//Cap if over 255
							if (a + 32 > 255)
							{
								a = 255;
							}
							//Increment otherwise
							else
							{
								a += 32;
							}
							break;

							//Increase alpha on x
						case SDLK_x:
							//Cap if below 0
							if (a - 32 < 0)
							{
								a = 0;
							}
							//Decrement otherwise
							else
							{
								a -= 32;
							}
							break;

						case SDLK_f:
							degrees -= 60;
							break;
						case SDLK_v:
							degrees += 60;
							break;
						case SDLK_g:
							flipType = SDL_FLIP_HORIZONTAL;
							break;
						case SDLK_h:
							flipType = SDL_FLIP_NONE;
							break;
						case SDLK_j:
							flipType = SDL_FLIP_VERTICAL;
							break;

						default:
							gCurrentSurface = gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT];
							break;
						}
					}

					// Mouse events on all buttons
					//Handle button events
					for (int i = 0; i < TOTAL_BUTTONS; ++i)
					{
						gButtons[i]->handleEvent(&e);
					}

					//Handle input for the dot
					dot.handleEvent( &e );

				} // End of poll event

				switch (drawMethod)
				{
				case TEXTURES:
				{
					const Uint8* currentKeyStates = SDL_GetKeyboardState( NULL);
					if (currentKeyStates[SDL_SCANCODE_UP])
					{
						currentTexture = SDL_CreateTextureFromSurface(gRenderer,
								gKeyPressSurfaces[KEY_PRESS_SURFACE_UP]);
					}
					else if (currentKeyStates[SDL_SCANCODE_DOWN])
					{
						currentTexture = SDL_CreateTextureFromSurface(gRenderer,
								gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN]);
					}
					else if (currentKeyStates[SDL_SCANCODE_LEFT])
					{
						currentTexture = SDL_CreateTextureFromSurface(gRenderer,
								gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT]);
					}
					else if (currentKeyStates[SDL_SCANCODE_RIGHT])
					{
						currentTexture = SDL_CreateTextureFromSurface(gRenderer,
								gKeyPressSurfaces[KEY_PRESS_SURFACE_RIGHT]);
					}
					else
					{
						currentTexture = SDL_CreateTextureFromSurface(gRenderer,
								gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT]);
					}
					// Clear the screen
					SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
					SDL_RenderClear(gRenderer);
					//Render texture to screen
					SDL_RenderCopy(gRenderer, currentTexture, NULL, NULL);

					//Update screen
					SDL_RenderPresent(gRenderer);
					SDL_DestroyTexture(currentTexture);

				}
					break;

				case SURFACES:
				{
					//Apply the image
					SDL_Rect stretchRect;
					stretchRect.x = 0;
					stretchRect.y = 0;
					stretchRect.w = SCREEN_WIDTH;
					stretchRect.h = SCREEN_HEIGHT;
					SDL_BlitScaled(gCurrentSurface, NULL, gScreenSurface, &stretchRect);

					// Update the surface
					SDL_UpdateWindowSurface(gWindow);
				}
					break;

				case GEOMETRY:
				{
					//Clear screen
					SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
					SDL_RenderClear(gRenderer);
					//Render red filled quad
					SDL_Rect fillRect =
					{ SCREEN_WIDTH / 4, SCREEN_HEIGHT / 4, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 };
					SDL_SetRenderDrawColor(gRenderer, 0xFF, 0x00, 0x00, 0xFF);
					SDL_RenderFillRect(gRenderer, &fillRect);
					//Render green outlined quad
					SDL_Rect outlineRect =
					{ SCREEN_WIDTH / 6, SCREEN_HEIGHT / 6, SCREEN_WIDTH * 2 / 3, SCREEN_HEIGHT * 2 / 3 };
					SDL_SetRenderDrawColor(gRenderer, 0x00, 0xFF, 0x00, 0xFF);
					SDL_RenderDrawRect(gRenderer, &outlineRect);
					//Draw blue horizontal line
					SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0xFF, 0xFF);
					SDL_RenderDrawLine(gRenderer, 0, SCREEN_HEIGHT / 2, SCREEN_WIDTH, SCREEN_HEIGHT / 2);
					//Draw vertical line of yellow dots
					SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0x00, 0xFF);
					for (int i = 0; i < SCREEN_HEIGHT; i += 4)
					{
						SDL_RenderDrawPoint(gRenderer, SCREEN_WIDTH / 2, i);
					}

					//Move the dot
					dot.move();
					//Render objects
					dot.render();

					//Update screen
					SDL_RenderPresent(gRenderer);
				}
					break;

				case VIEWPORT:
				{
					SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
					SDL_RenderClear(gRenderer);

					//Top left corner viewport
					SDL_Rect topLeftViewport;
					topLeftViewport.x = 0;
					topLeftViewport.y = 0;
					topLeftViewport.w = SCREEN_WIDTH / 2;
					topLeftViewport.h = SCREEN_HEIGHT / 2;
					SDL_RenderSetViewport(gRenderer, &topLeftViewport);
					//Render texture to screen
					gTexture->render(0, 0);

					//Top right viewport
					SDL_Rect topRightViewport;
					topRightViewport.x = SCREEN_WIDTH / 2;
					topRightViewport.y = 0;
					topRightViewport.w = SCREEN_WIDTH / 2;
					topRightViewport.h = SCREEN_HEIGHT / 2;
					SDL_RenderSetViewport(gRenderer, &topRightViewport);
					//Render texture to screen
					gTexture->render(0, 0);

					//Bottom viewport
					SDL_Rect bottomViewport;
					bottomViewport.x = 0;
					bottomViewport.y = SCREEN_HEIGHT / 2;
					bottomViewport.w = SCREEN_WIDTH;
					bottomViewport.h = SCREEN_HEIGHT / 2;
					SDL_RenderSetViewport(gRenderer, &bottomViewport);
					//Render texture to screen
					gTexture->render(0, 0);

					SDL_Rect fullViewport;
					fullViewport.x = 0;
					fullViewport.y = 0;
					fullViewport.w = SCREEN_WIDTH;
					fullViewport.h = SCREEN_HEIGHT;
					SDL_RenderSetViewport(gRenderer, &fullViewport);
					//Render current frame
					gTextTexture->render((SCREEN_WIDTH - gTextTexture->getWidth()) / 2,
							(SCREEN_HEIGHT - gTextTexture->getHeight()) / 2);

					//Set text to be rendered
					timeText.str("");
					timeText << "Seconds since start time " << (timer.get_ticks() / 1000.0f);
					//Render text
					if (!gTimeTextTexture->loadFromRenderedText(gFont, timeText.str().c_str(), textColor))
					{
						printf("Unable to render time texture!\n");
					}
					gTimerPromptTexture->render((SCREEN_WIDTH - gTimerPromptTexture->getWidth()) / 2,
							(50 + SCREEN_HEIGHT - gTimerPromptTexture->getHeight()) / 2);
					gTimeTextTexture->render((SCREEN_WIDTH - gTimeTextTexture->getWidth()) / 2,
							(75 + SCREEN_HEIGHT - gTimeTextTexture->getHeight()) / 2);

					//Calculate and correct fps
					float avgFPS = countedFrames / (fpsTimer.get_ticks() / 1000.f);
					if (avgFPS > 2000000)
					{
						avgFPS = 0;
					}
					//Set text to be rendered
					timeText.str("");
					timeText << "Average Frames Per Second (With Cap) " << avgFPS;
					//Render text
					if (!gFPSTextTexture->loadFromRenderedText(gFont, timeText.str().c_str(), textColor))
					{
						printf("Unable to render FPS texture!\n");
					}
					//Render textures
					gFPSTextTexture->render((SCREEN_WIDTH - gFPSTextTexture->getWidth()) / 2,
							(100 + SCREEN_HEIGHT - gFPSTextTexture->getHeight()) / 2);

					//Render buttons
					for (int i = 0; i < TOTAL_BUTTONS; ++i)
					{
						gButtons[i]->render();
					}

					//Move the dot
					dot.move();
					//Render objects
					dot.render();

					//Update screen
					SDL_RenderPresent(gRenderer);

				}
					break;

				case COLORKEY:
				{
					//Clear screen
					SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
					SDL_RenderClear(gRenderer);

					//Modulate and render texture
					gBackgroundTexture->setColor(r, g, b);

					//Render background blended
					gBackgroundTexture->setAlpha(a);

					//Render background texture to screen
					gBackgroundTexture->render(0, 0);

					//Render foo as an animation to the screen
					gSpriteSheetTexture->render(200, 140, NULL, degrees, NULL, flipType);
					//Render current frame
					gTextTexture->render((SCREEN_WIDTH - gTextTexture->getWidth()) / 2,
							(SCREEN_HEIGHT - gTextTexture->getHeight()) / 2);
					gSoundPromptTexture->render((SCREEN_WIDTH - gSoundPromptTexture->getWidth()) / 2,
							(50 + SCREEN_HEIGHT - gSoundPromptTexture->getHeight()) / 2);

					//Update screen
					SDL_RenderPresent(gRenderer);
				}
					break;

				default:
					/* No default */
					break;

				}

				++countedFrames;

				//If frame finished early
				int frameTicks = capTimer.get_ticks();
				if (frameTicks < SCREEN_TICKS_PER_FRAME)
				{
					//Wait remaining time
					SDL_Delay(SCREEN_TICKS_PER_FRAME - frameTicks);
				}

			} //While application is running
		} // LoadMedia
	} // Init
	//Free resources and close SDL
	close();

	return (0);
}

bool init()
{
// Initialize flag
	bool success = true;

// Initialize SDL
	if (SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		printf("Could not initialize SDL: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
// Create window SDL_WINDOW_OPENGL |
		gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Could not initialize Window: %s\n", SDL_GetError());
			success = false;
		}
		else
		{

			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (gRenderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				// Allocate screen textures
				gTexture = new LTexture(gRenderer);
				gFooTexture = new LTexture(gRenderer);
				gSpriteSheetTexture = new LAnimateTexture(gRenderer);
				gBackgroundTexture = new LTexture(gRenderer);
				gTextTexture = new LTexture(gRenderer);
				gSoundPromptTexture = new LTexture(gRenderer);
				gTimerPromptTexture = new LTexture(gRenderer);
				gTimeTextTexture = new LTexture(gRenderer);
				gFPSTextTexture = new LTexture(gRenderer);
				gButtonSpriteSheetTexture = new LButtonTexture(gRenderer);
				gDotTexture = new LTexture(gRenderer);

				// Initialize buttons
				gButtons[0] = new LButton(static_cast<LButtonTexture*>(gButtonSpriteSheetTexture));
				gButtons[1] = new LButton(static_cast<LButtonTexture*>(gButtonSpriteSheetTexture));
				gButtons[2] = new LButton(static_cast<LButtonTexture*>(gButtonSpriteSheetTexture));
				gButtons[3] = new LButton(static_cast<LButtonTexture*>(gButtonSpriteSheetTexture));

				gButtons[0]->setPosition(0, 0);
				gButtons[1]->setPosition(200, 0);
				gButtons[2]->setPosition(0, 210);
				gButtons[3]->setPosition(200, 210);

				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n",
					IMG_GetError());
					success = false;
				}
				else
				{
					//Initialize SDL_ttf
					if (TTF_Init() == -1)
					{
						printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
						success = false;
					}
					else
					{
						//Initialize SDL_mixer
						if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
						{
							printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
							success = false;
						}
						else
						{
							// Get Window surface
							gScreenSurface = SDL_GetWindowSurface(gWindow);
						}

					}
				}
			}
		}
	}
	return (success);
}

bool loadMedia()
{
// Loading success flag
	bool success = true;

//Load default surface
	gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] = loadPNGSurface("/home/rodney/rodney/resources/bg.png");
	if (gKeyPressSurfaces[KEY_PRESS_SURFACE_DEFAULT] == NULL)
	{
		printf("Failed to load default image!\n");
		success = false;
	}

//Load up surface
	gKeyPressSurfaces[KEY_PRESS_SURFACE_UP] = loadPNGSurface("/home/rodney/rodney/resources/up.PNG");
	if (gKeyPressSurfaces[KEY_PRESS_SURFACE_UP] == NULL)
	{
		printf("Failed to load up image!\n");
		success = false;
	}

//Load down surface
	gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN] = loadPNGSurface("/home/rodney/rodney/resources/down.PNG");
	if (gKeyPressSurfaces[KEY_PRESS_SURFACE_DOWN] == NULL)
	{
		printf("Failed to load down image!\n");
		success = false;
	}

//Load left surface
	gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT] = loadPNGSurface("/home/rodney/rodney/resources/left.PNG");
	if (gKeyPressSurfaces[KEY_PRESS_SURFACE_LEFT] == NULL)
	{
		printf("Failed to load left image!\n");
		success = false;
	}

//Load right surface
	gKeyPressSurfaces[KEY_PRESS_SURFACE_RIGHT] = loadPNGSurface("/home/rodney/rodney/resources/right.PNG");
	if (gKeyPressSurfaces[KEY_PRESS_SURFACE_RIGHT] == NULL)
	{
		printf("Failed to load up image!\n");
		success = false;
	}

//Load PNG textures
// Load main texture
	if (!gTexture->loadFromFile("/home/rodney/rodney/resources/gears.png"))
	{
		printf("Failed to load texture image!\n");
		success = false;
	}

//Load Foo' texture
	if (!gFooTexture->loadFromFile("/home/rodney/rodney/resources/foo2.png"))
	{
		printf("Failed to load Foo' texture image!\n");
		success = false;
	}
//Load background texture
	if (!gBackgroundTexture->loadFromFile("/home/rodney/rodney/resources/background.png"))
	{
		printf("Failed to load background texture image!\n");
		success = false;
	}
	else
	{
//Set standard alpha blending
		gBackgroundTexture->setBlendMode(SDL_BLENDMODE_BLEND);
	}

//Load sprite sheet texture
	if (!gSpriteSheetTexture->loadFromFile("/home/rodney/rodney/resources/foo.png"))
	{
		printf("Failed to load walking animation texture!\n");
		success = false;
	}

	if (!gButtonSpriteSheetTexture->loadFromFile("/home/rodney/rodney/resources/button.PNG"))
	{
		printf("Failed to load button sheet texture!\n");
		success = false;
	}
	if (!gDotTexture->loadFromFile("/home/rodney/rodney/resources/dot.bmp"))
	{
		printf("Failed to load dot texture!\n");
		success = false;
	}
//Open the font
	gFont = TTF_OpenFont("/home/rodney/rodney/resources/LSANS.TTF", 10);
	if (gFont == NULL)
	{
		printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		success = false;
	}
	else
	{
//Render text
		SDL_Color textColor =
		{ 0, 0, 0 };
		if (!gTextTexture->loadFromRenderedText(gFont, "The quick brown fox jumps over the lazy dog", textColor))
		{
			printf("Failed to render text texture!\n");
			success = false;
		}
		if (!gSoundPromptTexture->loadFromRenderedText(gFont,
				"Press 1, 2, 3, or 4 to play a sound effect. Press 9 to play/pause music. Press 0 to stop.", textColor))
		{
			printf("Failed to render sound prompt text texture!\n");
			success = false;
		}
		textColor =
		{	225, 0, 0};
		if (!gTimerPromptTexture->loadFromRenderedText(gFont,
				"Press s to start/stop, press d to pause/unpause the timer.", textColor))
		{
			printf("Failed to render sound prompt text texture!\n");
			success = false;
		}
	}

//Load music
	gMusic = Mix_LoadMUS("/home/rodney/rodney/resources/beat.wav");
	if (gMusic == NULL)
	{
		printf("Failed to load beat music! SDL_mixer Error: %s\n", Mix_GetError());
		success = false;
	}
//Load sound effects
	gScratch = Mix_LoadWAV("/home/rodney/rodney/resources/scratch.wav");
	if (gScratch == NULL)
	{
		printf("Failed to load scratch sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		success = false;
	}
	gHigh = Mix_LoadWAV("/home/rodney/rodney/resources/high.wav");
	if (gHigh == NULL)
	{
		printf("Failed to load high sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		success = false;
	}
	gMedium = Mix_LoadWAV("/home/rodney/rodney/resources/medium.wav");
	if (gMedium == NULL)
	{
		printf("Failed to load medium sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		success = false;
	}
	gLow = Mix_LoadWAV("/home/rodney/rodney/resources/low.wav");
	if (gLow == NULL)
	{
		printf("Failed to load low sound effect! SDL_mixer Error: %s\n", Mix_GetError());
		success = false;
	}

	return (success);
}

void close()
{
//Deallocate surfaces KEY_PRESS_SURFACE_TOTAL
	for (unsigned int index = 0; index < KEY_PRESS_SURFACE_TOTAL; ++index)
	{
		assert(index >= 0);
		assert(index < sizeof(gKeyPressSurfaces) / sizeof(gKeyPressSurfaces[0]));
		SDL_FreeSurface(gKeyPressSurfaces[index]);
		gKeyPressSurfaces[index] = NULL;
	}

// Deallocate buttons
	for (unsigned int index = 0; index < TOTAL_BUTTONS; ++index)
	{
		assert(index >= 0);
		assert(index < sizeof(gButtons) / sizeof(gButtons[0]));
		delete gButtons[index];
		gButtons[index] = NULL;
	}

	TTF_CloseFont(gFont);
	gFont = NULL;

//Free the sound effects
	Mix_FreeChunk(gScratch);
	Mix_FreeChunk(gHigh);
	Mix_FreeChunk(gMedium);
	Mix_FreeChunk(gLow);
	gScratch = NULL;
	gHigh = NULL;
	gMedium = NULL;
	gLow = NULL;

//Free the music
	Mix_FreeMusic(gMusic);
	gMusic = NULL;

//Free loaded images
	gTexture->free();
	gFooTexture->free();
	gSpriteSheetTexture->free();
	gBackgroundTexture->free();
	gTextTexture->free();
	gSoundPromptTexture->free();
	gTimerPromptTexture->free();
	gTimeTextTexture->free();
	gFPSTextTexture->free();
	gButtonSpriteSheetTexture->free();
	gDotTexture->free();
	delete gTexture;
	delete gFooTexture;
	delete gSpriteSheetTexture;
	delete gButtonSpriteSheetTexture;
	delete gBackgroundTexture;
	delete gTextTexture;
	delete gDotTexture;
	delete gSoundPromptTexture;
	delete gTimerPromptTexture;
	delete gTimeTextTexture;
	delete gFPSTextTexture;

//Destroy window
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gTexture = NULL;
	gFooTexture = NULL;
	gSpriteSheetTexture = NULL;
	gButtonSpriteSheetTexture = NULL;
	gDotTexture = NULL;
	gBackgroundTexture = NULL;
	gTextTexture = NULL;
	gSoundPromptTexture = NULL;
	gTimerPromptTexture = NULL;
	gTimeTextTexture = NULL;
	gFPSTextTexture = NULL;
	gWindow = NULL;
	gRenderer = NULL;

//Quit SDL subsystems
	Mix_Quit();
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

SDL_Surface* loadPNGSurface(std::string path)
{
//The final optimized image
	SDL_Surface* optimizedSurface = NULL;
//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(),
		IMG_GetError());
	}
	else
	{
//Convert surface to screen format
		optimizedSurface = SDL_ConvertSurface(loadedSurface, gScreenSurface->format, SDL_RLEACCEL);
		if (optimizedSurface == NULL)
		{
			printf("Unable to optimize image %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}
//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}
	return (optimizedSurface);
}
