/*
 * LButton.h
 *
 *  Created on: Mar 28, 2015
 *      Author: Rodney Woollett
 */

#ifndef DOT_H_
#define DOT_H_

extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

#include "LTexture.h"
#include "Circle.h"
#include <vector>

class Dot
{
public:
	//The dimensions of the dot
	static const int DOT_WIDTH = 30;
	static const int DOT_HEIGHT = 30;
	//Maximum axis velocity of the dot static
	const int DOT_VEL = 10;

	//Initializes variables
	Dot(LTexture* dotTexture, int x, int y);

	// Use default copy constructor Dot(const Dot& oldDot);

	//Deallocates memory
	virtual ~Dot();

	//Handles mouse event
	void handleEvent(SDL_Event* e);

#ifdef CIRCLE_COLLISION
	//Moves the dot and checks collision
	void move( SDL_Rect& square, Circle& circle );
	//Gets collision circle
	Circle& getCollider();
#endif /* CIRCLE_COLLISION */

	//Moves the dot and checks collision
	void move(std::vector<SDL_Rect>& otherColliders);
	//Shows button sprite
	void render();
	//Gets the collision boxes
	std::vector<SDL_Rect>& getColliders();

private:
	//The X and Y offsets of the dot
	int mPosX, mPosY;
	//The velocity of the dot
	int mVelX, mVelY;

	//Currently used global sprite
	LTexture* mTexture;

	//Dot's collision boxes
	std::vector<SDL_Rect> mColliders;

#ifdef CIRCLE_COLLISION
	//Dot's collision circle
	Circle mCollider;
#endif /* CIRCLE_COLLISION */

	//Moves the collision boxes relative to the dot's offset
	void shiftColliders();
};

#endif /* DOT_H_ */
