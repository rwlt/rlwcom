/*
 * Timer.h
 *
 *      Author: Lazy Foo(lazyfoo.net)
 */

#ifndef TIMER_CPP_
#define TIMER_CPP_
namespace EngineLayer
{
//this timer can be used for animation, fps regulation and other timed events.
class Timer
{
private:
	//The clock time when the timer started
	int startTicks;

	//The ticks stored when the timer was paused
	int pausedTicks;

	//The timer status
	bool paused;
	bool started;

public:
	//Initializes variables
	Timer();

	//The various clock actions
	void start(); //starts the timer, also resets the it if alreaty started.
	void stop();
	void pause(); //dose not reset like stop dose
	void unpause();

	//Gets the timer's time
	int get_ticks(); //returns in miliseconds

	//Checks the status of the timer
	bool is_started();
	bool is_paused();
};
}
#endif /* TIMER_CPP_ */
