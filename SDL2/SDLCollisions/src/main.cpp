/*
 * main.cpp
 *
 *  Created on: Mar 23, 2015
 *      Author: Rodney Woollett
 */

//Using SDL and standard IO
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <cassert>
#include <cstdio>
#include <string>
#include <sstream>
#include <cmath>
#include <vector>
#include "LTexture.h"
#include "Timer.h"
#include "Dot.h"

//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_FPS = 60;
const int SCREEN_TICKS_PER_FRAME = 1000 / SCREEN_FPS;

//Key press surfaces constants
enum KeyPressSurfaces
{
	KEY_PRESS_SURFACE_DEFAULT,
	KEY_PRESS_SURFACE_UP,
	KEY_PRESS_SURFACE_DOWN,
	KEY_PRESS_SURFACE_LEFT,
	KEY_PRESS_SURFACE_RIGHT,
	KEY_PRESS_SURFACE_TOTAL
};
enum DrawMethod
{
	SURFACES, TEXTURES, GEOMETRY, VIEWPORT, COLORKEY, OPENGL
};
// Function prototypes
// Starts up SDL and create window
bool init();
// Loads media
bool loadMedia();
//Frees media and shuts down SDL
void close();
//Box collision detector
bool checkCollision(SDL_Rect a, SDL_Rect b);
//Box set collision detector
bool checkBoxSetCollision(std::vector<SDL_Rect>& a, std::vector<SDL_Rect>& b);
//Circle/Circle collision detector
bool checkCircleCollision(Circle& a, Circle& b);
//Circle/Box collision detector
bool checkCircleCollision(Circle& a, SDL_Rect& b);
//Calculates distance squared between two points
double distanceSquared(int x1, int y1, int x2, int y2)
{
	int deltaX = x2 - x1;
	int deltaY = y2 - y1;
	return (deltaX * deltaX + deltaY * deltaY);
}

//The window we'll be rendering to
SDL_Window* gWindow = NULL;
//The window renderer
SDL_Renderer* gRenderer = NULL;
//Globally used font
TTF_Font *gFont = NULL;
//Scene textures
LTexture* gFPSTextTexture = NULL;
////Button texture with sprites
//Dot
LTexture* gDotTexture = NULL;

// Main
int main(int, char **)
{

	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}
	else
	{
		//Load media
		if (!loadMedia())
		{
			printf("Failed to load media!\n");
		}
		else
		{

			//Main loop flag
			bool quit = false;
			//Event handler
			SDL_Event e;
			// A Dot
			Dot dot(gDotTexture, Dot::DOT_WIDTH / 2, Dot::DOT_HEIGHT / 2);

			//The dot that will be collided against
			Dot otherDot(gDotTexture, SCREEN_WIDTH / 4, SCREEN_HEIGHT / 4);

			//Set text color as red
			SDL_Color textColor =
			{ 175, 0, 0, 255 };

			//The application timer
			EngineLayer::Timer timer;
			//In memory text stream
			std::stringstream timeText;

			EngineLayer::Timer fpsTimer;
			EngineLayer::Timer capTimer;

			//Start counting frames per second
			int countedFrames = 0;
			fpsTimer.start();

			SDL_Rect wall;
			wall.x = 300;
			wall.y = 40;
			wall.w = 40;
			wall.h = 400;

			//While application is running
			while (!quit)
			{
				//Start cap timer
				capTimer.start();

				//Handle events on queue
				while (SDL_PollEvent(&e) != 0)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
					//User presses return key
					else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN)
					{
						//NIY
					}
					//Handle input for the dot
					dot.handleEvent(&e);

				} // End of poll event

				//Move the dot
				dot.move(wall, otherDot.getCollider());

				//Clear screen
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
				SDL_RenderClear(gRenderer);

				//Render yellow filled quad
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0x00, 0xFF);
				SDL_RenderFillRect(gRenderer, &wall);

				//Calculate and correct fps
				float avgFPS = countedFrames / (fpsTimer.get_ticks() / 1000.f);
				if (avgFPS > 2000000)
				{
					avgFPS = 0;
				}
				//Set text to be rendered
				timeText.str("");
				timeText << "Average Frames Per Second (With Cap) " << avgFPS;
				//Render text
				if (!gFPSTextTexture->loadFromRenderedText(gFont, timeText.str().c_str(), textColor))
				{
					printf("Unable to render FPS texture!\n");
				}
				//Render textures
				gFPSTextTexture->render((SCREEN_WIDTH - gFPSTextTexture->getWidth()) / 2,
						(100 + SCREEN_HEIGHT - gFPSTextTexture->getHeight()) / 2);

				//Render objects
				dot.render();
				otherDot.render();

				//Update screen
				SDL_RenderPresent(gRenderer);

				++countedFrames;

				//If frame finished early
				int frameTicks = capTimer.get_ticks();
				if (frameTicks < SCREEN_TICKS_PER_FRAME)
				{
					//Wait remaining time
					SDL_Delay(SCREEN_TICKS_PER_FRAME - frameTicks);
				}

			} //While application is running
		} // LoadMedia
	} // Init
	  //Free resources and close SDL
	close();

	return (0);
}

bool init()
{
	// Initialize flag
	bool success = true;

	// Initialize SDL
	if (SDL_Init( SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		printf("Could not initialize SDL: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		// Create window SDL_WINDOW_OPENGL |
		gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("Could not initialize Window: %s\n", SDL_GetError());
			success = false;
		}
		else
		{

			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if (gRenderer == NULL)
			{
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				// Allocate screen textures
				gFPSTextTexture = new LTexture(gRenderer);
				gDotTexture = new LTexture(gRenderer);

				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags))
				{
					printf("SDL_image could not initialize! SDL_image Error: %s\n",
					IMG_GetError());
					success = false;
				}
				else
				{
					//Initialize SDL_ttf
					if (TTF_Init() == -1)
					{
						printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
						success = false;
					}
				}
			}
		}
	}
	return (success);
}

bool loadMedia()
{
	// Loading success flag
	bool success = true;

	if (!gDotTexture->loadFromFile("D:/app/resources/dot.png"))
	{
		printf("Failed to load dot texture!\n");
		success = false;
	}
	//Open the font
	gFont = TTF_OpenFont("D:/app/resources/LSANS.TTF", 10);
	if (gFont == NULL)
	{
		printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		success = false;
	}
	return (success);
}

void close()
{
	//Deallocate surfaces KEY_PRESS_SURFACE_TOTAL
	TTF_CloseFont(gFont);
	gFont = NULL;

	//Free loaded images
	gFPSTextTexture->free();
	delete gFPSTextTexture;
	gFPSTextTexture = NULL;

	gDotTexture->free();
	delete gDotTexture;
	gDotTexture = NULL;

	//Destroy window
	SDL_DestroyRenderer(gRenderer);
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	gRenderer = NULL;

	//Quit SDL subsystems
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

bool checkCollision(SDL_Rect a, SDL_Rect b)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;
	//Calculate the sides of rect A
	leftA = a.x;
	rightA = a.x + a.w;
	topA = a.y;
	bottomA = a.y + a.h;
	//Calculate the sides of rect B
	leftB = b.x;
	rightB = b.x + b.w;
	topB = b.y;
	bottomB = b.y + b.h;

	//If any of the sides from A are outside of B
	if (bottomA <= topB)
	{
		return (false);
	}
	if (topA >= bottomB)
	{
		return (false);
	}
	if (rightA <= leftB)
	{
		return (false);
	}
	if (leftA >= rightB)
	{
		return (false);
	}
	//If none of the sides from A are outside B
	return (true);
}

bool checkBoxSetCollision(std::vector<SDL_Rect>& a, std::vector<SDL_Rect>& b)
{
	//The sides of the rectangles
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	//Go through the A boxes
	for (unsigned int Abox = 0; Abox < a.size(); Abox++)
	{
		//Calculate the sides of rect A
		leftA = a[Abox].x;
		rightA = a[Abox].x + a[Abox].w;
		topA = a[Abox].y;
		bottomA = a[Abox].y + a[Abox].h;
		//Go through the B boxes
		for (unsigned int Bbox = 0; Bbox < b.size(); Bbox++)
		{
			//Calculate the sides of rect B
			leftB = b[Bbox].x;
			rightB = b[Bbox].x + b[Bbox].w;
			topB = b[Bbox].y;
			bottomB = b[Bbox].y + b[Bbox].h;
			//If no sides from A are outside of B
			if (((bottomA <= topB) || (topA >= bottomB) || (rightA <= leftB) || (leftA >= rightB)) == false)
			{
				//A collision is detected
				return (true);
			}
		}
	}
	//If neither set of collision boxes touched
	return (false);
}
//Circle/Circle collision detector
bool checkCircleCollision(Circle& a, Circle& b)
{
	//Calculate total radius squared
	int totalRadiusSquared = a.r + b.r;
	totalRadiusSquared = totalRadiusSquared * totalRadiusSquared;
	//If the distance between the centers of the circles is less than the sum of their radii
	if (distanceSquared(a.x, a.y, b.x, b.y) < (totalRadiusSquared))
	{
		//The circles have collided
		return (true);
	}
	//If not
	return (false);
}

//Circle/Box collision detector
bool checkCircleCollision(Circle& a, SDL_Rect& b)
{
	//Closest point on collision box
	int cX, cY;
	//Find closest x offset
	if (a.x < b.x)
	{
		cX = b.x;
	}
	else if (a.x > b.x + b.w)
	{
		cX = b.x + b.w;
	}
	else
	{
		cX = a.x;
	}
	//Find closest y offset
	if (a.y < b.y)
	{
		cY = b.y;
	}
	else if (a.y > b.y + b.h)
	{
		cY = b.y + b.h;
	}
	else
	{
		cY = a.y;
	}
	//If the closest point is inside the circle
	if (distanceSquared(a.x, a.y, cX, cY) < a.r * a.r)
	{
		//This box and the circle have collided
		return (true);
	}
	//If the shapes have not collided
	return (false);
}
