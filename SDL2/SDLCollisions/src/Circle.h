/*
 * Circle.h
 *
 *  Created on: Apr 2, 2015
 *      Author: Rodney Woollett
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_

//A circle stucture
struct Circle
{
	int x, y;
	int r;
};


#endif /* CIRCLE_H_ */
