/*
 * Dot.cpp
 *
 *  Created on: Mar 28, 2015
 *      Author: Rodney Woollett
 */

#include <SDL.h>
#include <vector>
#include "Dot.h"

extern bool checkBoxSetCollision(std::vector<SDL_Rect>& a, std::vector<SDL_Rect>& b);
extern bool checkCircleCollision(Circle& a, Circle& b);
extern bool checkCircleCollision(Circle& a, SDL_Rect& b);

Dot::Dot(LTexture* dotTexture, int x, int y)
{
	//Initialize the offsets
	mPosX = x;
	mPosY = y;
	//Initialize the velocity
	mVelX = 0;
	mVelY = 0;
	// Init the texture to render
	mTexture = dotTexture;
	//Create the necessary SDL_Rects
	mColliders.resize(19);
	//Initialize the collision boxes' width and height
	mColliders[0].w = 6;
	mColliders[0].h = 1;
	mColliders[1].w = 12;
	mColliders[1].h = 1;
	mColliders[2].w = 16;
	mColliders[2].h = 1;
	mColliders[3].w = 18;
	mColliders[3].h = 1;
	mColliders[4].w = 20;
	mColliders[4].h = 1;
	mColliders[5].w = 22;
	mColliders[5].h = 1;
	mColliders[6].w = 24;
	mColliders[6].h = 1;
	mColliders[7].w = 26;
	mColliders[7].h = 2;
	mColliders[8].w = 28;
	mColliders[8].h = 3;
	mColliders[9].w = 30;
	mColliders[9].h = 6;
	mColliders[10].w = 28;
	mColliders[10].h = 3;
	mColliders[11].w = 26;
	mColliders[11].h = 2;
	mColliders[12].w = 24;
	mColliders[12].h = 1;
	mColliders[13].w = 22;
	mColliders[13].h = 1;
	mColliders[14].w = 20;
	mColliders[14].h = 1;
	mColliders[15].w = 18;
	mColliders[15].h = 1;
	mColliders[16].w = 16;
	mColliders[16].h = 1;
	mColliders[17].w = 12;
	mColliders[17].h = 1;
	mColliders[18].w = 6;
	mColliders[18].h = 1;

#ifdef CIRCLE_COLLISION
	//Set collision circle size
	mCollider.r = DOT_WIDTH / 2;
#endif /* CIRCLE_COLLISION */

	//Initialize colliders relative to position
	shiftColliders();
}
Dot::~Dot()
{
	//Deallocate
}

void Dot::handleEvent(SDL_Event* e)
{
	// Lesson try to base the velocity on per second rather than 10 each frame
	//in this tutorial we're basing the velocity as amount moved per frame.
	//In most games, the velocity is done per second. The reason were doing it per
	//frame is that it is easier, but if you know physics it shouldn't be hard to
	//update the dots position based on time. Need know physics.
	//If a key was pressed
	if (e->type == SDL_KEYDOWN && e->key.repeat == 0)
	{
		//Adjust the velocity
		switch (e->key.keysym.sym)
		{
		case SDLK_UP:
			mVelY -= DOT_VEL;
			break;
		case SDLK_DOWN:
			mVelY += DOT_VEL;
			break;
		case SDLK_LEFT:
			mVelX -= DOT_VEL;
			break;
		case SDLK_RIGHT:
			mVelX += DOT_VEL;
			break;
		}
	}
	//If a key was released else
	if (e->type == SDL_KEYUP && e->key.repeat == 0)
	{
		//Adjust the velocity
		switch (e->key.keysym.sym)
		{
		case SDLK_UP:
			mVelY += DOT_VEL;
			break;
		case SDLK_DOWN:
			mVelY -= DOT_VEL;
			break;
		case SDLK_LEFT:
			mVelX += DOT_VEL;
			break;
		case SDLK_RIGHT:
			mVelX -= DOT_VEL;
			break;
		}
	}
}

void Dot::move(std::vector<SDL_Rect>& otherColliders)
{
	//Move the dot left or right
	mPosX += mVelX;
	shiftColliders();

	//If the dot went too far to the left or right
	if ((mPosX < 0) || (mPosX + DOT_WIDTH > SCREEN_WIDTH) || checkBoxSetCollision(mColliders, otherColliders))
	{
		//Move back
		mPosX -= mVelX;
		shiftColliders();
	}

	//Move the dot up or down
	mPosY += mVelY;
	shiftColliders();
	//If the dot went too far up or down
	if ((mPosY < 0) || (mPosY + DOT_HEIGHT > SCREEN_HEIGHT) || checkBoxSetCollision(mColliders, otherColliders))
	{
		//Move back
		mPosY -= mVelY;
		shiftColliders();
	}
}

#ifdef CIRCLE_COLLISION
void Dot::move(SDL_Rect& square, Circle& circle)
{
	//Move the dot left or right
	mPosX += mVelX;
	shiftColliders();

	//If the dot collided or went too far to the left or right
	if ((mPosX - mCollider.r < 0) || (mPosX + mCollider.r > SCREEN_WIDTH) || checkCircleCollision(mCollider, square)
			|| checkCircleCollision(mCollider, circle))
	{
		//Move back
		mPosX -= mVelX;
		shiftColliders();
	}

	//Move the dot up or down
	mPosY += mVelY;
	shiftColliders();
	//If the dot collided or went too far up or down
	if ((mPosY - mCollider.r < 0) || (mPosY + mCollider.r > SCREEN_HEIGHT) || checkCircleCollision(mCollider, square)
			|| checkCircleCollision(mCollider, circle))
	{
		//Move back
		mPosY -= mVelY;
		shiftColliders();
	}

}

Circle& Dot::getCollider()
{
	return (mCollider);
}

#endif /* CIRCLE_COLLISION */

void Dot::shiftColliders()
{
#ifdef CIRCLE_COLLISION
	//Show the dot
	mCollider.x = mPosX;
	mCollider.y = mPosY;
#else
	//The row offset
	int r = 0;
	//Go through the dot's collision boxes
	for (unsigned int set = 0; set < mColliders.size(); ++set)
	{
		//Center the collision box
		mColliders[set].x = mPosX + (DOT_WIDTH - mColliders[set].w) / 2;
		//Set the collision box at its row offset
		mColliders[set].y = mPosY + r;
		//Move the row offset down the height of the collision box
		r += mColliders[set].h;
	}
#endif
}

std::vector<SDL_Rect> &Dot::getColliders()
{
	return (mColliders);
}

void Dot::render()
{
#ifdef CIRCLE_COLLISION
	//Show the dot
	mTexture->render(mPosX - mCollider.r, mPosY - mCollider.r);
#else
	mTexture->render(mPosX, mPosY);
#endif /* CIRCLE_COLLISION */
}

