/*
 * Main.cpp
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#include <windows.h>
#include <commctrl.h>
#include "resource.h"
#include <iostream>
#include <cstring>
#include "app/WindowClass.h"
#include "app/MainWindowView.h"
#include "app/components/DockWnd.h"


//
///////////////////////////////////////////////////////////////////////////////
// WIN32 WinMain
///////////////////////////////////////////////////////////////////////////////
//
INT APIENTRY /*WINAPI*/WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst,
		LPSTR lpCmdLine, int nCmdShow)
{
	MSG Msg;
	HACCEL hAccelTable;

	//Initialize the Common controls
	INITCOMMONCONTROLSEX initCtrlEx;
	initCtrlEx.dwSize = sizeof(INITCOMMONCONTROLSEX);
	initCtrlEx.dwICC = ICC_TAB_CLASSES | ICC_BAR_CLASSES | ICC_COOL_CLASSES;
	InitCommonControlsEx(&initCtrlEx);

	// Use MainWindow as the application window
	// Create controller with a viewFactory to use with Module Controller
	// Create the view factory
	App::WindowClass windowsClass;
	App::WindowClass dockWndClass;
	App::MainWindowView mainView;// = new App::MainWindowView();

	windowsClass.Create(hInstance, "Win32App", App::MainWindowView::MainWndProc);
	windowsClass.Register();
	dockWndClass.CreateDockWnd(hInstance, "Win32DockWnd", App::DockWnd::DockWndProc);
	dockWndClass.Register();

	bool created = mainView.createView(hInstance, "Win32App");
	if (!created) {
		std::stringstream message;
		message
				<< "Implementation not made MainWindow" << std::endl;
		MessageBox(nullptr, message.str().c_str(), "Notice",
		MB_OK | MB_ICONINFORMATION);
		PostQuitMessage(WM_QUIT);

	} else {
	    mainView.showView();
	}

	hAccelTable = LoadAccelerators(hInstance,
	MAKEINTRESOURCE(IDR_ACCELERATOR1));
	//Process the main windows messages
	while (GetMessage(&Msg, NULL, 0, 0) > 0)
	{
//		if (!(viewFactory.checkTranslateAccelerator(hAccelTable, &Msg)))
//			if (!(viewFactory.checkDialogMessage(&Msg)))
//			{
				TranslateMessage(&Msg);
				DispatchMessage(&Msg);
//			}
	}

	DestroyWindow(mainView.windowHandle());
	windowsClass.UnRegister();
	dockWndClass.UnRegister();

	return (0);

}

