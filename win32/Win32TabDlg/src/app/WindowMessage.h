/*
 * WindowMessage.h
 *
 *  Created on: May 4, 2015
 *      Author: Rodney Woollett
 */

#ifndef WINDOWMESSAGE_H_
#define WINDOWMESSAGE_H_

#include <windows.h>
namespace App
{

enum ParameterKind {
	LPARAMETER = 1,
	WPARAMLPARAM = 2,
	THREE_ARG = 3
};

struct LParameter {
	LPARAM lparam;
};
struct WParamLParam {
	WPARAM wparam;
	LPARAM lparma;
};
struct ThreeArgs {
	unsigned int valueA;
	unsigned int valueB;
	unsigned int valueC;
};
//struct ArrayArgs {
//	Module::View* array[];
//};
struct Parameter {
	ParameterKind kind;
	union Args {
		LParameter lParameter;
		WParamLParam wParamLParam;
		ThreeArgs threeArgs;
	} args;
};
class WindowMessage
{
public:
	WindowMessage();
	virtual ~WindowMessage();
	Parameter::Args argumments() const;
	ParameterKind  kind() const;
	void setParameter(Parameter newParam);
private:
	Parameter::Args parameterArgs;
	ParameterKind parameterKind;
};

} /* namespace App */

#endif /* WINDOWMESSAGE_H_ */
