/*
 * AppCommand.h
 *
 *  Created on: Apr 29, 2015
 *      Author: Rodney Woollett
 */

#ifndef APPCOMMAND_H_
#define APPCOMMAND_H_

#include "WindowMessage.h"

namespace App
{

class Command
{
public:
	Command();
	virtual ~Command();

	virtual long execute(const App::WindowMessage &parameters) = 0;

};

} /* namespace App */

#endif /* APPCOMMAND_H_ */
