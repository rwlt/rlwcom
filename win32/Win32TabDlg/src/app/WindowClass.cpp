#include "WindowClass.h"
#include "../resource.h"
#include "app/components/ListView.h"
namespace App
{
//-------------------------------------------------------------------------------
WindowClass::WindowClass()
{
}

//-------------------------------------------------------------------------------
// A Main windows class
//-------------------------------------------------------------------------------
void WindowClass::Create(HINSTANCE hInst, const char *ClassName,
		WNDPROC WndPrc, const char* MenuName)
{
	//Initializing the application using the application member variable
	CommonClass(WndPrc, ClassName, hInst);

	//CS_VREDRAW | CS_HREDRAW |
	_WndClsEx.style = CS_DBLCLKS;

	_WndClsEx.hIcon = static_cast<HICON>(LoadImage(hInst,
	MAKEINTRESOURCE(IDI_RAINDROP),
	IMAGE_ICON, 32, 32,
	LR_DEFAULTSIZE));
	_WndClsEx.hIconSm = static_cast<HICON>(LoadImage(hInst,
	MAKEINTRESOURCE(IDI_RAINDROP),
	IMAGE_ICON, 16, 16,
	LR_DEFAULTSIZE));
	_WndClsEx.hCursor = LoadCursor(NULL, IDC_ARROW);
	_WndClsEx.hbrBackground = NULL; //static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
	_WndClsEx.lpszMenuName = MAKEINTRESOURCE(IDR_MAIN_MENU);

	_WndClsEx.cbWndExtra = 0;

}

//-------------------------------------------------------------------------------
// A Window class for a splitter window
//-------------------------------------------------------------------------------
void WindowClass::CreateSplitter(HINSTANCE hInst, const char *ClassName,
		WNDPROC WndPrc, const char* MenuName)
{
	//Initializing the application using the application member variable
	CommonClass(WndPrc, ClassName, hInst);

	_WndClsEx.style = 0;
	_WndClsEx.hIcon = 0;
	_WndClsEx.hIconSm = 0;
	_WndClsEx.hCursor = LoadCursor(0, IDC_SIZENS);
	_WndClsEx.hbrBackground = (HBRUSH) (COLOR_BTNFACE + 1);
	_WndClsEx.lpszMenuName = 0;

	_WndClsEx.cbWndExtra = 0;

}

//-------------------------------------------------------------------------------
// A Window class for a popup window
//-------------------------------------------------------------------------------
void WindowClass::CreateDialogPopup(HINSTANCE hInst, const char *ClassName,
		WNDPROC WndPrc, const char* MenuName)
{
	//Initializing the application using the application member variable
	CommonClass(WndPrc, ClassName, hInst);
	_WndClsEx.style = 0;
	_WndClsEx.hIcon = 0;
	_WndClsEx.hIconSm = 0;
	_WndClsEx.hCursor = LoadCursor(0, IDC_ARROW);
	_WndClsEx.hbrBackground = (HBRUSH) (COLOR_BTNFACE + 1);
	_WndClsEx.lpszMenuName = 0;

	_WndClsEx.cbWndExtra = DLGWINDOWEXTRA;

}

//-------------------------------------------------------------------------------
// A Window class for a dock window
//-------------------------------------------------------------------------------
void WindowClass::CreateDockWnd(HINSTANCE hInst, const char *ClassName,
		WNDPROC WndPrc, const char* MenuName)
{
	//Initializing the application using the application member variable
	CommonClass(WndPrc, ClassName, hInst);
	_WndClsEx.style = 0;
	_WndClsEx.hIcon = 0;
	_WndClsEx.hIconSm = 0;
	_WndClsEx.hCursor = LoadCursor(0, IDC_ARROW);
	_WndClsEx.hbrBackground = (HBRUSH) (COLOR_BTNFACE + 1);
	_WndClsEx.lpszMenuName = 0;

	_WndClsEx.cbWndExtra = 0;

}

//-------------------------------------------------------------------------------
// Common windows class initialize
//-------------------------------------------------------------------------------
void WindowClass::CommonClass(WNDPROC WndPrc, const char* ClassName,
		HINSTANCE hInst)
{
	//Initializing the application using the application member variable
	_WndClsEx.cbSize = sizeof(WNDCLASSEX);
	_WndClsEx.lpfnWndProc = WndPrc;
	_WndClsEx.cbClsExtra = 0;
	_WndClsEx.lpszClassName = ClassName;
	_WndClsEx.hInstance = hInst;
}
//-------------------------------------------------------------------------------
// A Window superclass for a listview
//-------------------------------------------------------------------------------
void WindowClass::SuperClass(HINSTANCE hInst, const char *ClassName,
		WNDPROC WndPrc, const char* MenuName)
{
	_WndClsEx.cbSize = sizeof(WNDCLASSEX);
	//Create superclass from SYSLISTVIEW32 comctl32 class
	//Get class info of window class
	GetClassInfoEx(NULL, "SYSLISTVIEW32", &_WndClsEx);

	// Save original class win proc
	//App::Component::ListView::wpspOrigListViewProc = _WndClsEx.lpfnWndProc;
	// New class name
	_WndClsEx.lpszClassName = "APPLISTVIEW";
	// New class registered in our module
	_WndClsEx.hInstance = hInst;
	// New window procedure
	_WndClsEx.lpfnWndProc = WndPrc;
	RegisterClassEx(&_WndClsEx);

}
//-------------------------------------------------------------------------------
void WindowClass::Register()
{
	RegisterClassEx(&_WndClsEx);
}
//-------------------------------------------------------------------------------
void WindowClass::UnRegister()
{
	UnregisterClass(_WndClsEx.lpszClassName, _WndClsEx.hInstance);
}
//-------------------------------------------------------------------------------

}
