/*
 * MainWindowCommand.h
 *
 *  Created on: May 07, 2015
 *      Author: Rodney Woollett
 */

#ifndef MAINWINDOWCOMMAND_H_
#define MAINWINDOWCOMMAND_H_

#include "AppCommand.h"
#include "MainWindowView.h"
#include "WindowMessage.h"

namespace App
{

class MainWindowCommand: public App::Command
{
public:
	MainWindowCommand(App::MainWindowView *view,
			long (App::MainWindowView::*meth)(const App::WindowMessage &));
	virtual ~MainWindowCommand();
	long execute(const App::WindowMessage &parameters);
private:
	App::MainWindowView *view;
	long (App::MainWindowView::*method)(const App::WindowMessage &);
};

} /* namespace App */

#endif /* MAINWINDOWCOMMAND_H_ */
