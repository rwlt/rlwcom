/*
 * MainWindowView.h
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#ifndef MAINWINDOWVIEW_H_
#define MAINWINDOWVIEW_H_

#include <windows.h>
#include <winuser.h>
#include <commctrl.h>
#include "../resource.h"
#include <string>
#include "control/BindSource.h"
#include "WindowMessage.h"
#include "app/components/DockContainer.h"
#include "app/components/WindowFramed.h"
#include "app/components/Edit.h"
#include "app/components/Tabbed.h"
#include "app/components/Style.h"
#include "app/components/DockWnd.h"
#include "app/components/Dialog.h"

namespace App
{
class MainViewBindSourceCommand;
class MainWindowView: public App::Component::WindowFramed,
                      public App::DockContainer,
					  public Control::BindingListener
{
public:
	MainWindowView();
	virtual ~MainWindowView();

	static LRESULT CALLBACK MainWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
			LPARAM lParam);

	int createView(HINSTANCE handleInstance,
			const char* windowClassName);
	HWND windowHandle();
	HWND dialogHandle();
	bool isDockContainer();
	void readEditControl();
	void showValidateError();
	void showError(std::string message);
	void showErrorDialog(std::string message);
	void showStatus(std::string message);
	int showView();
	int closeView();

    //MainViewBindSource Command functor
    int notified(Control::Command* sender, const Control::BindNotify &notify);
    //BindingListener event
	virtual void changed(Control::BindSourceBase* source, const Control::BindNotify &notify);

private:
    MainWindowView(const MainWindowView& old); // no copying allowed
    MainWindowView operator = (const MainWindowView& old); // no assignment allowed

	HWND hwndStatic;
    int  splitRatioBottom;    // in per cent
    int  splitRatioTop;    // in per cent
    enum { splitWidth = 8 };    // width of splitter

	int clientWidth;		// width of client area
	int clientHeight;		// height of client area

	int pageTabIndex;

	App::Component::Edit componentEdit;
	App::Component::Edit componentOutput;
	App::DockWnd componentEditDock;
	App::DockWnd componentOutputDock;
	App::Component::Tabbed componentTabbed;
	Control::BindSource<std::string> textSource;
	MainViewBindSourceCommand* bindCommand;

	void updateEditControl(const std::string &value);
	void SetWindowControls(RECT *middleRect, int splitTopY, int ySplitBottomY);
	LRESULT dispatchMessage(HWND hWnd, UINT Msg, WPARAM wParam,	LPARAM lParam);

	// Window message function
	LRESULT ncActivate(const App::WindowMessage &message);
	LRESULT enable(const App::WindowMessage &message);
	LRESULT paint(const App::WindowMessage &message);
	LRESULT size(const App::WindowMessage &message);
    LRESULT moveBotSplit (const App::WindowMessage &message);
    LRESULT moveTopSplit (const App::WindowMessage &message);
    LRESULT selectColour(const App::WindowMessage &message);
    LRESULT selectFont(const App::WindowMessage &message);
    LRESULT changeTab(const App::WindowMessage &message);
    LRESULT tabChangeDockWnd(const App::WindowMessage &message);
	LRESULT getDispInfo(const App::WindowMessage &message);
	BOOL onEndLabelEdit(const App::WindowMessage &message);
	int listViewCompare(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	long isDockWnd_Dockable(const App::WindowMessage &message);
	long dockWndClosed(const App::WindowMessage &message);
    long eraseBackground(const App::WindowMessage &message);
    long drawItem(const App::WindowMessage &message);
    long controlBackground(const App::WindowMessage &message);
	void AboutApplication(HWND hWnd);
	static BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam,
			LPARAM lParam);
	void ContainerBoundary(HWND hwnd, RECT& rc1, RECT& rc2);
	void Send_WM_Size();
	int calculateRatioExtent(int y, int height);
	void removeTabbedDockWnd(WPARAM wParam);
};

} /* namespace App */

#endif /* MAINWINDOWVIEW_H_ */
