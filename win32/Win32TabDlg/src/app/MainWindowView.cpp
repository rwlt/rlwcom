/*
 * MainWindowView.cpp
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#include <windows.h>
#include "MainWindowView.h"
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include "MainViewBindSourceCommand.h"
#include "control/Bind.h"

namespace App
{

//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
MainWindowView::MainWindowView() :
		hwndStatic(0), splitRatioBottom(0), splitRatioTop(0), clientWidth(
				0), clientHeight(0), pageTabIndex(0), bindCommand(
				new MainViewBindSourceCommand(this, &MainWindowView::notified))
{
}

///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
MainWindowView::~MainWindowView()
{
	std::cout << "Destructor main window view" << std::endl;
	DestroyWindow(hwndStatic);
	removeDockWindow(&componentEditDock);
	removeDockWindow(&componentOutputDock);

	textSource.detachChanged(this);
	delete bindCommand;
	bindCommand = nullptr;

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::createView(HINSTANCE handleInstance, const char* windowClassName)
{
	//Create the main window
	hMainWindow = CreateWindowEx(WS_EX_CLIENTEDGE, windowClassName, "Window32 Application",
	WS_OVERLAPPEDWINDOW, //| DS_3DLOOK | DS_SHELLFONT , //|WS_CLIPCHILDREN
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
			NULL, NULL, handleInstance, (LPVOID) this); // window creation data

	if (hMainWindow != NULL)
	{
		componentEdit.createControl(hMainWindow, handleInstance, IDC_MAIN_EDIT);
		componentOutput.createControl(hMainWindow, handleInstance, IDC_MAIN_OUTPUT);
		componentTabbed.createControl(hMainWindow, handleInstance, IDC_MAIN_TABBED);

		componentEdit.setTitle("Edit");
		componentOutput.setTitle("Output");
		componentTabbed.setTitle("Tabbed");
		addWindowClient(&componentTabbed);

		// New component dockwnd to decorate an edit component
		componentEditDock.createDockWnd("Edit", handleInstance, hMainWindow, &componentEdit);
		addDockWindow(&componentEditDock);
		addWindowClient(&componentEditDock);
		componentOutputDock.createDockWnd("Output", handleInstance, hMainWindow, &componentOutput);
		addDockWindow(&componentOutputDock);
		addWindowClient(&componentOutputDock);

		// Not set it as a docked window in tabbed place in control ID of a tabbed window
		componentEditDock.setDockedState(TRUE, DWS_DOCKED_TABBED, IDC_MAIN_TABBED);
		componentOutputDock.setDockedState(TRUE, DWS_DOCKED_TABBED, IDC_MAIN_TABBED);

		// Tabbed - add a tab sample
		componentTabbed.addTabWindow(&componentEditDock);
		componentTabbed.addTabWindow(&componentOutputDock); // Add the docked to the tab and toggle to docked

		// Main View style all the components
		style(styleFramed());
		textSource.attachChanged(this);
		return (TRUE);
	}
	return (FALSE);
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
HWND MainWindowView::windowHandle()
{
	return (hMainWindow);
}
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
HWND MainWindowView::dialogHandle()
{
	return (NULL);//componentPersonDialog.handle());
}
//
///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
//
int MainWindowView::notified(Control::Command* sender, const Control::BindNotify &notify)
{
	std::cout << "Notified at MainWIndowView " << std::endl;
	MainViewBindSourceCommand* send = static_cast<MainViewBindSourceCommand*>(sender);
	Control::BindAction action = notify.getAction();
	std::cout << "Sender: " << sender << " MainViewBindSourceCommand: " << send << " BindAction (0 = add) "
			<< bindCommand << " " << action << std::endl;
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
//
void MainWindowView::changed(Control::BindSourceBase* source, const Control::BindNotify &notify)
{
	std::cout << "CHANGED at Module::Person MainWindowView " << std::endl;
	std::cout << "bind source ptr=" << source << std::endl;
	std::cout << "Text source ptr=" << &textSource << std::endl;

	if (notify.getAction() == Control::BindAction::Clear)
	{
		std::cout << "Changed action: Clear - " << notify.getAction() << std::endl;

	}
	else if (notify.getAction() == Control::BindAction::Updated)
	{
		std::cout << "Changed action: Updated - " << notify.getAction() << std::endl;
		App::Component::UpdateItemVisitor updateItemVisitor;
		updateItemVisitor.setSourceIndex(source, notify.getIndex());
		accept(updateItemVisitor);

	}
	else if (notify.getAction() == Control::BindAction::Added)
	{
		std::cout << "Changed action: Added - " << notify.getAction() << std::endl;
		App::Component::AddItemVisitor addItemVisitor;
		addItemVisitor.setSourceIndex(source, notify.getIndex());
		accept(addItemVisitor);
//		if (source == &textSource)
//		{
//			std::cout << "TEXT SOURCE: Found text for edit control " << std::endl;
//			componentEdit.updateText(textSource.getSelectItem());
//		}
	}
}
//
///////////////////////////////////////////////////////////////////////////////
// Dispatch the view message from static window procedure
///////////////////////////////////////////////////////////////////////////////
//
LRESULT MainWindowView::dispatchMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	LRESULT returnValue = 0;        // return value
	App::WindowMessage windowMessage;
	App::Parameter paramWindowArgs;
	paramWindowArgs.kind = App::ParameterKind::WPARAMLPARAM;
	paramWindowArgs.args.wParamLParam.wparam = wParam;
	paramWindowArgs.args.wParamLParam.lparma = lParam;
	windowMessage.setParameter(paramWindowArgs);
	NMHDR *hdr;

	switch (Msg)
	{
	case WM_NCACTIVATE:
		returnValue = ncActivate(windowMessage);
		break;
	case WM_ENABLE:
		returnValue = enable(windowMessage);
		break;
	case WM_SIZE:
		returnValue = size(windowMessage);
		break;
	case WM_PAINT:
		paint(windowMessage);
		returnValue = ::DefWindowProc(hWnd, Msg, wParam, lParam);
		break;
	case WM_ERASEBKGND:
		returnValue = eraseBackground(windowMessage);
		break;
	case WM_CTLCOLOREDIT:
		returnValue = controlBackground(windowMessage);
		break;
	case WM_NOTIFY:
		hdr = (NMHDR *) lParam;
		switch (hdr->code)
		{
		case TCN_SELCHANGE:
			returnValue = changeTab(windowMessage);
			break;
		case DWN_ISDOCKABLE: // DockWnd notification message
			returnValue = isDockWnd_Dockable(windowMessage);
			break;
		case DWN_TABREMOVE: // DockWnd notification message code=4294965242
		case DWN_TABADD: // DockWnd notification message code=4294965243
			returnValue = tabChangeDockWnd(windowMessage);
			break;
		case DWN_CLOSED:
			returnValue = dockWndClosed(windowMessage);
			break;
		case IDOK:
			returnValue = FALSE;
			break;
		case IDCANCEL:
			std::cout << "IDCANCEL " << hdr->idFrom << std::endl;
			returnValue = FALSE;
			break;
		}
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDM_ABOUT:
			AboutApplication(hWnd);
			returnValue = 0;
			break;
		case IDM_FILE_NEW:
			returnValue = selectFont(windowMessage);
			break;
		case IDM_FILE_OPEN:
			returnValue = selectColour(windowMessage);
			break;
		case IDM_FILE_EXIT:
			returnValue = closeView();
			break;

		case IDM_DIALOG_SHOW:
			break;
		case IDM_DIALOG_HIDE:
			break;
		default:
			return (DefWindowProc(hWnd, Msg, wParam, lParam));
		}
		;
		break;

	case MSG_MOVESPLITTER:
		switch (LOWORD(wParam))
		{
		case IDC_TOPSPLIT:
			returnValue = moveTopSplit(windowMessage);
			break;
		case IDC_BOTSPLIT:
			returnValue = moveBotSplit(windowMessage);
			break;
		default:
			/* no default */
			break;

		}
		break;
	case WM_DRAWITEM:
		returnValue = drawItem(windowMessage);
		break;

	case WM_DESTROY:
		PostQuitMessage(WM_QUIT);
		break;

	default:
		return (DefWindowProc(hWnd, Msg, wParam, lParam));
	}

	return (returnValue);

}
//
///////////////////////////////////////////////////////////////////////////////
// Main application WinProc for Windows Class of Main Window
///////////////////////////////////////////////////////////////////////////////
//
LRESULT CALLBACK MainWindowView::MainWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	MainWindowView *view;

	if (Msg == WM_NCCREATE)  // Non-Client Create
	{
		// WM_NCCREATE message is called before non-client parts(border,
		// titlebar, menu,etc) are created. This message comes with a pointer
		// to CREATESTRUCT in lParam. The lpCreateParams member of CREATESTRUCT
		// actually contains the value of lpPraram of CreateWindowEX().
		// First, retrieve the pointer to the controller specified when
		// MainWin is setup.
		view = (MainWindowView*) (((CREATESTRUCT*) lParam)->lpCreateParams);

		// Second, store the pointer to the Controller into GWLP_USERDATA,
		// so, other message can be routed to the associated Controller.
		// Set in GWLP_USERDATA so other window procedures of the applications
		// window classes can use the same Module controller
		::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR) view);

		return (::DefWindowProc(hWnd, Msg, wParam, lParam));
	}

	view = (MainWindowView *) GetWindowLongPtr(hWnd, GWL_USERDATA);
	// check NULL pointer, because GWLP_USERDATA is initially 0, and
	// we store a valid pointer value when WM_NCCREATE is called.
	if (!view)
		return (::DefWindowProc(hWnd, Msg, wParam, lParam));
	else
		return (view->dispatchMessage(hWnd, Msg, wParam, lParam));

}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
// Update our bind sources from the WIN32 controls and dialogs
void MainWindowView::readEditControl()
{
	textSource.setItem(0, componentEdit.readText());
//	componentListView.acceptChanges();
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showValidateError()
{
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showError(std::string message)
{
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showErrorDialog(std::string message)
{
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showStatus(std::string message)
{
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::showView()
{
	// We will display the main window as a regular object and update it
	ShowWindow(hMainWindow, SW_SHOWNORMAL);
	UpdateWindow(hMainWindow);
	std::stringstream title;
	title << "Win32TabTest";
	SendMessage(hMainWindow, WM_SETTEXT, 0, (LPARAM) title.str().c_str());
	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::closeView()
{
	PostMessage(hMainWindow, WM_CLOSE, 0, 0);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// private function - window messages ncAtivate
///////////////////////////////////////////////////////////////////////////////
// For activation/deactivation of docked/floating windows
LRESULT MainWindowView::ncActivate(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	return (DockWnd::HANDLE_NCACTIVATE(hMainWindow, hMainWindow, args.wParamLParam.wparam, args.wParamLParam.lparma));

}
///////////////////////////////////////////////////////////////////////////////
// private function - window messages ncAtivate
///////////////////////////////////////////////////////////////////////////////
// For activation/deactivation of docked/floating windows
LRESULT MainWindowView::enable(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	return (DockWnd::HANDLE_ENABLE(hMainWindow, hMainWindow, args.wParamLParam.wparam, args.wParamLParam.lparma));

}
//
///////////////////////////////////////////////////////////////////////////////
// private function - the top half splitter
///////////////////////////////////////////////////////////////////////////////
//
int MainWindowView::calculateRatioExtent(int y, int height)
{
	int calcRatio = y * 100 / (height); // set size
	if (calcRatio < 5)
		calcRatio = 5;
	else if (calcRatio > 95)
		calcRatio = 95;

	return (calcRatio);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function
///////////////////////////////////////////////////////////////////////////////
//
LRESULT MainWindowView::getDispInfo(const App::WindowMessage &message)
{
	//	ListView_SubItemHitTest
	App::Parameter::Args args = message.argumments();
	LV_DISPINFO *pnmv = (LV_DISPINFO *)args.wParamLParam.lparma;
    // Provide the item or subitem's text, if requested.
    if (pnmv->item.mask & LVIF_TEXT) {
//        App::Component::ListItem *pItem = (App::Component::ListItem *) (pnmv->item.lParam);
//        std::cout << "iSubItem=" << pnmv->item.iSubItem ;
//        std::cout << "pItem=" << pItem << " size: " << pItem->subitems.size() << std::endl;
//        std::string value = pItem->subitems.at(pnmv->item.iSubItem);
//		strncpy(pnmv->item.pszText, value.c_str(), value.length() + 1);
    }

	return (0);
}
//
//---------------------------------------------------------------------------
// private function
// Main_OnEndLabelEdit - processes the LVN_ENDLABELEDIT notification message.
// Returns TRUE if the label is changed or FALSE otherwise.
//---------------------------------------------------------------------------
//
BOOL MainWindowView::onEndLabelEdit(const App::WindowMessage &message)
{
//	App::Parameter::Args args = message.argumments();
//	LV_DISPINFO *pnmv = (LV_DISPINFO *)args.wParamLParam.lparma;
//    App::Component::ListItem *pItem;
//
//    // The item is -1 if editing is being canceled.
//    if (pnmv->item.iItem == -1)
//        return (FALSE);
//
//	std::cout << "Parent Main onLabelEdit:"
//			<< " lParam:" << pnmv->item.lParam
//			<< " iItem:" << pnmv->item.iItem
//			<< " iSubItem:" << pnmv->item.iSubItem << std::endl;
//   // Copy the new text to the application-defined structure,
//    pItem = (App::Component::ListItem *) (pnmv->item.lParam);
//	pItem->subitems.at(pnmv->item.iSubItem) = std::string(pnmv->item.pszText);
//    // No need to set the item text, because it is a callback item.
    return (TRUE);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function
///////////////////////////////////////////////////////////////////////////////
//
// ListViewCompare - sorts the list view control. It is a
//     comparison function.
// Returns a negative value if the first item should precede the
//     second item, a positive value if the first item should

//     follow the second item, and zero if the items are equivalent.
// lParam1 and lParam2 - item data for the two items (in this
//     case, pointers to application-defined MYITEM structures)
// lParamSort - value specified by the LVM_SORTITEMS message
//     (in this case, the index of the column to sort)
int MainWindowView::listViewCompare(
    LPARAM lParam1,
    LPARAM lParam2,
    LPARAM lParamSort)
{
//    App::Component::ListItem *pItem1 = (App::Component::ListItem *) (lParam1);
//    App::Component::ListItem *pItem2 = (App::Component::ListItem *) (lParam2);
//    // Compare the specified column.
//    int iCmp = pItem1->subitems.at(lParamSort).compare(pItem2->subitems.at(lParamSort));
//    // Return the result if nonzero, or compare the
//    // first column otherwise.
//    return ((iCmp != 0) ? iCmp :
//    		pItem1->subitems.at(0).compare(pItem2->subitems.at(0)));
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// public function - the bottom half splitter
///////////////////////////////////////////////////////////////////////////////
//
LRESULT MainWindowView::moveBotSplit(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	LPARAM lParam = args.wParamLParam.lparma; // position of y for bottom split
	splitRatioBottom = calculateRatioExtent((int) lParam, clientHeight);
	Send_WM_Size();
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function - the top half splitter
///////////////////////////////////////////////////////////////////////////////
//
LRESULT MainWindowView::moveTopSplit(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	LPARAM lParam = args.wParamLParam.lparma; // position of y for top split
	splitRatioTop = calculateRatioExtent((int) lParam, clientHeight);
	Send_WM_Size();
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function - tabbed changes selection
///////////////////////////////////////////////////////////////////////////////
LRESULT MainWindowView::changeTab(const App::WindowMessage &/*message*/)
{
	pageTabIndex = TabCtrl_GetCurSel(componentTabbed.handle());	// Get tab page index
	InvalidateRect(componentTabbed.handle(), NULL, TRUE);
	UpdateWindow(componentTabbed.handle());
	Send_WM_Size();
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function - tabbed dock window removal from a tab control
///////////////////////////////////////////////////////////////////////////////
//
LRESULT MainWindowView::tabChangeDockWnd(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	App::NMDOCKTABCHANGE *dtc = (App::NMDOCKTABCHANGE*) args.wParamLParam.lparma;	//lParam;
	UINT code = dtc->hdr.code; // DWN_TABADD or DWN_TABREMOVE
	UINT idDock = dtc->hdr.idFrom; // All dock component contents have ID
	UINT idTabbed = dtc->uTabComponentId; // All tabbed components have ID
	App::Component::Tabbed* tabControl = nullptr;

	if (idTabbed > 0) // If removed or added the tabbed component is found
	{
		switch (idTabbed)
		{
		case IDC_MAIN_TABBED: // IDC_MAIN_TABBED
			tabControl = &componentTabbed;
			break;
		default: /* no default */
			break;
		}

		if (tabControl)
		{
			switch (idDock)
			{
			case IDC_MAIN_OUTPUT: // Output component
				if (code == DWN_TABREMOVE)
					tabControl->removeTabWindow(&componentOutputDock);
				if (code == DWN_TABADD)
					tabControl->addTabWindow(&componentOutputDock);
				break;

			case IDC_MAIN_EDIT: // Edit component
				if (code == DWN_TABREMOVE)
					tabControl->removeTabWindow(&componentEditDock);
				if (code == DWN_TABADD)
					tabControl->addTabWindow(&componentEditDock);
				break;
			default:/* Do nothing */
				break;
			}
		}
	}
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function - tabbed dock window removal from a tab control
///////////////////////////////////////////////////////////////////////////////
//
void MainWindowView::removeTabbedDockWnd(WPARAM wParam)
{
	App::DockWnd* dockWnd = DockWnd_ById(wParam); // All dockwnds are contained in Main window dock container
	if (dockWnd != nullptr)
	{
		if (dockWnd->IdTabbedWnd() != 0)
		{ // It is in a tabbed control - which one?
			if (dockWnd->IdTabbedWnd() == IDC_MAIN_TABBED)
				componentTabbed.removeTabWindow(DockWnd_ById(wParam));
		}
	}
}
//
//---------------------------------------------------------------
// Public function
// Begin dragging the dockwnd docking/floating rectangle
//---------------------------------------------------------------
//
long MainWindowView::dockWndClosed(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	WPARAM wParam = args.wParamLParam.wparam;
	switch (wParam)
	{
	case IDC_TOOLPOPUP: // Close the Module View windows
		// Make sure removed from main views tabbed control
		removeTabbedDockWnd(wParam);
		break;
	case IDC_RESULTVIEW:
		// Make sure removed from main views tabbed control
		removeTabbedDockWnd(wParam);
		break;
//	case IDD_PERSON: // Main views dockable window components are only hidden
//		componentPersonDock.setVisible(FALSE);
//		break;
	default:/* Do Nothing */
		break;
	}
	return (0);
}
//
//---------------------------------------------------------------
// Private function
//---------------------------------------------------------------
//
void MainWindowView::Send_WM_Size()
{
	LPARAM lparam = MAKELPARAM(clientWidth, clientHeight);
	SendMessage(hMainWindow, WM_SIZE, 0, (LPARAM) lparam);
}
//
//---------------------------------------------------------------
// Private function
//---------------------------------------------------------------
//
void MainWindowView::ContainerBoundary(HWND hwnd, RECT& rc1, RECT& rc2)
{
	// Get main window "outer" rectangle
	GetWindowRect(hwnd, &rc1);
	// Get main window "inner" rectangle
	GetClientRect(hwnd, &rc2);
	MapWindowPoints(hwnd, 0, (POINT*) (&rc2), 2);
	InflateRect(&rc2, -2, -2);
}
//
//---------------------------------------------------------------
// Public function
// Begin dragging the dockwnd docking/floating rectangle
//---------------------------------------------------------------
//
long MainWindowView::isDockWnd_Dockable(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	LPARAM lParam = args.wParamLParam.lparma;
	RECT rc1, rc2;
	// Get main window "outer" rectangle
	ContainerBoundary(hMainWindow, rc1, rc2);
	UINT uDockSide = DockWnd_GetDockSide(hMainWindow, (App::NMDOCKWNDQUERY*) lParam, &rc1, &rc2);

	if (uDockSide == DWS_DOCKED_FLOATING)
	{
		// Look for docked tabbed component
		ContainerBoundary(componentTabbed.handle(), rc1, rc2);
		uDockSide = DockWnd_GetTabbedClient(componentTabbed.handle(), (App::NMDOCKWNDQUERY*) lParam, &rc2);
	}
	return (uDockSide);

}
///////////////////////////////////////////////////////////////////////////////
// public function
// Paint WIN32 WM_PAINT
///////////////////////////////////////////////////////////////////////////////
LRESULT MainWindowView::paint(const App::WindowMessage &message)
{
	HDC hdc;
	PAINTSTRUCT ps;
	hdc = BeginPaint(hMainWindow, &ps);

	// Polygons drawn here - in polygon component class
	// Window Client information draw
	App::Component::WindowFramed::paint(hdc, ps);

	EndPaint(hMainWindow, &ps);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function
// handle Win32 WM_SIZE
///////////////////////////////////////////////////////////////////////////////
LRESULT MainWindowView::size(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	clientWidth = LOWORD(args.wParamLParam.lparma);
	clientHeight = HIWORD(args.wParamLParam.lparma);
	HDWP hdwp;
	RECT middleRect;
	SetRect(&middleRect, 0, 0, clientWidth, clientHeight);

	hdwp = BeginDeferWindowPos(dockerCount());
	DockWnd_Position(hMainWindow, hdwp, &middleRect);
	EndDeferWindowPos(hdwp);

	int ySplitTopY = ((clientHeight * splitRatioTop) / 100);
	int ySplitBottomY = ((clientHeight * splitRatioBottom) / 100);

	if (ySplitTopY < middleRect.top)
		ySplitTopY = middleRect.top + splitWidth;
	if (ySplitTopY >= (middleRect.bottom - splitWidth - 16))
		ySplitTopY = middleRect.bottom - splitWidth - 16;

	if (ySplitBottomY <= ySplitTopY)
		ySplitBottomY = ySplitTopY + splitWidth + 1;
	if (ySplitBottomY >= (middleRect.bottom - splitWidth - 8))
		ySplitBottomY = middleRect.bottom - splitWidth - 8;

	SetWindowControls(&middleRect, ySplitTopY, ySplitBottomY);

	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
LRESULT MainWindowView::selectFont(const App::WindowMessage &message)
{
	styleFramed().selectFont(hMainWindow);
	style(styleFramed());
	InvalidateRect(hMainWindow, NULL, TRUE);
	UpdateWindow(hMainWindow);
	Send_WM_Size();
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
LRESULT MainWindowView::selectColour(const App::WindowMessage &message)
{
	styleFramed().selectColour(hMainWindow);
	style(styleFramed());
	InvalidateRect(hMainWindow, NULL, TRUE);
	UpdateWindow(hMainWindow);
	return (0);
}
//
//---------------------------------------------------------------
// Public function
// Erase background - do the markers on main window
//---------------------------------------------------------------
//
long MainWindowView::controlBackground(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	HDC hdc = (HDC) args.wParamLParam.wparam;
	HWND controlHWnd = (HWND) args.wParamLParam.lparma;
	if (GetDlgCtrlID(controlHWnd) == IDC_MAIN_EDIT || GetDlgCtrlID(controlHWnd) == IDC_MAIN_OUTPUT
			|| GetDlgCtrlID(controlHWnd) == IDC_MAIN_PROBLEM)
	{
		HDC hdcEditControl = hdc;
		SetTextColor(hdcEditControl, styleFramed().rgbText);
		SetBkColor(hdcEditControl, styleFramed().rgbBackground);
		return ((LONG) styleFramed().hbrBackground);
	}
	else
	{
		return (0);
	}
}
//
//---------------------------------------------------------------
// Public function
// Mouse movement - do the markers on main window
//---------------------------------------------------------------
//
long MainWindowView::drawItem(const App::WindowMessage &message)
{
	LPDRAWITEMSTRUCT lpdis;
	HDC hdcMem;
	App::Parameter::Args args = message.argumments();
	lpdis = (LPDRAWITEMSTRUCT) args.wParamLParam.lparma;
	hdcMem = CreateCompatibleDC(lpdis->hDC);
	int offset = 0;  //64;
	if (lpdis->itemState & ODS_SELECTED)  // if selected
		offset = 16;
	else
		offset = 0;
	//SelectObject(hdcMem, hbm1);
	// Destination
	StretchBlt(lpdis->hDC,         // destination DC
			lpdis->rcItem.left, // x upper left
			lpdis->rcItem.top,  // y upper left
			// The next two lines specify the width and
			// height.
			lpdis->rcItem.right - lpdis->rcItem.left, lpdis->rcItem.bottom - lpdis->rcItem.top, hdcMem, // source device context
			offset, 0,      // x and y upper left
			16,        // source bitmap width
			16, SRCCOPY);        // source bitmap height

	DeleteDC(hdcMem);
	return (1L);
}
//
//---------------------------------------------------------------
// Public function
// Erase background - do the markers on main window
//---------------------------------------------------------------
//
long MainWindowView::eraseBackground(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	HDC hdc = (HDC) args.wParamLParam.wparam;
	RECT rc;
	GetClientRect(hMainWindow, &rc);
	SetMapMode(hdc, MM_ANISOTROPIC);
	SetWindowExtEx(hdc, 100, 100, NULL);
	SetViewportOrgEx(hdc, 0, 0, NULL);
	SetViewportExtEx(hdc, rc.right, rc.bottom, NULL);
	FillRect(hdc, &rc, styleFramed().hbrWhite);
	int i, x, y;
	for (i = 0; i < 13; i++)
	{
		x = (i * 40) % 100;
		y = ((i * 40) / 100) * 20;
		SetRect(&rc, x, y, x + 20, y + 20);
		FillRect(hdc, &rc, styleFramed().hbrGray);
	}
	SetMapMode(hdc, MM_TEXT);

	return (1L);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function
///////////////////////////////////////////////////////////////////////////////
//
void MainWindowView::SetWindowControls(RECT *rc, int sTopY, int sBotY)
{
	HDWP hdwp;
	RECT tabRc;
	if (componentTabbed.totalTabs() == 0)
	{
		SetRect(&tabRc, rc->left, rc->top, rc->right - rc->left, rc->bottom - rc->top);
	}
	else
	{
		SetRect(&tabRc, rc->left, rc->top, rc->right - rc->left, rc->bottom - rc->top);
	}

	hdwp = BeginDeferWindowPos(1);
	if (hdwp != NULL)
	{
		componentTabbed.setWindowRect(tabRc);
		componentTabbed.setPositionFlag(NULL, SWP_NOZORDER);
		hdwp = deferSize(hdwp);
		if (hdwp == NULL)
			return;

		if (hdwp != NULL)
		{
			if (EndDeferWindowPos(hdwp) == 0)
			{
				DWORD error = GetLastError();
				std::cerr << "ERROR: " << error << std::endl;
			}
		}
	}

}

///////////////////////////////////////////////////////////////////////////////
// About Application dialog
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::AboutApplication(HWND hWnd)
{
	// Show a modal dialog - it implements its own message loop and procedure
	int ret = DialogBox(GetModuleHandle(NULL),
			MAKEINTRESOURCE(IDD_ABOUT), hWnd, AboutDlgProc);
	if (ret == -1)
	{
		MessageBox(hWnd, "Dialog failed!", "Error",
		MB_OK | MB_ICONINFORMATION);
	}
}

///////////////////////////////////////////////////////////////////////////////
// About application WinProc
///////////////////////////////////////////////////////////////////////////////
BOOL CALLBACK MainWindowView::AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	LRESULT returnValue = 0;        // return value
	//Caution: use Windows BOOL, not C++ bool
	// Return FALSE for messages you don't handle and TRUE for those you do (If otherwise told not to!
	//  use the Help win32 notes. )
	switch (Message)
	{
	case WM_INITDIALOG: // For initializing dialogs (not WM_CREATE, controls wont be created if you did)
		returnValue = 1L;
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			EndDialog(hwnd, IDOK); //do not call DestroyWindow() for dialogs
			returnValue = 1L;
			break;

		case IDCANCEL:
			EndDialog(hwnd, IDCANCEL); // do not call DestroyWindow() for dialogs
			returnValue = 1L;
			break;
		}
		break;
	default:
		break; // Modal Dialog window procedures don't use DefWindowProc
	}
	return (returnValue);
}

} /* namespace App */

