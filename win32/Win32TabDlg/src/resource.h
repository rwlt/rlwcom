#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif


#define IDC_MAIN_EDIT              				101
#define IDC_MAIN_OUTPUT							102
#define IDI_RAINDROP                            103
#define IDC_FREEHAND                            105
#define IDC_MAIN_LISTVIEW						106
#define IDC_COMBOBOX							107
#define IDC_MAIN_TABBED							108
#define IDR_MAIN_MENU                           110
#define IDC_BOTSPLIT                            111
#define IDC_TOPSPLIT                            112
#define IDC_CLOSEDOCKBUTTON                     114
#define IDC_SPLIT_RANGE                         115
#define IDB_STANDARD                            116
#define IDB_APPBUTTONS                          117
#define IDR_ACCELERATOR1     		           	120
#define IDC_TOOLPOPUP                           130
#define IDC_TOOLBOX                             131
#define IDC_TOOLMESSAGE                         132
#define IDC_MAINHEADER                          133
#define IDC_MAIN_PROBLEM						134
#define IDC_MAINTOOLBAR						    135
#define IDC_DOCKEDIT                            136
#define IDC_DOCKOUTPUT                          137
#define IDC_DOCKPROBLEM                         138
#define IDC_RESULTVIEW                          139
#define ID_HELP                                 150
#define ID_TEXT                                 200
#define IDM_FILE_NEW                            40000
#define IDM_FILE_OPEN                           40001
#define IDM_FILE_SAVE                           40002
#define IDM_FILE_SAVEAS                         40003
#define IDM_FILE_PRINT                          40005
#define IDM_FILE_EXIT                           40007
#define IDM_DRAW_ARROW                          40010
#define IDM_DRAW_FREEHAND                       40011
#define IDM_ABOUT                               40012
#define IDC_PRESS                               40013
#define IDC_OTHER                               40014
#define IDM_DIALOG_SHOW                         40015
#define IDM_DIALOG_HIDE                         40016

#define IDD_ABOUT								40025
#define IDD_TOOLBAR								40026
#define MSG_MOVESPLITTER                        40030
#define IDD_PERSON                              40040
#define IDDC_PERSON_FIRSTNAME                   40041
#define IDDC_PERSON_SURNAME                     40042
#define IDDC_PERSON_STATUS                      40043
#define IDD_RESULTS                             40044
#define IDDC_RESULTS_STATUS                     40045

