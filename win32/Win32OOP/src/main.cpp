/*
 * Main.cpp
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#include <windows.h>
#include <commctrl.h>
#include "resource.h"
#include "module/Controller.h"
#include "app/ViewFactory.h"
#include <iostream>
#include <cstring>

//
///////////////////////////////////////////////////////////////////////////////
// WIN32 WinMain
///////////////////////////////////////////////////////////////////////////////
//
INT APIENTRY /*WINAPI*/WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst,
		LPSTR lpCmdLine, int nCmdShow)
{
	MSG Msg;
	HACCEL hAccelTable;

	//Initialize the Common controls
	INITCOMMONCONTROLSEX initCtrlEx;
	initCtrlEx.dwSize = sizeof(INITCOMMONCONTROLSEX);
	initCtrlEx.dwICC = ICC_TAB_CLASSES | ICC_BAR_CLASSES | ICC_COOL_CLASSES;
	InitCommonControlsEx(&initCtrlEx);

	// Create controller with a viewFactory to use with Module Controller
	// Create the view factory
	App::ViewFactory viewFactory(hInstance);
	Module::Controller controller(&viewFactory);
	controller.run(); //Use a view factory to create a Parent View or main view

	hAccelTable = LoadAccelerators(hInstance,
	MAKEINTRESOURCE(IDR_ACCELERATOR1));
	//Process the main windows messages
	while (GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		if (!(viewFactory.checkTranslateAccelerator(hAccelTable, &Msg)))
			if (!(viewFactory.checkDialogMessage(&Msg)))
			{
				TranslateMessage(&Msg);
				DispatchMessage(&Msg);
			}
	}

	viewFactory.close();

	return (0);

}

