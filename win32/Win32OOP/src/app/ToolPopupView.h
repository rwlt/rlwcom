/*
 * MainWindowView.h
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#ifndef TOOLPOPUPVIEW_H_
#define TOOLPOPUPVIEW_H_

#include <string>
#include "module/ToolBoxInterface.h"
#include "module/ToolBoxPresenter.h"
#include "module/Message.h"
#include "app/components/DockWnd.h"

namespace App
{

class ToolPopupView: public Module::ToolBoxInterface, public App::DockWnd
{
public:
	ToolPopupView();
	virtual ~ToolPopupView();

	int createView(HINSTANCE handleInstance, HWND hWndParent);
	HWND windowHandle();

	int showView(const Module::Message &message);
	int closeView(const Module::Message &message);
	int paint(const Module::Message &message);
    int size(const Module::Message &message);
	void save();
	void selectFont(){};
	void selectColour(){};

private:
	ToolPopupView(const ToolPopupView& old); // no copying allowed
	ToolPopupView operator = (const ToolPopupView& old); // no assignment allowed
	virtual Module::ToolBoxPresenter* ActivePresenter();
	Module::ToolBoxPresenter* presenter;
	HWND hToolPopup;
	int clientWidth;		// width of client area
	int clientHeight;		// height of client area

};

} /* namespace App */

#endif /* TOOLPOPUPVIEW_H_ */
