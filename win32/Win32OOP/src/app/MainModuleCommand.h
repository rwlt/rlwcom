/*
 * MoveSplitterCommand.h
 *
 *  Created on: Apr 29, 2015
 *      Author: Rodney Woollett
 */

#ifndef MAINMODULECOMMAND_H_
#define MAINMODULECOMMAND_H_

#include "module/View.h"
#include "module/Command.h"
#include "module/Message.h"
#include "MainWindowView.h"

namespace App
{

class MainModuleCommand: public Module::Command
{
public:
	MainModuleCommand(Module::View *view,
			int (App::MainWindowView::*meth)(const Module::Message &));
	virtual ~MainModuleCommand();
	int execute(const Module::Message &parameters);
private:
	int (App::MainWindowView::*method)(const Module::Message &);
};

} /* namespace App */

#endif /* MAINMODULECOMMAND_H_ */
