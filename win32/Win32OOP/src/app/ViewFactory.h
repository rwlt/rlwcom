/*
 * ViewFactory.h
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#ifndef VIEWFACTORY_H_
#define VIEWFACTORY_H_

#undef WINVER
#ifndef WINVER
#define WINVER _WIN32_WINNT_WINXP        // Originally 0x0600
#endif

#undef _WIN32_WINNT
#ifndef _WIN32_WINNT
#define _WIN32_WINNT _WIN32_WINNT_WINXP    // Originally 0x0600
#endif

#include <windows.h>
#include "module/AbstractViewFactory.h"
#include "WindowClass.h"
#include "WindowMessage.h"


namespace App
{

const int MAX_DOCK_WINDOWS = Module::TotalViews;

class ViewFactory: public Module::AbstractViewFactory
{
public:
	ViewFactory(HINSTANCE handleInstance);
	~ViewFactory();
	virtual Module::View* createInstance(
			Module::ViewIdentity viewIdentity, Module::ViewIdentity parentViewIdentity =
					Module::ViewIdentity::NullView);
	virtual void destroyInstance(Module::ViewIdentity viewIdentity);
	void close();
	int checkDialogMessage(MSG* msg);
	int checkTranslateAccelerator(HACCEL hAccelTable, MSG* msg);
	virtual Module::ViewIdentity viewIdentity(HWND hWnd);
	void destroyInstance(HWND hWnd);
	Module::View* identityInstance(Module::ViewIdentity viewIdentity);

	LRESULT PerformCommand(
			Module::ViewIdentity viewIdentity, int resId,
			const Module::Message &message);
	LRESULT PerformWindowCommand(
			Module::ViewIdentity viewIdentity, int resId,
			const App::WindowMessage &message);

private:
	ViewFactory(const ViewFactory& old); // no copying allowed
	ViewFactory operator = (const ViewFactory& old); // no assignment allowed

	HINSTANCE hInstance;
	const char* className;
	const char* classSplitterName;
	const char* dockWndName;
	const char* popupWndName;
	const char* superclassListViewWndName;
	App::WindowClass windowsClass;
	App::WindowClass windowSplitterClass;
	App::WindowClass dockWndClass;
	App::WindowClass dialogPopupClass;
	App::WindowClass listViewSuperclass;
	Module::View* moduleView[Module::ViewIdentity::TotalViews];
};

}
#endif /* VIEWFACTORY_H_ */
