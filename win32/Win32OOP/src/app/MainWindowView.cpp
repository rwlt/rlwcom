/*
 * MainWindowView.cpp
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#include <windows.h>
#include "MainWindowView.h"
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include "module/MainPresenter.h"
#include "module/ViewLoader.h"
#include "MainViewBindSourceCommand.h"
#include "control/Bind.h"

namespace App
{

//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
MainWindowView::MainWindowView() :
		presenter(new Module::MainPresenter(this)), hwndStatic(0), splitRatioBottom(65), splitRatioTop(40), clientWidth(
				0), clientHeight(0), pageTabIndex(0), bindCommand(
				new MainViewBindSourceCommand(this, &MainWindowView::notified))
{
	std::cout << "MainWindowView created:" << this << " presenter " << presenter << std::endl;
}

///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
MainWindowView::~MainWindowView()
{
	std::cout << "In MainWindowView destructor " << " presenter " << presenter << std::endl;
	DestroyWindow(hwndStatic);
	removeDockWindow(&componentProblemDock);
	removeDockWindow(&componentOutputDock);

	personListSource.detachChanged(this);
	personListSource.detach2Changed(bindCommand);
	personSource.detachChanged(this);
	personSource.detach2Changed(bindCommand);
	textSource.detachChanged(this);

	delete bindCommand;
	bindCommand = nullptr;
	delete presenter;
	presenter = nullptr;

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::createView(HINSTANCE handleInstance, const char* windowClassName)
{
	//Create the main window
	hMainWindow = CreateWindowEx(WS_EX_CLIENTEDGE, windowClassName, "Window32 Application",
	WS_OVERLAPPEDWINDOW, //| DS_3DLOOK | DS_SHELLFONT , //|WS_CLIPCHILDREN
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
			NULL, NULL, handleInstance, (LPVOID) this); // window creation data

	if (hMainWindow != NULL)
	{
		add(&componentPolygon);
//		componentHeader.createControl(hMainWindow, handleInstance, IDC_MAINHEADER);
		componentEdit.createControl(hMainWindow, handleInstance, IDC_MAIN_EDIT);
		componentOutput.createControl(hMainWindow, handleInstance, IDC_MAIN_OUTPUT);
		componentProblem.createControl(hMainWindow, handleInstance, IDC_MAIN_PROBLEM);
		componentTopSplit.createControl(hMainWindow, handleInstance, IDC_TOPSPLIT);
		componentBotSplit.createControl(hMainWindow, handleInstance, IDC_BOTSPLIT);
		componentTabbed.createControl(hMainWindow, handleInstance, IDC_MAIN_TABBED);
		componentListView.createControl(hMainWindow, handleInstance, IDC_MAIN_LISTVIEW);
		componentToolBar.createControl(hMainWindow, handleInstance, IDC_MAINTOOLBAR);
		componentPersonDialog.createControl(hMainWindow, handleInstance, IDD_PERSON);

		std::cout << "MainWIndowView createView before PersonDialog call" << std::endl;

		// DockWnd component adding:
		// Cannot set docked true as the dockwnd needs to have an owner window when created
		// To make docked initially first make fDocked equal FALSE then later toggle to
		// docked. So WS_VISBLE is option here to make window initially shown if only wanting to
		// make floating window shown. Or later a menu option could be used to show it

		componentPersonDialog.setTitle("Person Dialog");

		// TODO: New bind to source additions to window client components
		// 1) A component can bind to multiple bind sources ... so
		// 2) When items are added the index in the bind source needs persisting in the component
		// 3) How to do this, a map of bind source pinter and the index it is boud in
		// Means the bind source needs to keep in order until a removal or is re written,
		// then all components need refreshing to the bind source from the data from a presenter
		// List items - re refresh all list items
		// View item - the presneter will set up the the view item again to the componet.
		// ie A dialog will refresh from the presenter. An item is selected and setup from a presenter
		// View item interfaces will oly keep one item in a bind source.
		// If a dialog one item view uses a listview bind, then the dialog will refresh with selected
		// item in a refresh - so a presenter can set the selected item.
		// The multiple bind source in a dialog:
		// Can be done by storing the source index to a bind source pointer in the component.
		// The bind source events
		// Add an item: Event sets up the AddItemVisitor. Bind source and index given so only that source
		// is processed
		// No remove is made yet: if doing the compoents need refreshing again from cleared and re - load
		// bind sources - so Add an item is done to ???
		// Components functions in binds
		// RefreshVisitor - new function to refresh the componets with item or all items from a bind source
		// into the compoents controls.
		// BindToSourceVisitor - here the component uses this to save its data into the bindsource
		//   it uses the refsource index - and also should store the bind source ( its in the bind item)
		//   DO we need to stor e the index in the Bind item??

		// Person source is a one item ViewItem type of presenter data source
		componentPersonDialog.addBind(&Module::Person::getFirstName, &Module::Person::setFirstName, personSource,
		IDDC_PERSON_FIRSTNAME);
		componentPersonDialog.addBind(&Module::Person::getSurname, &Module::Person::setSurname, personSource,
		IDDC_PERSON_SURNAME);

		// Edit components with std::string text source binds
		componentEdit.addBind(textSource, IDC_MAIN_EDIT);

		// List component using a person list source bindsource
		// TODO: 1) Listitems have one main item and sub items
		// 2) Somehow set which person fileds go to what item and sub item
		// 3) The binds ref index should be stored in the list item item structure
		// 4) the binds items will have the last index to the bindsource so to enable
		//    update event on bind source for an index the index ref needs to be known in the list
		//    items structure.
		componentListView.addBind(&Module::Person::getFirstName,
				                  &Module::Person::setFirstName, "First Name",
								  personListSource, IDDC_PERSON_FIRSTNAME);
		componentListView.addBind(&Module::Person::getSurname,
				                  &Module::Person::setSurname, "Persons Surname",
								  personListSource, IDDC_PERSON_SURNAME);

		addWindowClient(&componentListView);// Just so style gets init by window frame before initListView
		componentListView.initListView(handleInstance);

		// Docked window components from dockable window components
		componentPersonDock.createDockWnd("Person", handleInstance, hMainWindow, &componentPersonDialog);
		addDockWindow(&componentPersonDock);
		addWindowClient(&componentPersonDock);
		componentPersonDock.setFloatPos(325, 175);
		componentPersonDock.setDockWndStyle(DWS_USEBORDERS |
		DWS_DRAWGRIPPERDOCKED |
		DWS_DRAWGRIPPERFLOATING |
		DWS_BORDERTOP |
		DWS_BORDERBOTTOM |
		DWS_NORESIZE);

		// New component dockwnd to decorate an edit component
		componentOutputDock.createDockWnd("Output", handleInstance, hMainWindow, &componentOutput);
		addDockWindow(&componentOutputDock);
		addWindowClient(&componentOutputDock);

		componentProblemDock.createDockWnd("Problems", handleInstance, hMainWindow, &componentProblem);
		addDockWindow(&componentProblemDock);
		addWindowClient(&componentProblemDock);

		// Not set it as a docked window in tabbed place in control ID of a tabbed window
		componentOutputDock.setDockedState(TRUE, DWS_DOCKED_TABBED, IDC_MAIN_TABBED);
		componentProblemDock.setDockedState(TRUE, DWS_DOCKED_TABBED, IDC_MAIN_TABBED);
		componentPersonDock.setDockedState(TRUE, DWS_DOCKED_TABBED, IDC_MAIN_TABBED);

		componentToolBar.setTitle("ToolBar");
		componentListView.setTitle("ListView");
		componentEdit.setTitle("EditInpit");
		componentProblem.setTitle("Problem");
		componentOutput.setTitle("Output");
		componentTopSplit.setTitle("Top Split");
		componentBotSplit.setTitle("Bottom Split");
		componentTabbed.setTitle("Tabbed");
		//addWindowClient(&componentProblem);
		//addWindowClient(&componentHeader); //
		//addWindowClient(&componentPersonDialog);
		addWindowClient(&componentToolBar);
		//addWindowClient(&componentListView);
		addWindowClient(&componentEdit);
		addWindowClient(&componentTopSplit);
		addWindowClient(&componentBotSplit);
		addWindowClient(&componentTabbed);

		// Tabbed - add a tab sample
		componentTabbed.addTabWindow(&componentOutputDock); // Add the docked to the tab and toggle to docked
		componentTabbed.addTabWindow(&componentProblemDock);
		componentTabbed.addTabWindow(&componentPersonDock);

		// Main View style all the components
		style(styleFramed());

		//hwndstatic here is not used for anything
		hwndStatic = CreateWindow("STATIC", "",
				WS_CHILD | WS_VISIBLE | WS_BORDER,
				120, 120, CW_USEDEFAULT, CW_USEDEFAULT,
				hMainWindow, NULL, handleInstance, NULL);

		personListSource.attach2Changed(bindCommand);
		personListSource.attachChanged(this);
		personSource.attachChanged(this);
		personSource.attach2Changed(bindCommand);
		textSource.attachChanged(this);
		// Presenter initializes itself now
		presenter->initialize();
		return (TRUE);
	}
	return (FALSE);
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
HWND MainWindowView::windowHandle()
{
	return (hMainWindow);
}
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
HWND MainWindowView::dialogHandle()
{
	return (componentPersonDialog.handle());
}
//
///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
//
int MainWindowView::notified(Control::Command* sender, const Control::BindNotify &notify)
{
	std::cout << "Notified at MainWIndowView " << std::endl;
	MainViewBindSourceCommand* send = static_cast<MainViewBindSourceCommand*>(sender);
	Control::BindAction action = notify.getAction();
	std::cout << "Sender: " << sender << " MainViewBindSourceCommand: " << send << " BindAction (0 = add) "
			<< bindCommand << " " << action << std::endl;
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
//
void MainWindowView::changed(Control::BindSourceBase* source, const Control::BindNotify &notify)
{
	std::cout << "CHANGED at Module::Person MainWindowView " << std::endl;
	std::cout << "bind source ptr=" << source << std::endl;
	std::cout << "Person source ptr=" << &personSource << std::endl;
	std::cout << "Person list source ptr=" << &personListSource << std::endl;
	std::cout << "Text source ptr=" << &textSource << std::endl;

	if (notify.getAction() == Control::BindAction::Clear)
	{
		std::cout << "Changed action: Clear - " << notify.getAction() << std::endl;

	}
	else if (notify.getAction() == Control::BindAction::Updated)
	{
		std::cout << "Changed action: Updated - " << notify.getAction() << std::endl;
		App::Component::UpdateItemVisitor updateItemVisitor;
		updateItemVisitor.setSourceIndex(source, notify.getIndex());
		accept(updateItemVisitor);

	}
	else if (notify.getAction() == Control::BindAction::Added)
	{
		std::cout << "Changed action: Added - " << notify.getAction() << std::endl;
		App::Component::AddItemVisitor addItemVisitor;
		addItemVisitor.setSourceIndex(source, notify.getIndex());
		accept(addItemVisitor);
//		if (source == &textSource)
//		{
//			std::cout << "TEXT SOURCE: Found text for edit control " << std::endl;
//			componentEdit.updateText(textSource.getSelectItem());
//		}
	}
}
//
///////////////////////////////////////////////////////////////////////////////
// Dispatch the view message from static window procedure
///////////////////////////////////////////////////////////////////////////////
//
Module::MainPresenter* MainWindowView::ActivePresenter()
{
	return (presenter);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public interface method
///////////////////////////////////////////////////////////////////////////////
//
Module::ViewList<Module::Person>& MainWindowView::getPersonList()
{
	static AppViewList<Module::Person> personList(personListSource);
	return (personList);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public interface method
///////////////////////////////////////////////////////////////////////////////
//
Module::ViewItem<Module::Person>& MainWindowView::getPerson()
{
	static AppViewItem<Module::Person> person(personSource);
	return (person);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public interface method
///////////////////////////////////////////////////////////////////////////////
//
Module::ViewItem<std::string>& MainWindowView::status()
{
	static AppViewItem<std::string> editStatus(textSource);
	return (editStatus);
}
///////////////////////////////////////////////////////////////////////////////
// Dispatch the view message from static window procedure
///////////////////////////////////////////////////////////////////////////////
//
//Module::ViewIdentity MainWindowView::ActiveViewInterface()
//{
//	return (this->moduleViewIdentity());
//}
//
///////////////////////////////////////////////////////////////////////////////
// Dispatch the view message from static window procedure
///////////////////////////////////////////////////////////////////////////////
//
LRESULT MainWindowView::dispatchMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	LRESULT returnValue = 0;        // return value
	Module::Message viewMessage;
	Module::Parameter paramArgs;
	App::WindowMessage windowMessage;
	App::Parameter paramWindowArgs;
	paramWindowArgs.kind = App::ParameterKind::WPARAMLPARAM;
	paramWindowArgs.args.wParamLParam.wparam = wParam;
	paramWindowArgs.args.wParamLParam.lparma = lParam;
	windowMessage.setParameter(paramWindowArgs);
	NMHDR *hdr;

	switch (Msg)
	{
	case WM_NCACTIVATE:
		returnValue = ncActivate(windowMessage);
		break;
	case WM_ENABLE:
		returnValue = enable(windowMessage);
		break;
	case WM_SIZE:
		returnValue = size(windowMessage);
		break;
	case WM_PAINT:
		paint(windowMessage);
		returnValue = ::DefWindowProc(hWnd, Msg, wParam, lParam);
		break;
	case WM_ERASEBKGND:
		returnValue = eraseBackground(windowMessage);
		break;
	case WM_CTLCOLOREDIT:
		returnValue = controlBackground(windowMessage);
		break;
	case WM_NOTIFY:
		hdr = (NMHDR *) lParam;
		switch (hdr->code)
		{
		case TCN_SELCHANGE:
			returnValue = changeTab(windowMessage);
			break;
		case DWN_ISDOCKABLE: // DockWnd notification message
			returnValue = isDockWnd_Dockable(windowMessage);
			break;
		case DWN_TABREMOVE: // DockWnd notification message code=4294965242
		case DWN_TABADD: // DockWnd notification message code=4294965243
			returnValue = tabChangeDockWnd(windowMessage);
			break;
		case DWN_CLOSED:
			returnValue = dockWndClosed(windowMessage);
			break;
		case IDOK:
			if (hdr->idFrom == IDD_PERSON)
			{
				std::cout << "IDOK " << hdr->idFrom << std::endl;
				paramArgs.kind = Module::ParameterKind::ONE_ARG;
				paramArgs.args.oneArg.message = lParam;
				viewMessage.setParameter(paramArgs);
				returnValue = save(viewMessage);
			}
			returnValue = FALSE;
			break;
		case IDCANCEL:
			std::cout << "IDCANCEL " << hdr->idFrom << std::endl;
			returnValue = FALSE;
			break;

		// ListView notifications
		// Process LVN_GETDISPINFO to supply information about
        // callback items.
        case LVN_GETDISPINFO:
			returnValue = getDispInfo(windowMessage);
            break;

        // Process LVN_ENDLABELEDIT to change item labels after
        // in-place editing.
        case LVN_ENDLABELEDIT:
			returnValue = onEndLabelEdit(windowMessage);
			break;

        // Process LVN_COLUMNCLICK to sort items by column.
        case LVN_COLUMNCLICK:
            #define pnm ((NM_LISTVIEW *) lParam)
            ListView_SortItems(
                pnm->hdr.hwndFrom,
                &MainWindowView::listViewCompare,
                (LPARAM) (pnm->iSubItem)
                );
            #undef pnm
            break;
		}
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDM_ABOUT:
			AboutApplication(hWnd);
			returnValue = 0;
			break;
		case IDM_FILE_NEW:
			returnValue = selectFont(windowMessage);
			break;
		case IDM_FILE_OPEN:
			returnValue = selectColour(windowMessage);
			break;
		case IDM_FILE_SAVE:
			paramArgs.kind = Module::ParameterKind::ONE_ARG;
			paramArgs.args.oneArg.message = lParam;
			viewMessage.setParameter(paramArgs);
			returnValue = save(viewMessage);
			break;
		case IDM_FILE_EXIT:
			paramArgs.kind = Module::ParameterKind::ONE_ARG;
			paramArgs.args.oneArg.message = lParam;
			viewMessage.setParameter(paramArgs);
			returnValue = closeView(viewMessage);
			break;

		case IDM_DIALOG_SHOW:
			paramArgs.kind = Module::ParameterKind::ONE_ARG;
			paramArgs.args.oneArg.message = 1;
			viewMessage.setParameter(paramArgs);
			presenter->showToolPopup(viewMessage);
			presenter->showResults(viewMessage);
			//presenter->showPersonEdit(viewMessage);
			componentProblemDock.setVisible(TRUE);
			componentOutputDock.setVisible(TRUE);
			break;
		case IDM_DIALOG_HIDE:
			paramArgs.kind = Module::ParameterKind::ONE_ARG;
			paramArgs.args.oneArg.message = 0;
			viewMessage.setParameter(paramArgs);
			presenter->showToolPopup(viewMessage);
			presenter->showResults(viewMessage);
			//presenter->showPersonEdit(viewMessage);
			break;
		default:
			return (DefWindowProc(hWnd, Msg, wParam, lParam));
		}
		;
		break;

	case MSG_MOVESPLITTER:
		switch (LOWORD(wParam))
		{
		case IDC_TOPSPLIT:
			returnValue = moveTopSplit(windowMessage);
			break;
		case IDC_BOTSPLIT:
			returnValue = moveBotSplit(windowMessage);
			break;
		default:
			/* no default */
			break;

		}
		break;
	case WM_DRAWITEM:
		returnValue = drawItem(windowMessage);
		break;

	case WM_DESTROY:
		PostQuitMessage(WM_QUIT);
		break;

	default:
		return (DefWindowProc(hWnd, Msg, wParam, lParam));
	}

	return (returnValue);

}
//
///////////////////////////////////////////////////////////////////////////////
// Main application WinProc for Windows Class of Main Window
///////////////////////////////////////////////////////////////////////////////
//
LRESULT CALLBACK MainWindowView::MainWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	MainWindowView *view;

	if (Msg == WM_NCCREATE)  // Non-Client Create
	{
		// WM_NCCREATE message is called before non-client parts(border,
		// titlebar, menu,etc) are created. This message comes with a pointer
		// to CREATESTRUCT in lParam. The lpCreateParams member of CREATESTRUCT
		// actually contains the value of lpPraram of CreateWindowEX().
		// First, retrieve the pointer to the controller specified when
		// MainWin is setup.
		view = (MainWindowView*) (((CREATESTRUCT*) lParam)->lpCreateParams);

		// Second, store the pointer to the Controller into GWLP_USERDATA,
		// so, other message can be routed to the associated Controller.
		// Set in GWLP_USERDATA so other window procedures of the applications
		// window classes can use the same Module controller
		::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR) view);

		return (::DefWindowProc(hWnd, Msg, wParam, lParam));
	}

	view = (MainWindowView *) GetWindowLongPtr(hWnd, GWL_USERDATA);
	// check NULL pointer, because GWLP_USERDATA is initially 0, and
	// we store a valid pointer value when WM_NCCREATE is called.
	if (!view)
		return (::DefWindowProc(hWnd, Msg, wParam, lParam));
	else
		return (view->dispatchMessage(hWnd, Msg, wParam, lParam));

}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
int MainWindowView::save(const Module::Message &message)
{
	presenter->saveEditControl();
	return (0);

}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
// Update our bind sources from the WIN32 controls and dialogs
void MainWindowView::readEditControl()
{
	textSource.setItem(0, componentEdit.readText());
	componentListView.acceptChanges();
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showValidateError()
{
//    DialogResult result = MessageBox.Show(
//        ViewUtility.BuildValidationMessage(m_validateFields, m_validationErrors),
//        "Error",
//        MessageBoxButtons.OK | MessageBoxButtons.OKCancel, MessageBoxIcon.Information,
//        MessageBoxDefaultButton.Button1);

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showError(std::string message)
{
	componentProblem.updateText(message);
//    labelErrorMessage.Text = message;
//    errorProvider1.SetError(labelErrorMessage,
//        ViewUtility.BuildValidationMessage(m_validateFields, m_validationErrors));

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showErrorDialog(std::string message)
{
//    labelErrorMessage.Text = "";
//    errorProvider1.SetError(labelErrorMessage, message);
//    DialogResult result = MessageBox.Show(message, "Error",
//        MessageBoxButtons.OK, MessageBoxIcon.Information,
//        MessageBoxDefaultButton.Button1);

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showStatus(std::string message)
{
//	SetDlgItemTextA(NULL, IDC_MAIN_EDIT, message.c_str());
//    toolStripStatusLabel1.Text = message;

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::showView(const Module::Message &message)
{
	// We will display the main window as a regular object and update it
	ShowWindow(hMainWindow, SW_SHOWNORMAL);
	UpdateWindow(hMainWindow);
	static int shown = 0;
	std::stringstream title;
	title << "Rainbow - Titled " << std::to_string(++shown);
	SendMessage(hMainWindow, WM_SETTEXT, 0, (LPARAM) title.str().c_str());
	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::closeView(const Module::Message &message)
{
	PostMessage(hMainWindow, WM_CLOSE, 0, 0);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// private function - window messages ncAtivate
///////////////////////////////////////////////////////////////////////////////
// For activation/deactivation of docked/floating windows
LRESULT MainWindowView::ncActivate(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	return (DockWnd::HANDLE_NCACTIVATE(hMainWindow, hMainWindow, args.wParamLParam.wparam, args.wParamLParam.lparma));

}
///////////////////////////////////////////////////////////////////////////////
// private function - window messages ncAtivate
///////////////////////////////////////////////////////////////////////////////
// For activation/deactivation of docked/floating windows
LRESULT MainWindowView::enable(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	return (DockWnd::HANDLE_ENABLE(hMainWindow, hMainWindow, args.wParamLParam.wparam, args.wParamLParam.lparma));

}
//
///////////////////////////////////////////////////////////////////////////////
// private function - the top half splitter
///////////////////////////////////////////////////////////////////////////////
//
int MainWindowView::calculateRatioExtent(int y, int height)
{
	int calcRatio = y * 100 / (height); // set size
	if (calcRatio < 5)
		calcRatio = 5;
	else if (calcRatio > 95)
		calcRatio = 95;

	return (calcRatio);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function
///////////////////////////////////////////////////////////////////////////////
//
LRESULT MainWindowView::getDispInfo(const App::WindowMessage &message)
{
	//	ListView_SubItemHitTest
	App::Parameter::Args args = message.argumments();
	LV_DISPINFO *pnmv = (LV_DISPINFO *)args.wParamLParam.lparma;
    // Provide the item or subitem's text, if requested.
    if (pnmv->item.mask & LVIF_TEXT) {
        App::Component::ListItem *pItem = (App::Component::ListItem *) (pnmv->item.lParam);
//        std::cout << "iSubItem=" << pnmv->item.iSubItem ;
//        std::cout << "pItem=" << pItem << " size: " << pItem->subitems.size() << std::endl;
        std::string value = pItem->subitems.at(pnmv->item.iSubItem);
		strncpy(pnmv->item.pszText, value.c_str(), value.length() + 1);
    }

	return (0);
}
//
//---------------------------------------------------------------------------
// private function
// Main_OnEndLabelEdit - processes the LVN_ENDLABELEDIT notification message.
// Returns TRUE if the label is changed or FALSE otherwise.
//---------------------------------------------------------------------------
//
BOOL MainWindowView::onEndLabelEdit(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	LV_DISPINFO *pnmv = (LV_DISPINFO *)args.wParamLParam.lparma;
    App::Component::ListItem *pItem;

    // The item is -1 if editing is being canceled.
    if (pnmv->item.iItem == -1)
        return (FALSE);

	std::cout << "Parent Main onLabelEdit:"
			<< " lParam:" << pnmv->item.lParam
			<< " iItem:" << pnmv->item.iItem
			<< " iSubItem:" << pnmv->item.iSubItem << std::endl;
   // Copy the new text to the application-defined structure,
    pItem = (App::Component::ListItem *) (pnmv->item.lParam);
	pItem->subitems.at(pnmv->item.iSubItem) = std::string(pnmv->item.pszText);
    // No need to set the item text, because it is a callback item.
    return (TRUE);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function
///////////////////////////////////////////////////////////////////////////////
//
// ListViewCompare - sorts the list view control. It is a
//     comparison function.
// Returns a negative value if the first item should precede the
//     second item, a positive value if the first item should

//     follow the second item, and zero if the items are equivalent.
// lParam1 and lParam2 - item data for the two items (in this
//     case, pointers to application-defined MYITEM structures)
// lParamSort - value specified by the LVM_SORTITEMS message
//     (in this case, the index of the column to sort)
int MainWindowView::listViewCompare(
    LPARAM lParam1,
    LPARAM lParam2,
    LPARAM lParamSort)
{
    App::Component::ListItem *pItem1 = (App::Component::ListItem *) (lParam1);
    App::Component::ListItem *pItem2 = (App::Component::ListItem *) (lParam2);
    // Compare the specified column.
    int iCmp = pItem1->subitems.at(lParamSort).compare(pItem2->subitems.at(lParamSort));
    // Return the result if nonzero, or compare the
    // first column otherwise.
    return ((iCmp != 0) ? iCmp :
    		pItem1->subitems.at(0).compare(pItem2->subitems.at(0)));
}
//
///////////////////////////////////////////////////////////////////////////////
// public function - the bottom half splitter
///////////////////////////////////////////////////////////////////////////////
//
LRESULT MainWindowView::moveBotSplit(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	LPARAM lParam = args.wParamLParam.lparma; // position of y for bottom split
	splitRatioBottom = calculateRatioExtent((int) lParam, clientHeight);
	Send_WM_Size();
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function - the top half splitter
///////////////////////////////////////////////////////////////////////////////
//
LRESULT MainWindowView::moveTopSplit(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	LPARAM lParam = args.wParamLParam.lparma; // position of y for top split
	splitRatioTop = calculateRatioExtent((int) lParam, clientHeight);
	Send_WM_Size();
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function - tabbed changes selection
///////////////////////////////////////////////////////////////////////////////
LRESULT MainWindowView::changeTab(const App::WindowMessage &/*message*/)
{
	pageTabIndex = TabCtrl_GetCurSel(componentTabbed.handle());	// Get tab page index
	InvalidateRect(componentTabbed.handle(), NULL, TRUE);
	UpdateWindow(componentTabbed.handle());
	Send_WM_Size();
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function - tabbed dock window removal from a tab control
///////////////////////////////////////////////////////////////////////////////
//
LRESULT MainWindowView::tabChangeDockWnd(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	App::NMDOCKTABCHANGE *dtc = (App::NMDOCKTABCHANGE*) args.wParamLParam.lparma;	//lParam;
	UINT code = dtc->hdr.code; // DWN_TABADD or DWN_TABREMOVE
	UINT idDock = dtc->hdr.idFrom; // All dock component contents have ID
	UINT idTabbed = dtc->uTabComponentId; // All tabbed components have ID
	App::Component::Tabbed* tabControl = nullptr;
	App::DockWnd* dockWnd = nullptr;

	if (idTabbed > 0) // If removed or added the tabbed component is found
	{
		switch (idTabbed)
		{
		case IDC_MAIN_TABBED: // IDC_MAIN_TABBED
			tabControl = &componentTabbed;
			break;
		default: /* no default */
			break;
		}

		if (tabControl)
		{
			switch (idDock)
			{
			case IDC_MAIN_OUTPUT: // Output component
				if (code == DWN_TABREMOVE)
					tabControl->removeTabWindow(&componentOutputDock);
				if (code == DWN_TABADD)
					tabControl->addTabWindow(&componentOutputDock);
				break;

			case IDC_MAIN_PROBLEM: // Problem component
				if (code == DWN_TABREMOVE)
					tabControl->removeTabWindow(&componentProblemDock);
				if (code == DWN_TABADD)
					tabControl->addTabWindow(&componentProblemDock);
				break;
			case IDD_PERSON: // Problem component
				if (code == DWN_TABREMOVE)
					tabControl->removeTabWindow(&componentPersonDock);
				if (code == DWN_TABADD)
					tabControl->addTabWindow(&componentPersonDock);
				break;
			case IDC_TOOLPOPUP: // View Loader component from Module controller
			case IDC_RESULTVIEW: // View Loader component from Module controller
				dockWnd = DockWnd_ById(idDock);
				if (dockWnd)
				{
					if (code == DWN_TABREMOVE)
						tabControl->removeTabWindow(dockWnd);
					if (code == DWN_TABADD)
						tabControl->addTabWindow(dockWnd);
				}
				break;

			default:/* Do nothing */
				break;
			}
		}
	}
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function - tabbed dock window removal from a tab control
///////////////////////////////////////////////////////////////////////////////
//
void MainWindowView::removeTabbedDockWnd(WPARAM wParam)
{
	App::DockWnd* dockWnd = DockWnd_ById(wParam); // All dockwnds are contained in Main window dock container
	if (dockWnd != nullptr)
	{
		if (dockWnd->IdTabbedWnd() != 0)
		{ // It is in a tabbed control - which one?
			if (dockWnd->IdTabbedWnd() == IDC_MAIN_TABBED)
				componentTabbed.removeTabWindow(DockWnd_ById(wParam));
		}
	}
}
//
//---------------------------------------------------------------
// Public function
// Begin dragging the dockwnd docking/floating rectangle
//---------------------------------------------------------------
//
long MainWindowView::dockWndClosed(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	WPARAM wParam = args.wParamLParam.wparam;
	switch (wParam)
	{
	case IDC_TOOLPOPUP: // Close the Module View windows
		// Make sure removed from main views tabbed control
		removeTabbedDockWnd(wParam);
		presenter->closeToolPopup(); // Will resize owner from its View module instance(this main window)
		break;
	case IDC_RESULTVIEW:
		// Make sure removed from main views tabbed control
		removeTabbedDockWnd(wParam);
		presenter->closeResults(); // Will resize owner from its View module instance(this main window)
		break;
	case IDC_MAIN_PROBLEM: // Main views dockable window components are only hidden
		componentProblemDock.setVisible(FALSE); // Will resize main window as is a dockwnd
		break;
	case IDC_MAIN_OUTPUT: // Main views dockable window components are only hidden
		componentOutputDock.setVisible(FALSE);
		break;
	case IDD_PERSON: // Main views dockable window components are only hidden
		componentPersonDock.setVisible(FALSE);
		break;
	default:/* Do Nothing */
		break;
	}
	return (0);
}
//
//---------------------------------------------------------------
// Private function
//---------------------------------------------------------------
//
void MainWindowView::Send_WM_Size()
{
	LPARAM lparam = MAKELPARAM(clientWidth, clientHeight);
	SendMessage(hMainWindow, WM_SIZE, 0, (LPARAM) lparam);
}
//
//---------------------------------------------------------------
// Private function
//---------------------------------------------------------------
//
void MainWindowView::ContainerBoundary(HWND hwnd, RECT& rc1, RECT& rc2)
{
	// Get main window "outer" rectangle
	GetWindowRect(hwnd, &rc1);
	// Get main window "inner" rectangle
	GetClientRect(hwnd, &rc2);
	MapWindowPoints(hwnd, 0, (POINT*) (&rc2), 2);
	InflateRect(&rc2, -2, -2);
}
//
//---------------------------------------------------------------
// Public function
// Begin dragging the dockwnd docking/floating rectangle
//---------------------------------------------------------------
//
long MainWindowView::isDockWnd_Dockable(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	LPARAM lParam = args.wParamLParam.lparma;
	RECT rc1, rc2;
	// Get main window "outer" rectangle
	ContainerBoundary(hMainWindow, rc1, rc2);
	UINT uDockSide = DockWnd_GetDockSide(hMainWindow, (App::NMDOCKWNDQUERY*) lParam, &rc1, &rc2);

	if (uDockSide == DWS_DOCKED_FLOATING)
	{
		// Look for docked tabbed component
		ContainerBoundary(componentTabbed.handle(), rc1, rc2);
		uDockSide = DockWnd_GetTabbedClient(componentTabbed.handle(), (App::NMDOCKWNDQUERY*) lParam, &rc2);
	}
	return (uDockSide);

}
///////////////////////////////////////////////////////////////////////////////
// public function
// Paint WIN32 WM_PAINT
///////////////////////////////////////////////////////////////////////////////
LRESULT MainWindowView::paint(const App::WindowMessage &message)
{
	HDC hdc;
	PAINTSTRUCT ps;
	hdc = BeginPaint(hMainWindow, &ps);

	// Polygons drawn here - in polygon component class
	// Window Client information draw
	App::Component::WindowFramed::paint(hdc, ps);

	EndPaint(hMainWindow, &ps);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function
// handle Win32 WM_SIZE
///////////////////////////////////////////////////////////////////////////////
LRESULT MainWindowView::size(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	clientWidth = LOWORD(args.wParamLParam.lparma);
	clientHeight = HIWORD(args.wParamLParam.lparma);
	HDWP hdwp;
	RECT middleRect;

	SendMessage(componentToolBar.handle(), TB_AUTOSIZE, 0, 0);
	RECT rect;
	GetWindowRect(componentToolBar.handle(), &rect);

	// First the dock containers dock wnd docked
	// Work out where dock windows are allowed
	// (Take into account status bar, toolbar etc)
	// Position the specified dock windows. rect will be modified to contain the
	// "inner" client rectangle, where we can position an MDI client,
	// view window, whatever
	SetRect(&middleRect, 0, rect.bottom - rect.top, clientWidth, clientHeight);
	hdwp = BeginDeferWindowPos(dockerCount());
	DockWnd_Position(hMainWindow, hdwp, &middleRect);
	EndDeferWindowPos(hdwp);

	int ySplitTopY = ((clientHeight * splitRatioTop) / 100);
	int ySplitBottomY = ((clientHeight * splitRatioBottom) / 100);

	if (ySplitTopY < middleRect.top)
		ySplitTopY = middleRect.top + splitWidth;
	if (ySplitTopY >= (middleRect.bottom - splitWidth - 16))
		ySplitTopY = middleRect.bottom - splitWidth - 16;

	if (ySplitBottomY <= ySplitTopY)
		ySplitBottomY = ySplitTopY + splitWidth + 1;
	if (ySplitBottomY >= (middleRect.bottom - splitWidth - 8))
		ySplitBottomY = middleRect.bottom - splitWidth - 8;

	SetWindowControls(&middleRect, ySplitTopY, ySplitBottomY);

	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
LRESULT MainWindowView::selectFont(const App::WindowMessage &message)
{
	styleFramed().selectFont(hMainWindow);
	style(styleFramed());
	InvalidateRect(hMainWindow, NULL, TRUE);
	UpdateWindow(hMainWindow);
	Send_WM_Size();
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
LRESULT MainWindowView::selectColour(const App::WindowMessage &message)
{
	styleFramed().selectColour(hMainWindow);
	style(styleFramed());
	InvalidateRect(hMainWindow, NULL, TRUE);
	UpdateWindow(hMainWindow);
	return (0);
}
//
//---------------------------------------------------------------
// Public function
// Erase background - do the markers on main window
//---------------------------------------------------------------
//
long MainWindowView::controlBackground(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	HDC hdc = (HDC) args.wParamLParam.wparam;
	HWND controlHWnd = (HWND) args.wParamLParam.lparma;
	if (GetDlgCtrlID(controlHWnd) == IDC_MAIN_EDIT || GetDlgCtrlID(controlHWnd) == IDC_MAIN_OUTPUT
			|| GetDlgCtrlID(controlHWnd) == IDC_MAIN_PROBLEM)
	{
		HDC hdcEditControl = hdc;
		SetTextColor(hdcEditControl, styleFramed().rgbText);
		SetBkColor(hdcEditControl, styleFramed().rgbBackground);
		return ((LONG) styleFramed().hbrBackground);
	}
	else
	{
		return (0);
	}
}
//
//---------------------------------------------------------------
// Public function
// Mouse movement - do the markers on main window
//---------------------------------------------------------------
//
long MainWindowView::drawItem(const App::WindowMessage &message)
{
	LPDRAWITEMSTRUCT lpdis;
	HDC hdcMem;
	App::Parameter::Args args = message.argumments();
	lpdis = (LPDRAWITEMSTRUCT) args.wParamLParam.lparma;
	hdcMem = CreateCompatibleDC(lpdis->hDC);
	int offset = 0;  //64;
	if (lpdis->itemState & ODS_SELECTED)  // if selected
		offset = 16;
	else
		offset = 0;
	//SelectObject(hdcMem, hbm1);
	// Destination
	StretchBlt(lpdis->hDC,         // destination DC
			lpdis->rcItem.left, // x upper left
			lpdis->rcItem.top,  // y upper left
			// The next two lines specify the width and
			// height.
			lpdis->rcItem.right - lpdis->rcItem.left, lpdis->rcItem.bottom - lpdis->rcItem.top, hdcMem, // source device context
			offset, 0,      // x and y upper left
			16,        // source bitmap width
			16, SRCCOPY);        // source bitmap height

	DeleteDC(hdcMem);
	return (1L);
}
//
//---------------------------------------------------------------
// Public function
// Erase background - do the markers on main window
//---------------------------------------------------------------
//
long MainWindowView::eraseBackground(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	HDC hdc = (HDC) args.wParamLParam.wparam;
	RECT rc;
	GetClientRect(hMainWindow, &rc);
	SetMapMode(hdc, MM_ANISOTROPIC);
	SetWindowExtEx(hdc, 100, 100, NULL);
	SetViewportOrgEx(hdc, 0, 0, NULL);
	SetViewportExtEx(hdc, rc.right, rc.bottom, NULL);
	FillRect(hdc, &rc, styleFramed().hbrWhite);
	int i, x, y;
	for (i = 0; i < 13; i++)
	{
		x = (i * 40) % 100;
		y = ((i * 40) / 100) * 20;
		SetRect(&rc, x, y, x + 20, y + 20);
		FillRect(hdc, &rc, styleFramed().hbrGray);
	}
	SetMapMode(hdc, MM_TEXT);

	return (1L);
}
//
///////////////////////////////////////////////////////////////////////////////
// private function
///////////////////////////////////////////////////////////////////////////////
//
void MainWindowView::SetWindowControls(RECT *rc, int sTopY, int sBotY)
{
	HDWP hdwp;
	RECT topRc, editRc, tabRc, staticRc, staticRc2, sBotRc, sTopRc;
//	HD_LAYOUT hdl;
//	WINDOWPOS wp;
	// Size the main window controls in the client area
	// topRc needs top third of middleRect
	// editRc need 2nd third of middleRect
	// tabbedRc needs last third of middleRect
	// The dividing positions are splitTopY for 1 to 2, and splitBottomY for 2 to 3
	SetRect(&topRc, rc->left, rc->top, rc->right - rc->left, (sTopY - splitWidth) - rc->top);
//	hdl.prc = &topRc;
//	hdl.pwpos = &wp;
//	SendMessage(componentHeader.handle(), HDM_LAYOUT, 0, (LPARAM) &hdl);

	SetRect(&sTopRc, rc->left, sTopY - splitWidth, rc->right - rc->left, splitWidth);

	if (componentTabbed.totalTabs() == 0)
	{ // No tabs then expand over
		SetRect(&editRc, rc->left, sTopY, rc->right - rc->left, rc->bottom - sTopY);

		SetRect(&sBotRc, rc->left, sBotY, rc->right - rc->left, splitWidth);
		SetRect(&tabRc, rc->left, sBotY + splitWidth, rc->right - rc->left, rc->bottom - sBotY - splitWidth);
		staticRc = tabRc;

	}
	else
	{
		SetRect(&editRc, rc->left, sTopY, rc->right - rc->left, sBotY - sTopY);
		SetRect(&sBotRc, rc->left, sBotY, rc->right - rc->left, splitWidth);
		SetRect(&tabRc, rc->left, sBotY + splitWidth, rc->right - rc->left, rc->bottom - sBotY - splitWidth);
		staticRc = tabRc;
		TabCtrl_AdjustRect(componentTabbed.handle(), FALSE, &staticRc);
		staticRc.bottom += tabRc.top;
		SetRect(&staticRc2, staticRc.left, staticRc.top, staticRc.right - staticRc.left,
				staticRc.bottom - staticRc.top);
	}

	hdwp = BeginDeferWindowPos(8);
	if (hdwp != NULL)
	{

		componentListView.setWindowRect(topRc);
		componentListView.setPositionFlag(NULL, SWP_NOZORDER);
//		componentPersonDialog.setWindowRect(topRc);
//		componentPersonDialog.setPositionFlag(NULL, SWP_NOZORDER);
		componentTopSplit.setWindowRect(sTopRc);
		componentTopSplit.setPositionFlag(NULL, (SWP_NOREDRAW | SWP_NOZORDER));
		componentEdit.setWindowRect(editRc);
		componentEdit.setPositionFlag(NULL, SWP_NOZORDER);

		componentBotSplit.setWindowRect(sBotRc);
		componentBotSplit.setPositionFlag(NULL, (SWP_NOREDRAW | SWP_NOZORDER));
		componentTabbed.setWindowRect(tabRc);
		componentTabbed.setPositionFlag(NULL, SWP_NOZORDER);

		hdwp = deferSize(hdwp);
		if (hdwp == NULL)
			return;

		if (hdwp != NULL)
		{
			if (EndDeferWindowPos(hdwp) == 0)
			{
				DWORD error = GetLastError();
				std::cerr << "ERROR: " << error << std::endl;
			}
		}
	}

	::InvalidateRect(componentTopSplit.handle(), 0, TRUE);
	::UpdateWindow(componentTopSplit.handle());
	::InvalidateRect(componentBotSplit.handle(), 0, TRUE);
	::UpdateWindow(componentBotSplit.handle());
	::InvalidateRect(componentToolBar.handle(), 0, TRUE);
	SendMessage(componentToolBar.handle(), TB_AUTOSIZE, 0, 0);
	ShowWindow(componentToolBar.handle(), TRUE);

}

///////////////////////////////////////////////////////////////////////////////
// About Application dialog
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::AboutApplication(HWND hWnd)
{
	// Show a modal dialog - it implements its own message loop and procedure
	int ret = DialogBox(GetModuleHandle(NULL),
			MAKEINTRESOURCE(IDD_ABOUT), hWnd, AboutDlgProc);
	if (ret == -1)
	{
		MessageBox(hWnd, "Dialog failed!", "Error",
		MB_OK | MB_ICONINFORMATION);
	}
}

///////////////////////////////////////////////////////////////////////////////
// About application WinProc
///////////////////////////////////////////////////////////////////////////////
BOOL CALLBACK MainWindowView::AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	LRESULT returnValue = 0;        // return value
	//Caution: use Windows BOOL, not C++ bool
	// Return FALSE for messages you don't handle and TRUE for those you do (If otherwise told not to!
	//  use the Help win32 notes. )
	switch (Message)
	{
	case WM_INITDIALOG: // For initializing dialogs (not WM_CREATE, controls wont be created if you did)
		returnValue = 1L;
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			EndDialog(hwnd, IDOK); //do not call DestroyWindow() for dialogs
			returnValue = 1L;
			break;

		case IDCANCEL:
			EndDialog(hwnd, IDCANCEL); // do not call DestroyWindow() for dialogs
			returnValue = 1L;
			break;
		}
		break;
	default:
		break; // Modal Dialog window procedures don't use DefWindowProc
	}
	return (returnValue);
}

} /* namespace App */

