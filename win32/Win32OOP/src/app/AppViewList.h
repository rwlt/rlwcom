/*
 * AppViewList.h
 *
 *  Created on: Jun 3, 2015
 *      Author: Rodney Woollett
 */

#ifndef APPVIEWLIST_H_
#define APPVIEWLIST_H_

#include "module/ViewList.h"
#include "control/BindSource.h"
#include <vector>
//
//-----------------------------------------------------------------
// AppViewItem<DataType>: Used to move a collection of items from the presenter using
//                        View interface function with ViewList<DataType>
//-----------------------------------------------------------------
//
template<class DataType>
class AppViewList: public Module::ViewList<DataType>
{
public:
	AppViewList(Control::BindSource<DataType> &container):
			m_container(&container)
	{
	}
	virtual ~AppViewList()
	{
		std::cout << "AppViewList deconstructor" << std::endl;
	}
	AppViewList(const AppViewList& oldViewList) :
			m_container(oldViewList.m_container)
	{
		std::cout << "AppViewList copy constructor" << std::endl;
	}
	AppViewList operator = (const AppViewList& oldViewList)
	{
		m_container = oldViewList.m_container;
		std::cout << "AppViewList assignment copy" << std::endl;
		return (*this);
	}
	virtual void add(DataType item)
	{
		std::cout << "AppViewList add item" << std::endl;
		m_container->add(item);//->push_back(item);
	}
	virtual void clear()
	{
		m_container->clear();
	}
	virtual unsigned int size()
	{
		return (m_container->size());
	}
	virtual void setPosition(int value){
		m_container->setPosition(value);
	}
	virtual int getPosition(){
		return (m_container->getPosition());
	}
	virtual DataType& getSelectItem()
	{
		return (m_container->getSelectItem());
	}
	// Define the iterator typedefs
	typedef typename std::vector<DataType>::iterator iterator;
	typedef typename std::vector<DataType>::const_iterator const_iterator;
	iterator begin()
	{
		std::cout << "AppViewList begin iteration" << std::endl;
		return (m_container->begin());
	}
	iterator end()
	{
		std::cout << "AppViewList begin iteration" << std::endl;
		return (m_container->end());
	}
	const_iterator begin() const
	{
		return (m_container->begin());
	}
	const_iterator end() const
	{
		return (m_container->end());
	}

protected:
	Control::BindSource<DataType> * m_container;

};

#endif /* APPVIEWLIST_H_ */
