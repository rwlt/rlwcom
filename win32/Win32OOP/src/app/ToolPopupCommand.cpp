/*
 * ToolPopupCommand.cpp
 *
 *  Created on: May 02, 2015
 *      Author: Rodney Woollett
 */

#include "ToolPopupCommand.h"
#include "ToolPopupView.h"
#include "module/Command.h"
#include "module/Message.h"

namespace App
{

ToolPopupCommand::ToolPopupCommand(Module::View *view,
		int (App::ToolPopupView::*meth)(const Module::Message &)) :
		Module::Command(view), method(meth)
{
}

ToolPopupCommand::~ToolPopupCommand()
{
}

int ToolPopupCommand::execute(const Module::Message &parameters)
{
	LRESULT returnValue = 0;
	App::ToolPopupView *main = static_cast<App::ToolPopupView*>(view);
	returnValue = ((main->*method)(parameters));
	return (returnValue);
}

} /* namespace App */
