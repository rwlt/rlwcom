/*
 * ResultView.cpp
 *
 *  Created on: Apr 25, 2015
 *      Author: Rodney Woollett
 */

#include "windows.h"
#include "../resource.h"
#include "ResultView.h"
#include "module/ToolBoxPresenter.h"
#include <iostream>
#include <string>
#include <sstream>

namespace App
{

///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
ResultView::ResultView() :
		presenter(new Module::ToolBoxPresenter(this)), hResultView(0), clientWidth(0), clientHeight(0)
{
	std::cout << "In ResultView constructor " << std::endl;

}

///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
ResultView::~ResultView()
{
	std::cout << "In ResultView destructor " << " presenter " << presenter << std::endl;
	DestroyWindow(hResultView);
	delete presenter;
	presenter = nullptr;

}
//
///////////////////////////////////////////////////////////////////////////////
// Dispatch the view message from static window procedure
///////////////////////////////////////////////////////////////////////////////
//
Module::ToolBoxPresenter* ResultView::ActivePresenter()
{
	return (presenter);
}
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
int ResultView::createView(HINSTANCE handleInstance, HWND hWndParent)
{
	//Using DockWnd create a dockwnd window
	createDockWnd("Result", handleInstance, hWndParent);
	setFloatPos(325, 175);
	setDockWndStyle(DWS_USEBORDERS |
	DWS_DRAWGRIPPERDOCKED |
	DWS_DRAWGRIPPERFLOATING |
	DWS_BORDERTOP |
	DWS_BORDERBOTTOM );//|
	//DWS_NORESIZE);

	std::stringstream title;
	title << "Contents " << dockWndHandle() << "IDC: IDC_RESULTVIEW";

	hResultView = CreateDialogParam(handleInstance, MAKEINTRESOURCE(IDD_RESULTS), hWndParent, DialogWndProc,
			(LONG_PTR) this);

	std::cout << "hResultView HANDLE: " << hResultView << std::endl;
	if (hResultView == NULL)
	{
		MessageBox(hWndParent, "CreateWindowEx IDC_RESULTVIEW returned NULL", "Warning!",
		MB_OK | MB_ICONINFORMATION);
		return (-1);
	}

	// Set the dock window to the content of the view contents
	this->controlId = (int) IDC_RESULTVIEW;
	SetDockContents(hResultView, hWndParent);

	std::cout << "ResultView created:" << this << " presenter " << presenter << std::endl;
	presenter->initialize();

	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public function
// The windowHandle of the this ToolPopup - use dockWndHandle if the DockWnd
// handle is needed
///////////////////////////////////////////////////////////////////////////////
//
HWND ResultView::windowHandle()
{
	return (hResultView);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
//
void ResultView::save()
{
//	presenter->saveEditControl();

}
//
///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
//
int ResultView::showView(const Module::Message &message)
{
	Module::Parameter::Args args = message.argumments();
	long show = args.oneArg.message;
	if (show > 0)
	{
		setVisible(TRUE); // Dock window will send WM_SHOW and resize owner
	}
	else
	{
		setVisible(FALSE);
	}
	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
int ResultView::closeView(const Module::Message &message)
{
	setVisible(FALSE);
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public function
// handle WM_PAINT
///////////////////////////////////////////////////////////////////////////////
//
LRESULT ResultView::paint(const App::WindowMessage &message)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rcPort;
	hdc = BeginPaint(hResultView, &ps);

	SetMapMode(hdc, MM_TEXT);
	SetViewportOrgEx(hdc, 0, 0, NULL);
	SetRect(&rcPort, 0, 0, 200, 100);

	char szSize[100];
	char szTitle[] = "These are the dimensions of your client area:";

	HFONT hfOld = static_cast<HFONT>(SelectObject(hdc, viewStyle.hFont));
	SetBkColor(hdc, viewStyle.rgbBackground);
	SetTextColor(hdc, viewStyle.rgbText);

	FillRect(hdc, &rcPort, viewStyle.hbrBackground);
	DrawText(hdc, szTitle, -1, &rcPort, DT_WORDBREAK);

	POINT aptPortRect[5] =
	{ 0, 0, 200, 0, 200, 100, 0, 100, 0, 0 };
	Polyline(hdc, aptPortRect, 5);
	wsprintf(szSize, "{%d, %d, %d, %d}", rcPort.left, rcPort.top, rcPort.right, rcPort.bottom);
	DrawText(hdc, szSize, -1, &rcPort,
	DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	SetViewportOrgEx(hdc, 0, 50, NULL);
	char szTitle3[] = "These are the dimensions of your main client area:";
	FillRect(hdc, &rcPort, viewStyle.hbrBackground);
	DrawText(hdc, szTitle3, -1, &rcPort, DT_WORDBREAK);
	Polyline(hdc, aptPortRect, 5);
	wsprintf(szSize, "{%d, %d, %d, %d}", rcPort.left, rcPort.top, rcPort.right, rcPort.bottom);
	DrawText(hdc, szSize, -1, &rcPort,
	DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	SelectObject(hdc, hfOld);  //Change back to stored old font

	EndPaint(hResultView, &ps);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// Public function
// handle WM_SIZE
///////////////////////////////////////////////////////////////////////////////
int ResultView::size(const Module::Message &message)
{
	Module::Parameter::Args args = message.argumments();
	clientWidth = args.twoArgs.valueA;
	clientHeight = args.twoArgs.valueB;
	std::cout << "ResultView size:" << std::endl;
	SetWindowPos(dockWndHandle(), NULL, 0, 0, clientWidth, clientHeight,
	SWP_NOMOVE);
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Dispatch the view message from static window procedure
///////////////////////////////////////////////////////////////////////////////
//
LRESULT ResultView::dispatchDialogMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	LRESULT returnValue = 0;        // return value
//	Module::Message viewMessage;
//	Module::Parameter paramArgs;
	App::WindowMessage windowMessage;
	App::Parameter paramWindowArgs;
	paramWindowArgs.kind = App::ParameterKind::WPARAMLPARAM;
	paramWindowArgs.args.wParamLParam.wparam = wParam;
	paramWindowArgs.args.wParamLParam.lparma = lParam;
	windowMessage.setParameter(paramWindowArgs);

	switch (Msg)
	{
	case WM_PAINT:
		paint(windowMessage);
		std::cout << "ResultView WM_PAINT??? " << std::endl;
		returnValue = FALSE;
		break;
//	case WM_ERASEBKGND:
//		returnValue = eraseBackground(windowMessage);
//		break;
//	case WM_CTLCOLOREDIT:
//		returnValue = controlBackground(windowMessage);
//		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			// BindToSourceVisitor accepted here to do the bind update to source
			//okAccept();
			std::cout << "IDOK " << std::endl;
			//returnValue = postMessage(windowMessage);
			break;
		case IDCANCEL:
			std::cout << "IDCANCEL " << std::endl;
			//returnValue = postMessage(windowMessage);
			break;

		default:
			return (FALSE);
		}
		;
		returnValue = FALSE;
		break;

	default:
		return (FALSE);
	}

	return (returnValue);

}
////
/////////////////////////////////////////////////////////////////////////////////
//// Main application WinProc for Windows Class of Main Window
/////////////////////////////////////////////////////////////////////////////////
////
BOOL CALLBACK ResultView::DialogWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	ResultView *view;
	if (Msg == WM_INITDIALOG)  // Init Dialog - use lParam for our class pointer
	{
		view = (ResultView*) lParam;
		::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR) view);
		return (TRUE);
	}
	view = (ResultView *) GetWindowLongPtr(hWnd, GWL_USERDATA);
	if (!view)
		return (FALSE);
	else
		return (view->dispatchDialogMessage(hWnd, Msg, wParam, lParam));
}
} /* namespace App */

