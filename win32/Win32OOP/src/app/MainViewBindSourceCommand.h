/*
 * MainViewBindSourceCommand.h
 *
 *  Created on: June 4, 2015
 *      Author: Rodney Woollett
 */

#ifndef MAINVIEWBINDSOURCECOMMAND_H_
#define MAINVIEWBINDSOURCECOMMAND_H_
#define CALL_MEMBER_FN(object,ptrToMember)  ((object)->*(ptrToMember))

#include "control/Command.h"
#include "control/BindNotify.h"
#include "MainWindowView.h"

namespace App
{
typedef  int (MainWindowView::*MainViewBindSourceNotify)(Control::Command* sender, const Control::BindNotify &notify);

class MainViewBindSourceCommand: public Control::Command
{
public:
	MainViewBindSourceCommand(MainWindowView *view, MainViewBindSourceNotify meth);
	virtual ~MainViewBindSourceCommand();
	virtual int execute(Control::Command* sender, const Control::BindNotify &notify);
private:
	MainViewBindSourceNotify method;
	MainWindowView *view;

};
}
#endif /* MAINVIEWBINDSOURCECOMMAND_H_ */
