/*
 * ResultView.h
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#ifndef RESULTVIEW_H_
#define RESULTVIEW_H_

#include <string>
#include "module/ToolBoxInterface.h"
#include "module/ToolBoxPresenter.h"
#include "module/Message.h"
#include "app/components/DockWnd.h"
#include "app/components/Style.h"

namespace App
{

class ResultView: public Module::ToolBoxInterface, public App::DockWnd
{
public:
	ResultView();
	virtual ~ResultView();

	int createView(HINSTANCE handleInstance, HWND hWndParent);
	HWND windowHandle();
	LRESULT dispatchDialogMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
	static BOOL CALLBACK DialogWndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

	int showView(const Module::Message &message);
	int closeView(const Module::Message &message);
	LRESULT paint(const App::WindowMessage &message);
    int size(const Module::Message &message);
	void save();
	void selectFont(){};
	void selectColour(){};

private:
	ResultView(const ResultView& old); // no copying allowed
	ResultView operator = (const ResultView& old); // no assignment allowed
	virtual Module::ToolBoxPresenter* ActivePresenter();
	Module::ToolBoxPresenter* presenter;
	HWND hResultView;
	int clientWidth;		// width of client area
	int clientHeight;		// height of client area
	App::Component::Style viewStyle;


};

} /* namespace App */

#endif /* RESULTVIEW_H_ */
