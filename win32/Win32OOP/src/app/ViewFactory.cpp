/*
 * ViewFactory.cpp
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#include "MainWindowView.h"
#include "ToolPopupView.h"
#include "ResultView.h"
#include "app/components/ListView.h"
#include "MainWindowCommand.h"
#include "MainModuleCommand.h"
#include "ToolPopupCommand.h"
#include "ViewFactory.h"
#include <iostream>
#include <sstream>
#include <cstring>

extern LRESULT CALLBACK WndProcSplitter(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam);

namespace App
{

ViewFactory::ViewFactory(HINSTANCE handleInstance) :
		hInstance(handleInstance), className("Win32App"), classSplitterName(
				"Win32SplitterClass"), dockWndName("Win32DockWnd"),
				popupWndName("DialogPopup"), superclassListViewWndName("APPLISTVIEW")
{
	std::memset(moduleView, '\0', sizeof(moduleView));

    //Initialize the application class
	windowsClass.Create(hInstance, className, MainWindowView::MainWndProc);
	windowsClass.Register();

	//Initialize the application splitter class
	// TODO: The Win32SplitterClass: It is a sizer on y axis only could
	//       be made for setting up for x sizes
	windowSplitterClass.CreateSplitter(hInstance, classSplitterName, WndProcSplitter);
	windowSplitterClass.Register();

	dockWndClass.CreateDockWnd(hInstance, dockWndName, DockWnd::DockWndProc);
	dockWndClass.Register();

//	listViewSuperclass.SuperClass(hInstance, superclassListViewWndName,
//			App::Component::ListView::ListViewSuperclassProc);
//	listViewSuperclass.Register();

//	dialogPopupClass.CreateDialogPopup(hInstance, popupWndName, ResultView::DialogPopupWndProc);
//	dialogPopupClass.Register();

}

ViewFactory::~ViewFactory()
{
	std::cout << "ViewFactory destructor" << std::endl;
	windowsClass.UnRegister();
	windowSplitterClass.UnRegister();
	dockWndClass.UnRegister();
	dialogPopupClass.UnRegister();
	listViewSuperclass.UnRegister();

}

//----------------------------------------------------------------------------------
// close
// Delete all open views
//----------------------------------------------------------------------------------
void ViewFactory::close()
{
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
	{
		if (moduleView[index] != 0)
		{
			std::cout << "Destruct " << moduleView[index] << std::endl;
			delete moduleView[index];
			moduleView[index] = 0;
		}
	}

}

//----------------------------------------------------------------------------------
// checkDialogMessage
// For all views created check their modeless dialogs are Main window messaged
//----------------------------------------------------------------------------------
int ViewFactory::checkDialogMessage(MSG* msg)
{
	int process = 0;
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
	{
		if (moduleView[index] != 0)
		{
			if (index == 0) // MainWindow
			{
				App::MainWindowView* mainView =
						static_cast<App::MainWindowView*>(moduleView[index]);
				process = mainView->checkDialogMessage(msg);
				//process = IsDialogMessage(mainView->dialogHandle(), msg);
				if (process)
				{
					break;
				}
			}
			if (index == 1) // ResultView
			{
				App::ResultView* view =
						static_cast<App::ResultView*>(moduleView[index]);
				process = IsDialogMessage(view->windowHandle(), msg);
				if (process)
				{
					break;
				}
			}
//			if (index == 2) // Tool Popup
//			{
//				App::ToolPopupView* view =
//						static_cast<App::ToolPopupView*>(moduleView[index]);
//				process = IsDialogMessage(view->dockWndHandle(), msg);
//				if (process)
//				{
//					break;
//				}
//			}
		}
	}
	return (process);
}

//----------------------------------------------------------------------------------
// checkTranslateAccelerator
// For all views created check their window handles for menu id acceleration
//----------------------------------------------------------------------------------
int ViewFactory::checkTranslateAccelerator(HACCEL hAccelTable, MSG* msg)
{
	int process = 0;
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
//	for (int index = Module::ViewIdentity::TotalViews; index >= 0; --index)
	{
		if (moduleView[index] != 0)
		{
			if (index == 0) // MainWindow
			{
				App::MainWindowView* mainView =
						static_cast<App::MainWindowView*>(moduleView[index]);
				process = TranslateAccelerator(mainView->windowHandle(),
						hAccelTable, msg);
				if (process)
				{
					break;
				}
			}
			if (index == 2) // Tool Popup
			{
				App::ToolPopupView* view =
						static_cast<App::ToolPopupView*>(moduleView[index]);
				process = TranslateAccelerator(view->dockWndHandle(),
						hAccelTable, msg);
				if (process)
				{
					break;
				}
			}
		}
	}
	return (process);
}

//----------------------------------------------------------------------------------
// viewIdentity by HWND
// Use Module ViewIdentity and the total views available to keep
// an array of views to return to caller
//----------------------------------------------------------------------------------
Module::ViewIdentity ViewFactory::viewIdentity(HWND hWnd)
{
	Module::ViewIdentity viewIdentity = Module::ViewIdentity::NullView;
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
	{
		if (moduleView[index] != 0)
		{
			if (index == 0) // MainWindow
			{
				App::MainWindowView* mainView =
						static_cast<App::MainWindowView*>(moduleView[index]);
				if (mainView->windowHandle() == hWnd)
				{
					viewIdentity = Module::ViewIdentity::MainWindow;
					break;
				}
			}
			if (index == 1) // ResultView's
			{
				App::ToolPopupView* view =
						static_cast<App::ToolPopupView*>(moduleView[index]);
				if (view->dockWndHandle() == hWnd)
				{
					viewIdentity = Module::ViewIdentity::ResultView;
					break;
				}
			}
			if (index == 2) // ToolPopup's
			{
				App::ToolPopupView* view =
						static_cast<App::ToolPopupView*>(moduleView[index]);
				if (view->dockWndHandle() == hWnd)
				{
					viewIdentity = Module::ViewIdentity::ToolPopup;
					break;
				}
			}
			if (index == 3) // PersonEdit
			{
				App::ToolPopupView* view =
						static_cast<App::ToolPopupView*>(moduleView[index]);
				if (view->dockWndHandle() == hWnd)
				{
					viewIdentity = Module::ViewIdentity::PersonEdit;
					break;
				}
			}
		}
	}
	return (viewIdentity);
}
//----------------------------------------------------------------------------------
// identityInstance
// Use Module ViewIdentity to get View Implementations instance
//----------------------------------------------------------------------------------
Module::View* ViewFactory::identityInstance(
		Module::ViewIdentity viewIdentity)
{
	Module::View* view = nullptr;

	// We find the view here
	if (moduleView[viewIdentity] != 0)
	{
		view = moduleView[viewIdentity];
	}
	return (view);

}
//----------------------------------------------------------------------------------
// createInstance
// Use Module ViewIdentity and the total views available to keep
// an array of views to return to caller
//----------------------------------------------------------------------------------
Module::View* ViewFactory::createInstance(
		Module::ViewIdentity viewIdentity,
		Module::ViewIdentity parentViewIdentity)
{
	Module::View* view = nullptr;
	std::string viewName = "_"; //Module::ViewIdentity;
	Module::View* parentView = nullptr;
	App::MainWindowView* mainView = nullptr;
	App::ToolPopupView* toolView = nullptr;
	App::ResultView* resultView = nullptr;
	BOOL created;

	// We find the view here
	if (moduleView[viewIdentity] != 0)
	{
		view = moduleView[viewIdentity];
	}
	else // If not found create and add to the moduleViews in array index
	{
		//If parentViewIdentity a view then get the views HWND for parent usage for a child view
		if (parentViewIdentity != Module::ViewIdentity::NullView)
		{
			if (moduleView[parentViewIdentity] != 0)
				parentView = moduleView[parentViewIdentity];
			else
			{
				std::stringstream message;
				message
						<< "Implementation required for Module::ViewIdentity. Enumerator index is "
						<< viewIdentity << " " << std::endl;
				MessageBox(nullptr, message.str().c_str(), "Notice",
				MB_OK | MB_ICONINFORMATION);
				PostQuitMessage(WM_QUIT);
			}
		}

		switch (viewIdentity)
		{
		case Module::ViewIdentity::MainWindow:

			mainView = new App::MainWindowView();
			std::cout << "Construct MainWindow" << std::endl;
			moduleView[viewIdentity] = mainView;
			created = mainView->createView(hInstance, className);
			if (!created) {
				delete mainView;
				mainView = nullptr;
			}
			view = mainView;
			break;
		case Module::ViewIdentity::ResultView:
			// Dockable window and the DockContainer is the parent MainWindow
			if (parentViewIdentity == Module::ViewIdentity::MainWindow)
			{
				mainView = static_cast<App::MainWindowView*>(parentView);

				resultView = new App::ResultView();
				std::cout << "Construct ResultView" << std::endl;
				moduleView[viewIdentity] = resultView;
				resultView->createView(hInstance, mainView->windowHandle());

				//After the Docked window created add it to the DockContainer(MainWindow)
				mainView->addDockWindow(resultView);

				view = resultView;
			}
			break;

		case Module::ViewIdentity::ToolPopup:
		//case Module::ViewIdentity::MessageBoxView:
			// Dockable window and the DockContainer is the parent MainWindow
			if (parentViewIdentity == Module::ViewIdentity::MainWindow)
			{
				mainView = static_cast<App::MainWindowView*>(parentView);

				toolView = new App::ToolPopupView();
				std::cout << "Construct ToolBox" << std::endl;
				moduleView[viewIdentity] = toolView;
				toolView->createView(hInstance, mainView->windowHandle());

				//After the Docked window created add it to the DockContainer(MainWindow)
				mainView->addDockWindow(toolView);

				view = toolView;
			}
			break;

		default:
			view = nullptr; //new Module::View(); // A default view
			break;
		}
	}
	if (view == nullptr)
	{
		std::stringstream message;
		message
				<< "Implementation required for Module::ViewIdentity. Enumerator index is "
				<< viewIdentity << " " << std::endl;
		MessageBox(nullptr, message.str().c_str(), "Notice",
		MB_OK | MB_ICONINFORMATION);
		PostQuitMessage(WM_QUIT);
	}
	return (view);
}
//----------------------------------------------------------------------------------
// destroyInstance by Module::ViewIdentity
//----------------------------------------------------------------------------------
void ViewFactory::destroyInstance(Module::ViewIdentity viewIdentity)
{
	if (moduleView[viewIdentity] != 0)
	{
		App::ToolPopupView* popupView;
		App::ResultView* resultView;
		App::MainWindowView* mainView;

		// the one command to perform by viewIdentity and resID
		switch (viewIdentity)
		{
		case Module::ViewIdentity::MainWindow:
			mainView = static_cast<App::MainWindowView*>(identityInstance(
					viewIdentity));

			if (mainView != nullptr)
			{
				destroyInstance(mainView->windowHandle());
			}
			break;
		case Module::ViewIdentity::ResultView:
			resultView = static_cast<App::ResultView*>(identityInstance(
					viewIdentity));
			if (resultView != nullptr)
			{
				destroyInstance(resultView->dockWndHandle());
			}
			break;
		case Module::ViewIdentity::ToolPopup:
		//case Module::ViewIdentity::MessageBoxView:
			popupView = static_cast<App::ToolPopupView*>(identityInstance(
					viewIdentity));
			if (popupView != nullptr)
			{
				destroyInstance(popupView->dockWndHandle());
			}
			break;
		default:
			/* no default */
			break;
		}
	}
}
//----------------------------------------------------------------------------------
// destroyInstance by HWND
//----------------------------------------------------------------------------------
void ViewFactory::destroyInstance(HWND hWnd)
{
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
	{
		if (moduleView[index] != 0)
		{
			if (index == 0) // MainWindow
			{
				App::MainWindowView* mainView =
						static_cast<App::MainWindowView*>(moduleView[index]);
				if (mainView->windowHandle() == hWnd)
				{
					std::cout << "Destruct " << moduleView[index] << std::endl;
					delete moduleView[index];
					moduleView[index] = 0;
					break;
				}
			}
			if (index >= 1 && index <= 3)
			{
				App::MainWindowView* parent =
						static_cast<App::MainWindowView*>(moduleView[Module::ViewIdentity::MainWindow]);
				App::ToolPopupView* view =
						static_cast<App::ToolPopupView*>(moduleView[index]);
				if (view->dockWndHandle() == hWnd)
				{
					std::cout
							<< "YES dockWnd Handle used to remove view identity"
							<< std::endl;
					parent->removeDockWindow(view);
					std::cout << "Destruct " << moduleView[index]
							<< std::endl;
					delete moduleView[index];
					moduleView[index] = 0;
					break;
				}
			}
		}
	}
}


} // end namespace App

