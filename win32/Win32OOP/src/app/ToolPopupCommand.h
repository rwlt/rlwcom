/*
 * ToolPopupCommand.h
 *
 *  Created on: May 02, 2015
 *      Author: Rodney Woollett
 */

#ifndef TOOPOPUPCOMMAND_H_
#define TOOPOPUPCOMMAND_H_

#include "ToolPopupView.h"
#include "module/View.h"
#include "module/Command.h"
#include "module/Message.h"

namespace App
{

class ToolPopupCommand: public Module::Command
{
public:
	ToolPopupCommand(Module::View *view,
			int (App::ToolPopupView::*meth)(const Module::Message &));
	virtual ~ToolPopupCommand();
	int execute(const Module::Message &parameters);
private:
	int (App::ToolPopupView::*method)(const Module::Message &);
};

} /* namespace App */

#endif /* TOOPOPUPCOMMAND_H_ */
