#ifndef APPVIEWITEM_H_
#define APPVIEWITEM_H_

#include "module/ViewList.h"
#include "control/BindSource.h"
#include <iterator>
#include <vector>
#include <iostream>
//
//-----------------------------------------------------------------
// AppViewItem<DataType>: Used to move an item from the presenter using
//                        View interface function with ViewItem<DataType>
//-----------------------------------------------------------------
//
template<typename DataType>
class AppViewItem: public Module::ViewItem<DataType>
{
public:
	AppViewItem(Control::BindSource<DataType> &container) :
		m_container(&container)
	{
	}
	virtual ~AppViewItem()
	{
		std::cout << "AppViewItem deconstructor" << std::endl;
	}
	virtual void setItem(DataType item)
	{
		if (m_container->size() > 0)
			m_container->clear(); // Only one item in bind source for AppViewItem to keep

		m_container->add(item);
		m_container->setPosition(1);
	}
	virtual DataType& getSelectItem()
	{
		return (m_container->getSelectItem());
	}

protected:
	Control::BindSource<DataType> * m_container;

};

#endif /* APPVIEWITEM_H_ */
