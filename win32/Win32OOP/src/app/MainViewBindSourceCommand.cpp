/*
 * MainViewBindSourceCommand.cpp
 *
 *  Created on: June 4, 2015
 *      Author: Rodney Woollett
 */

#include "MainViewBindSourceCommand.h"
#include "MainWindowView.h"
#include "control/Command.h"

namespace App
{
MainViewBindSourceCommand::MainViewBindSourceCommand(MainWindowView *view,
		MainViewBindSourceNotify meth) :
		Control::Command(),  method(meth), view(view)
{
}

MainViewBindSourceCommand::~MainViewBindSourceCommand()
{
}

int MainViewBindSourceCommand::execute(Command* sender, const Control::BindNotify &notify)
{
	return (CALL_MEMBER_FN(view, method)(this, notify));
//	LRESULT returnValue = 0;
//	App::MainWindowView *main = static_cast<App::MainWindowView*>(view);
//	returnValue = ((main->*method)(parameters));
//	return (returnValue);
}
}
