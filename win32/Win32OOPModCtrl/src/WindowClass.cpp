#include "WindowClass.h"
#include "resource.h"

namespace App
{
//-------------------------------------------------------------------------------
WindowClass::WindowClass()
{
}

//-------------------------------------------------------------------------------
// A Main windows class
//-------------------------------------------------------------------------------
void WindowClass::Create(HINSTANCE hInst, const char *ClassName,
		WNDPROC WndPrc, const char* MenuName)
{
	//Initializing the application using the application member variable
	CommonClass(WndPrc, ClassName, hInst);

	//CS_VREDRAW | CS_HREDRAW |
	_WndClsEx.style = CS_VREDRAW | CS_HREDRAW | CS_DBLCLKS;

	_WndClsEx.hIcon = static_cast<HICON>(LoadImage(hInst,
	MAKEINTRESOURCE(IDI_RAINDROP),
	IMAGE_ICON, 32, 32,
	LR_DEFAULTSIZE));
	_WndClsEx.hIconSm = static_cast<HICON>(LoadImage(hInst,
	MAKEINTRESOURCE(IDI_RAINDROP),
	IMAGE_ICON, 16, 16,
	LR_DEFAULTSIZE));
	_WndClsEx.hCursor = LoadCursor(NULL, IDC_ARROW);
	_WndClsEx.hbrBackground = NULL; //static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
	_WndClsEx.lpszMenuName = MAKEINTRESOURCE(IDR_MAIN_MENU);

}

//-------------------------------------------------------------------------------
// A Window class for a splitter window
//-------------------------------------------------------------------------------
void WindowClass::CreateSplitter(HINSTANCE hInst, const char *ClassName,
		WNDPROC WndPrc, const char* MenuName)
{
	//Initializing the application using the application member variable
	CommonClass(WndPrc, ClassName, hInst);

	_WndClsEx.style = 0;
	_WndClsEx.hIcon = 0;
	_WndClsEx.hIconSm = 0;
	_WndClsEx.hCursor = LoadCursor(0, IDC_SIZENS);
	_WndClsEx.hbrBackground = (HBRUSH) (COLOR_BTNFACE + 1);
	_WndClsEx.lpszMenuName = 0;
}

//-------------------------------------------------------------------------------
// A Window class for a popup window
//-------------------------------------------------------------------------------
void WindowClass::CreatePopup(HINSTANCE hInst, const char *ClassName,
		WNDPROC WndPrc, const char* MenuName)
{
	//Initializing the application using the application member variable
	CommonClass(WndPrc, ClassName, hInst);

	_WndClsEx.style = 0;
	_WndClsEx.hIcon = 0;
	_WndClsEx.hIconSm = 0;
	_WndClsEx.hCursor = LoadCursor(0, IDC_ARROW);
	_WndClsEx.hbrBackground = (HBRUSH) (COLOR_BTNFACE + 1);
	_WndClsEx.lpszMenuName = 0;
}

//-------------------------------------------------------------------------------
// A Window class for a dock window
//-------------------------------------------------------------------------------
void WindowClass::CreateDockWnd(HINSTANCE hInst, const char *ClassName,
		WNDPROC WndPrc, const char* MenuName)
{
	//Initializing the application using the application member variable
	CommonClass(WndPrc, ClassName, hInst);
	_WndClsEx.style = 0;
	_WndClsEx.hIcon = 0;
	_WndClsEx.hIconSm = 0;
	_WndClsEx.hCursor = LoadCursor(0, IDC_ARROW);
	_WndClsEx.hbrBackground = (HBRUSH) (COLOR_BTNFACE + 1);
	_WndClsEx.lpszMenuName = 0;
}

//-------------------------------------------------------------------------------
// Common windows class initialize
//-------------------------------------------------------------------------------
void WindowClass::CommonClass(WNDPROC WndPrc, const char* ClassName,
		HINSTANCE hInst)
{
	//Initializing the application using the application member variable
	_WndClsEx.cbSize = sizeof(WNDCLASSEX);
	_WndClsEx.lpfnWndProc = WndPrc;
	_WndClsEx.cbClsExtra = 0;
	_WndClsEx.cbWndExtra = 0;
	_WndClsEx.lpszClassName = ClassName;
	_WndClsEx.hInstance = hInst;
}

//-------------------------------------------------------------------------------
void WindowClass::Register()
{
	RegisterClassEx(&_WndClsEx);
}
//-------------------------------------------------------------------------------
void WindowClass::UnRegister()
{
	UnregisterClass(_WndClsEx.lpszClassName, _WndClsEx.hInstance);
}
//-------------------------------------------------------------------------------

}
