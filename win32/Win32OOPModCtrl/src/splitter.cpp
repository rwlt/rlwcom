#include "resource.h"
#include "splitter.h"
#include <windows.h>
#include <iostream>
#include "Canvas.h"

Pens3d::Pens3d ()
:
	_penLight (GetSysColor (COLOR_3DLIGHT + 1)),
	_penHilight (GetSysColor (COLOR_3DHILIGHT)),
	_penShadow (GetSysColor (COLOR_3DSHADOW)),
	_penDkShadow (GetSysColor (COLOR_3DDKSHADOW))
{}

LRESULT CALLBACK WndProcSplitter (HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam)
{
	SplitController * pCtrl = (SplitController*) ::GetWindowLongPtr(hWnd, GWLP_USERDATA);

    switch (Msg)
    {
    case WM_CREATE:
        try
        {
            pCtrl = new SplitController (hWnd, reinterpret_cast<CREATESTRUCT *>(lParam));
    		::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR) pCtrl);
        }
        catch (char const * msg)
        {
            MessageBox (hWnd, msg, "Initialization",
                MB_ICONEXCLAMATION | MB_OK);
            return (-1);
        }
        catch (...)
        {
            MessageBox (hWnd, "Unknown Error", "Initialization",
                MB_ICONEXCLAMATION | MB_OK);
            return (-1);
        }
        return (0);
	case WM_SIZE:
		pCtrl->Size (LOWORD(lParam), HIWORD(lParam));
		return (0);
	case WM_PAINT:
		pCtrl->Paint ();
		return (0);
    case WM_LBUTTONDOWN:
        pCtrl->LButtonDown (MAKEPOINTS (lParam));
        return (0);
    case WM_LBUTTONUP:
        pCtrl->LButtonUp (MAKEPOINTS (lParam));
        return (0);
    case WM_MOUSEMOVE:
        if (wParam & MK_LBUTTON)
            pCtrl->LButtonDrag (MAKEPOINTS (lParam));
        return (0);
	case WM_CAPTURECHANGED:
		pCtrl->CaptureChanged ();
		return (0);
	// Revisit: Abort drag when user presses ESC
    case WM_DESTROY:
		::SetWindowLongPtr(hWnd, GWLP_USERDATA, 0);
        delete pCtrl;
        return (0);
	}
    return (DefWindowProc (hWnd, Msg, wParam, lParam));
}

SplitController::SplitController (HWND hwnd, CREATESTRUCT * pCreat)
    : _hwnd (hwnd), 
	  _hwndParent (pCreat->hwndParent),
	  _cx(0),_cy(0),_dragStart(0),_dragY(0)
{}

void SplitController::Size (int cx, int cy) {
	_cx = cx;
	_cy = cy;
}

void SplitController::Paint ()
{
	std::cout << "splitter paint" << std::endl;
	PaintCanvas canvas (_hwnd);
	{
		PenHolder pen (canvas, _pens.Light ());
		canvas.Line (0, 0, 0, _cy - 1);
	}
	{
		PenHolder pen (canvas, _pens.Hilight ());
		canvas.Line (1, 0, 1, _cy - 1);
	}
	{
		PenHolder pen (canvas, _pens.Shadow ());
		canvas.Line (_cx - 2, 0, _cx - 2, _cy - 1);
	}
	{
		PenHolder pen (canvas, _pens.DkShadow ());
		canvas.Line (_cx - 1, 0, _cx - 1, _cy - 1);
	}

}

void SplitController::LButtonDown (POINTS pt)
{
	RECT rect, parentrect;
	GetWindowRect(_hwnd, &rect);
	GetWindowRect(_hwndParent, &parentrect);
	int leftOffset = rect.left-parentrect.left;
	::SetCapture (_hwnd);

	// Find y offset of splitter
	// with respect to parent client area
	POINT ptOrg = {0, 0 };
	::ClientToScreen (_hwndParent, &ptOrg);
	int yParent = ptOrg.y;
	ptOrg.y = 0;
	::ClientToScreen (_hwnd, &ptOrg);
	int yChild = ptOrg.y;

    _dragStart = yChild - yParent + _cy / 2 - pt.y;
	_dragY = _dragStart + pt.y;

	// Draw a divider using XOR mode
	UpdateCanvas canvas (_hwndParent);
	ModeSetter mode (canvas, R2_NOTXORPEN);
	canvas.Line (leftOffset, _dragY, _cx - 1 + leftOffset, _dragY );

}

void SplitController::LButtonDrag (POINTS pt)
{
	RECT rect, parentrect;
	GetWindowRect(_hwnd, &rect);
	GetWindowRect(_hwndParent, &parentrect);
	int leftOffset = rect.left-parentrect.left;
	// Erase previous divider and draw new one
	UpdateCanvas canvas (_hwndParent);
	ModeSetter mode (canvas, R2_NOTXORPEN);
	canvas.Line (rect.left-parentrect.left, _dragY, _cx - 1 + leftOffset, _dragY);
	_dragY = _dragStart + pt.y;
	canvas.Line (rect.left-parentrect.left, _dragY, _cx - 1 + leftOffset, _dragY);
}

void SplitController::LButtonUp (POINTS pt)
{
	// Calling ReleaseCapture will send us the WM_CAPTURECHANGED
	//TODO: the IDC_SPLITTER here is wrong, should be the _hwnd value
	// in case more than one split control on a parent
	int id = GetDlgCtrlID(_hwnd);
	::ReleaseCapture ();
	::SendMessage (_hwndParent, MSG_MOVESPLITTER, LOWORD(id), _dragStart + pt.y);
}

void SplitController::CaptureChanged ()
{
	RECT rect, parentrect;
	GetWindowRect(_hwnd, &rect);
	GetWindowRect(_hwndParent, &parentrect);
	int leftOffset = rect.left-parentrect.left;
	// We are losing capture
	// End drag selection -- for whatever reason
	// Erase previous divider
	UpdateCanvas canvas (_hwndParent);
	ModeSetter mode (canvas, R2_NOTXORPEN);
	canvas.Line (rect.left-parentrect.left, _dragY, _cx - 1 + leftOffset,  _dragY);
}
