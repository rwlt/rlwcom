#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "ViewLoader.h"
#include "model.h"
#include "view.h"

namespace Module
{

class Controller : public ViewLoader
{
public:
	Controller(Module::AbstractViewFactory* abstractViewFactory);
	~Controller(); // dtor

    void loadMainWindow(); // Load main window view
    void loadToolPopupWindow();
    void loadToolBoxWindow();
    void loadMessageBoxWindow();


    AbstractViewFactory* viewFactory() const
    {
    	return (abstractViewFactory);
    }

private:
    AbstractViewFactory* abstractViewFactory;
    void loadView(View* view);

};

}
#endif
