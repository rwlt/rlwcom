/*
 * AbstractViewFactory.h
 *
 *  Created on: Apr 8, 2015
 *      Author: Rodney Woollett
 */

#ifndef VIEWLOADER_H_
#define VIEWLOADER_H_

#include "view.h"
namespace Module
{

enum ViewIdentity
{
	MainWindow = 0,
	ToolBox = 1,
	ToolPopup = 2,
	MessageBoxView = 3,
	TotalViews = 4,
	NullView = 10
};

class ViewLoader
{
public:
	ViewLoader();
	virtual ~ViewLoader();
	virtual void loadMainWindow() = 0;
	virtual void loadToolPopupWindow() = 0;
	virtual void loadToolBoxWindow() = 0;

};

class AbstractViewFactory
{
public:
	AbstractViewFactory();
	virtual ~AbstractViewFactory();
	virtual View* createInstance(const Module::ViewLoader * const loader,
			Module::ViewIdentity viewIdentity,
			Module::ViewIdentity parentViewIdentity =
					Module::ViewIdentity::NullView) = 0;
	virtual void close() = 0;
};

}

#endif /* VIEWLOADER_H_ */
