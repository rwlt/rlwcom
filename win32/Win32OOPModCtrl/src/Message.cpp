/*
 * Parameters.cpp
 *
 *  Created on: May 1, 2015
 *      Author: Rodney Woollett
 */

#include "Message.h"

namespace Module
{

Message::Message(): parameterKind(Module::ParameterKind::ONE_ARG)
{
	Parameter::Args args;
	args.oneArg.message = 0;
	parameterArgs = args;
}

Message::~Message()
{
}

Parameter::Args Message::argumments() const
{
	return (parameterArgs);
}
ParameterKind  Message::kind() const
{
	return (parameterKind);
}
void  Message::setParameter(Parameter newParam)
{
	parameterKind = newParam.kind;
	parameterArgs = newParam.args;
}

} /* namespace Module */
