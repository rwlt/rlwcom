/*
 * Parameters.h
 *
 *  Created on: May 1, 2015
 *      Author: Rodney Woollett
 */

#ifndef MESSAGE_H_
#define MESSAGE_H_
namespace Module
{

enum ParameterKind {
	ONE_ARG = 0,
	TWO_ARG = 1,
	THREE_ARG = 2
};

struct OneArg {
	long message;
};
struct TwoArgs {
	unsigned int valueA;
	unsigned int valueB;
};
struct ThreeArgs {
	unsigned int valueA;
	unsigned int valueB;
	unsigned int valueC;
};
//struct ArrayArgs {
//	Module::View* array[];
//};
struct Parameter {
	ParameterKind kind;
	union Args {
		OneArg oneArg;
		TwoArgs twoArgs;
		ThreeArgs threeArgs;
	} args;
};
class Message
{
public:
	Message();
	virtual ~Message();
	Parameter::Args argumments() const;
	ParameterKind  kind() const;
	void setParameter(Parameter newParam);
private:
	Parameter::Args parameterArgs;
	ParameterKind parameterKind;
};

} /* namespace Module */

#endif /* MESSAGE_H_ */
