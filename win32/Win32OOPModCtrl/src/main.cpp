
#include <windows.h>
#include <winuser.h>
#include <commctrl.h>
#include "resource.h"

#include "Controller.h"
#include "ViewFactory.h"
#include "View.h"
#include "WindowClass.h"
#include "splitter.h"
#include "MainWindowCommand.h"
#include "MainModuleCommand.h"
#include "ToolPopupCommand.h"
#include "DockWndCommand.h"
#include "Message.h"
#include "DockWnd.h"
#include <iostream>
//------------------------------------------------------------------------------
LRESULT CALLBACK MainWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam);
LRESULT CALLBACK DockWndProc(HWND hwnd, UINT msg, WPARAM wParam,
		LPARAM lParam);
BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam,
		LPARAM lParam);

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst,
		LPSTR lpCmdLine, int nCmdShow);

void AboutApplication(HWND hWnd);

Module::ViewIdentity WindowHandleViewIdentity(Module::Controller* ctrl,
		HWND hWnd);
void WindowHandleDestroy(Module::Controller* ctrl, HWND hWnd);
LRESULT PerformCommand(Module::Controller* ctrl,
		Module::ViewIdentity viewIdentity, int resId,
		const Module::Message &message);
LRESULT PerformWindowCommand(Module::Controller* ctrl,
		Module::ViewIdentity viewIdentity, int resId,
		const App::WindowMessage &message);
LRESULT HANDLE_NCACTIVATE(Module::Controller* ctrl, HWND hwndMain,
		HWND hwnd, WPARAM wParam, LPARAM lParam);
LRESULT HANDLE_ENABLE(Module::Controller* ctrl, HWND hwndMain, HWND hwnd,
		WPARAM wParam, LPARAM lParam);

const int MAX_DOCK_WINDOWS = Module::TotalViews;

//static WORD _dotPatternBmp1[] =
//{
////0x00aa, 0x0055, 0x00aa, 0x0055, 0x00aa, 0x0055, 0x00aa, 0x0055
//0xaaaa, 0x5555, 0xaaaa, 0x5555, 0xaaaa, 0x5555, 0xaaaa, 0x5555
//		};
//
///////////////////////////////////////////////////////////////////////////////
// WIN32 WinMain
///////////////////////////////////////////////////////////////////////////////
//
INT APIENTRY /*WINAPI*/WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst,
		LPSTR lpCmdLine, int nCmdShow)
{
	MSG Msg;
	HACCEL hAccelTable;

	//Initialize the Common controls
	INITCOMMONCONTROLSEX initCtrlEx;
	initCtrlEx.dwSize = sizeof(INITCOMMONCONTROLSEX);
	initCtrlEx.dwICC = ICC_TAB_CLASSES | ICC_BAR_CLASSES | ICC_COOL_CLASSES;
	InitCommonControlsEx(&initCtrlEx);

	// Create controller with a viewFactory to use with Module Controller
	// Create the view factory
	App::ViewFactory viewFactory(hInstance);
	Module::Controller controller(&viewFactory);
	controller.loadMainWindow(); //Use a view factory to create a Parent View or main view

	hAccelTable = LoadAccelerators(hInstance,
	MAKEINTRESOURCE(IDR_ACCELERATOR1));
	//Process the main windows messages
	while (GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		if (!(viewFactory.checkTranslateAccelerator(hAccelTable, &Msg)))
			if (!(viewFactory.checkDialogMessage(&Msg)))
			{
				TranslateMessage(&Msg);
				DispatchMessage(&Msg);
			}
	}

	viewFactory.close();

	return (0);

}

///////////////////////////////////////////////////////////////////////////////
// Main application WinProc
///////////////////////////////////////////////////////////////////////////////
LRESULT CALLBACK MainWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam)
{
	LRESULT returnValue = 0;        // return value
	static Module::Controller *ctrl = 0; // This WndProc always has the Module COntroller

//	static App::ViewFactory* factory;
	Module::ViewIdentity viewIdentity;
	Module::Message viewMessage;
	Module::Parameter paramArgs;
	App::WindowMessage windowMessage;
	App::Parameter paramWindowArgs;
	NMHDR *hdr;

	if (Msg == WM_NCCREATE)  // Non-Client Create
	{
		// WM_NCCREATE message is called before non-client parts(border,
		// titlebar, menu,etc) are created. This message comes with a pointer
		// to CREATESTRUCT in lParam. The lpCreateParams member of CREATESTRUCT
		// actually contains the value of lpPraram of CreateWindowEX().
		// First, retrieve the pointer to the controller specified when
		// MainWin is setup.
		ctrl =
				(Module::Controller*) (((CREATESTRUCT*) lParam)->lpCreateParams);

		// Second, store the pointer to the Controller into GWLP_USERDATA,
		// so, other message can be routed to the associated Controller.
		// Set in GWLP_USERDATA so other window procedures of the applications
		// window classes can use the same Module controller
		::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR) ctrl);

		return (::DefWindowProc(hWnd, Msg, wParam, lParam));
	}

	// check NULL pointer, because GWLP_USERDATA is initially 0, and
	// we store a valid pointer value when WM_NCCREATE is called.
	if (!ctrl)
		return (::DefWindowProc(hWnd, Msg, wParam, lParam));

	//factory = static_cast<App::ViewFactory*>(ctrl->viewFactory());
	switch (Msg)
	{
	case WM_NCACTIVATE:
		// For activation/deactivation of docked/floating windows
		returnValue = HANDLE_NCACTIVATE(ctrl, hWnd, hWnd, wParam, lParam);
		break;
	case WM_ENABLE:
		// For activation/deactivation of docked/floating windows
		returnValue = HANDLE_ENABLE(ctrl, hWnd, hWnd, wParam, lParam);
		break;

	case WM_SIZE:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = Module::ParameterKind::TWO_ARG;
		paramArgs.args.twoArgs.valueA = LOWORD(lParam);
		paramArgs.args.twoArgs.valueB = HIWORD(lParam);
		viewMessage.setParameter(paramArgs);
		returnValue = PerformCommand(ctrl, viewIdentity, Msg, viewMessage);
		break;

	case WM_PAINT:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = Module::ParameterKind::ONE_ARG;
		paramArgs.args.oneArg.message = lParam;
		viewMessage.setParameter(paramArgs);
		PerformCommand(ctrl, viewIdentity, Msg, viewMessage);
		returnValue = ::DefWindowProc(hWnd, Msg, wParam, lParam);
		break;

	case WM_ERASEBKGND:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramWindowArgs.kind = App::ParameterKind::WPARAMLPARAM;
		paramWindowArgs.args.wParamLParam.wparam = wParam;
		paramWindowArgs.args.wParamLParam.lparma = lParam;
		windowMessage.setParameter(paramWindowArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
		break;

	case WM_CTLCOLOREDIT:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramWindowArgs.kind = App::ParameterKind::WPARAMLPARAM;
		paramWindowArgs.args.wParamLParam.wparam = wParam;
		paramWindowArgs.args.wParamLParam.lparma = lParam;
		windowMessage.setParameter(paramWindowArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
		break;

	case WM_NOTIFY:
		hdr = (NMHDR *)lParam;
		switch (hdr->code)
		{
		case TCN_SELCHANGE:
			// TODO:The passed values should get mapped from win32 to module
			viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
			paramArgs.kind = Module::ParameterKind::ONE_ARG;
			paramArgs.args.oneArg.message = lParam;
			viewMessage.setParameter(paramArgs);
			returnValue = PerformCommand(ctrl, viewIdentity,
					((LPNMHDR) lParam)->idFrom, viewMessage);
			break;
		case DWN_ISDOCKABLE:
			viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
			paramWindowArgs.kind = App::ParameterKind::WPARAMLPARAM;
			paramWindowArgs.args.wParamLParam.wparam = wParam;
			paramWindowArgs.args.wParamLParam.lparma = lParam;
			windowMessage.setParameter(paramWindowArgs);
			returnValue = PerformWindowCommand(ctrl, viewIdentity,
					hdr->code,
					windowMessage);
			break;

		}
		break;

	case WM_COMMAND:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		switch (LOWORD(wParam))
		{
		case IDM_ABOUT:
			AboutApplication(hWnd);
			break;
		case IDM_FILE_NEW:
		case IDM_FILE_OPEN:
		case IDM_FILE_SAVE:
		case IDM_FILE_EXIT:
			paramArgs.kind = Module::ParameterKind::ONE_ARG;
			paramArgs.args.oneArg.message = lParam;
			viewMessage.setParameter(paramArgs);
			returnValue = PerformCommand(ctrl, viewIdentity,
					LOWORD(wParam), viewMessage);
			break;

		case IDM_DIALOG_SHOW:
		case IDM_DIALOG_HIDE:
			paramArgs.kind = Module::ParameterKind::ONE_ARG;
			paramArgs.args.oneArg.message = lParam;
			viewMessage.setParameter(paramArgs);
			returnValue = PerformCommand(ctrl,
					Module::ViewIdentity::ToolPopup, LOWORD(wParam),
					viewMessage);
			returnValue = PerformCommand(ctrl,
					Module::ViewIdentity::ToolBox, LOWORD(wParam),
					viewMessage);
			returnValue = PerformCommand(ctrl,
					Module::ViewIdentity::MessageBoxView, LOWORD(wParam),
					viewMessage);
			//return (DefWindowProc(hWnd, Msg, wParam, lParam));
			break;

		default:
			return (DefWindowProc(hWnd, Msg, wParam, lParam));
		}
		;
		break;

	case MSG_MOVESPLITTER:
		// Now it just run the one moveSPlitter method in the MainWindoView
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = Module::ParameterKind::ONE_ARG;
		paramArgs.args.oneArg.message = lParam;
		viewMessage.setParameter(paramArgs);
		returnValue = PerformCommand(ctrl, viewIdentity, LOWORD(wParam),
				viewMessage);
		break;
//	case WM_MOUSEMOVE:
//		std::cout << "MAIN WINDOW PROC MOUSE MOVE" << std::endl;
//		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
//		paramWindowArgs.kind = App::ParameterKind::WPARAMLPARAM;
//		paramWindowArgs.args.wParamLParam.wparam = wParam;
//		paramWindowArgs.args.wParamLParam.lparma = lParam;
//		windowMessage.setParameter(paramWindowArgs);
//		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
//				windowMessage);
//		break;
	case WM_DRAWITEM:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramWindowArgs.kind = App::ParameterKind::WPARAMLPARAM;
		paramWindowArgs.args.wParamLParam.wparam = wParam;
		paramWindowArgs.args.wParamLParam.lparma = lParam;
		windowMessage.setParameter(paramWindowArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
		break;

	case WM_DESTROY:
		PostQuitMessage(WM_QUIT);
		break;

	default:
		return (DefWindowProc(hWnd, Msg, wParam, lParam));
	}

	return (returnValue);

}

///////////////////////////////////////////////////////////////////////////////
// About Application dialog
///////////////////////////////////////////////////////////////////////////////
void AboutApplication(HWND hWnd)
{
	// Show a modal dialog - it implements its own message loop and procedure
	int ret = DialogBox(GetModuleHandle(NULL),
			MAKEINTRESOURCE(IDD_ABOUT), hWnd, AboutDlgProc);
	if (ret == -1)
	{
		MessageBox(hWnd, "Dialog failed!", "Error",
		MB_OK | MB_ICONINFORMATION);
	}
}

///////////////////////////////////////////////////////////////////////////////
// About application WinProc
///////////////////////////////////////////////////////////////////////////////
BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam,
		LPARAM lParam)
{
	LRESULT returnValue = 0;        // return value
	//Caution: use Windows BOOL, not C++ bool
	// Return FALSE for messages you don't handle and TRUE for those you do (If otherwise told not to!
	//  use the Help win32 notes. )
	switch (Message)
	{
	case WM_INITDIALOG: // For initializing dialogs (not WM_CREATE, controls wont be created if you did)
		returnValue = 1L;
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			EndDialog(hwnd, IDOK); //do not call DestroyWindow() for dialogs
			returnValue = 1L;
			break;

		case IDCANCEL:
			EndDialog(hwnd, IDCANCEL); // do not call DestroyWindow() for dialogs
			returnValue = 1L;
			break;
		}
		break;
	default:
		break; // Modal Dialog window procedures don't use DefWindowProc
	}
	return (returnValue);
}

///////////////////////////////////////////////////////////////////////////////
// A dockWnd Procedure
///////////////////////////////////////////////////////////////////////////////
LRESULT CALLBACK DockWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam)
{
	LRESULT returnValue = 0;         // return value
	static Module::Controller *ctrl = 0; // This WndProc always has the Module Controller
	static App::ViewFactory *factory = 0;
	UINT	uHitTest;
	Module::ViewIdentity viewIdentity;
	App::WindowMessage windowMessage;
	App::Parameter paramArgs;
	HWND parent = GetParent(hWnd);

	if (Msg == WM_NCCREATE)  // Non-Client Create
	{
		// First, retrieve the pointer to the controller specified when
		// MainWindow was created.
		ctrl = (Module::Controller*) ::GetWindowLongPtr(parent,
		GWLP_USERDATA);
		std::cout << "NC CREATE" << std::endl;
		factory = static_cast<App::ViewFactory*>(ctrl->viewFactory());
		returnValue = factory->addDockWindow(hWnd);
	}
	if (!ctrl)
		return (::DefWindowProc(hWnd, Msg, wParam, lParam));

	switch (Msg)
	{
	case WM_NCDESTROY:
		factory = static_cast<App::ViewFactory*>(ctrl->viewFactory());
		returnValue = factory->removeDockWindow(hWnd);
		break;
	case WM_NCACTIVATE:
		returnValue = HANDLE_NCACTIVATE(ctrl, parent, hWnd, wParam,
				lParam);
		break;
	case WM_NCHITTEST:
		//	Allow dragging by the client area
		uHitTest = DefWindowProc(hWnd, WM_NCHITTEST, wParam, lParam);
		if(uHitTest == HTCLIENT)
			uHitTest = HTCAPTION;
		return (uHitTest);
	case WM_NCLBUTTONDBLCLK:
		// ToggleDockingMode
		// Prevent standard double-click on the caption area
		if (wParam == HTCAPTION)
		{
			viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
			paramArgs.kind = App::ParameterKind::LPARAMETER;
			paramArgs.args.lParameter.lparam = lParam;
			windowMessage.setParameter(paramArgs);
			returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
					windowMessage);
		}
		break;
	case WM_NCLBUTTONDOWN:	//begin drag
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = App::ParameterKind::WPARAMLPARAM;
		paramArgs.args.wParamLParam.wparam = wParam;
		paramArgs.args.wParamLParam.lparma = lParam;
		windowMessage.setParameter(paramArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
		if (returnValue > 0)
		{
			returnValue = DefWindowProc(hWnd, Msg, wParam, lParam);
		}
		break;
	case WM_CANCELMODE:
	case WM_LBUTTONUP:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = App::ParameterKind::WPARAMLPARAM;
		paramArgs.args.wParamLParam.wparam = wParam;
		paramArgs.args.wParamLParam.lparma = lParam;
		windowMessage.setParameter(paramArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
        break;
	case WM_MOUSEMOVE:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = App::ParameterKind::WPARAMLPARAM;
		paramArgs.args.wParamLParam.wparam = wParam;
		paramArgs.args.wParamLParam.lparma = lParam;
		windowMessage.setParameter(paramArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
		break;
	case WM_ERASEBKGND:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = App::ParameterKind::WPARAMLPARAM;
		paramArgs.args.wParamLParam.wparam = wParam;
		paramArgs.args.wParamLParam.lparma = lParam;
		windowMessage.setParameter(paramArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
		break;

	case WM_WINDOWPOSCHANGED:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = App::ParameterKind::LPARAMETER;
		paramArgs.args.lParameter.lparam = lParam;
		windowMessage.setParameter(paramArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
		break;
	case WM_GETMINMAXINFO:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = App::ParameterKind::WPARAMLPARAM;
		paramArgs.args.wParamLParam.wparam = wParam;
		paramArgs.args.wParamLParam.lparma = lParam;
		windowMessage.setParameter(paramArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
		if (returnValue > 0)
		{
			returnValue = DefWindowProc(hWnd, Msg, wParam, lParam);
		}
		break;
	case WM_SETTINGCHANGE:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = App::ParameterKind::WPARAMLPARAM;
		paramArgs.args.wParamLParam.wparam = wParam;
		paramArgs.args.wParamLParam.lparma = lParam;
		windowMessage.setParameter(paramArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
		break;

	case WM_DRAWITEM:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		paramArgs.kind = App::ParameterKind::WPARAMLPARAM;
		paramArgs.args.wParamLParam.wparam = wParam;
		paramArgs.args.wParamLParam.lparma = lParam;
		windowMessage.setParameter(paramArgs);
		returnValue = PerformWindowCommand(ctrl, viewIdentity, Msg,
				windowMessage);
		break;

	case WM_COMMAND:
		viewIdentity = WindowHandleViewIdentity(ctrl, hWnd);
		switch (LOWORD(wParam))
		{
		case IDC_CLOSEDOCKBUTTON:
			paramArgs.args.wParamLParam.wparam = wParam;
			paramArgs.args.wParamLParam.lparma = lParam;
			windowMessage.setParameter(paramArgs);
			returnValue = PerformWindowCommand(ctrl, viewIdentity, LOWORD(wParam),
					windowMessage);
			break;

		default:
			return (DefWindowProc(hWnd, Msg, wParam, lParam));
		}
		;
		break;

	case WM_CLOSE:
		// Be sure to destroy the DockWnd handles
		WindowHandleDestroy(ctrl, hWnd);
		break;
	default:
		returnValue = DefWindowProc(hWnd, Msg, wParam, lParam);
		break;
	}
	return (returnValue);
}
///////////////////////////////////////////////////////////////////////////////
// Find the Views Identity using the applications view factory method
// TODO: These global function could be put into the ViewFactory implementation.
// Which means the View Factory needs to be the main Global var to use with
// Loading/finding Views and by the Module View Identity names
///////////////////////////////////////////////////////////////////////////////
Module::ViewIdentity WindowHandleViewIdentity(Module::Controller* ctrl,
		HWND hWnd)
{
	App::ViewFactory* factory =
			static_cast<App::ViewFactory*>(ctrl->viewFactory());
	return (factory->viewIdentity(hWnd));
}
///////////////////////////////////////////////////////////////////////////////
// Find the Views Identity using the applications view factory method
///////////////////////////////////////////////////////////////////////////////
void WindowHandleDestroy(Module::Controller* ctrl, HWND hWnd)
{
	App::ViewFactory* factory =
			static_cast<App::ViewFactory*>(ctrl->viewFactory());
	factory->destroyInstance(hWnd);
}

///////////////////////////////////////////////////////////////////////////////
// Find the Views Identity using the applications view factory method
///////////////////////////////////////////////////////////////////////////////
LRESULT PerformCommand(Module::Controller* ctrl,
		Module::ViewIdentity viewIdentity, int resId,
		const Module::Message &message)
{
	LRESULT result = 0; // Zero means we process fine here
	App::ViewFactory* factory =
			static_cast<App::ViewFactory*>(ctrl->viewFactory());
	Module::View* view = factory->identityInstance(viewIdentity);

	// If no view instance active create it depending on the Command and View
	// identity
	if (view == nullptr)
	{
		if (resId == IDM_DIALOG_SHOW)
		{
			if (viewIdentity == Module::ViewIdentity::ToolPopup)
				ctrl->loadToolPopupWindow();
			else if (viewIdentity == Module::ViewIdentity::ToolBox)
				ctrl->loadToolBoxWindow();
			else if (viewIdentity == Module::ViewIdentity::MessageBoxView)
				ctrl->loadMessageBoxWindow();
			else
				return (1L);
			view = factory->identityInstance(viewIdentity);
		}
		else
		{
			return (1L);
		}
	}

	Module::Command* command = nullptr;

	// the one command to perform
	switch (viewIdentity)
	{
	case Module::ViewIdentity::MainWindow:
		switch (resId)
		{
		case WM_SIZE:
			command = new App::MainModuleCommand(view,
					&App::MainWindowView::size);
			break;
		case WM_PAINT:
			command = new App::MainModuleCommand(view,
					&App::MainWindowView::paint);
			break;
		case IDM_FILE_NEW:
			command = new App::MainModuleCommand(view,
					&App::MainWindowView::selectFont);
			break;
		case IDM_FILE_OPEN:
			command = new App::MainModuleCommand(view,
					&App::MainWindowView::selectColour);
			break;
		case IDM_FILE_SAVE:
			command = new App::MainModuleCommand(view,
					&App::MainWindowView::save);
			break;
		case IDM_FILE_EXIT:
			command = new App::MainModuleCommand(view,
					&App::MainWindowView::closeView);
			break;
		case IDC_MAIN_TABBED:
			command = new App::MainModuleCommand(view,
					&App::MainWindowView::changeTab);
			break;
		case IDC_SPLITTER1:
			command = new App::MainModuleCommand(view,
					&App::MainWindowView::moveSplitter);
			break;
		case IDC_SPLITTER2:
			command = new App::MainModuleCommand(view,
					&App::MainWindowView::moveSplitter2);
			break;
		default:
			/* no default */
			break;
		}
		break;

	case Module::ViewIdentity::ToolPopup:
	case Module::ViewIdentity::ToolBox:
	case Module::ViewIdentity::MessageBoxView:
		switch (resId)
		{
		case IDM_DIALOG_SHOW:
			command = new App::ToolPopupCommand(view,
					&App::ToolPopupView::showView);
			break;
		case IDM_DIALOG_HIDE:
			command = new App::ToolPopupCommand(view,
					&App::ToolPopupView::closeView);
			break;
		}
		break;
	default:
		/* no default */
		break;
	}
	if (command == nullptr)
		return (1L);

	result = command->execute(message);
	delete command;
	return (result);
}

///////////////////////////////////////////////////////////////////////////////
// Find the Views Identity using the applications view factory method
///////////////////////////////////////////////////////////////////////////////
LRESULT PerformWindowCommand(Module::Controller* ctrl,
		Module::ViewIdentity viewIdentity, int resId,
		const App::WindowMessage &message)
{

	LRESULT result = 0; // Zero means we process fine here
	App::ViewFactory* factory =
			static_cast<App::ViewFactory*>(ctrl->viewFactory());
	App::ToolPopupView* popupView;
	App::MainWindowView* mainView;
	App::Command* command = nullptr;

	// the one command to perform by viewIdentity and resID
	switch (viewIdentity)
	{
	case Module::ViewIdentity::MainWindow:
		mainView =
				static_cast<App::MainWindowView*>(factory->identityInstance(
						viewIdentity));

		if (mainView != nullptr)
		{
			switch (resId)
			{
			case WM_ERASEBKGND:
				command = new App::MainWindowCommand(mainView,
						&App::MainWindowView::eraseBackground);
				break;
			case WM_CTLCOLOREDIT:
				command = new App::MainWindowCommand(mainView,
						&App::MainWindowView::controlBackground);
				break;
			case DWN_ISDOCKABLE:
				command = new App::MainWindowCommand(mainView,
						&App::MainWindowView::isDockWnd_Dockable);
				break;
			case WM_MOUSEMOVE:
				command = new App::MainWindowCommand(mainView,
						&App::MainWindowView::mouseMove);
				break;
			case WM_DRAWITEM:
				command = new App::MainWindowCommand(mainView,
						&App::MainWindowView::drawItem);
				break;
			}
		}
		break;
	case Module::ViewIdentity::ToolPopup:
	case Module::ViewIdentity::ToolBox:
	case Module::ViewIdentity::MessageBoxView:
		popupView =
				static_cast<App::ToolPopupView*>(factory->identityInstance(
						viewIdentity));
		if (popupView != nullptr)
		{
			switch (resId)
			{
			case WM_WINDOWPOSCHANGED:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::windowPosChanged);
				break;
			case WM_NCLBUTTONDBLCLK:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::toggleDockingMode);
				break;
			case WM_NCLBUTTONDOWN:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::beginDrag);
				break;
			case WM_CANCELMODE:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::cancelDrag);
				break;
			case WM_LBUTTONUP:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::leftButtonUp);
				break;
			case WM_MOUSEMOVE:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::mouseMove);
				break;
			case WM_ERASEBKGND:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::eraseBackground);
				break;
			case WM_GETMINMAXINFO:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::minMaxInfo);
				break;
			case WM_SETTINGCHANGE:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::settingChange);
				break;
			case WM_DRAWITEM:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::drawItem);
				break;
			case IDC_CLOSEDOCKBUTTON:
				command = new App::DockWndCommand(popupView,
						&App::DockWnd::closeView);

			}
		}
		break;
	default:
		/* no default */
		break;
	}
	if (command == nullptr)
		return (1L);

	result = command->execute(message);
	delete command;

	return (result);
}

///////////////////////////////////////////////////////////////////////////////
//	The main window of app should call this in response to WM_NCACTIVATE
//  For activation/deactivation of docked/floating windows
//
//	hwndMain - handle to top-level owner window
//	hwnd     - handle to window which received message (can be same as hwndMain)
//
///////////////////////////////////////////////////////////////////////////////
LRESULT HANDLE_NCACTIVATE(Module::Controller* ctrl, HWND hwndMain,
		HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	static HWND hwndList[MAX_DOCK_WINDOWS + 1];
	int i, nNumWnds;
	App::ViewFactory* factory =
			static_cast<App::ViewFactory*>(ctrl->viewFactory());

	HWND hParam = (HWND) lParam;

	BOOL fKeepActive = wParam;
	BOOL fSyncOthers = TRUE;

	nNumWnds = factory->getPopupList(hwndMain, hwndList,
			MAX_DOCK_WINDOWS + 1,
			TRUE);

	// UNDOCUMENTED FEATURE:
	// Attention that lParam will be NULL when the other window being
	// activated/deactivate belongs to another thread - not just a different
	// process. Therefore care should be taken when using this technique - all
	// toolbars / windows involved in this activation scheme must belong to the
	// same thread.
	// if the other window being activated/deactivated (i.e. NOT this one)
	// is one of our popups, then go (or stay) active, otherwise.
	for (i = 0; i < nNumWnds; i++)
	{
		if (hParam == hwndList[i])
		{
			fKeepActive = TRUE;
			fSyncOthers = FALSE;
			break;
		}
	}

	// If this message was sent by the synchronise-loop (below)
	// then exit normally
	if (hParam == (HWND) -1)
	{
		return (DefWindowProc(hwnd, WM_NCACTIVATE, fKeepActive, 0));
	}

	// This window is about to change (inactive/active).
	// Sync all other popups to the same state
	if (fSyncOthers == TRUE)
	{
		for (i = 0; i < nNumWnds; i++)
		{
			//DO NOT send this message to ourselves!!!!
			if (hwndList[i] != hwnd && hwndList[i] != hParam)
				SendMessage(hwndList[i], WM_NCACTIVATE, fKeepActive,
						(LONG) -1);
		}
	}

	return (DefWindowProc(hwnd, WM_NCACTIVATE, fKeepActive, lParam));
}

///////////////////////////////////////////////////////////////////////////////
//	The main window of app should call this in response to WM_ENABLE
//  For activation/deactivation of docked/floating windows
//
//	hwndMain - handle to top-level owner window
//	hwnd     - handle to window which received message (can be same as hwndMain)
//
///////////////////////////////////////////////////////////////////////////////
LRESULT HANDLE_ENABLE(Module::Controller* ctrl, HWND hwndMain, HWND hwnd,
		WPARAM wParam, LPARAM lParam)
{
	static HWND hwndList[MAX_DOCK_WINDOWS + 1];
	int i, nNumWnds;
	App::ViewFactory* factory =
			static_cast<App::ViewFactory*>(ctrl->viewFactory());

	//HWND hParam = (HWND) lParam;

	nNumWnds = factory->getPopupList(hwndMain, hwndList,
			MAX_DOCK_WINDOWS + 1,
			FALSE);

	for (i = 0; i < nNumWnds; i++)
	{
		if (hwndList[i] != hwnd)
		{
			EnableWindow(hwndList[i], wParam);
		}
	}

	//just do the default
	return (DefWindowProc(hwnd, WM_ENABLE, wParam, lParam));
}
