/*
 * DockWndCommand.cpp
 *
 *  Created on: May 04, 2015
 *      Author: Rodney Woollett
 */

#include "DockWndCommand.h"
#include "DockWnd.h"
#include "AppCommand.h"
#include "WindowMessage.h"

namespace App
{

DockWndCommand::DockWndCommand(App::DockWnd *view,
		long (App::DockWnd::*meth)(const App::WindowMessage &)) :
		view(view), method(meth)
{
}

DockWndCommand::~DockWndCommand()
{
}

long DockWndCommand::execute(const App::WindowMessage &parameters)
{
	LRESULT returnValue = 0;
	returnValue = ((view->*method)(parameters));
	return (returnValue);
}

} /* namespace App */
