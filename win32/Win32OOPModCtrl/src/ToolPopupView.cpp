/*
 * ToolPopupView.cpp
 *
 *  Created on: Apr 25, 2015
 *      Author: Rodney Woollett
 */

#include "windows.h"
#include "ToolPopupView.h"
#include "ToolBoxPresenter.h"
#include "Controller.h"
#include <iostream>
#include <string>
#include <sstream>

namespace App
{

///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
ToolPopupView::ToolPopupView():
		presenter(new Module::ToolBoxPresenter(this)), hToolPopup(0),
		clientWidth(0), clientHeight(0)

{
	std::cout << "In ToolPopupView constructor " << std::endl;
}

///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
ToolPopupView::~ToolPopupView()
{
	std::cout << "In ToolPopupView destructor " << " presenter "
			<< presenter << std::endl;
	DestroyWindow(hToolPopup);

	delete presenter;
	presenter = nullptr;

}

///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
int ToolPopupView::createView(HINSTANCE handleInstance, HWND hWndParent){

	//Using DockWnd create a dockwnd window
    createDockWnd(handleInstance, hWndParent);

	std::stringstream title;
	title << "Contents " << dockWndHandle();
	hToolPopup = CreateWindowEx(WS_EX_CLIENTEDGE,
				"EDIT", title.str().c_str(),
				ES_MULTILINE|ES_AUTOVSCROLL|ES_AUTOHSCROLL|WS_VSCROLL|WS_HSCROLL|WS_CHILD|WS_VISIBLE,
				CW_USEDEFAULT, CW_USEDEFAULT, 400, 64,
				dockWndHandle(),
				0,GetModuleHandle(0),0);
	std::cout << "TooPopup HANDLE: " << hToolPopup << std::endl;
	if (hToolPopup == NULL)
	{
		MessageBox(hWndParent, "CreateWindowEx Win32ToolBox returned NULL", "Warning!",
		MB_OK | MB_ICONINFORMATION);
		return (-1) ;
	}

    // Set the dock window to the content of the view contents
	SetDockContents(hToolPopup, hWndParent);

	std::cout << "ToolPopupView created:" << this << " presenter "
			<< presenter << std::endl;
	presenter->initialize();

	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// Public function
// The windowHandle of the this ToolPopup - use dockWndHandle if the DockWnd
// handle is needed
///////////////////////////////////////////////////////////////////////////////
HWND ToolPopupView::windowHandle()
{
	return (hToolPopup);
}

///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
void ToolPopupView::save()
{
//	presenter->saveEditControl();

}

///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
int ToolPopupView::showView(const Module::Message &message)
{
	ShowWindow(dockWndHandle(), SW_SHOW);
	changeVisibility(TRUE);
	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
int ToolPopupView::closeView(const Module::Message &message)
{
	ShowWindow(dockWndHandle(), SW_HIDE);
	changeVisibility(FALSE);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// Public function
// handle WM_PAINT
///////////////////////////////////////////////////////////////////////////////
int ToolPopupView::paint(const Module::Message &message)
{
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// Public function
// handle WM_SIZE
///////////////////////////////////////////////////////////////////////////////
int ToolPopupView::size(const Module::Message &message)
{
	Module::Parameter::Args args = message.argumments();
	clientWidth = args.twoArgs.valueA;
	clientHeight = args.twoArgs.valueB;
	std::cout << "ToolPopupView size:" << std::endl;
	SetWindowPos(dockWndHandle(),NULL,0,0,clientWidth,clientHeight,SWP_NOMOVE);
	return (0);
}


} /* namespace App */


