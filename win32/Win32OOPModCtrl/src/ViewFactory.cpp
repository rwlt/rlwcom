/*
 * ViewFactory.cpp
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#include "MainWindowView.h"
#include "ToolPopupView.h"
#include <iostream>
#include <sstream>
#include <cstring>
#include "ViewFactory.h"

extern LRESULT CALLBACK MainWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam);
extern LRESULT CALLBACK WndProcSplitter(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam);
extern LRESULT CALLBACK DockWndProc(HWND hwnd, UINT msg, WPARAM wParam,
		LPARAM lParam);

namespace App
{

ViewFactory::ViewFactory(HINSTANCE handleInstance) :
		hInstance(handleInstance), className("Win32App"), classSplitterName(
				"Win32SplitterClass"), dockWndName("Win32DockWnd"), nNumDockWnds(
				0)
{
	std::memset(moduleView, '\0', sizeof(moduleView));
	std::memset(hDockList, '\0', sizeof(hDockList));

	//Initialize the application class
	windowsClass.Create(hInstance, className, MainWndProc);
	windowsClass.Register();

	//Initialize the application splitter class
	// TODO: The Win32SplitterClass: It is a sizer on y axis only could
	//       be made for setting up for x sizes
	windowSplitterClass.CreateSplitter(hInstance, classSplitterName,
			WndProcSplitter);
	windowSplitterClass.Register();

	dockWndClass.CreatePopup(hInstance, dockWndName, DockWndProc);
	dockWndClass.Register();

}

ViewFactory::~ViewFactory()
{
	std::cout << "ViewFactory destructor" << std::endl;
	windowsClass.UnRegister();
	windowSplitterClass.UnRegister();
	dockWndClass.UnRegister();

}

//----------------------------------------------------------------------------------
// close
// Delete all open views
//----------------------------------------------------------------------------------
void ViewFactory::close()
{
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
	{
		if (moduleView[index] != 0)
		{
			std::cout << "Destruct " << moduleView[index] << std::endl;
			delete moduleView[index];
			moduleView[index] = 0;
		}
	}

}

//----------------------------------------------------------------------------------
// Public method - add a dock window to dock list
// Does this on NC Create and before the initial NC ACTIVATE
//----------------------------------------------------------------------------------
int ViewFactory::addDockWindow(HWND hWnd)
{
	if (nNumDockWnds < Module::ViewIdentity::TotalViews)
	{
		hDockList[nNumDockWnds++] = hWnd;
		//SetWindowText(hWnd, "Hi");
		return (TRUE);
	}
	else
	{
		return (FALSE);
	}
}
//----------------------------------------------------------------------------------
// Public method - remove a dock window to dock list
//----------------------------------------------------------------------------------
int ViewFactory::removeDockWindow(HWND hWnd)
{
	int index;
	for (index = 0; index < nNumDockWnds; index++)
	{
		if (hDockList[index] == hWnd)
		{
			for (; index < nNumDockWnds - 1; index++)
			{
				hDockList[index] = hDockList[index + 1];
			}

			nNumDockWnds--;
			std::cout << "reomve dock window" << std::endl;
			break;
		}
	}

	return (0);
}

//----------------------------------------------------------------------------------
// checkDialogMessage
// For all views created check their modeless dialogs are Main window messaged
//----------------------------------------------------------------------------------
int ViewFactory::checkDialogMessage(MSG* msg)
{
	int process = 0;
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
	{
		if (moduleView[index] != 0)
		{
//			if (index == 0) // MainWindow
//			{
//				App::MainWindowView* mainView =
//						static_cast<App::MainWindowView*>(moduleView[index]);
//				process = IsDialogMessage(mainView->windowHandle(), msg);
//				if (process)
//				{
//					break;
//				}
//			}
//			if (index == 2) // Tool Popup
//			{
//				App::ToolPopupView* view =
//						static_cast<App::ToolPopupView*>(moduleView[index]);
//				process = IsDialogMessage(view->dockWndHandle(), msg);
//				if (process)
//				{
//					break;
//				}
//			}
		}
	}
	return (process);
}

//----------------------------------------------------------------------------------
// checkTranslateAccelerator
// For all views created check their window handles for menu id acceleration
//----------------------------------------------------------------------------------
int ViewFactory::checkTranslateAccelerator(HACCEL hAccelTable, MSG* msg)
{
	int process = 0;
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
//	for (int index = Module::ViewIdentity::TotalViews; index >= 0; --index)
	{
		if (moduleView[index] != 0)
		{
			if (index == 0) // MainWindow
			{
				App::MainWindowView* mainView =
						static_cast<App::MainWindowView*>(moduleView[index]);
				process = TranslateAccelerator(mainView->windowHandle(),
						hAccelTable, msg);
				if (process)
				{
					break;
				}
			}
			if (index == 2) // Tool Popup
			{
				App::ToolPopupView* view =
						static_cast<App::ToolPopupView*>(moduleView[index]);
				process = TranslateAccelerator(view->dockWndHandle(),
						hAccelTable, msg);
				if (process)
				{
					break;
				}
			}
		}
	}
	return (process);
}

//----------------------------------------------------------------------------------
// viewIdentity by HWND
// Use Module ViewIdentity and the total views available to keep
// an array of views to return to caller
//----------------------------------------------------------------------------------
Module::ViewIdentity ViewFactory::viewIdentity(HWND hWnd)
{
	Module::ViewIdentity viewIdentity = Module::ViewIdentity::NullView;
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
	{
		if (moduleView[index] != 0)
		{
			if (index == 0) // MainWindow
			{
				App::MainWindowView* mainView =
						static_cast<App::MainWindowView*>(moduleView[index]);
				if (mainView->windowHandle() == hWnd)
				{
					viewIdentity = Module::ViewIdentity::MainWindow;
					break;
				}
			}
			if (index == 1) // ToolPopup's
			{
				App::ToolPopupView* view =
						static_cast<App::ToolPopupView*>(moduleView[index]);
				if (view->dockWndHandle() == hWnd)
				{
					viewIdentity = Module::ViewIdentity::ToolBox;
					break;
				}
			}
			if (index == 2) // ToolPopup's
			{
				App::ToolPopupView* view =
						static_cast<App::ToolPopupView*>(moduleView[index]);
				if (view->dockWndHandle() == hWnd)
				{
					viewIdentity = Module::ViewIdentity::ToolPopup;
					break;
				}
			}
			if (index == 3) // ToolPopup's
			{
				App::ToolPopupView* view =
						static_cast<App::ToolPopupView*>(moduleView[index]);
				if (view->dockWndHandle() == hWnd)
				{
					viewIdentity = Module::ViewIdentity::MessageBoxView;
					break;
				}
			}
		}
	}
	return (viewIdentity);
}
//----------------------------------------------------------------------------------
// identityInstance
// Use Module ViewIdentity to get View Implementations instance
//----------------------------------------------------------------------------------
Module::View* ViewFactory::identityInstance(
		Module::ViewIdentity viewIdentity)
{
	Module::View* view = nullptr;

	// We find the view here
	if (moduleView[viewIdentity] != 0)
	{
		view = moduleView[viewIdentity];
	}
	return (view);

}
//----------------------------------------------------------------------------------
// createInstance
// Use Module ViewIdentity and the total views available to keep
// an array of views to return to caller
//----------------------------------------------------------------------------------
Module::View* ViewFactory::createInstance(
		const Module::ViewLoader * const loader,
		Module::ViewIdentity viewIdentity,
		Module::ViewIdentity parentViewIdentity)
{
	Module::View* view = nullptr;
	std::string viewName = "_"; //Module::ViewIdentity;
	Module::View* parentView = nullptr;
	App::MainWindowView* mainView = nullptr;;
	App::ToolPopupView* toolView = nullptr;;

	// We find the view here
	if (moduleView[viewIdentity] != 0)
	{
		view = moduleView[viewIdentity];
	}
	else // If not found create and add to the moduleViews in array index
	{
		//If parentViewIdentity a view then get the views HWND for parent usage for a child view
		if (parentViewIdentity != Module::ViewIdentity::NullView)
		{
			if (moduleView[parentViewIdentity] != 0)
				parentView = moduleView[parentViewIdentity];
			else
			{
				std::stringstream message;
				message
						<< "Implementation required for Module::ViewIdentity. Enumerator index is "
						<< viewIdentity << " " << std::endl;
				MessageBox(nullptr, message.str().c_str(), "Notice",
				MB_OK | MB_ICONINFORMATION);
				PostQuitMessage(WM_QUIT);
			}
		}

		switch (viewIdentity)
		{
		case Module::ViewIdentity::MainWindow:

			mainView = new App::MainWindowView();
			std::cout << "Construct MainWindow" << std::endl;
			moduleView[viewIdentity] = mainView;
			mainView->createView(hInstance, className, loader);

			view = mainView;
			break;
		case Module::ViewIdentity::ToolBox:
		case Module::ViewIdentity::ToolPopup:
		case Module::ViewIdentity::MessageBoxView:
			// Dockable window and the DockContainer is the parent MainWindow
			if (parentViewIdentity == Module::ViewIdentity::MainWindow)
			{
				mainView = static_cast<App::MainWindowView*>(parentView);

				toolView = new App::ToolPopupView();
				std::cout << "Construct ToolBox" << std::endl;
				moduleView[viewIdentity] = toolView;
				toolView->createView(hInstance, mainView->windowHandle());

				//After the Docked window created add it to the DockContainer(MainWindow)
				mainView->addDockWindow(toolView);

				view = toolView;
			}
			break;

		default:
			view = nullptr; //new Module::View(); // A default view
			break;
		}
	}
	if (view == nullptr)
	{
		std::stringstream message;
		message
				<< "Implementation required for Module::ViewIdentity. Enumerator index is "
				<< viewIdentity << " " << std::endl;
		MessageBox(nullptr, message.str().c_str(), "Notice",
		MB_OK | MB_ICONINFORMATION);
		PostQuitMessage(WM_QUIT);
	}
	return (view);
}

//----------------------------------------------------------------------------------
// destroyInstance by HWND
//----------------------------------------------------------------------------------
void ViewFactory::destroyInstance(HWND hWnd)
{
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
	{
		if (moduleView[index] != 0)
		{
			if (index == 0) // MainWindow
			{
				App::MainWindowView* mainView =
						static_cast<App::MainWindowView*>(moduleView[index]);
				if (mainView->windowHandle() == hWnd)
				{
					std::cout << "Destruct " << moduleView[index]
							<< std::endl;
					delete moduleView[index];
					moduleView[index] = 0;
					break;
				}
			}
			if (index >= 1 && index <= 3)
			{
				//TODO: Be sure to move the DockWnd form DockContainers
				App::MainWindowView* parent =
						static_cast<App::MainWindowView*>(moduleView[Module::ViewIdentity::MainWindow]);
				App::ToolPopupView* view =
						static_cast<App::ToolPopupView*>(moduleView[index]);
				if (view->dockWndHandle() == hWnd)
				{
					std::cout
							<< "YES dockWnd Handle used to remove view identity"
							<< std::endl;
					parent->removeDockWindow(view);
					std::cout << "Destruct " << moduleView[index]
							<< std::endl;
					delete moduleView[index];
					moduleView[index] = 0;
					break;
				}
			}
		}
	}
}

//----------------------------------------------------------------------------------
// Private member function
//----------------------------------------------------------------------------------
HWND ViewFactory::getOwner(HWND hwnd)
{
	return (GetWindow(hwnd, GW_OWNER));
}

//----------------------------------------------------------------------------------
// Private member function
//----------------------------------------------------------------------------------
BOOL ViewFactory::isOwnedBy(HWND hwndMain, HWND hwnd)
{
	return ((hwnd == hwndMain) || (getOwner(hwnd) == hwndMain));
}
//----------------------------------------------------------------------------------
//	This function only returns the popups which belong
//	to the specified "main" window
//
//	hwndMain - handle to top-level owner of the popups to retrieve
//	hwndList - where to store the list of popups
//	nItems   - [in] - size of hwndList, [out] - returned no. of windows
//	fIncMain - include the main window in the returned list
//----------------------------------------------------------------------------------
int ViewFactory::getPopupList(HWND hwndMain, HWND hwndList[], int nSize,
BOOL fIncMain)
{
	int index, count = 0;

	if (hwndList == 0)
		return (0);

	for (index = 0; index < nNumDockWnds && index < nSize; index++)
	{
		if (isOwnedBy(hwndMain, hDockList[index]))
			hwndList[count++] = hDockList[index];
	}

	if (fIncMain && count < nSize)
	{
		hwndList[count++] = hwndMain;
	}

	return (count);
}

}

