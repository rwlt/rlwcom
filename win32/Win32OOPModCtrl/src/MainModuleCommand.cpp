/*
 * MoveSplitterCommand.cpp
 *
 *  Created on: Apr 29, 2015
 *      Author: Rodney Woollett
 */

#include "MainModuleCommand.h"
#include "MainWindowView.h"
#include "Command.h"
#include "Message.h"

namespace App
{

MainModuleCommand::MainModuleCommand(Module::View *view,
		int (App::MainWindowView::*meth)(const Module::Message &)) :
		Module::Command(view),  method(meth)
{
}

MainModuleCommand::~MainModuleCommand()
{
}

int MainModuleCommand::execute(const Module::Message &parameters)
{
	LRESULT returnValue = 0;
	App::MainWindowView *main = static_cast<App::MainWindowView*>(view);
	returnValue = ((main->*method)(parameters));
	return (returnValue);
}

} /* namespace App */
