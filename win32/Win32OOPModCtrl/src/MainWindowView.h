/*
 * MainWindowView.h
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#ifndef MAINWINDOWVIEW_H_
#define MAINWINDOWVIEW_H_

#include <string>
#include "MainWindowInterface.h"
#include "MainPresenter.h"
#include "Controller.h"
#include "view.h"
#include "DockContainer.h"
#include "Message.h"

extern HBRUSH hbrBackground;
extern COLORREF rgbBackground;
extern COLORREF rgbText;

namespace App
{

class MainWindowView: public Module::MainWindowInterface,
		public Module::View, public App::DockContainer
{
public:
	MainWindowView();
	virtual ~MainWindowView();

    // TODO: these function are windows specific so change to a WindowCommand
	//Module message function
    int moveSplitter (const Module::Message &message);
    int moveSplitter2 (const Module::Message &message);
    int changeTab(const Module::Message &message);
    int selectFont(const Module::Message &message);
    int selectColour(const Module::Message &message);
    // Window message function
    long eraseBackground(const App::WindowMessage &message);
    long mouseMove(const App::WindowMessage &message);
    long drawItem(const App::WindowMessage &message);
    long controlBackground(const App::WindowMessage &message);
	long isDockWnd_Dockable(const App::WindowMessage &message);

	int createView(HINSTANCE handleInstance,
			const char* windowClassName,
			const Module::ViewLoader * const loader);
	HWND modelessDialog();
	HWND windowHandle();
	bool isDockContainer();

	//IPresenceViewList<TaskPresence> TaskList { get; }
	//public void ShowTaskJob(TaskJobPresence taskJobPresence)
	void updateEditControl(const std::string &value);
	std::string readEditControl();
	//
	//void ConfigureFieldToModel(ValidateField field);
	//ITransferList<ValidationError> ValidationErrorList { get; }
	void showValidateError();
	void showError(std::string message);
	void showErrorDialog(std::string message);
	void showStatus(std::string message);
	int save(const Module::Message &message);

	int showView(const Module::Message &message);
	int closeView(const Module::Message &message);
	int paint(const Module::Message &message);
    int size(const Module::Message &message);

private:
    MainWindowView(const MainWindowView& old); // no copying allowed
    MainWindowView operator = (const MainWindowView& old); // no assignment allowed

	HWND createMainWindow(HINSTANCE hinst, const char* clsname, const char* wndname,
			const Module::ViewLoader* const controller);

	BOOL createEditControl(HWND hWndParent, HINSTANCE hInst);
	BOOL createOutputControl(HWND hWndParent, HINSTANCE hInst);
	BOOL createReBarControl(HWND hWndParent, HINSTANCE hInst);
	BOOL createSimpleToolbar(HWND hWndParent, HINSTANCE hInst);
	BOOL createTabbedControl(HWND hWndParent, HINSTANCE hInst);
	BOOL createSplitterControl(HWND hWndParent, HINSTANCE hInst);
	BOOL findDefaultFont();
	//void SetWindowControls(int height, int width, int range, int ySplit);
	void SetWindowControls(RECT *middleRect, int splitTopY, int ySplitBottomY);

	Module::MainPresenter* presenter;
	POINT *ppt;
	int cpt;
	POINT aptStar[6];
	POINT aptTriangle[4], aptRectangle[5], aptPentagon[6], aptHexagon[7];

	HIMAGELIST hImageList;
	HWND hMainWindow;
	HWND hEditControl;
	HWND hOutputControl;
	HBITMAP hBmp;
	HWND hRebarControl;
	HWND hWndToolbar;
	HWND hTabbedControl;
	HWND hwndStatic;
    HWND hSplitterControl;
    HWND hSplitterControl2;
//    HWND hwndButton;
    int  splitRatioBottom;    // in per cent
    int  splitRatioTop;    // in per cent
    enum { splitWidth = 8 };    // width of splitter

	char achTemp[256];  // temporary buffer for strings

	HFONT hFont;
	HBRUSH hbrBackground;
	HBRUSH hbrWhite, hbrGray;
//	HBITMAP hbm1;
	COLORREF rgbBackground;
	COLORREF rgbText;
	BOOL bOpaque;
	COLORREF rgbCustom[16];
	REBARINFO ri;
	REBARBANDINFO rbi;

	int clientWidth;		// width of client area
	int clientHeight;		// height of client area

	std::string bindRefEditValue;
	int pageTabIndex;

	//    private List<ValidateField> m_validateFields = new List<ValidateField>();
//    private List<ValidationError> m_validationErrors = new List<ValidationError>();

};

} /* namespace App */

#endif /* MAINWINDOWVIEW_H_ */
