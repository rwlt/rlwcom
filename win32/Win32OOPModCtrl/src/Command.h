/*
 * Command.h
 *
 *  Created on: Apr 29, 2015
 *      Author: Rodney Woollett
 */

#ifndef COMMAND_H_
#define COMMAND_H_

#include "View.h"
#include "Message.h"

namespace Module
{

class Command
{
public:
	Command(View *view);
	virtual ~Command();

	virtual int execute(const Message &parameters) = 0;
protected:
	View *view;

};

} /* namespace Module */

#endif /* COMMAND_H_ */
