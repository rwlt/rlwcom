/*
 * MainPresenter.h
 *
 *  Created on: Apr 10, 2015
 *      Author: Rodney Woollett
 */

#ifndef MAINPRESENTER_H_
#define MAINPRESENTER_H_

#include "MainWindowInterface.h"

namespace Module
{

class MainPresenter
{
public:
	MainPresenter(MainWindowInterface* const viewInterface);
	virtual ~MainPresenter();

	void initialize();
	bool saveEditControl();

	std::string editValue;

private:
	MainWindowInterface* const view;

};

} /* namespace Module */

#endif /* MAINPRESENTER_H_ */
