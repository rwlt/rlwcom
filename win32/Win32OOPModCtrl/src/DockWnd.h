/*
 * DockWnd.h
 *
 *  Created on: May 3, 2015
 *      Author: Rodney Woollett
 */

#ifndef DOCKWND_H_
#define DOCKWND_H_

#include "WindowMessage.h"
//
//	DockWnd styles
//
#define DWS_BORDERTOP			0x0001	//draw a top    etched border WHEN DOCKED
#define DWS_BORDERBOTTOM		0x0002	//draw a bottom etched border WHEN DOCKED
#define DWS_BORDERLEFT			0x0004	//draw a top    etched border WHEN DOCKED
#define DWS_BORDERRIGHT			0x0008	//draw a bottom etched border WHEN DOCKED
#define DWS_DRAWGRIPPERDOCKED	0x0010	//draw a gripper when docked
#define DWS_DRAWGRIPPERFLOATING	0x0020	//draw a gripper when floating
#define DWS_FORCEDOCK			0x0040
#define DWS_FORCEFLOAT			0x0080
#define DWS_RESIZABLE			0x0100	//is resizable when docked?
#define DWS_NORESIZE			0x0200	//prevent resize when floating

#define DWS_USEBORDERS			0x1000	//use the rcBorders member to define additional border space
#define DWS_NOSETFOCUS			0x2000
#define DWS_NODESTROY			0x4000	//hides the dock-window instead of destroying it

#define DWS_ALLOW_DOCKLEFT	   0x10000
#define DWS_ALLOW_DOCKRIGHT	   0x20000
#define DWS_ALLOW_DOCKTOP	   0x40000
#define DWS_ALLOW_DOCKBOTTOM   0x80000

#define DWS_ALLOW_DOCKALL		(DWS_ALLOW_DOCKLEFT   | \
								 DWS_ALLOW_DOCKBOTTOM | \
								 DWS_ALLOW_DOCKRIGHT  | \
								 DWS_ALLOW_DOCKTOP)

#define DWS_DOCKED_NOTDOCKED	0
#define DWS_DOCKED_FLOATING		0
#define DWS_DOCKED_LEFT			0x10000
#define DWS_DOCKED_RIGHT		0x20000
#define DWS_DOCKED_TOP			0x40000
#define DWS_DOCKED_BOTTOM		0x80000

//
//	DockWnd message notifications...
//
#define DWN_BASE		(0U - 2048U)
#define DWN_HIDDEN		(DWN_BASE - 0)
#define DWN_DOCKED		(DWN_BASE - 1)
#define DWN_UNDOCKED	(DWN_BASE - 2)
#define DWN_CLOSED		(DWN_BASE - 3)
#define DWN_ISDOCKABLE	(DWN_BASE - 4)




namespace App
{

class DockWnd
{
public:
	explicit DockWnd();
	virtual ~DockWnd();

	int createDockWnd(HINSTANCE handleInstance, HWND hWndParent);
	void SetDockContents(HWND hwndContents, HWND hwndParent);
	HWND dockWndHandle();

	// Command Messages to DockWnd
	long windowPosChanged(const App::WindowMessage &message);
	long toggleDockingMode(const App::WindowMessage &message);
	long beginDrag(const App::WindowMessage &message);
	long cancelDrag(const App::WindowMessage &message);
	long leftButtonUp(const App::WindowMessage &message);
	long mouseMove(const App::WindowMessage &message);
	long eraseBackground(const App::WindowMessage &message);
	long drawItem(const App::WindowMessage &message);
	long minMaxInfo(const App::WindowMessage &message);
	long settingChange(const App::WindowMessage &message);
	long closeView(const App::WindowMessage &message);


	// Container drawing and position of docked windows
	BOOL containerPosition(HWND hwndMain, HDWP hdwp, RECT *rect, UINT stateWant);
	//
	void changeVisibility(BOOL show);

	static HHOOK	draghook;
	static HWND		g_hwndDockWnd;
	static BOOL		fControl;
	// Static hook procedure
	static LRESULT CALLBACK draghookproc(int code, WPARAM wParam, LPARAM lParam);

private:
	DockWnd(const DockWnd& oldDockWnd); // no copying allowed
	DockWnd operator = (const DockWnd& oldDockWnd); // no assignment allowed

	void SetFloatingWinPos(DWORD dwSWFlags);
	void CalcFloatingRect();
	UINT IsDockable(RECT *dragrect);
	void DrawXorFrame(HWND hwnd, RECT *rect, BOOL fDocked);
	void TogglePopupStyle();
	void DrawGripper(HDC hdc, int x, int y, int height);
	void DrawCloseButton(HDC hdc, int x, int y, int height, DWORD dwSWFlags);


	HWND GetOwner(HWND hwnd)
	{
		return (GetWindow(hwnd, GW_OWNER));
	}
	//	Send a "fake" WM_SIZE to the specified window
	void Send_WM_SIZE(HWND hwnd)
	{
		RECT rect;
		GetClientRect(hwnd, &rect);
		SendMessage(hwnd, WM_SIZE,  SIZE_RESTORED, MAKELPARAM(rect.right-rect.left,rect.bottom-rect.top));
	}

	DWORD	dwStyle;		//styles..

	int		xpos;			//coordinates of FRAME when floating
	int		ypos;

	int		cxFloating;		//size of CONTENTS when floating
	int		cyFloating;

	int		nDockedSize;	//width/height of window when docked..
	BOOL	fDocked;		//docked/floating?
	UINT	uDockedState;	//left/right/top/bottom when docked?

	RECT	rcBorderDock;	//border to place around each side of contents
	RECT	rcBorderFloat;	//border to place around each side of contents

	//DWORD	dwUser;			//whatever..

	//
	//	Internal to DockWindow library, do not modify
	//
	int		nFrameWidth;	//window frame width when floating
	int		nFrameHeight;	//(Private)

	HWND	hwndContents;	//handle to the window to host
	HWND	hwnd;			//handle to container (THIS!!!)

	BOOL	fDragging;		//Are we dragging?
	RECT	oldrect;		//
	HBITMAP hbm1;
    HWND    hwndButton;

	//
	// Used for dragging
	// Static is able as only one DockWnd can be dragged in Windows UI
	static BOOL		fOldControl;
	static BOOL		fOldDrawDocked;
	static	BOOL  fMouseMoved;
	static	POINT spt;
	static	RECT  rcStart;
	static	POINT oldpt;

	static int count;


};

// Use in DWN_ISDOCKABLE
typedef struct
{
    NMHDR	 hdr;
	HWND     hwndDock;      /* Handle to dock-window */
    App::DockWnd *pDockWnd;      /* Pointer to the DockWnd structure for that window */
    RECT    *dragrect;      /* Current drag-rectangle */

} NMDOCKWNDQUERY;


} /* namespace App */

#endif /* DOCKWND_H_ */
