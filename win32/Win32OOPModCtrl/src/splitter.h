#if !defined (SPLITTER_H)
#define SPLITTER_H

#include "tools.h"

class Pens3d
{
public:
	Pens3d ();
	Pen & Hilight () { return (_penHilight); }
	Pen & Light () { return (_penLight); }
	Pen & Shadow () { return (_penShadow); }
	Pen & DkShadow () { return (_penDkShadow); }
private:
	Pen		_penLight;
	Pen		_penHilight;
	Pen		_penShadow;
	Pen		_penDkShadow;
};

class SplitController
{
public:
	SplitController (HWND hwnd, CREATESTRUCT * pCreat);

    HWND Hwnd () const { return (_hwnd); }
	void Size (int cx, int cy);
	void Paint ();

    void LButtonDown (POINTS pt);
    void LButtonUp (POINTS pt);
    void LButtonDrag (POINTS pt);
	void CaptureChanged ();
private:
	HWND	_hwnd;
	HWND	_hwndParent;
	int		_cx;
	int		_cy;
	int		_dragStart;
	int		_dragY;
	Pens3d	_pens;
};

#endif
