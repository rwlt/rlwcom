/*
 * DockContainer.h
 *
 *  Created on: May 4, 2015
 *      Author: Rodney Woollett
 */

#ifndef DOCKCONTAINER_H_
#define DOCKCONTAINER_H_

#include "DockWnd.h"
#include "ViewLoader.h"

namespace App
{

class DockContainer
{
public:
	DockContainer();
	virtual ~DockContainer();

	int addDockWindow(DockWnd* dockWnd);
	int removeDockWindow(DockWnd* dockWnd);

	UINT DockWnd_GetDockSide(HWND hwnd, NMDOCKWNDQUERY *nmdwq, RECT *prc1, RECT *prc2);
	BOOL DockWnd_Position(HWND hwndMain, HDWP hdwp, RECT *rect);
	int dockerCount(){	return (nNumDockers); }

private:
	DockWnd* dwndDocker[Module::TotalViews];
	int     nNumDockers = 0;

};

} /* namespace App */

#endif /* DOCKCONTAINER_H_ */
