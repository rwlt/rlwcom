#include "model.h"

namespace App
{
//------------------------------------------------------------------------------
Model::Model()
{
	hInst = NULL;
	hImageList = NULL;
	hToolbar = NULL;
	hWndToolbar = NULL;
	hEditControl = NULL;
//	rgbBackground = RGB(0, 0, 0);
//	rgbText = RGB(255, 255, 255);
//	hFont = NULL;
//	hbrBackground = NULL;
//	bOpaque = TRUE;
}

////---------------------------------------------------------------------------
//BOOL Model::CreateEditControl(HWND hWndParent, HINSTANCE hInst)
//{
//	HDC hdc;
//	long lfHeight;
//	HFONT hDefaultFont;
//
//	hEditControl = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "",
//	WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | ES_MULTILINE |
//	ES_AUTOVSCROLL | ES_AUTOHSCROLL, 0, 0, 100, 100, hWndParent,
//			(HMENU) IDC_MAIN_EDIT, GetModuleHandle(NULL), NULL);
//	if (hEditControl == NULL)
//	{
//		MessageBox(hWndParent, "Could not create edit box.", "Error",
//		MB_OK | MB_ICONERROR);
//		return (FALSE);
//	}

	// Set the font used

//	hdc = GetDC(NULL);
//	lfHeight = -MulDiv(11, GetDeviceCaps(hdc, LOGPIXELSY), 72);
//	ReleaseDC(NULL, hdc);
//	hDefaultFont = CreateFont(lfHeight, 0, 0, 0, FW_SEMIBOLD, 0, 0, 0,
//	DEFAULT_CHARSET,
//	OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
//	DEFAULT_PITCH, "Arial");
//	if (hDefaultFont)
//	{
//		DeleteObject(hFont);
//		hFont = hDefaultFont;
//	}
//	else
//	{
//		hFont = static_cast<HFONT>(GetStockObject(DEFAULT_GUI_FONT));
//	}

//	hbrBackground = CreateSolidBrush(RGB(0, 0, 0));

//	SendMessage(hEditControl, WM_SETFONT, (WPARAM) hFont,
//			MAKELPARAM(FALSE, 0));
//	return (TRUE);
//
//}
//------------------------------------------------------------------------------
BOOL Model::CreateStandardToolbar(HWND hWndParent, HINSTANCE hInst)
{
//TODO: check this toolbar code
	// Define some constants.
	//  const int ImageListID = 0;
	const int numButtons = 7;
	const DWORD buttonStyles = BTNS_AUTOSIZE;
	//const int bitmapSize = 16;

	// Create the toolbar.
	hWndToolbar = CreateWindowEx(0, TOOLBARCLASSNAME, NULL,
	WS_CHILD | TBSTYLE_WRAPABLE, 0, 0, 96, 16, hWndParent, NULL, hInst,
	NULL);
	if (hWndToolbar == NULL)
	{
		return (FALSE);
	}

	// Create the imagelist.
	/*hImageList = ImageList_Create(
	 bitmapSize, bitmapSize,   // Dimensions of individual bitmaps.
	 ILC_COLOR16 | ILC_MASK,   // Ensures transparent background.
	 numButtons, 0);

	 if (hImageList)
	 {
	 HANDLE hBitmap = LoadImage (NULL, "standard.bmp", IMAGE_BITMAP, 96, 16, LR_LOADFROMFILE);
	 if (hBitmap)
	 {
	 if (-1 == ImageList_Add (hImageList, (HBITMAP)hBitmap, NULL))
	 {
	 MessageBoxW( hWndParent, L"Failed to add bitmap to toolbar imagelist", L"Error", MB_OK );
	 }
	 }
	 }*/

	// Set the image list.
	//SendMessage(hWndToolbar, TB_SETIMAGELIST, (WPARAM)ImageListID, (LPARAM)hImageList);
	// Load the button images.
	//SendMessage(hWndToolbar, TB_LOADIMAGES, (WPARAM)IDB_STD_SMALL_COLOR, (LPARAM)HINST_COMMCTRL);
	//SendMessage(hWndToolbar, TB_LOADIMAGES, (WPARAM)IDB_STANDARD, (LPARAM)hInst);
	// Initialize button info.
	TBBUTTON tbButtons[numButtons] =
	{
	{ MAKELONG(0, 0), IDM_FILE_NEW, TBSTATE_ENABLED, buttonStyles,
	{ 0 }, 0, 0 },
	{ MAKELONG(1, 0), IDM_FILE_OPEN, TBSTATE_ENABLED, buttonStyles,
	{ 0 }, 0, 0 },
	{ MAKELONG(2, 0), IDM_FILE_SAVE, TBSTATE_ENABLED, buttonStyles,
	{ 0 }, 0, 0 },
	{ MAKELONG(3, 0), IDM_FILE_PRINT, TBSTATE_ENABLED, buttonStyles,
	{ 0 }, 0, 0 },
	{ 0, 0, TBSTATE_ENABLED,
	TBSTYLE_SEP,
	{ 0 }, 0, 0 },
	{ MAKELONG(4, 0), IDM_DRAW_ARROW, TBSTATE_ENABLED, buttonStyles
			| TBSTYLE_GROUP | TBSTYLE_CHECK,
	{ 0 }, 0, 0 },
	{ MAKELONG(5, 0), IDM_DRAW_FREEHAND, TBSTATE_ENABLED, buttonStyles
			| TBSTYLE_GROUP | TBSTYLE_CHECK,
	{ 0 }, 0, 0 } };
	//tbButtons[0].

	// Add buttons.
	SendMessage(hWndToolbar, TB_BUTTONSTRUCTSIZE,
			(WPARAM) sizeof(TBBUTTON), 0);

	DWORD backgroundColor = GetSysColor(COLOR_BTNFACE);
	COLORMAP colorMap;
	colorMap.from = RGB(0, 0, 0);
	colorMap.to = backgroundColor;
	HBITMAP hbm = CreateMappedBitmap(hInst, IDB_STANDARD, 0, &colorMap, 1);
	TBADDBITMAP tb;
	tb.hInst = NULL;
	tb.nID = (UINT_PTR) hbm;

	// hWndToolbar is the window handle of the toolbar.
	// Do not forget to send TB_BUTTONSTRUCTSIZE if the toolbar was created by using CreateWindowEx.
	SendMessage(hWndToolbar, TB_ADDBITMAP, numButtons, (LPARAM) &tb);
	SendMessage(hWndToolbar, TB_ADDBUTTONS, (WPARAM) numButtons,
			(LPARAM) &tbButtons);

	// Tell the toolbar to resize itself, and show it.
	SendMessage(hWndToolbar, TB_AUTOSIZE, 0, 0);
	ShowWindow(hWndToolbar, TRUE);

	SendMessage(hWndToolbar, TB_SETSTATE, IDM_DRAW_ARROW,
	TBSTATE_CHECKED | TBSTATE_ENABLED);
	return (TRUE);

}

//---------------------------------------------------------------------------
//BOOL Model::CreateToolbar(HWND hWndParent, HINSTANCE hInst)
//{
//	hToolbar =
//	CreateDialog(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_TOOLBAR),
//			hWndParent, ToolDlgProc);
//	if (hToolbar == NULL)
//	{
//		MessageBox(hWndParent, "CreateDialog returned NULL", "Warning!",
//		MB_OK | MB_ICONINFORMATION);
//		return (FALSE);
//	}
//	ShowWindow(hToolbar, SW_SHOW);
//	return (TRUE);
//
//}
//---------------------------------------------------------------------------
void Model::ChangeCurrentCursor(HWND hWnd, LPCTSTR cursor)
{
	HCURSOR crsCross;

	crsCross = LoadCursor(hInst, cursor);
	SetClassLong(hWnd, GCL_HCURSOR, (LONG) crsCross);
}
//---------------------------------------------------------------------------
}
