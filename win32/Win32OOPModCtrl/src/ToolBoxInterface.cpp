
/*
 * ToolBoxInterface.cpp
 *
 *  Created on: Apr 14, 2015
 *      Author: Rodney Woollett
 */

#include "ToolBoxInterface.h"
#include <iostream>

namespace Module
{

ToolBoxInterface::ToolBoxInterface()
{
}

ToolBoxInterface::~ToolBoxInterface()
{
	std::cout << "In ToolBoxInterface destructor " << std::endl;
}

} /* namespace Module */
