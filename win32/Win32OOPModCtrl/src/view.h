/*
 * View.h
 *
 *  Created on: Apr 8, 2015
 *      Author: Rodney Woollett
 */

#ifndef VIEW_H_
#define VIEW_H_

#include "Message.h"
namespace Module
{

class View
{
public:
	View();
	virtual ~View();
	virtual int showView(const Message &message) = 0;
	virtual int closeView(const Message &message) = 0;
    virtual int paint(const Message &message) = 0;
    virtual int size(const Message &message) = 0;//int width, int height) = 0;

};

}

#endif /* VIEW_H_ */
