/*
 * ToolBoxPresenter.h
 *
 *  Created on: Apr 14, 2015
 *      Author: Rodney Woollett
 */

#ifndef TOOLBOXPRESENTER_H_
#define TOOLBOXPRESENTER_H_

#include "ToolBoxInterface.h"

namespace Module
{

class ToolBoxPresenter
{
public:
	ToolBoxPresenter(ToolBoxInterface* const viewInterface);
	virtual ~ToolBoxPresenter();

	void initialize();

private:
	ToolBoxInterface* const view;

};

} /* namespace Module */

#endif /* TOOLBOXPRESENTER_H_ */
