/*
 * MainWindowInterface.h
 *
 *  Created on: Apr 10, 2015
 *      Author: Rodney Woollett
 */

#ifndef MAINWINDOWINTERFACE_H_
#define MAINWINDOWINTERFACE_H_

#include <string>
#include "Message.h"
namespace Module
{

class MainWindowInterface
{
public:
	MainWindowInterface();
	virtual ~MainWindowInterface();

	//IPresenceViewList<TaskPresence> TaskList { get; }
    //void ConfigureFieldToModel(ValidateField field);
    //ITransferList<ValidationError> ValidationErrorList { get; }
	virtual void updateEditControl(const std::string &value) = 0 ;
	virtual std::string readEditControl() = 0;
	virtual void showValidateError() = 0;
    virtual void showError(std::string message) = 0;
    virtual void showErrorDialog(std::string message) = 0;
    virtual void showStatus(std::string message) = 0;

    virtual int save(const Message &message) = 0;
    //    virtual void selectFont() = 0;
    //    virtual void selectColour() = 0;


};

} /* namespace Module */

#endif /* MAINWINDOWINTERFACE_H_ */
