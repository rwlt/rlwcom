/*
 * Command.cpp
 *
 *  Created on: Apr 29, 2015
 *      Author: Rodney Woollett
 */

#include "Command.h"
#include <iostream>

namespace Module
{

Command::Command(View *view): view(view)
{

}

Command::~Command()
{
}

} /* namespace Module */
