/*
 * MainWindowInterface.cpp
 *
 *  Created on: Apr 10, 2015
 *      Author: Rodney Woollett
 */

#include "MainWindowInterface.h"
#include <iostream>

namespace Module
{

MainWindowInterface::MainWindowInterface()
{
}

MainWindowInterface::~MainWindowInterface()
{
	std::cout << "In MainWindowInterface destructor " << std::endl;
}

} /* namespace Module */
