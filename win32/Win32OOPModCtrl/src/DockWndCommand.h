/*
 * DockWndCommand.h
 *
 *  Created on: May 02, 2015
 *      Author: Rodney Woollett
 */

#ifndef DOCKWNDCOMMAND_H_
#define DOCKWNDCOMMAND_H_

#include "View.h"
#include "AppCommand.h"
#include "WindowMessage.h"
#include "DockWnd.h"

namespace App
{

class DockWndCommand: public App::Command
{
public:
	DockWndCommand(App::DockWnd *view,
			long (App::DockWnd::*meth)(const App::WindowMessage &));
	virtual ~DockWndCommand();
	long execute(const App::WindowMessage &parameters);
private:
	App::DockWnd *view;
	long (App::DockWnd::*method)(const App::WindowMessage &);
};

} /* namespace App */

#endif /* DOCKWNDCOMMAND_H_ */
