/*
 * MainWindowView.cpp
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#include <windows.h>
#include "MainWindowView.h"
#include "MainPresenter.h"
#include "Controller.h"
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>

namespace App
{

///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
MainWindowView::MainWindowView() :
		presenter(new Module::MainPresenter(this)), ppt(aptPentagon), cpt(
				6), aptStar(
		{
		{ 50, 2 },
		{ 2, 98 },
		{ 98, 33 },
		{ 2, 33 },
		{ 98, 98 },
		{ 50, 2 } }), aptTriangle(
		{
		{ 50, 2 },
		{ 98, 86 },
		{ 2, 86 },
		{ 50, 2 } }), aptRectangle(
		{
		{ 2, 2 },
		{ 98, 2 },
		{ 98, 98 },
		{ 2, 98 },
		{ 2, 2 } }), aptPentagon(
		{
		{ 50, 2 },
		{ 98, 35 },
		{ 79, 90 },
		{ 21, 90 },
		{ 2, 35 },
		{ 50, 2 } }), aptHexagon(
		{
		{ 50, 2 },
		{ 93, 25 },
		{ 93, 75 },
		{ 50, 98 },
		{ 7, 75 },
		{ 7, 25 },
		{ 50, 2 } }), clientWidth(0), clientHeight(0)
// ,       pEditValue(0)
{
	std::cout << "MainWindowView created:" << this << " presenter "
			<< presenter << std::endl;
	hEditControl = nullptr;
	rgbBackground = RGB(0, 0, 0);
	rgbText = RGB(255, 255, 255);
	hFont = nullptr;
	hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	hbrWhite = static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
	hbrGray = static_cast<HBRUSH>(GetStockObject(GRAY_BRUSH));
	bOpaque = TRUE;

	// TODO: the rebar background image - will rebars be used or just plain win32 toolbar
	std::string file;
	file = "D:\\app\\resources\\dot.bmp";
	hBmp = reinterpret_cast<HBITMAP>(LoadImage(0, file.c_str(),
	IMAGE_BITMAP, 0, 0,
	LR_LOADFROMFILE));

	std::memset(rgbCustom, -1, sizeof(rgbCustom));

	//setup REBARBANDINFO with details common to both bands such as text colour
	//or background bitmap. Note that the text colour can only be set for rebars
	//not using winxp styles (ie. no manifest).
	rbi =
	{	0};
	rbi.cbSize = sizeof(REBARBANDINFO);
	rbi.fMask = RBBIM_COLORS | RBBIM_TEXT | RBBIM_BACKGROUND | RBBIM_STYLE
			| RBBIM_CHILD | RBBIM_CHILDSIZE | RBBIM_SIZE;
	rbi.fStyle = RBBS_CHILDEDGE | RBBS_FIXEDBMP | RBBS_GRIPPERALWAYS;
	//rbi.hbmBack = hBmp;
	rbi.clrFore = RGB(222, 255, 255); //text colour(only for non-winxp styles)
	rbi.clrBack = GetSysColor(COLOR_BTNFACE);

	findDefaultFont();

	splitRatioTop = 40;
	splitRatioBottom = 85;

}

///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
MainWindowView::~MainWindowView()
{
	std::cout << "In MainWindowView destructor " << " presenter "
			<< presenter << std::endl;
	DestroyWindow(hEditControl);
	DestroyWindow(hOutputControl);
	DestroyWindow(hRebarControl);
	DestroyWindow(hWndToolbar);
	DestroyWindow(hTabbedControl);
	DestroyWindow(hSplitterControl);
	DestroyWindow(hSplitterControl2);
	DestroyWindow(hwndStatic);
	DeleteObject(hFont);
	DeleteObject(hBmp);
	DeleteObject(hbrBackground);
	DeleteObject(hbrWhite);
	DeleteObject(hbrGray);
	DestroyWindow(hMainWindow);
	DeleteObject(hImageList);
//	DeleteObject(hbm1);
//	DestroyWindow(hwndButton);

	delete presenter;
	presenter = nullptr;

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::createView(HINSTANCE handleInstance,
		const char* windowClassName,
		const Module::ViewLoader * const loader)
{
	//Create the main window
	hMainWindow = createMainWindow(handleInstance, windowClassName,
			"Win32 OOP Programming", loader);
	std::cout << "HMAINWINDOW: " << hMainWindow << std::endl;
	createEditControl(hMainWindow, handleInstance);
	createSplitterControl(hMainWindow, handleInstance);
	createTabbedControl(hMainWindow, handleInstance);
	createOutputControl(hMainWindow, handleInstance);
	createReBarControl(hMainWindow, handleInstance);
//	createSimpleToolbar(hMainWindow, handleInstance);

	/* Set the button image */
//	SendMessage(hwndButton,
//	BM_SETIMAGE,
//	IMAGE_BITMAP,
//	(LPARAM)LoadBitmap(handleInstance, MAKEINTRESOURCE(IDB_APPBUTTONS)));

	// Presenter initializes itself now
	presenter->initialize();
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
HWND MainWindowView::windowHandle()
{
	return (hMainWindow);
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::save(const Module::Message &message)
{
	presenter->saveEditControl();
	return (0);

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::updateEditControl(const std::string &value)
{
	bindRefEditValue = std::move(value);
//	SetDlgItemText(mainWindow.operator HWND__ *(), IDC_MAIN_EDIT,
//			bindRefEditValue.c_str());
	SendMessage(hEditControl, WM_SETTEXT, 0,
			(LPARAM) bindRefEditValue.c_str());

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
std::string MainWindowView::readEditControl()
{
	int lineCount = SendMessage(hEditControl, EM_GETLINECOUNT, 0, 0);
	std::cout << "readEditControl linecount = " << lineCount << std::endl;

	int lengthText = GetWindowTextLength(hEditControl);
	if (lengthText < 0)
		return ("");
	char read[lengthText + 1]; // Create a string big enough for the data
//	GetDlgItemText(mainWindow.operator HWND__ *(), IDC_MAIN_EDIT, read,
//			lengthText + 1);
	SendMessage(hEditControl, WM_GETTEXT, (lengthText + 1),
			(LPARAM) &read);
	// Return new string back
	return (static_cast<std::string>(read));
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showValidateError()
{
//    DialogResult result = MessageBox.Show(
//        ViewUtility.BuildValidationMessage(m_validateFields, m_validationErrors),
//        "Error",
//        MessageBoxButtons.OK | MessageBoxButtons.OKCancel, MessageBoxIcon.Information,
//        MessageBoxDefaultButton.Button1);

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showError(std::string message)
{
//    labelErrorMessage.Text = message;
//    errorProvider1.SetError(labelErrorMessage,
//        ViewUtility.BuildValidationMessage(m_validateFields, m_validationErrors));

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showErrorDialog(std::string message)
{
//    labelErrorMessage.Text = "";
//    errorProvider1.SetError(labelErrorMessage, message);
//    DialogResult result = MessageBox.Show(message, "Error",
//        MessageBoxButtons.OK, MessageBoxIcon.Information,
//        MessageBoxDefaultButton.Button1);

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::showStatus(std::string message)
{
//	SetDlgItemTextA(NULL, IDC_MAIN_EDIT, message.c_str());
//    toolStripStatusLabel1.Text = message;

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::showView(const Module::Message &message)
{
	// We will display the main window as a regular object and update it
	ShowWindow(hMainWindow, SW_SHOWNORMAL);
	UpdateWindow(hMainWindow);
	static int shown = 0;
	std::stringstream title;
	title << "Rainbow - Titled " << std::to_string(++shown);
	SendMessage(hMainWindow, WM_SETTEXT, 0, (LPARAM) title.str().c_str());
	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::closeView(const Module::Message &message)
{
	PostMessage(hMainWindow, WM_CLOSE, 0, 0);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function - the bottom half splitter
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::changeTab(const Module::Message &/*message*/)
{
	std::stringstream title;
	title << "Rainbow - Titled from Perform Command ";
	pageTabIndex = TabCtrl_GetCurSel(hTabbedControl);
	std::cout << "View notified Tabbed hit " << pageTabIndex << std::endl;
	SendMessage(hOutputControl, WM_SETTEXT, 0,
			(LPARAM) title.str().c_str());
	SendMessage(hwndStatic, WM_SETTEXT, 0, (LPARAM) title.str().c_str());

	// TODO: make lparam usage MAKELPARAM
	LPARAM lparam = clientHeight << 16 | LOWORD(clientWidth);
	SendMessage(hMainWindow, WM_SIZE, 0, (LPARAM) lparam);
	//size(clientWidth, clientHeight);
	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// public function - the bottom half splitter
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::moveSplitter(const Module::Message &message)
{
	Module::Parameter::Args args = message.argumments();
	int y = args.oneArg.message;

	//LPARAM lparam = clientHeight << 16 | LOWORD(clientWidth);
	// TODO: make lparam usage MAKELPARAM
	//PostMessage(mainWindow.operator HWND__ *(), WM_SIZE, 0, (LPARAM)lparam);
	//moveSplitter (45);
	// y from splitter is position of main window client
	// calculate percentage to the range of the main window
	splitRatioBottom = y * 100 / (clientHeight);  // set size
	if (splitRatioBottom < 0)
		splitRatioBottom = 5;

	else if (splitRatioBottom > 100)
		splitRatioBottom = 95;

	// TODO: make lparam usage MAKELPARAM
	LPARAM lparam = clientHeight << 16 | LOWORD(clientWidth);
	SendMessage(hMainWindow, WM_SIZE, 0, (LPARAM) lparam);
	//size(clientWidth, clientHeight);
	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// public function - the top half splitter
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::moveSplitter2(const Module::Message &message)
{
	Module::Parameter::Args args = message.argumments();
	int y = args.oneArg.message;
	// y from splitter is position of main window client
	// calculate percentage to the range of the main window
	splitRatioTop = y * 100 / (clientHeight);  // set size
	if (splitRatioTop < 0)
		splitRatioTop = 5;

	else if (splitRatioTop > 100)
		splitRatioTop = 95;
	// TODO: make lparam usage MAKELPARAM
	LPARAM lparam = clientHeight << 16 | LOWORD(clientWidth);
	SendMessage(hMainWindow, WM_SIZE, 0, (LPARAM) lparam);
	//size(clientWidth, clientHeight);
	return (0);
}
//
//---------------------------------------------------------------
// Public function
// Mouse movement - do the markers on main window
//---------------------------------------------------------------
//
long MainWindowView::mouseMove(const App::WindowMessage &message)
{
	std::cout << "MOUSE MOVE" <<  std::endl;
	return (0);
}
//
//---------------------------------------------------------------
// Public function
// Mouse movement - do the markers on main window
//---------------------------------------------------------------
//
long MainWindowView::drawItem(const App::WindowMessage &message)
{
	LPDRAWITEMSTRUCT lpdis;
    HDC hdcMem;
	App::Parameter::Args args = message.argumments();
    lpdis = (LPDRAWITEMSTRUCT) args.wParamLParam.lparma;
    hdcMem = CreateCompatibleDC(lpdis->hDC);
    int offset = 0;//64;
	if (lpdis->itemState & ODS_SELECTED)  // if selected
		offset = 16;
	else
		offset = 0;
    //SelectObject(hdcMem, hbm1);
    // Destination
    StretchBlt(
        lpdis->hDC,         // destination DC
        lpdis->rcItem.left, // x upper left
        lpdis->rcItem.top,  // y upper left
        // The next two lines specify the width and
        // height.
        lpdis->rcItem.right - lpdis->rcItem.left,
        lpdis->rcItem.bottom - lpdis->rcItem.top,
        hdcMem,    // source device context
		offset, 0,      // x and y upper left
        16,        // source bitmap width
        16, SRCCOPY);        // source bitmap height

    DeleteDC(hdcMem);
	return (1L);
}
//
//---------------------------------------------------------------
// Public function
// Erase background - do the markers on main window
//---------------------------------------------------------------
//
long MainWindowView::eraseBackground(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	HDC hdc = (HDC) args.wParamLParam.wparam;
	RECT rc;
	GetClientRect(hMainWindow, &rc);
	SetMapMode(hdc, MM_ANISOTROPIC);
	SetWindowExtEx(hdc, 100, 100, NULL);
	SetViewportOrgEx(hdc, 0, 0, NULL);
	SetViewportExtEx(hdc, rc.right, rc.bottom, NULL);
	FillRect(hdc, &rc, hbrWhite);
	int i, x, y;
	for (i = 0; i < 13; i++)
	{
		x = (i * 40) % 100;
		y = ((i * 40) / 100) * 20;
		SetRect(&rc, x, y, x + 20, y + 20);
		FillRect(hdc, &rc, hbrGray);
	}

	return (1L);
}

//---------------------------------------------------------------
// Public function
// Erase background - do the markers on main window
//---------------------------------------------------------------
//
long MainWindowView::controlBackground(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	HDC hdc = (HDC) args.wParamLParam.wparam;
	HWND controlHWnd = (HWND) args.wParamLParam.lparma;
	if (GetDlgCtrlID(controlHWnd) == IDC_MAIN_EDIT
			|| GetDlgCtrlID(controlHWnd) == IDC_MAIN_OUTPUT)
	{
		HDC hdcEditControl = hdc;
		SetTextColor(hdcEditControl, rgbText);
		SetBkColor(hdcEditControl, rgbBackground);
		return ((LONG) hbrBackground);
	}
	else
	{
		return (0);
	}
}
//
//---------------------------------------------------------------
// Public function
// Begin dragging the dock wnd docking/floating rectangle
//---------------------------------------------------------------
//
long MainWindowView::isDockWnd_Dockable(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	WPARAM lParam = args.wParamLParam.lparma;
	RECT rc1, rc2;

	// Get main window "outer" rectangle
	GetWindowRect(hMainWindow, &rc1);

	// Get main window "inner" rectangle
	GetClientRect(hMainWindow, &rc2);
	MapWindowPoints(hMainWindow, 0, (POINT *) &rc2, 2);
	InflateRect(&rc2, -2, -2);
	return (DockWnd_GetDockSide(hMainWindow, (NMDOCKWNDQUERY *) lParam,
			&rc1, &rc2));
}
///////////////////////////////////////////////////////////////////////////////
// public function
// Paint WIN32 WM_PAINT
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::paint(const Module::Message &message)
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc, rcPort, rcStatic;

	hdc = BeginPaint(hMainWindow, &ps);
	GetClientRect(hMainWindow, &rc);
	GetClientRect(hwndStatic, &rcStatic);

	SetMapMode(hdc, MM_ANISOTROPIC);
	SetWindowExtEx(hdc, 100, 100, NULL);
	SetViewportExtEx(hdc, 100, 100, NULL);
	SetRect(&rcPort, 0, 0, 100, 100);
	SetViewportOrgEx(hdc, 0, 0, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, ppt, cpt);

	SetViewportOrgEx(hdc, 100, 00, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, ppt, cpt);

	SetViewportOrgEx(hdc, 0, 100, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, ppt, cpt);

	SetViewportOrgEx(hdc, 100, 100, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, aptHexagon, 7);

	// Font of edit control previews
	SetMapMode(hdc, MM_TEXT);
	SetViewportOrgEx(hdc, 210, 0, NULL);
	SetRect(&rcPort, 0, 0, 200, 100);

	char szSize[100];
	char szTitle[] = "These are the dimensions of your client area:";
	HFONT hfOld = static_cast<HFONT>(SelectObject(hdc, hFont));

	SetBkColor(hdc, rgbBackground);
	SetTextColor(hdc, rgbText);

	FillRect(hdc, &rcPort, hbrBackground); //(HBRUSH)COLOR_WINDOW	+ 1
	DrawText(hdc, szTitle, -1, &rcPort, DT_WORDBREAK);

	POINT aptPortRect[5] =
	{ 0, 0, 200, 0, 200, 100, 0, 100, 0, 0 };
	Polyline(hdc, aptPortRect, 5);

	wsprintf(szSize, "{%d, %d, %d, %d}", rcPort.left, rcPort.top,
			rcPort.right, rcPort.bottom);
	DrawText(hdc, szSize, -1, &rcPort,
	DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	SetViewportOrgEx(hdc, 420, 0, NULL);
	char szTitle2[] =
			"These are the dimensions of your hwndStatic client area:";
	FillRect(hdc, &rcPort, hbrBackground); //(HBRUSH)COLOR_WINDOW	+ 1
	DrawText(hdc, szTitle2, -1, &rcPort, DT_WORDBREAK);
	Polyline(hdc, aptPortRect, 5);
	wsprintf(szSize, "{%d, %d, %d, %d}", rcStatic.left, rcStatic.top,
			rcStatic.right, rcStatic.bottom);
	DrawText(hdc, szSize, -1, &rcPort,
	DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	SetViewportOrgEx(hdc, 630, 0, NULL);
	char szTitle3[] = "These are the dimensions of your main client area:";
	FillRect(hdc, &rcPort, hbrBackground); //(HBRUSH)COLOR_WINDOW	+ 1
	DrawText(hdc, szTitle3, -1, &rcPort, DT_WORDBREAK);
	Polyline(hdc, aptPortRect, 5);
	wsprintf(szSize, "{%d, %d, %d, %d}", rc.left, rc.top, rc.right,
			rc.bottom);
	DrawText(hdc, szSize, -1, &rcPort,
	DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	SelectObject(hdc, hfOld);  //Change back to stored old font

	EndPaint(hMainWindow, &ps);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function
// handle Win32 WM_SIZE
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::size(const Module::Message &message)
{
	Module::Parameter::Args args = message.argumments();
	clientWidth = args.twoArgs.valueA;
	clientHeight = args.twoArgs.valueB;
	HDWP hdwp;
	RECT middleRect;

	SendMessage(hRebarControl, WM_SIZE, 0, 0);
    SendMessage(hWndToolbar, TB_AUTOSIZE, 0, 0);

	// First the dock containers dock wnd docked
	// Work out where dock windows are allowed
	// (Take into account status bar, toolbar etc)
	SetRect(&middleRect, 0, 0, clientWidth, clientHeight);
	std::cout << "Draw the docked windows..." << std::endl;
	hdwp = BeginDeferWindowPos(dockerCount());

	// Position the specified dock windows. rect will be modified to contain the
	// "inner" client rectangle, where we can position an MDI client,
	// view window, whatever
	DockWnd_Position(hMainWindow, hdwp, &middleRect);
	EndDeferWindowPos(hdwp);

	std::cout << "Drawn docked windows." << std::endl;
	int ySplitTopY = ((clientHeight * splitRatioTop) / 100);
	int ySplitBottomY = ((clientHeight * splitRatioBottom) / 100);

	if (ySplitTopY < middleRect.top)
		ySplitTopY = middleRect.top + 5;
	if (ySplitTopY >= (middleRect.bottom - splitWidth - 16))
		ySplitTopY = middleRect.bottom - splitWidth - 16;

	if (ySplitBottomY <= ySplitTopY)
		ySplitBottomY = ySplitTopY + splitWidth + 1;
	if (ySplitBottomY >= (middleRect.bottom - splitWidth - 8))
		ySplitBottomY = middleRect.bottom - splitWidth - 8;

	SetWindowControls(&middleRect, ySplitTopY, ySplitBottomY);

	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::selectFont(const Module::Message &message)
{
	CHOOSEFONT cf =
	{ sizeof(CHOOSEFONT) };
	LOGFONT lf;

	GetObject(hFont, sizeof(LOGFONT), &lf);

	cf.Flags = CF_EFFECTS | CF_INITTOLOGFONTSTRUCT | CF_SCREENFONTS;
	cf.hwndOwner = hMainWindow;
	cf.lpLogFont = &lf;
	cf.rgbColors = rgbText;

	if (ChooseFont(&cf))
	{
		HFONT hf = CreateFontIndirect(&lf);
		if (hf)
		{
			hFont = hf;
		}
		else
		{
			MessageBox(hMainWindow, "Font creation failed!", "Error",
			MB_OK | MB_ICONEXCLAMATION);
		}

		rgbText = cf.rgbColors;
	}

	SendMessage(hEditControl, WM_SETFONT, (WPARAM) hFont,
			MAKELPARAM(FALSE, 0));
	SendMessage(hwndStatic, WM_SETFONT, (WPARAM) hFont,
			MAKELPARAM(FALSE, 0));
	SendMessage(hOutputControl, WM_SETFONT, (WPARAM) hFont,
			MAKELPARAM(FALSE, 0));
	SendMessage(hRebarControl, WM_SETFONT, (WPARAM) hFont,
			MAKELPARAM(FALSE, 0));
	SendMessage(hTabbedControl, WM_SETFONT, (WPARAM) hFont,
			MAKELPARAM(FALSE, 0));

	// TODO: make lparam usage MAKELPARAM
	InvalidateRect(hMainWindow, NULL, TRUE);
	UpdateWindow(hMainWindow);
	LPARAM lparam = clientHeight << 16 | LOWORD(clientWidth);
	SendMessage(hMainWindow, WM_SIZE, 0, (LPARAM) lparam);
	//size(clientWidth, clientHeight);

	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
int MainWindowView::selectColour(const Module::Message &message)
{
	CHOOSECOLOR cc =
	{ sizeof(CHOOSECOLOR) };

	cc.Flags = CC_RGBINIT | CC_FULLOPEN | CC_ANYCOLOR;
	cc.hwndOwner = hMainWindow;
	cc.rgbResult = rgbBackground;
	cc.lpCustColors = rgbCustom;

	if (ChooseColor(&cc))
	{
		DeleteObject(hbrBackground);
		rgbBackground = cc.rgbResult;
		hbrBackground = CreateSolidBrush(rgbBackground);
	}
	InvalidateRect(hMainWindow, NULL, TRUE);
	UpdateWindow(hMainWindow);
	return (0);

}

///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
HWND MainWindowView::modelessDialog()
{
	return (nullptr);
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

HWND MainWindowView::createMainWindow(HINSTANCE hinst,
		const char * clsname, const char* wndname,
		const Module::ViewLoader* const controller)
{
	hMainWindow = CreateWindowEx(WS_EX_CLIENTEDGE, clsname, wndname,
	WS_OVERLAPPEDWINDOW, //|WS_CLIPCHILDREN
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
			NULL, NULL, hinst, (LPVOID) controller); // window creation data
	if (hMainWindow != NULL)
		return (hMainWindow);
	return (NULL);
}

///////////////////////////////////////////////////////////////////////////////
// Private create
///////////////////////////////////////////////////////////////////////////////
BOOL MainWindowView::createEditControl(HWND hWndParent, HINSTANCE hInst)
{
	hEditControl = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "",
	WS_CHILD | WS_CLIPSIBLINGS |WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | ES_MULTILINE |
	ES_AUTOVSCROLL | ES_AUTOHSCROLL, 0, 0, 100, 100, hWndParent,
			(HMENU) IDC_MAIN_EDIT, hInst, NULL);
	if (hEditControl == NULL)
	{
		MessageBox(hWndParent, "Could not create edit box.", "Error",
		MB_OK | MB_ICONERROR);
		return (FALSE);
	}

	SendMessage(hEditControl, WM_SETFONT, (WPARAM) hFont,
			MAKELPARAM(FALSE, 0));

	return (TRUE);

}

///////////////////////////////////////////////////////////////////////////////
// Private create
///////////////////////////////////////////////////////////////////////////////
BOOL MainWindowView::createOutputControl(HWND hWndParent, HINSTANCE hInst)
{
	hOutputControl = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "",
	WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | ES_MULTILINE |
	ES_AUTOVSCROLL | ES_AUTOHSCROLL, 0, 0, 100, 100, hWndParent,
			(HMENU) IDC_MAIN_OUTPUT, hInst, NULL);
	if (hOutputControl == NULL)
	{
		MessageBox(hWndParent, "Could not create output text control.",
				"Error",
				MB_OK | MB_ICONERROR);
		return (FALSE);
	}

	SendMessage(hOutputControl, WM_SETFONT, (WPARAM) hFont,
			MAKELPARAM(FALSE, 0));

	return (TRUE);
}
///////////////////////////////////////////////////////////////////////////////
// Private create
///////////////////////////////////////////////////////////////////////////////
BOOL MainWindowView::createTabbedControl(HWND hWndParent, HINSTANCE hInst)
{
	TC_ITEM tie;

	hTabbedControl = CreateWindowEx(WS_EX_OVERLAPPEDWINDOW,
	WC_TABCONTROL, "",
			WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN
					| RBS_VARHEIGHT | CCS_NODIVIDER | WS_BORDER, 0, 120,
			400, 250, hWndParent, (HMENU) IDC_MAIN_TABBED, hInst,
			NULL);
	if (hTabbedControl == NULL)
	{
		MessageBox(hWndParent, "Could not create tabbed control.", "Error",
		MB_OK | MB_ICONERROR);
		return (FALSE);
	}
	// Add tabs for each day of the week.
	tie.mask = TCIF_TEXT;//| TCIF_IMAGE;
	tie.iImage = -1;
	tie.pszText = achTemp;

	char days[3][255];
	strcpy(days[0], "Output\0");
	strcpy(days[1], "Problems\0");
	for (int i = 0; i < 2; i++)
	{
		strncpy(achTemp, days[i], sizeof(achTemp));
		if (TabCtrl_InsertItem(hTabbedControl, i, &tie) == -1)
		{
			MessageBox(hWndParent, "Could not create a  tabbed control.",
					"Error",
					MB_OK | MB_ICONERROR);
			DestroyWindow(hTabbedControl);
			return (FALSE);
		}
	}
	//TabCtrl_SetItemSize(hTabbedControl, 20, 12);
	//TabCtrl_SetPadding(hTabbedControl, 1, 1);
	//SetWindowTheme(hTabbedControl, L" ", L" ");

	//set unique REBARBANDINFO values for the combobox control rebar band
//	RECT rc;
//	GetClientRect(hTabbedControl, &rc);    //get dimensions of combobox control
//	rbi.lpText = "Band#1";
//	rbi.hwndChild = hTabbedControl;
//	rbi.cxMinChild = 0;
//	rbi.cyMinChild = rc.bottom - rc.top;
//	rbi.cx = 100;
	//Attach tabbed band to rebar common control
//	SendMessage(hRebarControl, RB_INSERTBAND, static_cast<WPARAM>(-1),
//			reinterpret_cast<LPARAM>(&rbi));
	// TODO: hwndstatic here is where the Problems edit box will go.
	hwndStatic = CreateWindow("STATIC", "",
			WS_CHILD | WS_VISIBLE | WS_BORDER,
			120, 120, CW_USEDEFAULT, CW_USEDEFAULT,
			hWndParent, NULL, hInst, NULL);

	return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////
// Private create
///////////////////////////////////////////////////////////////////////////////
BOOL MainWindowView::createSplitterControl(HWND hWndParent,
		HINSTANCE hInst)
{
	hSplitterControl = CreateWindowEx(0, "Win32SplitterClass", "",
	WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE | WS_OVERLAPPED,
	CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, hWndParent, (HMENU) IDC_SPLITTER1,
			hInst, NULL);

	if (hSplitterControl == NULL)
	{
		MessageBox(hWndParent, "Could not create splitter control.",
				"Error",
				MB_OK | MB_ICONERROR);
		return (FALSE);
	}

	hSplitterControl2 = CreateWindowEx(0, "Win32SplitterClass", "",
	WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE | WS_OVERLAPPED,
	CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, hWndParent, (HMENU) IDC_SPLITTER2,
			hInst, NULL);

	if (hSplitterControl2 == NULL)
	{
		MessageBox(hWndParent, "Could not create splitter2 control.",
				"Error",
				MB_OK | MB_ICONERROR);
		return (FALSE);
	}

	return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////
// Private create
///////////////////////////////////////////////////////////////////////////////
BOOL MainWindowView::createReBarControl(HWND hWndParent, HINSTANCE hInst)
{

	hRebarControl = CreateWindowEx(WS_EX_CLIENTEDGE, REBARCLASSNAME, "",
			WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN
					| RBS_VARHEIGHT | CCS_NODIVIDER | WS_BORDER, 0, 0, 0,
			0, hWndParent, (HMENU) IDC_MAIN_REBAR, hInst, NULL);
	if (hRebarControl == NULL)
	{
		MessageBox(hWndParent, "Could not create rebar control.", "Error",
		MB_OK | MB_ICONERROR);
		return (FALSE);
	}

	//The rebar control has been successfully created so attach two bands, each with
	//a single standard control (combobox and button).
	//no imagelist to attach to rebar
	ri =
	{	0};
	ri.cbSize = sizeof(REBARINFO);
	SendMessage(hRebarControl, RB_SETBARINFO, 0,
			reinterpret_cast<LPARAM>(&ri));

	return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////
// Private create
///////////////////////////////////////////////////////////////////////////////
BOOL MainWindowView::createSimpleToolbar(HWND hWndParent, HINSTANCE hInst)
{
    // Declare and initialize local constants.
    const int ImageListID    = 0;
    const int numButtons     = 3;
    const int bitmapSize     = 16;

    const DWORD buttonStyles = BTNS_AUTOSIZE;
    		//|TBSTYLE_FLAT;

    // Create the toolbar.
    HWND hWndToolbar = CreateWindowEx(0, TOOLBARCLASSNAME, NULL,
                                      WS_CHILD | TBSTYLE_WRAPABLE, 0, 0, 0, 0,
                                      hWndParent, NULL, hInst, NULL);

	if (hWndToolbar == NULL)
	{
		MessageBox(hWndParent, "Could not create simple toolbar control.", "Error",
		MB_OK | MB_ICONERROR);
		return (FALSE);
	}

    // Create the image list.
    hImageList = ImageList_Create(bitmapSize, bitmapSize,   // Dimensions of individual bitmaps.
                                    ILC_COLOR16 | ILC_MASK,   // Ensures transparent background.
                                    numButtons, 0);

    // Set the image list.
    SendMessage(hWndToolbar, TB_SETIMAGELIST,
                (WPARAM)ImageListID,
                (LPARAM)hImageList);

    // Load the button images.
    SendMessage(hWndToolbar, TB_LOADIMAGES,
                (WPARAM)IDB_STD_SMALL_COLOR,
                (LPARAM)HINST_COMMCTRL);

    // Initialize button info.
    // IDM_NEW, IDM_OPEN, and IDM_SAVE are application-defined command constants.

    TBBUTTON tbButtons[numButtons] =
    {
        { MAKELONG(STD_FILENEW,  ImageListID), IDM_FILE_NEW,  TBSTATE_ENABLED, buttonStyles, {0}, 0, (INT_PTR)L"New" },
        { MAKELONG(STD_FILEOPEN, ImageListID), IDM_FILE_OPEN, TBSTATE_ENABLED, buttonStyles, {0}, 0, (INT_PTR)L"Open"},
        { MAKELONG(STD_FILESAVE, ImageListID), IDM_FILE_SAVE, 0,               buttonStyles, {0}, 0, (INT_PTR)L"Save"}
    };

    // Add buttons.
    SendMessage(hWndToolbar, TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0);
    SendMessage(hWndToolbar, TB_ADDBUTTONS,       (WPARAM)numButtons,       (LPARAM)&tbButtons);

    // Resize the toolbar, and then show it.
    SendMessage(hWndToolbar, TB_AUTOSIZE, 0, 0);
    ShowWindow(hWndToolbar,  TRUE);

    return (TRUE);
}

///////////////////////////////////////////////////////////////////////////////
// private function
///////////////////////////////////////////////////////////////////////////////
BOOL MainWindowView::findDefaultFont()
{
	HDC hdc;
	long lfHeight;
	HFONT hDefaultFont;
	// Set the font used
	hdc = GetDC(NULL);
	lfHeight = -MulDiv(11, GetDeviceCaps(hdc, LOGPIXELSY), 72);
	ReleaseDC(NULL, hdc);
	hDefaultFont = CreateFont(lfHeight, 0, 0, 0, FW_SEMIBOLD, 0, 0, 0,
	DEFAULT_CHARSET,
	OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
	DEFAULT_PITCH, "Arial");
	if (hDefaultFont)
	{
		DeleteObject(hFont);
		hFont = hDefaultFont;
	}
	else
	{
		hFont = static_cast<HFONT>(GetStockObject(DEFAULT_GUI_FONT));
	}
	return (TRUE);

}

///////////////////////////////////////////////////////////////////////////////
// private function
///////////////////////////////////////////////////////////////////////////////
void MainWindowView::SetWindowControls(RECT *middleRect, int splitTopY,
		int splitBottomY)
{
	HDWP hdwp;
	RECT topRc, editRc, tabbedRc, buttonRc, staticRc, splitterRc;
	// Size the main window controls in the client area
	// topRc needs top third of middleRect
	// editRc need 2nd third of middleRect
	// tabbedRc needs last third of middleRect
	// The dividing positions are splitTopY for 1 to 2, and splitBottomY for 2 to 3
	std::cout << middleRect->left << " " << middleRect->top << " " << middleRect->right << " " << middleRect->bottom << std::endl;
	SetRect(&topRc, middleRect->left, middleRect->top, middleRect->right - middleRect->left,
			splitTopY);

	SetRect(&buttonRc, middleRect->right - 90, middleRect->top + 24, 12, 12);

	SetRect(&editRc, middleRect->left, splitTopY, middleRect->right - middleRect->left,
			splitBottomY - splitTopY);
	std::cout << editRc.left << " " << editRc.top << " " << editRc.right << " " << editRc.bottom << std::endl;

	SetRect(&splitterRc, middleRect->left, splitBottomY,
			middleRect->right - middleRect->left, splitWidth);
	SetRect(&tabbedRc, middleRect->left, splitBottomY + splitWidth,
			middleRect->right - middleRect->left,
			middleRect->bottom - splitBottomY - splitWidth);
	SetRect(&staticRc, middleRect->left, splitBottomY + splitWidth,
			middleRect->right,
			middleRect->bottom - splitBottomY - splitWidth);
	TabCtrl_AdjustRect(hTabbedControl, FALSE, &staticRc);

	std::cout << "Drawing middle windows..." << std::endl;
	hdwp = BeginDeferWindowPos(4);
	if (hdwp != NULL)
	{
		hdwp = DeferWindowPos(hdwp, hEditControl, NULL, editRc.left,
				editRc.top, editRc.right, editRc.bottom, SWP_NOZORDER);

		if (hdwp == NULL)
			return;

		hdwp = DeferWindowPos(hdwp, hSplitterControl, NULL,
				splitterRc.left, splitterRc.top, splitterRc.right,
				splitterRc.bottom,
				SWP_NOREDRAW | SWP_NOZORDER);

		if (hdwp == NULL)
			return;

		hdwp = DeferWindowPos(hdwp, hTabbedControl, NULL, tabbedRc.left,
				tabbedRc.top, tabbedRc.right, tabbedRc.bottom,
				SWP_NOZORDER);

		if (hdwp == NULL)
			return;

		if (pageTabIndex == 0)
		{
			hdwp = DeferWindowPos(hdwp, hOutputControl,
			HWND_TOP, staticRc.left, staticRc.top,
					staticRc.right - staticRc.left,
					staticRc.bottom - (staticRc.top - tabbedRc.top), 0);
		}
		else if (pageTabIndex == 1)
		{
			hdwp = DeferWindowPos(hdwp, hwndStatic, HWND_TOP,
					staticRc.left, staticRc.top,
					staticRc.right - staticRc.left,
					staticRc.bottom - (staticRc.top - tabbedRc.top), 0);
		}
		if (hdwp != NULL)
		{
			std::cout << "Checking ERROR: " << std::endl;
			if (EndDeferWindowPos(hdwp) == 0)
			{
				DWORD error = GetLastError();
				std::cout << "ERROR: " << error << std::endl;
			}
		}
	}
	// SHow the tabbed window
	if (pageTabIndex == 0)
	{
		ShowWindow(hOutputControl, SW_SHOW);
		ShowWindow(hwndStatic, SW_HIDE);
	}
	else if (pageTabIndex == 1)
	{
		ShowWindow(hwndStatic, SW_SHOW);
		ShowWindow(hOutputControl, SW_HIDE);
	}

//	hdwp = BeginDeferWindowPos(1);
//	if (hdwp != NULL)
//	{
//		hdwp = DeferWindowPos(hdwp, hwndButton, NULL, buttonRc.left,
//				buttonRc.top, buttonRc.right, buttonRc.bottom,
//				SWP_NOZORDER);
//
//		if (hdwp == NULL)
//			return;
//		if (EndDeferWindowPos(hdwp) == 0)
//		{
//			DWORD error = GetLastError();
//			std::cout << "ERROR: " << error << std::endl;
//		}
//	}


	::InvalidateRect(hSplitterControl, 0, TRUE);
	::UpdateWindow(hSplitterControl);
	std::cout << "Drawn middle windows." << std::endl;

}

} /* namespace App */

