/*
 * MainWindowCommand.cpp
 *
 *  Created on: May 07, 2015
 *      Author: Rodney Woollett
 */

#include "MainWindowCommand.h"
#include "MainWindowView.h"
#include "AppCommand.h"
#include "WindowMessage.h"

namespace App
{

MainWindowCommand::MainWindowCommand(App::MainWindowView *view,
		long (App::MainWindowView::*meth)(const App::WindowMessage &)) :
		view(view), method(meth)
{
}

MainWindowCommand::~MainWindowCommand()
{
}

long MainWindowCommand::execute(const App::WindowMessage &parameters)
{
	LRESULT returnValue = 0;
	returnValue = ((view->*method)(parameters));
	return (returnValue);
}

} /* namespace App */
