#ifndef WINDOWCLASS_H
#define WINDOWCLASS_H

#include <windows.h>

namespace App
{
//-----------------------------------------------------------------------------
class WindowClass
{
public:

	//This constructor will initial the application
	WindowClass();
	void Create(HINSTANCE hInst, const char *ClassName, WNDPROC WndPrc,
			const char* MenuName = NULL);
	void CreateSplitter(HINSTANCE hInst, const char *ClassName,
			WNDPROC WndPrc, const char* MenuName = NULL);
	void CreatePopup(HINSTANCE hInst, const char *ClassName,
			WNDPROC WndPrc, const char* MenuName = NULL);
	void CreateDockWnd(HINSTANCE hInst, const char *ClassName,
			WNDPROC WndPrc, const char* MenuName = NULL);

	//Class Registration
	void Register();
	void UnRegister();

protected:
	//Global variable that holds the application
	WNDCLASSEX _WndClsEx;

private:
	WindowClass(const WindowClass& old); // no copying allowed
	WindowClass operator =(const WindowClass& old); // no assignment allowed
	void CommonClass(WNDPROC WndPrc, const char* ClassName,
			HINSTANCE hInst);
};
}
//-----------------------------------------------------------------------------
#endif
