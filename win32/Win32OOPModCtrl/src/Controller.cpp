#include "Controller.h"
#include <iostream>
#include <string>
#include <sstream>

namespace Module
{
Controller::Controller(Module::AbstractViewFactory* abstractViewFactory) :
		abstractViewFactory(abstractViewFactory)
{
}

Controller::~Controller()
{
}

///////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////
//	case IDM_DRAW_ARROW:
//		SendMessage(mExo->hWndToolbar, TB_SETSTATE, IDM_DRAW_ARROW,
//		TBSTATE_CHECKED | TBSTATE_ENABLED);
//		mExo->ChangeCurrentCursor(handle, IDC_ARROW);
//		break;
//
//	case IDM_DRAW_FREEHAND:
//		SendMessage(mExo->hWndToolbar, TB_SETSTATE, IDM_DRAW_FREEHAND,
//		TBSTATE_CHECKED | TBSTATE_ENABLED);
//		mExo->ChangeCurrentCursor(handle, MAKEINTRESOURCE(IDC_FREEHAND));
//		break;

//	case IDC_PRESS:
//		ppt = aptHexagon;
//		cpt = 7;
		//InvalidateRect(handle, NULL, TRUE);
//		return (1L);

//	case IDC_OTHER:
//		ppt = aptRectangle;
//		cpt = 5;
//		InvalidateRect(handle, NULL, TRUE);
//		return (1L);
//		child = abstractViewFactory->createInstance(this,
//				Module::ViewIdentity::ToolBox,
//				Module::ViewIdentity::MainWindow);
//		child->closeView();

///////////////////////////////////////////////////////////////////////////////
// handle LoadParent view IViewLoader interface
// This the main window to the application
///////////////////////////////////////////////////////////////////////////////
void Controller::loadMainWindow()
{
	Module::ViewLoader* loader = static_cast<Module::ViewLoader*>(this);
	View* parentView = abstractViewFactory->createInstance(loader,
			Module::ViewIdentity::MainWindow);
	if (parentView == nullptr)
		return;

	Module::Message viewMessage;
	Module::Parameter paramArgs;
	paramArgs.kind = Module::ParameterKind::ONE_ARG; // TODO: Initial show
	paramArgs.args.oneArg.message = 0;
	viewMessage.setParameter(paramArgs);

	parentView->showView(viewMessage);
}

///////////////////////////////////////////////////////////////////////////////
// handle LoadToolPopup view IViewLoader interface
///////////////////////////////////////////////////////////////////////////////
void Controller::loadToolPopupWindow()
{
	abstractViewFactory->createInstance(this,
			Module::ViewIdentity::ToolPopup,
			Module::ViewIdentity::MainWindow);
}
///////////////////////////////////////////////////////////////////////////////
// handle LoadToolBox view IViewLoader interface
///////////////////////////////////////////////////////////////////////////////
void Controller::loadToolBoxWindow()
{
	abstractViewFactory->createInstance(this,
			Module::ViewIdentity::ToolBox,
			Module::ViewIdentity::MainWindow);
}

///////////////////////////////////////////////////////////////////////////////
// handle MessageBox view IViewLoader interface
///////////////////////////////////////////////////////////////////////////////
void Controller::loadMessageBoxWindow()
{
	abstractViewFactory->createInstance(this,
			Module::ViewIdentity::MessageBoxView,
			Module::ViewIdentity::MainWindow);
}

}
