/*
 * ViewFactory.h
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#ifndef VIEWFACTORY_H_
#define VIEWFACTORY_H_

#include <windows.h>
#include "WindowClass.h"
#include "ViewLoader.h"

namespace App
{

class ViewFactory: public Module::AbstractViewFactory
{
public:
	ViewFactory(HINSTANCE handleInstance);
	~ViewFactory();
	virtual Module::View* createInstance(
			const Module::ViewLoader * const loader,
			Module::ViewIdentity viewIdentity, Module::ViewIdentity parentViewIdentity =
					Module::ViewIdentity::NullView);
	virtual void close();

	int checkDialogMessage(MSG* msg);
	int checkTranslateAccelerator(HACCEL hAccelTable, MSG* msg);
	virtual Module::ViewIdentity viewIdentity(HWND hWnd);
	void destroyInstance(HWND hWnd);
	Module::View* identityInstance(Module::ViewIdentity viewIdentity);
	int getPopupList(HWND hwndMain, HWND hwndList[], int nSize, BOOL fIncMain);

	// Add dock window to dock list
	int addDockWindow(HWND hWnd);
	int removeDockWindow(HWND hWnd);

private:
	ViewFactory(const ViewFactory& old); // no copying allowed
	ViewFactory operator = (const ViewFactory& old); // no assignment allowed

	HWND getOwner(HWND hwnd);
	BOOL isOwnedBy(HWND hwndMain, HWND hwnd);
	HINSTANCE hInstance;
	const char* className;
	const char* classSplitterName;
	const char* dockWndName;
	App::WindowClass windowsClass;
	App::WindowClass windowSplitterClass;
	App::WindowClass dockWndClass;
	int nNumDockWnds;
	Module::View* moduleView[Module::ViewIdentity::TotalViews];
	HWND hDockList[Module::ViewIdentity::TotalViews];
};

}
#endif /* VIEWFACTORY_H_ */
