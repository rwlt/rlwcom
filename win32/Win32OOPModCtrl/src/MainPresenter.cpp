/*
 * MainPresenter.cpp
 *
 *  Created on: Apr 10, 2015
 *      Author: Rodney Woollett
 */

#include "MainPresenter.h"
#include <iostream>
namespace Module
{

MainPresenter::MainPresenter(MainWindowInterface* const viewInterface): view(viewInterface)
{
	editValue = "sample ";
}

MainPresenter::~MainPresenter()
{
}

void MainPresenter::initialize()
{
    view->updateEditControl(editValue);

}

bool MainPresenter::saveEditControl()
{
    editValue = view->readEditControl();
    std::cout << "MainPresenter::saveEditControl " << editValue << " length=" << editValue.length() << std::endl;
	return (0);

}


} /* namespace Module */
