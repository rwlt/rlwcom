/*
 * ToolBoxInterface.h
 *
 *  Created on: Apr 14, 2015
 *      Author: Rodney Woollett
 */

#ifndef TOOLBOXINTERFACE_H_
#define TOOLBOXINTERFACE_H_

namespace Module
{

class ToolBoxInterface
{
public:
	ToolBoxInterface();
	virtual ~ToolBoxInterface();

};

} /* namespace Module */

#endif /* TOOLBOXINTERFACE_H_ */
