/*
 * ToolPopupCommand.h
 *
 *  Created on: May 02, 2015
 *      Author: Rodney Woollett
 */

#ifndef TOOPOPUPCOMMAND_H_
#define TOOPOPUPCOMMAND_H_

#include "View.h"
#include "Command.h"
#include "Message.h"
#include "ToolPopupView.h"

namespace App
{

class ToolPopupCommand: public Module::Command
{
public:
	ToolPopupCommand(Module::View *view,
			int (App::ToolPopupView::*meth)(const Module::Message &));
	virtual ~ToolPopupCommand();
	int execute(const Module::Message &parameters);
private:
	int (App::ToolPopupView::*method)(const Module::Message &);
};

} /* namespace App */

#endif /* TOOPOPUPCOMMAND_H_ */
