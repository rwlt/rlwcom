#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDC_MAIN_EDIT              				101
#define IDC_MAIN_OUTPUT							102
#define IDI_RAINDROP                            103
#define IDC_FREEHAND                            105
#define IDC_MAIN_REBAR							106
#define IDC_COMBOBOX							107
#define IDC_MAIN_TABBED							108
#define IDR_MAIN_MENU                           110
#define IDC_SPLITTER1                           111
#define IDC_SPLITTER2                           112
#define IDC_CLOSEDOCKBUTTON                     114
#define IDC_SPLIT_RANGE                         115
#define IDB_STANDARD                            116
#define IDB_APPBUTTONS                          117
#define IDR_ACCELERATOR1     		           	120
#define IDC_TOOLPOPUP                           130
#define IDM_FILE_NEW                            40000
#define IDM_FILE_OPEN                           40001
#define IDM_FILE_SAVE                           40002
#define IDM_FILE_SAVEAS                         40003
#define IDM_FILE_PRINT                          40005
#define IDM_FILE_EXIT                           40007
#define IDM_DRAW_ARROW                          40010
#define IDM_DRAW_FREEHAND                       40011
#define IDM_ABOUT                               40012
#define IDC_PRESS                               40013
#define IDC_OTHER                               40014
#define IDM_DIALOG_SHOW                         40015
#define IDM_DIALOG_HIDE                         40016

#define IDD_ABOUT								40025
#define IDD_TOOLBAR								40026
#define MSG_MOVESPLITTER                        40030
