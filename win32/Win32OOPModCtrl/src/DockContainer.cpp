/*
 * DockContainer.cpp
 *
 *  Created on: May 4, 2015
 *      Author: Rodney Woollett
 */

#include <windows.h>
#include "DockContainer.h"
#include <iostream>

namespace App
{

//----------------------------------------------------------------------------------
// Public constructor
//----------------------------------------------------------------------------------
DockContainer::DockContainer(): nNumDockers(0)
{
	ZeroMemory(&dwndDocker, sizeof(dwndDocker));

}

//----------------------------------------------------------------------------------
// Public deconstructor
//----------------------------------------------------------------------------------
DockContainer::~DockContainer()
{
}

//----------------------------------------------------------------------------------
// Public function - addDockWindow
//----------------------------------------------------------------------------------
int DockContainer::addDockWindow(DockWnd* dockWnd)
{
	if (nNumDockers < Module::ViewIdentity::TotalViews)
	{
		dwndDocker[nNumDockers++] = dockWnd;
		std::cout << "DockContainer added dockWnd " << nNumDockers << std::endl;
		return (TRUE);
	}
	else
	{
		return (FALSE);
	}
}

//----------------------------------------------------------------------------------
// Public function - removeDockWindow
//----------------------------------------------------------------------------------
int DockContainer::removeDockWindow(DockWnd* dockWnd)
{
	int index;
	for (index = 0; index < nNumDockers; index++)
	{
		if (dwndDocker[index]->dockWndHandle() == dockWnd->dockWndHandle())
		{
			for (; index < nNumDockers - 1; index++)
			{
				dwndDocker[index] = dwndDocker[index + 1];
			}

			nNumDockers--;
			std::cout << "DockContainer remove dockWnd " << nNumDockers << std::endl;
			break;
		}
	}

	return (0);
}

///////////////////////////////////////////////////////////////////////////////
//	DockWnd API
//
//	Given an array of DockWnds, position any docked windows
//	in the specified "Main" window,
//  The HDWP parameter must be obtained via BeginDeferWindowPos
//
///////////////////////////////////////////////////////////////////////////////
BOOL DockContainer::DockWnd_Position(HWND hwndMain, HDWP hdwp, RECT *rect)
{
	int i;

	// Dock the horizontal bars first (across the TOP+BOTTOM)
	UINT stateWant = (DWS_DOCKED_TOP | DWS_DOCKED_BOTTOM);
	for(i = 0; i < nNumDockers; i++)
	{
		dwndDocker[i]->containerPosition(hwndMain, hdwp, rect, stateWant);
	}

	// Only position if docked
	stateWant = (DWS_DOCKED_LEFT | DWS_DOCKED_RIGHT);
	for(i = 0; i < nNumDockers; i++)
	{
		dwndDocker[i]->containerPosition(hwndMain, hdwp, rect, stateWant);
	}

	return (TRUE);
}

//	For the specified "Main" window, calculate where (if at all) the
//  specified DockWindow might dock. Return DWS_DOCKED_FLOATING if
//  you don't want the window to dock in a certain place.
//
//	The NMDOCKWNDQUERY structure is obtained via the WM_NOTIFY message -
//  you would probably only call this function whilst processing the DWN_ISDOCKABLE notification
//
//	ARGUMENTS:	hwnd		- Container window
//				nmdwq		- pointer to NMDOCKWNDQUERY structure
//				rc1			- outer bounding rectangle of where docking is allowed (i.e. window frame)
//				rc2			- inner bounding rectangle of where docking is allowed (i.e. client area)
//
UINT DockContainer::DockWnd_GetDockSide(HWND hwnd, NMDOCKWNDQUERY *nmdwq, RECT *prc1, RECT *prc2)
{
	RECT    *dragrect = nmdwq->dragrect;
	//HWND     hwndDock = nmdwq->hwndDock;

//	std::cout << "Container get dock side: dockwnd " << dwp->dockWndHandle() << std::endl;
//	std::cout << "Drag rect " << dragrect->left << " " << dragrect->top << " "<< dragrect->right << " "<< dragrect->bottom << std::endl;
	RECT rc, rc1, rc2, inter;

	// Make local copies of parameters
	rc1 = *prc1;
	rc2 = *prc2;

	// Check intersection at top
	SetRect(&rc, rc1.left,rc1.top,rc1.right,rc2.top);
	if(IntersectRect(&inter, dragrect, &rc))
		return (DWS_DOCKED_TOP);

	// Check intersection at bottom
	SetRect(&rc, rc1.left,rc2.bottom, rc1.right,rc1.bottom);
	if(IntersectRect(&inter, dragrect, &rc))
		return (DWS_DOCKED_BOTTOM);

	// Check intersection at left
	SetRect(&rc, rc1.left,rc2.top,rc2.left,rc2.bottom);
	if(IntersectRect(&inter, dragrect, &rc))
		return (DWS_DOCKED_LEFT);

	// Check intersection at right
	SetRect(&rc, rc2.right, rc2.top, rc1.right, rc2.bottom);
	if(IntersectRect(&inter, dragrect, &rc))
		return (DWS_DOCKED_RIGHT);


	return (DWS_DOCKED_FLOATING);

}

} /* namespace App */
