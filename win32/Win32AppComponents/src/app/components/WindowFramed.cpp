/*
 * WindowFramed.cpp
 *
 *  Created on: Jun 10, 2015
 *      Author: Rodney Woollett
 */

#include "WindowFramed.h"
#include "WindowClient.h"

namespace App
{
namespace Component
{

WindowFramed::WindowFramed():hMainWindow(0)
{

}

WindowFramed::~WindowFramed()
{
	if (hMainWindow != 0)
	   DestroyWindow(hMainWindow);
}
//
///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
//
void WindowFramed::addWindowClient(WindowClient *c)
{
	c->setWindowFrame(this);
	Composite::add(c);
}

} /* namespace Main */
} /* namespace App */
