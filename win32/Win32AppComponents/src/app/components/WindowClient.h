/*
 * WindowClient.h
 *
 *  Created on: Jun 10, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_WINDOWCLIENT_H_
#define APP_COMPONENTS_WINDOWCLIENT_H_

#include "Component.h"
#include "WindowFramed.h"
#include <string>

namespace App
{
namespace Component
{

class WindowClient: public Component
{
public:
	WindowClient();
	virtual ~WindowClient();
	HWND handle() {
		return (hWndControl);
	}
	void setWindowRect(RECT rect);
	void setTitle(std::string newTitle);
	const std::string getTitle();
	void setPositionFlag(HWND hwndAfter, UINT postitionFlag);
	void setVisible(BOOL newVisible);
	BOOL isVisible();
	UINT getControlId();
	Style* getStyle();

	virtual HDWP deferSize(HDWP hdwp);
	virtual LRESULT paint(HDC hdc, PAINTSTRUCT ps);
	virtual LRESULT style(Style &style);
	virtual LRESULT size();
	virtual BOOL checkDialogMessage(MSG* msg);
	virtual void accept(Visitor &v);
	std::string className(HWND handle);
	bool checkValidControl(HWND handle);

	void ShowLastError(LPCTSTR message);

protected:
	HWND hWndControl;
	UINT controlId;
	RECT rect;
	Style* clientStyle;
	BOOL visible;

private:
	std::string title;
	WindowFramed* frameWindow;
	HWND hwndAfter;
	UINT postitionFlag;
//	std::vector<Control::BindBase*> binds;

	void setWindowFrame(WindowFramed* frameWindow);
	friend class WindowFramed;
};

} /* namespace Main */
} /* namespace App */

#endif /* APP_COMPONENTS_WINDOWCLIENT_H_ */
