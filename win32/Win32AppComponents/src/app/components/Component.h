/*
 * Component.h
 *
 *  Created on: Jun 10, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_COMPONENT_H_
#define APP_COMPONENTS_COMPONENT_H_

#include <windows.h>
#include "../WindowMessage.h"
#include "Style.h"
#include "Visitor.h"

namespace App
{
namespace Component
{

class Component
{
public:
	Component();
	virtual ~Component();

	virtual LRESULT paint(HDC hdc, PAINTSTRUCT ps) = 0;
	virtual LRESULT style(Style &style) = 0;
	virtual LRESULT size() = 0;
	virtual HDWP deferSize(HDWP hdwp) = 0;
	virtual BOOL checkDialogMessage(MSG* msg) = 0;

	// Accept visitors
	virtual void accept(class Visitor &v) = 0;
//    friend class Composite;
};

} /* namespace Main */
} /* namespace App */

#endif /* APP_COMPONENTS_COMPONENT_H_ */
