/*
 * Header.cpp
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#include "SplitBar.h"
#include <commctrl.h>

namespace App
{
namespace Component
{
//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
//
SplitBar::SplitBar()
{

}
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
//
SplitBar::~SplitBar()
{
}
//
///////////////////////////////////////////////////////////////////////////////
// Public create
///////////////////////////////////////////////////////////////////////////////
//
BOOL SplitBar::createControl(HWND hWndParent, HINSTANCE hInst, int controlId)
{
	hWndControl = CreateWindowEx(0, "Win32SplitterClass", "",
	WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE | WS_OVERLAPPED,
	CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, hWndParent, (HMENU) controlId,
			hInst, NULL);

	if (hWndControl == NULL)
	{
		MessageBox(hWndParent, "Could not create splitter control.",
				"Error",
				MB_OK | MB_ICONERROR);
		return (FALSE);
	}
	this->controlId = controlId;
	return (TRUE);
}

//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT SplitBar::paint(HDC hdc, PAINTSTRUCT ps)
{
	return (0);
}
} /* namespace Component */
} /* namespace App */
