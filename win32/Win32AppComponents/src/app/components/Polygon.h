/*
 * Polygon.h
 *
 *  Created on: Jun 10, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_POLYGON_H_
#define APP_COMPONENTS_POLYGON_H_

#include <windows.h>
#include "Component.h"
namespace App
{
namespace Component
{

class Polygon: public Component
{
public:
	Polygon() :
			ppt(aptPentagon), cpt(6), aptStar(
			{
			{ 50, 2 },
			{ 2, 98 },
			{ 98, 33 },
			{ 2, 33 },
			{ 98, 98 },
			{ 50, 2 } }), aptTriangle(
			{
			{ 50, 2 },
			{ 98, 86 },
			{ 2, 86 },
			{ 50, 2 } }), aptRectangle(
			{
			{ 2, 2 },
			{ 98, 2 },
			{ 98, 98 },
			{ 2, 98 },
			{ 2, 2 } }), aptPentagon(
			{
			{ 50, 2 },
			{ 98, 35 },
			{ 79, 90 },
			{ 21, 90 },
			{ 2, 35 },
			{ 50, 2 } }), aptHexagon(
			{
			{ 50, 2 },
			{ 93, 25 },
			{ 93, 75 },
			{ 50, 98 },
			{ 7, 75 },
			{ 7, 25 },
			{ 50, 2 } })
	{

	}
	virtual ~Polygon()
	{
	}

	virtual LRESULT paint(HDC hdc, PAINTSTRUCT ps);
	virtual LRESULT style(Style &style);
	virtual LRESULT size();
	virtual HDWP deferSize(HDWP hdwp);
	virtual BOOL checkDialogMessage(MSG* msg);
	void accept(Visitor &v);

private:
	POINT *ppt;
	int cpt;
	POINT aptStar[6];
	POINT aptTriangle[4], aptRectangle[5], aptPentagon[6], aptHexagon[7];

};

} /* namespace Main */
} /* namespace App */

#endif /* APP_COMPONENTS_POLYGON_H_ */
