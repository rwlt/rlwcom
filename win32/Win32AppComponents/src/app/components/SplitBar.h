/*
 * SplitBar.h
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_SPLITBAR_H_
#define APP_COMPONENTS_SPLITBAR_H_

#include "WindowClient.h"

namespace App
{
namespace Component
{

class SplitBar: public WindowClient
{
public:
	SplitBar();
	virtual ~SplitBar();
	BOOL createControl(HWND hWndParent, HINSTANCE hInst, int controlId);

	LRESULT paint(HDC hdc, PAINTSTRUCT ps);

};

} /* namespace Component */
} /* namespace App */

#endif /* APP_COMPONENTS_SPLITBAR_H_ */
