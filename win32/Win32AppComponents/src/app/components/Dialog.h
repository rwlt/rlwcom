/*
 * Dialog.h
 *
 *  Created on: Jun 24, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_DIALOG_H_
#define APP_COMPONENTS_DIALOG_H_

#include "WindowClient.h"
#include "ComponentException.h"
#include "control/Bind.h"
#include <vector>
#include <sstream>

namespace App
{
namespace Component
{

class Dialog: public WindowClient
{
public:
	Dialog();
	virtual ~Dialog();
	BOOL createControl(HWND hWndParent, HINSTANCE hInst, int controlId);//, DLGPROC dialogProc);

	void updateText(UINT id, const std::string &value);
	std::string &readText(UINT id);

	template<class DataType>
	void addBind(const std::string (DataType::*getPtr)() const,
			     void (DataType::*setPtr)(const std::string &value),
			     Control::BindSource<DataType> &source, int id)
	{
		if (hWndControl == 0)
			return;
		HWND handle = GetDlgItem(hWndControl, id);
		if (checkValidControl(handle))
		{
			Control::Bind<DataType> * bind =
					new Control::Bind<DataType>(getPtr, setPtr, &source, id);
			binds.push_back(bind);
		}
	}

	static BOOL CALLBACK DialogWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
			LPARAM lParam);
	BOOL dispatchMessage(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);

	virtual void accept(Visitor &v);
	virtual BOOL checkDialogMessage(MSG* msg);

	LRESULT postMessage(const App::WindowMessage &message);
	LRESULT controlBackground(const App::WindowMessage &message);


private:
	std::string controlText;
	HWND hWndPostMessage; // Normally parent window where post messages goes to start processing dialog
	std::vector<Control::BindBase*> binds;

	Dialog(const Dialog&); // Private not copy able
	Dialog operator =(const Dialog&);
	void okAccept();

	friend class AddItemVisitor;
	friend class BindToSourceVisitor;
	friend class UpdateItemVisitor;

};

} /* namespace Component */
} /* namespace App */

#endif /* APP_COMPONENTS_DIALOG_H_ */
