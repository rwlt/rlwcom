/*
 * Tabbed.h
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_TABBED_H_
#define APP_COMPONENTS_TABBED_H_

#include "WindowClient.h"
#include "DockWnd.h"
#include <vector>
namespace App
{
namespace Component
{


class Tabbed: public WindowClient
{
	typedef std::vector<App::DockWnd*> vec_wnd_index;
	typedef vec_wnd_index::iterator iterv_wnd_index;

public:
	Tabbed();
	virtual ~Tabbed();
	BOOL createControl(HWND hWndParent, HINSTANCE hInst, int controlId);
	LRESULT paint(HDC hdc, PAINTSTRUCT ps);
	LRESULT style(Style &style);
	BOOL addTabWindow(App::DockWnd* tabbedWindow);
	BOOL removeTabWindow(App::DockWnd* tabbedWindow);
	virtual HDWP deferSize(HDWP hdwp);
	int totalTabs();

private:
	int index;
	char achTemp[256];  // temporary buffer for strings
	vec_wnd_index tabs;
	int minHeight;
	int minWidth;

	BOOL TabInsert(int insertIndex, App::DockWnd* tabbedWindow);
	BOOL showSelectedTabbed(vec_wnd_index &shownTabs);
};

} /* namespace Component */
} /* namespace App */

#endif /* APP_COMPONENTS_TABBED_H_ */
