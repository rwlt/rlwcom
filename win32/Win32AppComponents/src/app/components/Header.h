/*
 * Header.h
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_HEADER_H_
#define APP_COMPONENTS_HEADER_H_

#include "WindowClient.h"
#include "control/Bind.h"
#include "control/BindBase.h"
#include "control/BindSource.h"
#include <vector>

namespace App
{
namespace Component
{

class Header: public WindowClient
{
public:
	Header();
	virtual ~Header();
	BOOL createControl(HWND hWndParent, HINSTANCE hInst, int controlId);

	LRESULT paint(HDC hdc, PAINTSTRUCT ps);
	virtual void accept(Visitor &v);

	void updateText(int controlId, const std::string &value);
	std::string readText(int controlId);

	template<class DataType>
	void addBind(const std::string (DataType::*getPtr)() const,
			     void (DataType::*setPtr)(const std::string &value),
			     Control::BindSource<DataType> &source, int id)
	{
		Control::Bind<DataType> * bind =
				new Control::Bind<DataType>(getPtr, setPtr, &source, id);
		binds.push_back(bind);
	}
private:
	std::string controlText;
	std::vector<Control::BindBase*> binds;
	//int bindRefIndex;

	Header(const Header&); // Private not copy able
	Header operator =(const Header&);
	friend class AddItemVisitor;
	friend class UpdateItemVisitor;

};

} /* namespace Component */
} /* namespace App */

#endif /* APP_COMPONENTS_HEADER_H_ */
