/*
 * Tabbed.cpp
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#include "Tabbed.h"
#include <commctrl.h>
#include <iostream>
#include <algorithm>

namespace App
{
namespace Component
{
//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
//
Tabbed::Tabbed() :
		index(0), minHeight(40), minWidth(40)
{
	//tabs.resize(4);
}
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
//
Tabbed::~Tabbed()
{
}
//
///////////////////////////////////////////////////////////////////////////////
// Public create
///////////////////////////////////////////////////////////////////////////////
//
BOOL Tabbed::createControl(HWND hWndParent, HINSTANCE hInst, int controlId)
{
    InitCommonControls();
	hWndControl = CreateWindow(
			//WS_EX_OVERLAPPEDWINDOW,
	WC_TABCONTROL, "",
	WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN| WS_BORDER,
//	| RBS_VARHEIGHT | CCS_NODIVIDER | WS_BORDER,
	        0, 400, 400, 250, hWndParent,
			(HMENU) controlId,
			hInst,
			NULL);
	if (hWndControl == NULL)
	{
		MessageBox(hWndParent, "Could not create tabbed control.", "Error",
		MB_OK | MB_ICONERROR);
		return (FALSE);
	}
	this->controlId = controlId;
	return (TRUE);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public tabWindow
///////////////////////////////////////////////////////////////////////////////
//
BOOL Tabbed::addTabWindow(App::DockWnd* tabbedWindow)
{
	BOOL done = TRUE;
	int selectedIndex = 0;

	done = TabInsert(index, tabbedWindow);
	if (done == FALSE)
		return (done);

	iterv_wnd_index iterv;
	iterv = std::find(tabs.begin(), tabs.end(), tabbedWindow);
	if (iterv == tabs.end())
	{
		tabs.push_back(tabbedWindow);
		selectedIndex = index;
		++index; // ready for next
	} else {
		selectedIndex = iterv - tabs.begin();
	}

	TabCtrl_SetCurSel(hWndControl, selectedIndex); // Show selected page tab
	return (TRUE);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public tabWindow
///////////////////////////////////////////////////////////////////////////////
//
BOOL Tabbed::removeTabWindow(App::DockWnd* tabbedWindow)
{
	// When removing with App::DockWnd* do find the shown tabs again using order of tabs vector
	// Then get the DELETEITEM index from shown tabs find
	// Then remove from the tabs vector
	iterv_wnd_index iterv;
	iterv = std::find(tabs.begin(), tabs.end(), tabbedWindow);
	if (iterv != tabs.end())
	{
		int tabIndex = 0;
		for (App::DockWnd* dockWnd: tabs) {
			if (dockWnd->isVisible()== TRUE) {
				if (tabbedWindow == dockWnd) {
					break;
				}
				++tabIndex;
			}
		}
		std::cout << "removeTab pageTabIndex " << tabIndex << " " << std::endl;
		long result = SendMessage(hWndControl, TCM_DELETEITEM, (WPARAM) tabIndex, (LPARAM) 0);
		if (result == FALSE)
		{
			MessageBox(GetParent(hWndControl), "Could not delete a tab.", "Error",
			MB_OK | MB_ICONERROR);
			//return (FALSE);
		}
		tabs.erase(iterv);
		--index; // ready for next

		// Set the new selected tab
		if ((index - 1) >= 0)
		{
			TabCtrl_SetCurSel(hWndControl, index - 1); // Show selected page tab
		}

	}
	return (TRUE);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
HDWP Tabbed::deferSize(HDWP hdwp)
{
	RECT staticRc, rcTab;
	UINT stateWant = (DWS_DOCKED_TABBED);
	int pageTabIndex;
	int insertIndex = 0;
	//int index;
	vec_wnd_index shownTabs;
    DWORD dwDlgBase = GetDialogBaseUnits();
    int cxMargin = LOWORD(dwDlgBase) / 4;
    int cyMargin = HIWORD(dwDlgBase) / 8;

    //SetRectEmpty(&rcTab);
    CopyRect(&rcTab, &rect);
    //rcTab = rect;
//    for (index = 0; index < tabs.size(); index++) {
//        if (pHdr->apRes[i]->cx > rcTab.right)
//            rcTab.right = pHdr->apRes[i]->cx;
//        if (pHdr->apRes[i]->cy > rcTab.bottom)
//            rcTab.bottom = pHdr->apRes[i]->cy;
//    }
//    rcTab.right = rcTab.right * LOWORD(dwDlgBase) / 4;
//    rcTab.bottom = rcTab.bottom * HIWORD(dwDlgBase) / 8;

	if (hWndControl)
	{
 		hdwp = WindowClient::deferSize(hdwp);
		pageTabIndex = TabCtrl_GetCurSel(hWndControl);		// Get selected page tab
		shownTabs.clear();
		SendMessage(hWndControl, TCM_DELETEALLITEMS, 0, 0);
		for (App::DockWnd* dockWnd: tabs) {
			if (dockWnd->isVisible()== TRUE) {
				TabInsert(insertIndex, dockWnd);
				shownTabs.push_back(dockWnd);
				++insertIndex;
			}
		}
		if (pageTabIndex < 0 || (pageTabIndex > (insertIndex - 1)))
			pageTabIndex = insertIndex - 1;
		// Draw tabs for visible dock windows as they can be made Hidden
		// Now draw the tabbed client window - the selected one
		// Work with DockWnd and their drawing functions.
		// Dock the window in tab
		staticRc = rect;
		TabCtrl_AdjustRect(hWndControl, FALSE, &staticRc);
		staticRc.bottom += rect.top;
		if (pageTabIndex >= 0)
		{
			App::DockWnd* tabbedWindow = shownTabs.at(pageTabIndex);
			ShowWindow(tabbedWindow->dockWndHandle(), SW_SHOW); // Show window to get new size
			tabbedWindow->containerPosition(hWndControl, hdwp, &staticRc, stateWant);
			TabCtrl_SetCurSel(hWndControl, pageTabIndex); // Show selected page tab
		}
		showSelectedTabbed(shownTabs);
	}
	return (hdwp);
}
//
///////////////////////////////////////////////////////////////////////////////
// Protected functions
///////////////////////////////////////////////////////////////////////////////
//
BOOL Tabbed::showSelectedTabbed(vec_wnd_index &shownTabs)
{
	if (hWndControl)
	{
		int pageTabIndex = TabCtrl_GetCurSel(hWndControl); // Get tab page index
		if (pageTabIndex < 0)
			return (FALSE);
		App::DockWnd* tabbedWindow = shownTabs.at(pageTabIndex);
		iterv_wnd_index iterv;
		for (App::DockWnd* dockWnd : tabs)
		{
			if (tabbedWindow == dockWnd)
			{
				ShowWindow(dockWnd->dockWndHandle(), SW_SHOW);
			}
			else
			{
				ShowWindow(dockWnd->dockWndHandle(), SW_HIDE);
			}
		}
	}
	return (TRUE);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT Tabbed::paint(HDC hdc, PAINTSTRUCT ps)
{
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT Tabbed::style(Style &style)
{
	SendMessage(hWndControl, WM_SETFONT, (WPARAM) style.hFont,
			MAKELPARAM(FALSE, 0));
	InvalidateRect(hWndControl, NULL, TRUE);
	UpdateWindow(hWndControl);

	return (0);
}

//
///////////////////////////////////////////////////////////////////////////////
// Private tabWindow
///////////////////////////////////////////////////////////////////////////////
//
BOOL Tabbed::TabInsert(int insertIndex, App::DockWnd* tabbedWindow)
{
	BOOL done = TRUE;
	TC_ITEM tcItem;
	tcItem.mask = TCIF_TEXT  | TCIF_IMAGE;
	tcItem.iImage = -1;
	tcItem.pszText = achTemp;
	strncpy(achTemp, tabbedWindow->getTitle().c_str(), sizeof(achTemp));
	//long result = SendMessage(hWndControl, TCM_INSERTITEM, (WPARAM) insertIndex, (LPARAM) &tcItem);
    long result = TabCtrl_InsertItem(hWndControl, insertIndex, &tcItem);
	if (result < 0)
	{
		MessageBox(GetParent(hWndControl), "Could not insert a tab.", "Error",
		MB_OK | MB_ICONERROR);
		done = FALSE;
	}
	return (done);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
//
int Tabbed::totalTabs()
{
	int count = 0;
	for (App::DockWnd* dockWnd: tabs) {
		if (dockWnd->isVisible()== TRUE) {
			++count;
		}
	}
	return (count);
}

} /* namespace Component */
} /* namespace App */
