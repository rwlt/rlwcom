/*
 * DockWnd.cpp
 *
 *  Created on: May 3, 2015
 *      Author: Rodney Woollett
 */

#include <windows.h>
#include "DockWnd.h"
#include <iostream>
#include <string>
#include <cstring>

//WS_CLIPCHILDREN | WS_CLIPSIBLINGS
#define POPUP_STYLES   (WS_POPUP | WS_SYSMENU | WS_CAPTION | WS_THICKFRAME)
#define POPUP_EXSTYLES (WS_EX_TOOLWINDOW | WS_EX_WINDOWEDGE)
#define CHILD_STYLES   (WS_CHILD  | WS_CLIPSIBLINGS | WS_VISIBLE)
//| WS_CLIPCHILDREN
#define CHILD_EXSTYLES (WS_EX_TOOLWINDOW)


namespace App
{

HHOOK DockWnd::draghook = 0;
HWND DockWnd::g_hwndDockWnd = 0;
BOOL DockWnd::fControl = FALSE;
BOOL DockWnd::fOldControl = FALSE;
BOOL DockWnd::fOldDrawDocked = FALSE;
BOOL DockWnd::fMouseMoved = FALSE;
POINT DockWnd::spt;
RECT  DockWnd::rcStart;
POINT DockWnd::oldpt;
int DockWnd::count = 1;
int DockWnd::nNumDockWnds = 0;
HWND DockWnd::hDockList[] = {0};
//std::memset(App::DockWnd::hDockList, '\0', sizeof(App::DockWnd::hDockList));
//std::fill_n(array, 100, -1);
//
//---------------------------------------------------------------
// Public constructor
//---------------------------------------------------------------
//
DockWnd::DockWnd():
		dwStyle(DWS_USEBORDERS |
				DWS_DRAWGRIPPERDOCKED |
				DWS_DRAWGRIPPERFLOATING |
				DWS_BORDERTOP |
				DWS_BORDERBOTTOM ), xpos(200), ypos(200),
				cxFloating(400), cyFloating(64), nDockedSize(0), fDocked(FALSE), uDockedState(
		DWS_DOCKED_TOP), uTabComponentId(0), nFrameWidth(0), nFrameHeight(0), hwndContents(
				0), hwnd(0), fDragging(FALSE), hbm1(0), hwndButton(0),
				wcContents(nullptr)
{
	std::cout << "In DockWnd constructor " << std::endl;

}
//
//---------------------------------------------------------------
// Public destructor
//---------------------------------------------------------------
//
DockWnd::~DockWnd()
{
	Send_WM_SIZE(GetOwner(hwnd));
	std::cout << "In DockWnd destructor " << std::endl;
	DeleteObject(hbm1);
	DestroyWindow(hwnd);
	DestroyWindow(hwndButton);
}
//
//---------------------------------------------------------------
// Public function setDockContents with a HWND
// hwndcontents - the window to be contents to the dock window
//---------------------------------------------------------------
//
int DockWnd::createDockWnd(std::string title, HINSTANCE handleInstance, HWND hWndParent, WindowClient *windowClient)
{
	// Dock Wnd creation
	SetRect(&rcBorderDock, 4, 3, 3, 3);
	SetRect(&rcBorderFloat, 4, 3, 3, 3);
	RECT parentRc;
	GetWindowRect(hWndParent, &parentRc);
	xpos = (parentRc.left + 30) + (count*10);
	ypos = (parentRc.top + 30) + (count*10);
	++count;
	std::cout << "MainWindow " << title << " " << hWndParent << " from createDockWnd" << std::endl;

	HWND hwnd;
	UINT dwindowStyle;
	UINT dwExStyle;

	setTitle(title);
	if (fDocked == FALSE)
	{
		dwindowStyle = POPUP_STYLES;
		dwExStyle = POPUP_EXSTYLES;
	}
	else
	{
		dwindowStyle = CHILD_STYLES;
		dwExStyle = CHILD_EXSTYLES;
	}

	//if no docking given, then give all docking possibilities
	if ((this->dwStyle & (DWS_ALLOW_DOCKALL | DWS_FORCEFLOAT)) == 0)
		dwindowStyle |= DWS_ALLOW_DOCKALL;

	hwnd = CreateWindowEx(dwExStyle, "Win32DockWnd", title.c_str(),
			dwindowStyle, this->xpos, this->ypos, 0, 0,
			hWndParent,
			0,//(HMENU) idc,
			handleInstance
			, this);

	//hbm1 = static_cast<HBITMAP>(LoadBitmap(handleInstance, MAKEINTRESOURCE(IDB_STDBUTTONS)));
	hwndButton = CreateWindow(
	    "BUTTON",   // predefined class
	    "-",       // button text
		WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON ,//| BS_OWNERDRAW,
	    4,         // starting x position
	    0,         // starting y position
	    16,        // button width
	    16,        // button height
		hwnd,       // parent window
		(HMENU) IDC_CLOSEDOCKBUTTON, // Button ID for BN_CLICKED notification in WM_COMMAND
		handleInstance,
	    NULL);      // pointer not needed

	this->hwnd = hwnd;
	this->fDragging = FALSE;

	// WindowClient created when dockwnd used a decoration to window client components
	// and set the dock window to the content of the windowClient contents
	wcContents = windowClient;
	if (wcContents != nullptr) {
		std::cout << "Client Window" << hWndParent << " dock wind " << hwnd << std::endl;
		this->hWndControl = hwnd; // This is WindowCLients base handle, DockWnd originally was not derived from WindowClient
		this->controlId = wcContents->getControlId();
	    SetDockContents(wcContents->handle(), hWndParent);
	}
	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
long DockWnd::closeView(const App::WindowMessage &message)
{
	PostMessage(hwnd, WM_CLOSE, 0, 0);
	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// public function
// Notify we are closing the dock window
///////////////////////////////////////////////////////////////////////////////
long DockWnd::close(const App::WindowMessage &message)
{
	NMHDR dwhdr;
	int idContents = getControlId();
	dwhdr.code     = DWN_CLOSED;
	dwhdr.hwndFrom = hwnd;
	dwhdr.idFrom   = idContents;
	return (SendMessage(GetParent(hwnd), WM_NOTIFY, idContents, (LPARAM)&dwhdr));
}

//
//---------------------------------------------------------------
// Public function setDockContents with a HWND
// hwndcontents - the window to be contents to the dock window
//---------------------------------------------------------------
//
HWND DockWnd::dockWndHandle()
{
	return (hwnd);
}
//
//---------------------------------------------------------------
// Public function setDockContents with a HWND
// hwndcontents - the window to be contents to the dock window
//---------------------------------------------------------------
//
UINT DockWnd::IdContentWnd()
{
	int idContents = getControlId();
	return (idContents);
}
//
//---------------------------------------------------------------
// Public function setDockContents with a HWND
// hwndcontents - the window to be contents to the dock window
//---------------------------------------------------------------
//
UINT DockWnd::IdTabbedWnd()
{
	return (uTabComponentId);
}
///////////////////////////////////////////////////////////////////////////////
// Dispatch the view message from static window procedure
///////////////////////////////////////////////////////////////////////////////
//
LRESULT DockWnd::dispatchMessage(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam)
{

	LRESULT returnValue = 0;        // return value
	UINT	uHitTest;
	App::WindowMessage windowMessage;
	App::Parameter paramArgs;
	paramArgs.kind = App::ParameterKind::WPARAMLPARAM;
	paramArgs.args.wParamLParam.wparam = wParam;
	paramArgs.args.wParamLParam.lparma = lParam;
	windowMessage.setParameter(paramArgs);
	HWND parent = GetParent(hWnd);

	switch (Msg)
	{
	case WM_NCACTIVATE:
		returnValue = DockWnd::HANDLE_NCACTIVATE(parent, hWnd, wParam,
				lParam);
		break;
	case WM_NCDESTROY:
		returnValue = DockWnd::removeDockWindow(hWnd);
		break;
	case WM_NCHITTEST:
		//	Allow dragging by the client area
		uHitTest = DefWindowProc(hWnd, WM_NCHITTEST, wParam, lParam);
		if(uHitTest == HTCLIENT)
			uHitTest = HTCAPTION;
		return (uHitTest);
	case WM_NCLBUTTONDBLCLK:
		// ToggleDockingMode
		// Prevent standard double-click on the caption area
		if (wParam == HTCAPTION)
		{
			returnValue = toggleDockingMode(windowMessage);
		}
		break;
	case WM_NCLBUTTONDOWN:	//begin drag
		returnValue = beginDrag(windowMessage);
		if (returnValue > 0)
		{
			returnValue = DefWindowProc(hWnd, Msg, wParam, lParam);
		}
		break;
	case WM_CANCELMODE:
		returnValue = cancelDrag(windowMessage);
		break;
	case WM_LBUTTONUP:
		returnValue = leftButtonUp(windowMessage);
        break;
	case WM_MOUSEMOVE:
		returnValue = mouseMove(windowMessage);
		break;
	case WM_ERASEBKGND:
		returnValue = eraseBackground(windowMessage);
		break;
	case WM_CTLCOLORDLG:
	case WM_CTLCOLOREDIT:
		returnValue = controlBackground(windowMessage);
		break;
	case WM_WINDOWPOSCHANGED:
		returnValue = windowPosChanged(windowMessage);
		break;
	case WM_GETMINMAXINFO:
		returnValue = minMaxInfo(windowMessage);
		if (returnValue > 0)
		{
			returnValue = DefWindowProc(hWnd, Msg, wParam, lParam);
		}
		break;
	case WM_SETTINGCHANGE:
		returnValue = settingChange(windowMessage);
		break;
	case WM_DRAWITEM:
		returnValue = drawItem(windowMessage);
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_CLOSEDOCKBUTTON:
			returnValue = closeView(windowMessage);
			break;

		default:
			return (DefWindowProc(hWnd, Msg, wParam, lParam));
		}
		;
		break;

	case WM_CLOSE:
		returnValue = close(windowMessage);
		break;
	default:
		returnValue = DefWindowProc(hWnd, Msg, wParam, lParam);
		break;
	}
	return (returnValue);
}
//
///////////////////////////////////////////////////////////////////////////////
// Main application WinProc for Windows Class of Main Window
///////////////////////////////////////////////////////////////////////////////
//
// View instance function from NC CREATE to set up at NC CREATE data
void DockWnd::addNewDocWnd(HWND hWnd, DockWnd *view) {
	//std::cout << "NC CREATE" << std::endl;
	::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR) view);
	DockWnd::addDockWindow(hWnd);
}

LRESULT CALLBACK DockWnd::DockWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam)
{
	DockWnd *view;
	if (Msg == WM_NCCREATE)  // Non-Client Create
	{
		// WM_NCCREATE message is called before non-client parts(border,
		view = (DockWnd*) (((CREATESTRUCT*) lParam)->lpCreateParams);
		view->addNewDocWnd(hWnd, view);
		return (::DefWindowProc(hWnd, Msg, wParam, lParam));
	}

    view = (DockWnd *)GetWindowLongPtr(hWnd, GWL_USERDATA);
	// check NULL pointer, because GWLP_USERDATA is initially 0, and
	// we store a valid pointer value when WM_NCCREATE is called.
	if (!view)
		return (::DefWindowProc(hWnd, Msg, wParam, lParam));
	else
		return (view->dispatchMessage(hWnd, Msg, wParam, lParam));

}
//
//---------------------------------------------------------------
// Public function setDockContents with a HWND
// hwndcontents - the window to be contents to the dock window
//---------------------------------------------------------------
//
BOOL DockWnd::containerPosition(HWND hwndContainer, HDWP hdwp, RECT *rect,
		UINT stateWant)
{
	//std::cout << "hwnd " << hwnd << "docked " << fDocked << "stated " << uDockedState << std::endl;
	if (stateWant == (DWS_DOCKED_TOP | DWS_DOCKED_BOTTOM))
	{
		if (fDocked && IsWindowVisible(hwnd))
		{
			int n = nDockedSize;

			switch (uDockedState)
			{
			case DWS_DOCKED_TOP:
				DeferWindowPos(hdwp, hwnd, 0, rect->left, rect->top,
						rect->right - rect->left, n, SWP_NOZORDER);
				rect->top += n;
				break;

			case DWS_DOCKED_BOTTOM:
				DeferWindowPos(hdwp, hwnd, 0, rect->left, rect->bottom - n,
						rect->right - rect->left, n, SWP_NOZORDER);
				rect->bottom -= n;
				break;
			default:
				break;
			}
		}
	}
	else if (stateWant == (DWS_DOCKED_LEFT | DWS_DOCKED_RIGHT))
	{
		if(fDocked && IsWindowVisible(hwnd))
		{
			int n = nDockedSize;

			switch(uDockedState)
			{
			case DWS_DOCKED_LEFT:
				DeferWindowPos(hdwp, hwnd, 0, rect->left, rect->top, n, rect->bottom-rect->top, SWP_NOZORDER);
				rect->left += n;
				break;

			case DWS_DOCKED_RIGHT:
				DeferWindowPos(hdwp, hwnd, 0, rect->right-n, rect->top, n, rect->bottom-rect->top, SWP_NOZORDER);
				rect->right -= n;
				break;
			default:
				break;
			}
		}

	}
	else if (stateWant == (DWS_DOCKED_TABBED))
	{
		if(fDocked && IsWindowVisible(hwnd))
		{
			switch(uDockedState)
			{
			case DWS_DOCKED_TABBED:
				DeferWindowPos(hdwp, hwnd, HWND_TOP,
						       rect->left, rect->top,
						       rect->right,// - rect->left,
						       rect->bottom-rect->top, 0);
				break;

			default:
				break;
			}
		}

	}
	return (TRUE);
}
//
//---------------------------------------------------------------
// Public function setDockContents with a HWND
// hwndcontents - the window to be contents to the dock window
//---------------------------------------------------------------
//
void DockWnd::SetDockContents(HWND hwndContents, HWND hwndParent)
{
	RECT rect;

	this->hwndContents = hwndContents;
	SetParent(this->hwndContents, hwnd);

	CalcFloatingRect();

	if (this->nDockedSize <= 0)
	{
		this->nDockedSize = this->cyFloating;

		if (this->dwStyle & DWS_BORDERTOP)
			this->nDockedSize += 2;
		if (this->dwStyle & DWS_BORDERBOTTOM)
			this->nDockedSize += 2;
	}

	if (this->fDocked)
	{
		//force the parent frame to resize its contents,
		//which now includes US..
		GetClientRect(hwndParent, &rect);
		PostMessage(hwndParent, WM_SIZE, SIZE_RESTORED,
				MAKELPARAM(rect.right - rect.left,
						rect.bottom - rect.top));
	}
	else
	{
		DWORD dwShowFlags = 0;//SWP_NOACTIVATE;

		//display the dock window in its floating state
		//dwShowFlags |= SWP_SHOWWINDOW;

		SetFloatingWinPos(dwShowFlags);
	}

	//SetFocus(this->hwndContents);
	if (!(this->dwStyle & DWS_NOSETFOCUS))
		SetFocus(this->hwndContents);
	else
		SendMessage(hwnd, WM_NCACTIVATE, TRUE, 0);

}
//
//---------------------------------------------------------------
// Public function
//	Toggle the specified dock-window between docked/floating status
//---------------------------------------------------------------
//
void DockWnd::setDockedState(BOOL newfDocked, UINT newuDockedState, UINT newuTabComponentId){

	App::WindowMessage message;
	//dwStyle |=
	uDockedState = newuDockedState;
	if (newuTabComponentId > 0) {
		uTabComponentId = newuTabComponentId;
	}
	if (fDocked != newfDocked)
		toggleDockingMode(message); // Will set fDocked to newfDocked TRUE or FALSE value

}
//
//---------------------------------------------------------------
// Public function
//---------------------------------------------------------------
//
void DockWnd::setFloatPos(int fx, int fy)
{
	cxFloating = fx;
	cyFloating= fy;
}
//
//---------------------------------------------------------------
// Public function
//---------------------------------------------------------------
//
void DockWnd::setDockWndStyle(DWORD style)
{
	dwStyle = style;
}
//
//---------------------------------------------------------------
// Public function
//	Toggle the specified dock-window between docked/floating status
//---------------------------------------------------------------
//
long DockWnd::toggleDockingMode(const App::WindowMessage &message)
{
	HWND hwndParent = GetOwner(hwnd);
	// Hide the window, because we don't want it to
	// be in the wrong position when it is docked/undocked
	ShowWindow(hwnd, SW_HIDE);

	TogglePopupStyle();
	fDocked = !fDocked;

	// Get parent to reposition if docked
	//Send_WM_SIZE(hwndParent);

	if(fDocked == FALSE)
	{
		//SetWindowPos(hwnd, HWND_TOP,
			//dwp->wpFloating.x, dwp->wpFloating.y,
			//dwp->wpFloating.cx, dwp->wpFloating.cy, 0);

		SetFloatingWinPos(0);
	}

	if(!(dwStyle & DWS_NOSETFOCUS))
		SetFocus(hwndContents);

	// Finally show the dock window when it is in the right place
	ShowWindow(hwnd, SW_SHOW);
	Send_WM_SIZE(hwndParent);

	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
// The dockwnd setVisible handles any wcContents decorated.
// Overrides any window client setVisible, docked or floating
void DockWnd::setVisible(BOOL newVisible)
{
	visible = newVisible;
	if (newVisible)
	{
		ShowWindow(hwnd, SW_SHOW);
	}
	else
	{
		ShowWindow(hwnd, SW_HIDE);
	}
	Send_WM_SIZE(GetOwner(hwnd));
}
//
//---------------------------------------------------------------
// Public function
// Update the floating rectangle coords
//---------------------------------------------------------------
//
long DockWnd::windowPosChanged(const App::WindowMessage &message)
{

	App::Parameter::Args args = message.argumments();
	WINDOWPOS *wp;
	wp = (WINDOWPOS *) args.wParamLParam.lparma;
	RECT border;
	RECT rect;

	// Is the window floating?
	if (fDocked == FALSE)
	{
		//
		if (!(wp->flags & SWP_NOMOVE))
		{
			xpos = wp->x;
			ypos = wp->y;
		}

		if (!(wp->flags & SWP_NOSIZE))
		{
			RECT rect;

			nFrameWidth = wp->cx;
			nFrameHeight = wp->cy;

			// Update content window size, because
			// the frame size is always based on this
			GetClientRect(hwnd, &rect);
			cxFloating = rect.right;
			cyFloating = rect.bottom;
		}
	}

	//resize the contents so it fills our client area
	SetRect(&border, 0, 0, 0, 0);

	//Work out what our borders are..
	if (fDocked)
	{
		if (dwStyle & DWS_BORDERTOP)
			border.top += 2;
		if (dwStyle & DWS_BORDERBOTTOM)
			border.bottom += 2;
		if (dwStyle & DWS_BORDERLEFT)
			border.left += 2;
		if (dwStyle & DWS_BORDERRIGHT)
			border.right += 2;

		if (dwStyle & DWS_USEBORDERS)
		{
			border.left += rcBorderDock.left;
			border.right += rcBorderDock.right;
			border.top += rcBorderDock.top;
			border.bottom += rcBorderDock.bottom;
		}
	}
	else
	{
		if (dwStyle & DWS_USEBORDERS)
		{
			border.left += rcBorderFloat.left;
			border.right += rcBorderFloat.right;
			border.top += rcBorderFloat.top;
			border.bottom += rcBorderFloat.bottom;
		}
	}

	//Allow space for the gripper
	if (dwStyle & (DWS_DRAWGRIPPERDOCKED | DWS_DRAWGRIPPERFLOATING))
	{
		border.left += 7;
	}

	if (fDocked)
	{
		// Allow space the the docked buttons ( on top for left or right, and on beside gripper for top
		// or bottom always )
		if (uDockedState & ( DWS_DOCKED_LEFT | DWS_DOCKED_RIGHT))
		{
			border.top += 10;
		}
		if (uDockedState & (DWS_DOCKED_TABBED | DWS_DOCKED_TOP | DWS_DOCKED_BOTTOM))
		{
			border.left += 12;
		}
	}

	//position the contents in our client area
	if (fDocked)
	{
		GetClientRect(hwnd, &rect);
	}
	else
	{
		SetRect(&rect, 0, 0, cxFloating, cyFloating);
	}

	if (!(wp->flags & SWP_NOSIZE))
	{
		InvalidateRect(hwnd, 0, TRUE);
	}
	MoveWindow(hwndContents, border.left, border.top,
			rect.right - border.right - border.left,
			rect.bottom - border.bottom - border.top, TRUE);

	return (0);
}
//
//---------------------------------------------------------------
// Public function
// Draw the custom close button
//---------------------------------------------------------------
//
long DockWnd::drawItem(const App::WindowMessage &message)
{
	LPDRAWITEMSTRUCT lpdis;
    HDC hdcMem;
	App::Parameter::Args args = message.argumments();
    lpdis = (LPDRAWITEMSTRUCT) args.wParamLParam.lparma;

//    hdcMem = CreateCompatibleDC(lpdis->hDC);
//
//    int offset = 0;
//	if (lpdis->itemState & ODS_SELECTED)  // if selected
//		offset = 16;
//	else
//		offset = 0;
//    SelectObject(hdcMem, hbm1);
//
//    // Destination
//    StretchBlt(
//        lpdis->hDC,         // destination DC
//        lpdis->rcItem.left, // x upper left
//        lpdis->rcItem.top,  // y upper left
//        // The next two lines specify the width and
//        // height.
//        lpdis->rcItem.right - lpdis->rcItem.left,
//        lpdis->rcItem.bottom - lpdis->rcItem.top,
//        hdcMem,    // source device context
//		offset, 0,      // x and y upper left
//        16,        // source bitmap width
//        16, SRCCOPY);        // source bitmap height
//
//    DeleteDC(hdcMem);
	return (1L);
}
//
//---------------------------------------------------------------
// Public function
// Erase background - do the makrs on dock window for dock/float position
//---------------------------------------------------------------
//
long DockWnd::eraseBackground(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	HDC hdc = (HDC)args.wParamLParam.wparam;
	RECT rc;

	GetClientRect(hwnd, &rc);

	if(fDocked)
	{
		UINT bf = 0;

		if(dwStyle & DWS_BORDERTOP)	bf |= BF_TOP;
		if(dwStyle & DWS_BORDERBOTTOM) bf |= BF_BOTTOM;
		if(dwStyle & DWS_BORDERLEFT)	bf |= BF_LEFT;
		if(dwStyle & DWS_BORDERRIGHT)  bf |= BF_RIGHT;

		//draw whatever edges we need
		if(bf)
			DrawEdge(hdc, &rc, EDGE_ETCHED, bf | BF_ADJUST);
	}

	SetBkColor(hdc, GetSysColor(COLOR_BTNFACE));
	ExtTextOut(hdc, 0, 0, ETO_OPAQUE, &rc, "", 0, 0);

	if(fDocked && (dwStyle & DWS_DRAWGRIPPERDOCKED))
	{
		int y = 1, height = rc.bottom - 1;
		int x = 1;

		if(dwStyle & DWS_BORDERTOP)	{ y += 3; height -= 3; }
		if(dwStyle & DWS_BORDERBOTTOM)	{ height -= 2; }
		if(dwStyle & DWS_BORDERLEFT)	{ x += 3; }

		DrawGripper(hdc, x, y, height);

		if (uDockedState & (DWS_DOCKED_LEFT | DWS_DOCKED_RIGHT))
		{
			x += 10;
		}
		if (uDockedState & (DWS_DOCKED_TABBED | DWS_DOCKED_TOP | DWS_DOCKED_BOTTOM))
		{
			x += 10;
		}

		DrawCloseButton(hdc, x, y, 12, SWP_SHOWWINDOW);
	}

	if(!fDocked && (dwStyle & DWS_DRAWGRIPPERFLOATING))
	{
		DrawGripper(hdc, 1, 3, rc.bottom - 4);
		// No docked buttons for floating tool windows
		DrawCloseButton(hdc, 1, 3, 12, SWP_HIDEWINDOW);
	}

	return (1L);
}
//---------------------------------------------------------------
// Public function
// Erase background - do the markers on main window
//---------------------------------------------------------------
//
long DockWnd::controlBackground(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	HDC hdc = (HDC) args.wParamLParam.wparam;
	App::Component::Style* style = nullptr;

	// Try the wcContents style first
	if (wcContents != nullptr)
		style = wcContents->getStyle();

	// If no style for that use dockwnd style
	if (style == nullptr)
		style = getStyle();

	if (style != nullptr) {
		SetTextColor(hdc, style->rgbText);
		SetBkColor(hdc, style->rgbBackground);
		return ((LONG) style->hbrBackground);
	} else {
		return (0);
	}
}
//
//---------------------------------------------------------------
// Public function
// Begin dragging the dock wnd docking/floating rectangle
//---------------------------------------------------------------
//
long DockWnd::beginDrag(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	WPARAM wParam = args.wParamLParam.wparam;

	fDragging = FALSE;

	if(fDocked)
	{
		if(dwStyle & DWS_FORCEDOCK)
			return (0);

		fControl		= FALSE;
		fOldDrawDocked  = TRUE;
	}
	else
	{
		if(dwStyle & DWS_FORCEFLOAT)
			return (0);

		fControl		= TRUE;
		fOldDrawDocked  = FALSE;
	}

	fMouseMoved = FALSE;
	fOldControl = fControl;

	// Prevent standard dragging by the caption area
	if(wParam == HTCAPTION)
	{
		SetWindowPos(hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);//|SWP_NOACTIVATE);

		GetWindowRect(hwnd, &oldrect);

		DrawXorFrame(hwnd, &oldrect, fOldDrawDocked);

		GetCursorPos(&spt);
		GetWindowRect(hwnd, &rcStart);
		// Map rect to centered at the point position
//		if (fOldDrawDocked)
//		   OffsetRect(&rcStart, oldrect.left-spt.x, oldrect.top-spt.y);
//		DrawXorFrame(hwnd, &rcStart, fOldDrawDocked);

		SetCapture(hwnd);

		fMouseMoved		= FALSE;
		fDragging	= TRUE;

		oldpt = spt;

		// Install the mouse hook
		g_hwndDockWnd = hwnd;
		draghook = SetWindowsHookEx(WH_KEYBOARD, DockWnd::draghookproc, GetModuleHandle(0), 0);

		// Prevent "normal" action here
		return (0); // NIY
	}
	else
	{
		// Otherwise, let normal behaviour happen
		return (1L);
	}

}
//
//---------------------------------------------------------------
// Public function
// Begin dragging the dock wnd docking/floating rectangle
//---------------------------------------------------------------
//
long DockWnd::cancelDrag(const App::WindowMessage &message)
{
	if(fDragging == TRUE)
	{
		fDragging = FALSE;

		DrawXorFrame(hwnd, &oldrect, fOldDrawDocked);
		// Remove the keyboard hook
		if(draghook)
		{
			UnhookWindowsHookEx(draghook);
			draghook = 0;
		}
		ReleaseCapture();

	}
	return (0);
}
//
//---------------------------------------------------------------
// Public function
// Begin dragging the dock wnd docking/floating rectangle
//---------------------------------------------------------------
//
long DockWnd::leftButtonUp(const App::WindowMessage &message)
{
	RECT	dragrect;
	NMDOCKWNDQUERY dwq;
	POINT pt;
	UINT uOldDockedState = uDockedState; // Used the tabbed removal of tabbed dock

	if(fDragging == TRUE)
	{

		fDragging = FALSE;

		DrawXorFrame(hwnd, &oldrect, fOldDrawDocked);//IsDockableKey(hwnd, dwp, &dwp->oldrect, fControl));

		// Remove the keyboard hook
		if(draghook)
		{
			UnhookWindowsHookEx(draghook);
			draghook = 0;
		}

		GetCursorPos(&pt);
		CopyRect(&dragrect, &rcStart);
		OffsetRect(&dragrect, pt.x-spt.x, pt.y-spt.y);

		// If the mouse moved
		if(fMouseMoved != FALSE)
		{
			BOOL fToggleDock;
			BOOL fSetWindowPos;
			UINT uDockSide;

			fDragging = FALSE;

			// If the window was docked
			if(fDocked)
			{
				uDockSide = IsDockable(&dragrect, &pt, &dwq);

				if(fControl == TRUE || uDockSide == 0)
				{
					fToggleDock   = TRUE;
					fSetWindowPos = FALSE;

					//adjust where the dockbar will appear
					xpos = oldrect.left;
					ypos = oldrect.top;
				}
				else
				{
					fToggleDock   = FALSE;
					fSetWindowPos = FALSE;

					uDockedState = uDockSide;
				}
			}
			// If it was floating
			else
			{
				uDockSide = IsDockable(&dragrect, &pt, &dwq);

				if(fControl == FALSE && uDockSide)
				{
					fToggleDock   = TRUE;
					fSetWindowPos = FALSE;

					uDockedState = uDockSide;
				}
				else
				{
					fToggleDock   = FALSE;
					fSetWindowPos = TRUE;
				}
			}

			// Send message to owner of DWS_DOCKED_TABBED add or remove
			if (uOldDockedState & DWS_DOCKED_TABBED) {
				TabbedDockChange(DWN_TABREMOVE);
			}
			if (uDockedState & DWS_DOCKED_TABBED) {
			    uTabComponentId = GetWindowLong(dwq.hwndDockContainer, GWL_ID);
			    TabbedDockChange(DWN_TABADD);
			}


			if(fToggleDock)
			{
				toggleDockingMode(message);
			}
			else if(fSetWindowPos)
			{
				SetWindowPos(hwnd, 0, oldrect.left, oldrect.top, 0, 0,
					SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOZORDER |
					SWP_DRAWFRAME | SWP_NOSENDCHANGING);
			}
			else
			{
				Send_WM_SIZE(GetOwner(hwnd));
			}

		}

		ReleaseCapture();
	}

	return(0);
}
//
//---------------------------------------------------------------
// Public function
// Begin dragging the dock wnd docking/floating rectangle
//---------------------------------------------------------------
//
long DockWnd::mouseMove(const App::WindowMessage &message)
{
	RECT	dragrect;
	NMDOCKWNDQUERY dwq;
	POINT pt;

	if(fDragging)
	{
		static POINT oldpt;
		BOOL fIsDockable;
		RECT rc;
		UINT uDockSide;

		GetCursorPos(&pt);

		if(oldpt.x == pt.x && pt.y == oldpt.y && fControl == fOldControl)
		{
			return (0);
		}

		fMouseMoved = TRUE;

		fControl = GetKeyState(VK_CONTROL) < 0;
		oldpt = pt;

		// ORiginal
		CopyRect(&dragrect, &rcStart);
		OffsetRect(&dragrect, pt.x-spt.x, pt.y-spt.y);

		uDockSide   = IsDockable(&dragrect, &pt, &dwq);
		fIsDockable = uDockSide != DWS_DOCKED_NOTDOCKED && fControl == FALSE;

		if(fIsDockable)
		{
			//offset back again
			if(uDockSide & (DWS_DOCKED_LEFT|DWS_DOCKED_RIGHT))
				OffsetRect(&dragrect, 0, -(pt.y-spt.y));
			else
				OffsetRect(&dragrect, -(pt.x-spt.x), 0);
		}

		if(EqualRect(&dragrect, &oldrect) == 0 || fOldControl != fControl)
		{
			//Erase where the dragging frame used to be..
			DrawXorFrame(hwnd, &oldrect, fOldDrawDocked);

			//check to see if we have moved over a dock/nodock area, and
			//update the size of the drag rectangle to reflect size
			if(fIsDockable)
			{

				if(uDockSide & (DWS_DOCKED_LEFT|DWS_DOCKED_RIGHT))
				{
					GetClientRect(dwq.hwndDockContainer, &rc);
					MapWindowPoints(dwq.hwndDockContainer, 0, (POINT *)&rc, 2);

					dragrect.top    = rc.top;
					dragrect.bottom = dragrect.top + (rc.bottom-rc.top);
					if (uDockSide & DWS_DOCKED_LEFT) {
						dragrect.left = rc.left;
						dragrect.right = dragrect.left + nDockedSize;
					}
					if (uDockSide & DWS_DOCKED_RIGHT) {
						dragrect.right = rc.right;
						dragrect.left = dragrect.right - nDockedSize;
					}
				}
				if(uDockSide & (DWS_DOCKED_TOP|DWS_DOCKED_BOTTOM))
				{
					GetClientRect(dwq.hwndDockContainer, &rc);
					MapWindowPoints(dwq.hwndDockContainer, 0, (POINT *)&rc, 2);

					dragrect.left   = rc.left;
					dragrect.right  = dragrect.left+ (rc.right-rc.left);
					if (uDockSide & DWS_DOCKED_TOP) {
						dragrect.top = rc.top;
						dragrect.bottom = dragrect.top + nDockedSize;
					}
					if (uDockSide & DWS_DOCKED_BOTTOM) {
						dragrect.bottom = rc.bottom;
						dragrect.top = dragrect.bottom - nDockedSize;
					}
				}
				if(uDockSide & (DWS_DOCKED_TABBED))
				{
					GetClientRect(dwq.hwndDockContainer, &rc);
					MapWindowPoints(dwq.hwndDockContainer, 0, (POINT *)&rc, 2);
					InflateRect(&rc, -2, -2);
					dragrect = rc;
				}
			}
			else
			{
				if (fOldDrawDocked){
					// From docked, make the drag rect
					// a floating rect at mouse point
					dragrect.top  = pt.y;
					dragrect.left = pt.x;
				}

				dragrect.right  = dragrect.left + nFrameWidth;
				dragrect.bottom = dragrect.top  + nFrameHeight;

				if (fOldDrawDocked){
					rcStart = dragrect;
					spt = pt;
				}
			}

			//Draw the new drag frame
			DrawXorFrame(hwnd, &dragrect, fIsDockable);
			fOldDrawDocked = fIsDockable;
		}

		oldrect = dragrect;
		fOldControl = fControl;
	}

	return(0);
}
//
//---------------------------------------------------------------
// Private function
//	Toggle any window between WS_POPUP and WS_CHILD
//---------------------------------------------------------------
//
void DockWnd::TogglePopupStyle()
{
	DWORD dwStyle   = GetWindowLong(hwnd, GWL_STYLE);
	DWORD dwExStyle = GetWindowLong(hwnd, GWL_EXSTYLE);

	if(dwStyle & WS_CHILD)
	{
		SetWindowLong(hwnd, GWL_STYLE,   (dwStyle   & ~CHILD_STYLES)   | POPUP_STYLES);
		SetWindowLong(hwnd, GWL_EXSTYLE, (dwExStyle & ~CHILD_EXSTYLES) | POPUP_EXSTYLES);
		SetParent(hwnd, NULL);
	}
	else
	{
		SetWindowLong(hwnd, GWL_STYLE,   (dwStyle   & ~POPUP_STYLES)   | CHILD_STYLES);
		SetWindowLong(hwnd, GWL_EXSTYLE, (dwExStyle & ~POPUP_EXSTYLES) | CHILD_EXSTYLES);
		SetParent(hwnd, GetOwner(hwnd));
	}

	// Send the window a WM_NCCALCSIZE message, because the
	// frame-style has changed.
	SetWindowPos(hwnd, 0, 0, 0, 0, 0,   SWP_NOMOVE   | SWP_NOSIZE     |
										SWP_NOZORDER | SWP_NOACTIVATE |
										SWP_FRAMECHANGED);
}
//
//---------------------------------------------------------------
// Private function
// Return if the specified dock-window is dockable
//---------------------------------------------------------------
//
UINT DockWnd::IsDockable(RECT *dragrect, POINT *pt, App::NMDOCKWNDQUERY *dwq)
{
	int id = GetWindowLong(hwnd, GWL_ID);//hwndContents
	dwq->hdr.code     = DWN_ISDOCKABLE;
	dwq->hdr.hwndFrom = hwnd;
	dwq->hdr.idFrom   = id;
	dwq->dragrect     = dragrect;
	dwq->point        = pt;
	dwq->hwndDock     = hwnd;
//	dwq->pDockWnd     = this;

	return (SendMessage(GetParent(hwnd), WM_NOTIFY, id, (LPARAM)dwq));
}
//
//---------------------------------------------------------------
// Private function
// Return if the specified dock-window is dockable
//---------------------------------------------------------------
//
UINT DockWnd::TabbedDockChange(UINT code)
{
	NMDOCKTABCHANGE dtc;
	int id = getControlId();
	dtc.hdr.code     = code;
	dtc.hdr.hwndFrom = hwnd;
	dtc.hdr.idFrom   = id;
	dtc.uTabComponentId = uTabComponentId;

	return (SendMessage(GetParent(hwnd), WM_NOTIFY, id, (LPARAM)&dtc));
}
//
//---------------------------------------------------------------
// Private function
// Draw both types of frame - checkered and solid line
//---------------------------------------------------------------
//
void DockWnd::DrawXorFrame(HWND hwnd, RECT *rect, BOOL fDocked)
{
	static WORD _dotPatternBmp1[] =
	{// 0x00aa, 0x0055, 0x00aa, 0x0055, 0x00aa, 0x0055, 0x00aa, 0x0055
	0xaaaa, 0x5555, 0xaaaa, 0x5555, 0xaaaa, 0x5555, 0xaaaa, 0x5555
			};

	static WORD _dotPatternBmp2[] =
	{ 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff };

	HBITMAP hbm;
	HBRUSH hbr;
	HANDLE hbrushOld;
	WORD *bitmap;

	int width, height, x, y;
	int border;

	HDC hdc = GetDC(0);

	if (fDocked)
	{
		border = 2;
		bitmap = _dotPatternBmp2;
	}
	else
	{
		border = 3;
		bitmap = _dotPatternBmp1;
	}

	x = rect->left;
	y = rect->top;
	width = rect->right - rect->left;
	height = rect->bottom - rect->top;

	hbm = CreateBitmap(8, 8, 1, 1, bitmap);
	hbr = CreatePatternBrush(hbm);

	SetBrushOrgEx(hdc, x, y, 0);
	hbrushOld = SelectObject(hdc, hbr);

	PatBlt(hdc, x + border, y, width - border, border, PATINVERT);
	PatBlt(hdc, x + width - border, y + border, border, height - border,
	PATINVERT);
	PatBlt(hdc, x, y + height - border, width - border, border, PATINVERT);
	PatBlt(hdc, x, y, border, height - border, PATINVERT);

	SelectObject(hdc, hbrushOld);
	DeleteObject(hbr);
	DeleteObject(hbm);
	ReleaseDC(0, hdc);
}

//
// Keyboard hook for the Drag-Dropping a ToolWindow.
// This hook just lets the user toggle the insert mode
// by monitoring the <control> key
//
LRESULT CALLBACK DockWnd::draghookproc(int code, WPARAM wParam, LPARAM lParam)
{
	ULONG state = (ULONG)lParam;

	if(code < 0)
		return (CallNextHookEx(draghook, code, wParam, lParam));

	if(wParam == VK_CONTROL)
	{
		if(state & 0x80000000)	fControl = FALSE;
		else					fControl = TRUE;

		SendMessage(g_hwndDockWnd, WM_MOUSEMOVE, 0, 0);
		return (-1);
	}

	if(wParam == VK_ESCAPE)
	{
		PostMessage(g_hwndDockWnd, WM_CANCELMODE, 0, 0);
		return (0);
	}

	return (CallNextHookEx(draghook, code, wParam, lParam));
}

//
//---------------------------------------------------------------
// Public function
// Begin dragging the dock wnd docking/floating rectangle
//---------------------------------------------------------------
//
long DockWnd::minMaxInfo(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	LPARAM lParam = args.wParamLParam.lparma;
	MINMAXINFO *pmmi;

	pmmi = (MINMAXINFO *)lParam;

	// Prevent window sizing
	if(fDocked == FALSE && (dwStyle & DWS_NORESIZE))
	{
		pmmi->ptMaxTrackSize.x = nFrameWidth;
		pmmi->ptMaxTrackSize.y = nFrameHeight;
		pmmi->ptMinTrackSize.x = nFrameWidth;
		pmmi->ptMinTrackSize.y = nFrameHeight;
		return (0);
	} else {
		return (1L);
	}
}

//
//---------------------------------------------------------------
// Public function
// Begin dragging the dock wnd docking/floating rectangle
//---------------------------------------------------------------
//
long DockWnd::settingChange(const App::WindowMessage &message)
{
	if(fDocked == FALSE)
		SetFloatingWinPos(SWP_NOACTIVATE|SWP_NOZORDER);
	return (0);
}

//
//---------------------------------------------------------------
//	Private function
//  Work out how big a floating window should be,
//  taking into account the window contents, and current
//  system settings
//---------------------------------------------------------------
//
void DockWnd::CalcFloatingRect()
{
	RECT rect;

	SetRect(&rect, 0, 0, cxFloating, cyFloating);
	AdjustWindowRectEx(&rect, POPUP_STYLES, FALSE, POPUP_EXSTYLES);

	nFrameWidth = rect.right - rect.left;
	nFrameHeight = rect.bottom - rect.top;
}

//
//---------------------------------------------------------------
//	Private function
//	Adjust a dock-window's floating size
//---------------------------------------------------------------
//
void DockWnd::SetFloatingWinPos(DWORD dwSWFlags)
{
	CalcFloatingRect();
	SetWindowPos(hwnd, HWND_TOP , xpos, ypos, nFrameWidth,
			nFrameHeight, dwSWFlags);
}

//---------------------------------------------------------------
// Private function
// Does what it says on the tin
//---------------------------------------------------------------
void DockWnd::DrawGripper(HDC hdc, int x, int y, int height)
{
	RECT rect;
	SetRect(&rect, x,y,x+3,y+height);

	DrawEdge(hdc, &rect,  BDR_RAISEDINNER, BF_RECT);
	OffsetRect(&rect, 3, 0);
	DrawEdge(hdc, &rect,  BDR_RAISEDINNER, BF_RECT);
}

//---------------------------------------------------------------
// Private function
// Does what it says on the tin
//---------------------------------------------------------------
void DockWnd::DrawCloseButton(HDC hdc, int x, int y, int height, DWORD dwSWFlags)
{
	RECT buttonRc;
	SetRect(&buttonRc, x, y, height, height);
	SetWindowPos(hwndButton, NULL, buttonRc.left,
			buttonRc.top, buttonRc.right, buttonRc.bottom,
			SWP_NOZORDER | dwSWFlags);

}

//----------------------------------------------------------------------------------
// Public method - add a dock window to dock list
// Does this on NC Create and before the initial NC ACTIVATE
//----------------------------------------------------------------------------------
int DockWnd::addDockWindow(HWND hWnd)
{
	if (nNumDockWnds < MAX_DOCKWNDS)
	{
		hDockList[nNumDockWnds++] = hWnd;
		//SetWindowText(hWnd, "Hi");
		return (TRUE);
	}
	else
	{
		return (FALSE);
	}
}
//----------------------------------------------------------------------------------
// Public method - remove a dock window to dock list
//----------------------------------------------------------------------------------
int DockWnd::removeDockWindow(HWND hWnd)
{
	int index;
	for (index = 0; index < nNumDockWnds; index++)
	{
		if (hDockList[index] == hWnd)
		{
			for (; index < nNumDockWnds - 1; index++)
			{
				hDockList[index] = hDockList[index + 1];
			}

			nNumDockWnds--;
			std::cout << "reomve dock window" << std::endl;
			break;
		}
	}

	return (0);
}

//----------------------------------------------------------------------------------
// Private member function
//----------------------------------------------------------------------------------
HWND DockWnd::getOwner(HWND hwnd)
{
	return (GetWindow(hwnd, GW_OWNER));
}

//----------------------------------------------------------------------------------
// Private member function
//----------------------------------------------------------------------------------
BOOL DockWnd::isOwnedBy(HWND hwndMain, HWND hwnd)
{
	return ((hwnd == hwndMain) || (getOwner(hwnd) == hwndMain));
}
//----------------------------------------------------------------------------------
//	This function only returns the popups which belong
//	to the specified "main" window
//
//	hwndMain - handle to top-level owner of the popups to retrieve
//	hwndList - where to store the list of popups
//	nItems   - [in] - size of hwndList, [out] - returned no. of windows
//	fIncMain - include the main window in the returned list
//----------------------------------------------------------------------------------
int DockWnd::getPopupList(HWND hwndMain, HWND hwndList[], int nSize,
BOOL fIncMain)
{
	int index, count = 0;

	if (hwndList == 0)
		return (0);

	for (index = 0; index < nNumDockWnds && index < nSize; index++)
	{
		if (isOwnedBy(hwndMain, hDockList[index]))
			hwndList[count++] = hDockList[index];
	}

	if (fIncMain && count < nSize)
	{
		hwndList[count++] = hwndMain;
	}

	return (count);
}

///////////////////////////////////////////////////////////////////////////////
//	The main window of app should call this in response to WM_NCACTIVATE
//  For activation/deactivation of docked/floating windows
//
//	hwndMain - handle to top-level owner window
//	hwnd     - handle to window which received message (can be same as hwndMain)
//
///////////////////////////////////////////////////////////////////////////////
LRESULT DockWnd::HANDLE_NCACTIVATE(HWND hwndMain,
		HWND hwnd, WPARAM wParam, LPARAM lParam)
{
	static HWND hwndList[MAX_DOCKWNDS + 1];
	int i, nNumWnds;
	HWND hParam = (HWND) lParam;

	BOOL fKeepActive = wParam;
	BOOL fSyncOthers = TRUE;

	nNumWnds = DockWnd::getPopupList(hwndMain, hwndList,
			MAX_DOCKWNDS + 1,
			TRUE);

	// UNDOCUMENTED FEATURE:
	// Attention that lParam will be NULL when the other window being
	// activated/deactivate belongs to another thread - not just a different
	// process. Therefore care should be taken when using this technique - all
	// toolbars / windows involved in this activation scheme must belong to the
	// same thread.
	// if the other window being activated/deactivated (i.e. NOT this one)
	// is one of our popups, then go (or stay) active, otherwise.
	for (i = 0; i < nNumWnds; i++)
	{
		if (hParam == hwndList[i])
		{
			fKeepActive = TRUE;
			fSyncOthers = FALSE;
			break;
		}
	}

	// If this message was sent by the synchronise-loop (below)
	// then exit normally
	if (hParam == (HWND) -1)
	{
		return (DefWindowProc(hwnd, WM_NCACTIVATE, fKeepActive, 0));
	}

	// This window is about to change (inactive/active).
	// Sync all other popups to the same state
	if (fSyncOthers == TRUE)
	{
		for (i = 0; i < nNumWnds; i++)
		{
			//DO NOT send this message to ourselves!!!!
			if (hwndList[i] != hwnd && hwndList[i] != hParam)
				SendMessage(hwndList[i], WM_NCACTIVATE, fKeepActive,
						(LONG) -1);
		}
	}

	return (DefWindowProc(hwnd, WM_NCACTIVATE, fKeepActive, lParam));
}

///////////////////////////////////////////////////////////////////////////////
//	The main window of app should call this in response to WM_ENABLE
//  For activation/deactivation of docked/floating windows
//
//	hwndMain - handle to top-level owner window
//	hwnd     - handle to window which received message (can be same as hwndMain)
//
///////////////////////////////////////////////////////////////////////////////
LRESULT DockWnd::HANDLE_ENABLE(HWND hwndMain, HWND hwnd,
		WPARAM wParam, LPARAM lParam)
{
	static HWND hwndList[MAX_DOCKWNDS + 1];
	int i, nNumWnds;

	//HWND hParam = (HWND) lParam;

	nNumWnds = DockWnd::getPopupList(hwndMain, hwndList,
			MAX_DOCKWNDS + 1,
			FALSE);

	for (i = 0; i < nNumWnds; i++)
	{
		if (hwndList[i] != hwnd)
		{
			EnableWindow(hwndList[i], wParam);
		}
	}

	//just do the default
	return (DefWindowProc(hwnd, WM_ENABLE, wParam, lParam));
}

//
///////////////////////////////////////////////////////////////////////////////
// Protected functions
///////////////////////////////////////////////////////////////////////////////
//
// Window Clients get sized using the DockWnd container position method
// so this deferSize method only returns the HDWP value
HDWP DockWnd::deferSize(HDWP hdwp)
{
	// Dock Wnd handles sizing of the wcContents and any control in dock wnd
	return (hdwp);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
BOOL DockWnd::checkDialogMessage(MSG* msg)
{
	if (wcContents != nullptr) {
		return (wcContents->checkDialogMessage(msg));
	}
	return (FALSE);
}
//
///////////////////////////////////////////////////////////////////////////////
// Protected functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT DockWnd::style(App::Component::Style &style)
{
	if (wcContents != nullptr) {
		wcContents->style(style);
		if (fDocked == FALSE) { // Make floating window invalidate and redraw
			InvalidateRect(hwnd, NULL, TRUE);
			UpdateWindow(hwnd);
			LPARAM lparam = MAKELPARAM(nFrameWidth, nFrameHeight);
			SendMessage(hwnd, WM_SIZE, 0, (LPARAM) lparam);
		}
	}
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Protected functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT DockWnd::paint(HDC hdc, PAINTSTRUCT ps)
{
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
void DockWnd::accept(App::Component::Visitor &v)
{
	if (wcContents != nullptr) {
        wcContents->accept(v);
	}
}


} /* namespace App */
