/*
 * ListView.cpp
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#include "ListView.h"
#include <commctrl.h>
#include "DialogTemplate.h"

namespace App
{
namespace Component
{

std::string ListView::origWndProc = "WndProc";
std::string ListView::wndInstance = "WndInstance";

//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
//
ListView::ListView() :
		dialogTest(0), iItem(0),
		hWndPostMessage(0)
{
}
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
//
ListView::~ListView()
{
	std::cout << "ListView deconstructor." << std::endl;
	binds_iterator iter;
	for (iter = binds.begin(); iter != binds.end(); ++iter)
	{
		delete *iter;
	}
	std::vector<ListItem*>::iterator iter2;
	for (iter2 = listItems.begin(); iter2 != listItems.end(); ++iter2)
	{
		delete *iter2;
	}

	// Remove the subclass from the list view control.
	WNDPROC wndProc = (WNDPROC) GetProp(hWndControl, origWndProc.c_str());
	SetWindowLongPtr(hWndControl, GWL_WNDPROC, (LONG) wndProc);
	RemoveProp(hWndControl, origWndProc.c_str());
	RemoveProp(hWndControl, wndInstance.c_str());

	if (dialogTest != NULL)
	{
		SendMessage(dialogTest, WM_CLOSE, 0, 0);
		DestroyWindow(dialogTest);
	}
}
//
///////////////////////////////////////////////////////////////////////////////
// Public create
///////////////////////////////////////////////////////////////////////////////
//
BOOL ListView::createControl(HWND hWndParent, HINSTANCE hInst, int controlId)
{
	hWndControl = CreateWindowEx(WS_EX_CLIENTEDGE, WC_LISTVIEW, "",
	WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | LVS_REPORT | LVS_EDITLABELS, 0, 0, 0, 0, hWndParent, (HMENU) controlId,
			hInst, NULL);

	if (hWndControl == NULL)
	{
		MessageBox(hWndParent, "Could not create list view control.", "Error",
		MB_OK | MB_ICONERROR);
		return (FALSE);
	}
	WNDPROC wndProc = (WNDPROC) SetWindowLongPtr(hWndControl, GWL_WNDPROC, (LONG) ListViewSubclassProc);
	SetProp(hWndControl, origWndProc.c_str(), (HANDLE) wndProc);
	SetProp(hWndControl, wndInstance.c_str(), this);

	this->controlId = controlId;
	this->hWndPostMessage = hWndParent;

	if (!initListViewImageLists(hInst))
	{
		return (FALSE);
	}
	return (TRUE);
}
//
///////////////////////////////////////////////////////////////////////////////
// Private function
///////////////////////////////////////////////////////////////////////////////
//
void ListView::editDialogOnListItem(int itemIndex)
{
	std::cout << "edit Dialog:"<< itemIndex << std::endl;

	RECT itemrect;
	RECT dialogRect;
	ListView_GetItemRect(hWndControl, itemIndex, &itemrect, LVIR_BOUNDS);
	dialogRect = getDialogRectFromItemRect(itemrect);
	DialogTemplate dialogTemplate(_T("Assert Failed!"), WS_POPUP | WS_VISIBLE, dialogRect.left, dialogRect.top,
			(dialogRect.right - dialogRect.left), (dialogRect.bottom - dialogRect.top));
	//, _T("Tahoma"));
	binds_iterator iter;
	for (iter = binds.begin(); iter != binds.end(); ++iter)
	{
		Control::BindBase* bindPtr = *iter;
		dialogTemplate.AddEditBox("Sample",
		WS_VISIBLE | WS_CLIPSIBLINGS | WS_BORDER | ES_WANTRETURN | WS_TABSTOP, 0, 0, 0, 100,
				(dialogRect.bottom - dialogRect.top) + 1, bindPtr->getControlId());
	}
	// Create new dialogTest for iItem selected
	dialogTest = CreateDialogIndirectParam(GetModuleHandle(NULL), dialogTemplate, hWndControl,
			(DLGPROC) MessageDlgProc, (LONG_PTR) this);
	DWORD dwStyle = GetWindowLong(dialogTest, GWL_STYLE);
	SetWindowLong(dialogTest, GWL_STYLE, (dwStyle & ~(WS_POPUP)) | (WS_CHILD | WS_VISIBLE));
	SetParent(dialogTest, hWndControl);
	SetWindowPos(dialogTest, NULL, itemrect.left, itemrect.top - 3, (itemrect.right - itemrect.left) - 1,
			(itemrect.bottom - itemrect.top) + 6,
			SWP_NOZORDER);
	SetFocus(dialogTest);
	WNDPROC wndProc;
	for (iter = binds.begin(); iter != binds.end(); ++iter)
	{
		Control::BindBase* bindPtr = *iter;
		wndProc = (WNDPROC) SetWindowLongPtr(GetDlgItem(dialogTest, bindPtr->getControlId()),
		GWL_WNDPROC, (LONG) EditSubclassProc);
		SetProp(GetDlgItem(dialogTest, bindPtr->getControlId()), origWndProc.c_str(), (HANDLE) wndProc);
		SetProp(GetDlgItem(dialogTest, bindPtr->getControlId()), wndInstance.c_str(), this);
		SendMessage(GetDlgItem(dialogTest, bindPtr->getControlId()), WM_SETFONT, (WPARAM) getStyle()->hFont, MAKELPARAM(FALSE, 0));
	}
	// Populate dialog with list item contents in order of the bind ( the same order in dialog )
	LVITEM lvItem;
	lvItem.iItem = itemIndex;
	lvItem.iSubItem = 0;
	lvItem.mask = LVIF_PARAM;
	ListView_GetItem(hWndControl, &lvItem);
	App::Component::ListItem* pItem = (App::Component::ListItem*) ((lvItem.lParam));
	int index = 0;
	for (iter = binds.begin(); iter != binds.end(); ++iter)
	{
		Control::BindBase* bindPtr = *iter;
		std::string value = pItem->subitems.at(index);
		HWND editHWnd = GetDlgItem(dialogTest, bindPtr->getControlId());
		SendMessage(editHWnd, WM_SETTEXT, 0, (LPARAM) value.c_str());
		index++;
	}
	iItem = itemIndex;
}

////
/////////////////////////////////////////////////////////////////////////////////
//// ListViewSubclassProc for Windows Class of List View subclass
/////////////////////////////////////////////////////////////////////////////////
////
LRESULT CALLBACK ListView::ListViewSubclassProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	WNDPROC wndProc = (WNDPROC) GetProp(hWnd, origWndProc.c_str());
	ListView* view = (ListView*) GetProp(hWnd, wndInstance.c_str());

	switch (Msg)
	{
	case WM_PAINT:
		CallWindowProc(wndProc, hWnd, Msg, wParam, lParam);
		view->EditDialogPosition();
		break;
	case WM_HSCROLL:
	case WM_VSCROLL:
		CallWindowProc(wndProc, hWnd, Msg, wParam, lParam);
		if (view->dialogTest != NULL)
		{
			ListView_EnsureVisible(view->hWndControl, view->iItem, TRUE);
			InvalidateRect(view->dialogTest, NULL, TRUE);
			UpdateWindow(view->dialogTest);
		}
		break;
	case LVM_SORTITEMS:
		if (view->dialogTest != NULL)
		{
			view->doRedrawItem();
			SendMessage(view->dialogTest, WM_CLOSE, 0, 0);
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 13:
			// Tell list view to edit selected item
			if (ListView_GetSelectedCount(view->hWndControl) > 0) {
				int index = -1;
				index = ListView_GetNextItem(view->hWndControl, index, LVNI_ALL | LVNI_SELECTED | LVIS_FOCUSED);
				if (index >= 0) {
					if (view->dialogTest != NULL) // Close an earlier edit dialog now
					{
						view->doRedrawItem();
						SendMessage(view->dialogTest, WM_CLOSE, 0, 0);
					};
					view->editDialogOnListItem(index); // Make edit dialog and focus it
				}
			}
			break;
		}
		break;
	case WM_LBUTTONDOWN:
	{
		CallWindowProc(wndProc, hWnd, Msg, wParam, lParam);
		long x, y;
		x = (long) LOWORD(lParam);
		y = (long) HIWORD(lParam);
		LVHITTESTINFO itemclicked;
		itemclicked.pt.x = x;
		itemclicked.pt.y = y;
		int lResult = ListView_SubItemHitTest(hWnd, &itemclicked);
		if (lResult != -1)
		{
			if (view->dialogTest != NULL) // Close an earlier edit dialog now
			{
				view->doRedrawItem();
				SendMessage(view->dialogTest, WM_CLOSE, 0, 0);
			};
			view->editDialogOnListItem(itemclicked.iItem); // Make edit dialog and focus it

			// Only one item can be selected
			int index = 0;
			while (index >= 0)
			{
				index = ListView_GetNextItem(view->hWndControl, index, LVNI_ALL | LVNI_SELECTED | LVIS_FOCUSED);
				ListView_SetItemState(view->hWndControl, index, 0, LVIS_SELECTED | LVIS_FOCUSED);
			}

			ListView_SetItemState(view->hWndControl, itemclicked.iItem, LVIS_SELECTED | LVIS_FOCUSED,
					LVIS_SELECTED | LVIS_FOCUSED);
			std::cout << "End LBUTTON DOWN" << std::endl;
		} else {
			if (view->dialogTest != NULL) // Make sure we focused on edit dialog
			{
				SetFocus(view->dialogTest);
			};
		}
		return (TRUE);
		break;
	}

	}
	//Other messages are sent to the original Proc
	return (CallWindowProc(wndProc, hWnd, Msg, wParam, lParam));

}
//
//-------------------------------------------------------
// Private function
//-------------------------------------------------------
//
int ListView::subItemFromDialogChildWHnd(HWND hWnd)
{
	int subItem = 0;
	binds_iterator iter;
	for (iter = binds.begin(); iter != binds.end(); ++iter)
	{
		if ((*iter)->getControlId() == GetDlgCtrlID(hWnd))
			break;
		++subItem;
	}
	return (subItem);
}
//
//-------------------------------------------------------
// Private function
//-------------------------------------------------------
//
void ListView::notifyEditText(HWND hWnd)
{
	LVITEM lvItem;
	lvItem.iItem = iItem;
	lvItem.iSubItem = subItemFromDialogChildWHnd(hWnd);
	lvItem.mask = LVIF_PARAM;
	ListView_GetItem(hWndControl, &lvItem);
	LV_DISPINFO lvDispinfo;
	ZeroMemory(&lvDispinfo,sizeof(LV_DISPINFO));
	lvDispinfo.hdr.hwndFrom = hWnd;
	lvDispinfo.hdr.idFrom = GetDlgCtrlID(hWnd);
	lvDispinfo.hdr.code = LVN_ENDLABELEDIT;
	lvDispinfo.item.mask = LVIF_TEXT;
	lvDispinfo.item.iItem = iItem;
	lvDispinfo.item.iSubItem = lvItem.iSubItem;
	lvDispinfo.item.lParam = lvItem.lParam;
	lvDispinfo.item.pszText = NULL;
	char szEditText[30];
	GetWindowText(hWnd, szEditText, 30);
	lvDispinfo.item.pszText = szEditText;
	lvDispinfo.item.cchTextMax = lstrlen(szEditText);
	SendMessage(hWndPostMessage, WM_NOTIFY, (WPARAM) controlId, (LPARAM) &lvDispinfo);
}
//
//-------------------------------------------------------
// Private function
//-------------------------------------------------------
//
void ListView::doRedrawItem()
{
	LVITEM lvItem;
	lvItem.iItem = iItem;
	lvItem.iSubItem = 0;//subItemFromDialogChildWHnd(hWnd);
	lvItem.mask = LVIF_PARAM;
	ListView_GetItem(hWndControl, &lvItem);
	lvItem.mask = LVIF_TEXT | LVIF_PARAM | LVIF_STATE;
	lvItem.state = LVIS_SELECTED | LVIS_FOCUSED;//0;
	lvItem.stateMask = LVIS_SELECTED | LVIS_FOCUSED; //0;
	lvItem.pszText = LPSTR_TEXTCALLBACK;   // app. maintains text
	ListView_SetItem(hWndControl, &lvItem);
	SetFocus(hWndControl);
}
//
//-----------------------------------------------------------------
// EditSubclassProc for Windows Class of List View subclass
//-----------------------------------------------------------------
//
LRESULT CALLBACK ListView::EditSubclassProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
{
	WNDPROC wndProc = (WNDPROC) GetProp(hWnd, origWndProc.c_str());
	ListView* view = (ListView*) GetProp(hWnd, wndInstance.c_str());
	if (Msg == WM_GETDLGCODE)
		return (DLGC_WANTALLKEYS);
	switch (Msg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 9:
			// Do dialog next ctrl
			view->notifyEditText(hWnd);
			SetFocus(GetNextDlgTabItem(view->dialogTest, hWnd, FALSE));
			return (TRUE);
			break;
		case 13:
			// Tell dialog to close - send changes with onFieldEdit
			view->notifyEditText(hWnd);
			view->doRedrawItem();
			SendMessage(view->dialogTest, WM_CLOSE, 0, 0);
			return (TRUE);
			break;
		}
		break;
//	case WM_KILLFOCUS:
//	{
//		//view->notifyEditText(hWnd);
//		break;
//	}

	}
	return (CallWindowProc(wndProc, hWnd, Msg, wParam, lParam));

}
//
//-------------------------------------------------------
// Private function
//-------------------------------------------------------
//
void ListView::printRect(const RECT& rect)
{
	std::cout << "Rect:" << rect.left << " " << rect.top << " " << rect.right << " " << rect.bottom << std::endl;
}
//
//-------------------------------------------------------
// Private function
//-------------------------------------------------------
//
void ListView::EditDialogPosition()
{
	if (dialogTest != NULL)
	{
		RECT itemrect;
		ListView_GetItemRect(hWndControl, iItem, &itemrect, LVIR_BOUNDS);
		if (binds.size() == 1)
		{
			Control::BindBase* bindPtr = binds.at(0);
			// Edit control is the same size as iItem rect ( no subitem columns to use )
			SetWindowPos(GetDlgItem(dialogTest, bindPtr->getControlId()), NULL, 0, 0, (itemrect.right - itemrect.left),
					(itemrect.bottom - itemrect.top),
					SWP_NOZORDER);

		}
		else if (binds.size() > 1)
		{
			RECT subitemrect;
			binds_iterator iter;

			int subItem = 1;
			ListView_GetSubItemRect(hWndControl, iItem, subItem, LVIR_BOUNDS, &subitemrect);
			Control::BindBase* bindPtr = binds.at(0);
			SetWindowPos(GetDlgItem(dialogTest, bindPtr->getControlId()), NULL, 0, 0,
					(subitemrect.left - itemrect.left) - 1, (itemrect.bottom - itemrect.top),
					SWP_NOZORDER);

			// Start iteration at 2nd subitem (First subitem is the first column)
			for (iter = binds.begin() + 1; iter != binds.end(); ++iter)
			{
				Control::BindBase* bindPtr = *iter;
				ListView_GetSubItemRect(hWndControl, iItem, subItem, LVIR_BOUNDS, &subitemrect);
				SetWindowPos(GetDlgItem(dialogTest, bindPtr->getControlId()), NULL, (subitemrect.left - itemrect.left),
						0, (subitemrect.right - subitemrect.left) - 1, (subitemrect.bottom - subitemrect.top),
						SWP_NOZORDER);
				++subItem;
			}
		}

		SetWindowPos(dialogTest, NULL, itemrect.left, itemrect.top - 3, (itemrect.right - itemrect.left) - 1,
				(itemrect.bottom - itemrect.top) + 6, SWP_NOZORDER);
		InvalidateRect(dialogTest, NULL, TRUE);
		UpdateWindow(dialogTest);

		// The top of dialog is not in view
		if (itemrect.top <= (itemrect.bottom - itemrect.top))
		{
			ShowWindow(dialogTest, SW_HIDE); // Hide or Close/Destroy?
		}
		else
		{
			ShowWindow(dialogTest, SW_SHOW);
		}
	}
}

//
///////////////////////////////////////////////////////////////////////////////
// Edit Dialog WinProc
///////////////////////////////////////////////////////////////////////////////
//
BOOL CALLBACK ListView::MessageDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	LRESULT returnValue = FALSE;        // return value
	ListView* view;
	WNDPROC wndProc;
	binds_iterator iter;

	if (Message == WM_GETDLGCODE)
		return (DLGC_WANTALLKEYS);
	if (Message == WM_INITDIALOG)  // Non-Client Create
	{
		view = (ListView*) lParam;
		::SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR) view);
		return (TRUE);
	}
	view = (ListView *) GetWindowLongPtr(hwnd, GWL_USERDATA);
	if (!view)
		return (FALSE);

	switch (Message)
	{
	case WM_CLOSE:
		for (iter = view->binds.begin(); iter != view->binds.end(); ++iter)
		{
			Control::BindBase* bindPtr = *iter;
			wndProc = (WNDPROC) GetProp(GetDlgItem(view->dialogTest, bindPtr->getControlId()), origWndProc.c_str());
			SetWindowLongPtr(GetDlgItem(view->dialogTest, bindPtr->getControlId()), GWL_WNDPROC, (LONG) wndProc);
			RemoveProp(GetDlgItem(view->dialogTest, bindPtr->getControlId()), origWndProc.c_str());
			RemoveProp(GetDlgItem(view->dialogTest, bindPtr->getControlId()), wndInstance.c_str());
		}
		DestroyWindow(view->dialogTest);
		view->dialogTest = 0;
		return (FALSE);
		break;
	case WM_DESTROY:
		std::cout << "LOOK!!!!!!_____WM_DESTROY on edit dialog" << std::endl;
		return (FALSE);
		break;
	default:
		break;
	}
	return (returnValue);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
BOOL ListView::checkDialogMessage(MSG* msg)
{
	BOOL done = IsDialogMessage(dialogTest, msg);
	return (done);
}

//
///////////////////////////////////////////////////////////////////////////////
// Public create
///////////////////////////////////////////////////////////////////////////////
//
BOOL ListView::initListView(HINSTANCE hInst)
{
	if (!initListViewColumns(hInst))	// || !initListViewItems( hInst))
	{
		return (FALSE);
	}
	return (TRUE);
}
//
///////////////////////////////////////////////////////////////////////////////
// Private create
///////////////////////////////////////////////////////////////////////////////
//
// InitListViewImageList - creates image lists for a list view.
// Returns TRUE if successful or FALSE otherwise.
// hwndLV - handle of the list view control
BOOL ListView::initListViewImageLists(HINSTANCE hInst)
{
	HICON hiconItem;        // icon for list view items
	HIMAGELIST himlLarge;   // image list for icon view
	HIMAGELIST himlSmall;   // image list for other views

	// Create the full-sized and small icon image lists.
	himlLarge = ImageList_Create(GetSystemMetrics(SM_CXICON), GetSystemMetrics(SM_CYICON), TRUE, 1, 1);
	himlSmall = ImageList_Create(GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), TRUE, 1, 1);

	// Add an icon to each image list.
	//hiconItem = LoadIcon(hInst, MAKEINTRESOURCE(IDI_RAINDROP));
//	ImageList_AddIcon(himlLarge, hiconItem);
//	ImageList_AddIcon(himlSmall, hiconItem);
//	DeleteObject(hiconItem);

	//LVM_SETIMAGELIST could be better here
	// Assign the image lists to the list view control.
	SendMessage(hWndControl, LVM_SETIMAGELIST, (WPARAM) (LVSIL_NORMAL), (LPARAM) (HIMAGELIST) (himlLarge));
	SendMessage(hWndControl, LVM_SETIMAGELIST, (WPARAM) (LVSIL_SMALL), (LPARAM) (HIMAGELIST) (himlSmall));
//    ListView_SetImageList(hwndLV, himlLarge, LVSIL_NORMAL);

	return (TRUE);
}
//
///////////////////////////////////////////////////////////////////////////////
// Private create
///////////////////////////////////////////////////////////////////////////////
//
// InitListViewColumns - adds columns to a list view control.
// Returns TRUE if successful or FALSE otherwise.
// hwndLV - handle of the list view control
BOOL ListView::initListViewColumns(HINSTANCE hInst)
{
	LV_COLUMN lvc;
	int iCol;
	UINT cxChar;
	HDC hdc = GetDC( NULL);

	Style *style = getStyle();
	style->hFont = (HFONT) SelectObject(hdc, style->hFont);
	cxChar = GetTextMetricsCorrectly(hdc, NULL);
	style->hFont = (HFONT) SelectObject(hdc, style->hFont);
	ReleaseDC( NULL, hdc);

	std::cout << "GetTextMetricCorrectly = " << cxChar << " : one char lenght in pixels" << std::endl;

	// Initialize the LV_COLUMN structure.
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	lvc.fmt = LVCFMT_LEFT;

	// Add the columns.
	columns_iterator iter;
	iCol = 0;
	for (iter = columnNames.begin(); iter != columnNames.end(); ++iter)
	{
		lvc.iSubItem = iCol;
		char *str = (char*) malloc((*iter).length() + 1);
		strncpy(str, (*iter).c_str(), (*iter).length() + 1);
		lvc.pszText = str;
		lvc.cx = cxChar * (*iter).length();
		std::cout << "Column name=" << (*iter) << " cx=" << lvc.cx << std::endl;
		ListView_InsertColumn(hWndControl, iCol, &lvc);	// == -1);
		iCol++;
		free(str);
	}
	return (TRUE);
}
//
//---------------------------------------------------
// Private function
//---------------------------------------------------
//
RECT ListView::getDialogRectFromItemRect(const RECT& itemrect)
{
	RECT dialogRect;
	LONG baseunits = GetDialogBaseUnits();
	int baseunitY = HIWORD(baseunits);
	int baseunitX = LOWORD(baseunits);
	dialogRect.left = (itemrect.left * 4) / baseunitX;
	dialogRect.top = (itemrect.top * 8) / baseunitY;
	dialogRect.right = (itemrect.right * 4) / baseunitX;
	dialogRect.bottom = (itemrect.bottom * 8) / baseunitY;
	return (dialogRect);
}
/****************************************************************
 *  int GetTextMetricsCorrectly( HDC hDC, LPTEXTMETRIC lpTextMetrics )
 *
 *  Description:
 *
 *    This function gets the textmetrics of the font currently
 *    selected into the hDC.  It returns the average char width as
 *    the return value.
 *
 *    This function computes the average character width correctly
 *    by using GetTextExtent() on the string "abc...xzyABC...XYZ"

 *    which works out much better for proportional fonts.
 *
 *    Note that this function returns the same TEXTMETRIC
 *    values that GetTextMetrics() does, it simply has a different
 *    return value.
 *
 *  Comments:
 *
 ****************************************************************/
int ListView::GetTextMetricsCorrectly(HDC hDC, LPTEXTMETRIC lpTM)
{
	int nAveWidth;
	TCHAR rgchAlphabet[52];
	int i;
	SIZE size;

	// Get the TM of the current font.  Note that GetTextMetrics
	// gets the incorrect AveCharWidth value for proportional fonts.
	// This is the whole point in this exercise.
	//
	if (lpTM)
		GetTextMetrics(hDC, lpTM);

	// If it's not a variable pitch font GetTextMetrics was correct
	// so just return.
	//
	if (lpTM && !(lpTM->tmPitchAndFamily & FIXED_PITCH))
		return (lpTM->tmAveCharWidth);
	else
	{
		for (i = 0; i <= 25; i++)
			rgchAlphabet[i] = (TCHAR) (i + (int) 'a');

		for (i = 0; i <= 25; i++)

			rgchAlphabet[i + 25] = (TCHAR) (i + (int) 'A');

		GetTextExtentPoint(hDC, (LPTSTR) rgchAlphabet, 52, &size);
		nAveWidth = size.cx / 26;
		nAveWidth = (nAveWidth + 1) / 2;
	}

	// Return the calculated average char width
	//
	return (nAveWidth);

} /* GetTextMetricsCorrectly()  */
//
///////////////////////////////////////////////////////////////////////////////
// Private create
///////////////////////////////////////////////////////////////////////////////
//
BOOL ListView::addListViewItem(std::vector<std::string> subItems, int refBindIndex)
{
	LV_ITEM lvi;
	ListItem* listItem = new ListItem;
	listItem->subitems = subItems; // copy it
	listItem->bindRefIndex = refBindIndex;
	listItems.push_back(listItem);

	// Initialize LV_ITEM members that are common to all items.
//	lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM | LVIF_STATE;
	lvi.mask = LVIF_TEXT | LVIF_PARAM | LVIF_STATE;
	lvi.state = 0; //LVIS_SELECTED | LVIS_FOCUSED;//0;
	lvi.stateMask = LVIS_SELECTED | LVIS_FOCUSED; //0;
	lvi.pszText = LPSTR_TEXTCALLBACK;   // app. maintains text
//	lvi.iImage = 0;                     // image list index

	// Initialize item-specific LV_ITEM members.
	lvi.iItem = listItems.size() - 1;
	lvi.iSubItem = 0;

	std::cout << "pItem=" << (listItems.back()) << " size: " << listItems.back()->subitems.size() << std::endl;
	lvi.lParam = (LPARAM) (listItems.back());    // item data

	// Add the item.
	ListView_InsertItem(hWndControl, &lvi);

	ListView_Arrange(hWndControl, LVA_SNAPTOGRID);
	return (TRUE);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT ListView::paint(HDC hdc, PAINTSTRUCT ps)
{

	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT ListView::style(Style &style)
{
//	SendMessage(GetDlgItem(dialogTest, IDDC_PERSON_FIRSTNAME), WM_SETFONT, (WPARAM) style.hFont, MAKELPARAM(FALSE, 0));
//	SendMessage(GetDlgItem(dialogTest, IDDC_PERSON_SURNAME), WM_SETFONT, (WPARAM) style.hFont, MAKELPARAM(FALSE, 0));
	SendMessage(hWndControl, WM_SETFONT, (WPARAM) style.hFont, MAKELPARAM(FALSE, 0));
	return (WindowClient::style(style));
}

//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
void ListView::acceptChanges()
{
	// BindToSourceVisitor accepted here to do the bind update to source
	BindToSourceVisitor bindToSourceVisitor;
	accept(bindToSourceVisitor);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
void ListView::accept(Visitor &v)
{
	v.visit(this);
}

} /* namespace Component */
} /* namespace App */
