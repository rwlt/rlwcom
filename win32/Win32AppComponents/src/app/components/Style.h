/*
 * Style.h
 *
 *  Created on: Jun 10, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_STYLE_H_
#define APP_COMPONENTS_STYLE_H_
#include <windows.h>
namespace App
{
namespace Component
{

class Style
{
public:
	Style();
	virtual ~Style();

	LRESULT selectFont(HWND owner);
	LRESULT selectColour(HWND owner);

	HFONT hFont;
	HBRUSH hbrBackground;
	COLORREF rgbBackground;
	COLORREF rgbText;
	HBRUSH hbrWhite, hbrGray;
	BOOL bOpaque;
	COLORREF rgbCustom[16];

private:
	BOOL findDefaultFont();

};

} /* namespace Main */
} /* namespace App */

#endif /* APP_COMPONENTS_STYLE_H_ */
