/*
 * Polygon.cpp
 *
 *  Created on: Jun 10, 2015
 *      Author: Rodney Woollett
 */

#include "Polygon.h"

namespace App
{
namespace Component
{

//
///////////////////////////////////////////////////////////////////////////////
// Protected functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT Polygon::style(Style &style)
{
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT Polygon::paint(HDC hdc, PAINTSTRUCT ps)
{
	RECT rcPort;
	SetMapMode(hdc, MM_ANISOTROPIC);
	SetWindowExtEx(hdc, 100, 100, NULL);
	SetViewportExtEx(hdc, 100, 100, NULL);
	SetRect(&rcPort, 0, 0, 100, 100);

	SetViewportOrgEx(hdc, 0, 0, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, ppt, cpt);

	SetViewportOrgEx(hdc, 100, 00, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, ppt, cpt);

	SetViewportOrgEx(hdc, 0, 100, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, ppt, cpt);

	SetViewportOrgEx(hdc, 100, 100, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, aptHexagon, 7);

	return (0);

}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT Polygon::size()
{
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
HDWP Polygon::deferSize(HDWP hdwp)
{
	return (hdwp);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
BOOL Polygon::checkDialogMessage(MSG* msg){
	return (FALSE);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
void Polygon::accept(Visitor &v)
{
	/* No visiting allowed here*/
}

} /* namespace Main */
} /* namespace App */
