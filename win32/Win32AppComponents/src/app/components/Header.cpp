/*
 * Header.cpp
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#include "Header.h"
#include <commctrl.h>

namespace App
{
namespace Component
{
//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
//
Header::Header(): controlText("")
{

}
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
//
Header::~Header()
{
	binds_iterator iter;
	for (iter = binds.begin(); iter != binds.end(); ++iter)
	{
		delete *iter;
	}
}
//
///////////////////////////////////////////////////////////////////////////////
// Public create
///////////////////////////////////////////////////////////////////////////////
//
BOOL Header::createControl(HWND hWndParent, HINSTANCE hInst, int controlId)
{
	hWndControl = CreateWindowEx(0, WC_HEADER, (LPCTSTR) NULL,
			WS_CHILD | WS_VISIBLE | WS_BORDER | WS_CLIPSIBLINGS
					| WS_CLIPCHILDREN | HDS_BUTTONS | HDS_HORZ, 0, 0, 0, 0,
					hWndParent, (HMENU) controlId, hInst, (LPVOID) NULL);
	if (hWndControl == NULL)
	{
		MessageBox(hWndControl, "Could not create header.", "Error",
		MB_OK | MB_ICONERROR);
		return (FALSE);
	}
	this->controlId = controlId;

	return (TRUE);

}

//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT Header::paint(HDC hdc, PAINTSTRUCT ps)
{
	return (0);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
void Header::accept(Visitor &v)
{
    v.visit(this);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
void Header::updateText(int id, const std::string &value)
{
//	if (controlId == id)
//		controlText = value;
	SendMessage(hWndControl, WM_SETTEXT, 0, (LPARAM) value.c_str());
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
std::string Header::readText(int id){
	controlText = "";
	int lengthText = GetWindowTextLength(hWndControl);
	if (lengthText < 0)
		return (controlText);

	char *str = (char*) malloc(lengthText + 1);
	SendMessage(hWndControl, WM_GETTEXT, (lengthText + 1), (LPARAM) str);
	controlText.append(str);
	free(str);
	return (controlText);
}

} /* namespace Component */
} /* namespace App */
