/*
 * ComponentException.h
 *
 *  Created on: Jul 4, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_COMPONENTEXCEPTION_H_
#define APP_COMPONENTS_COMPONENTEXCEPTION_H_

#include <exception>

namespace App
{
namespace Component
{


class ComponentException: public std::exception
{
public:
	ComponentException(std::string what):reason(what)
	{
	}
	virtual ~ComponentException()
	{
	}

    const char* what() const _GLIBCXX_USE_NOEXCEPT {
    	return (reason.c_str());
    }

private:
	std::string reason;

};

} /* namespace Component */
} /* namespace App */
#endif /* APP_COMPONENTS_COMPONENTEXCEPTION_H_ */
