/*
 * Style.cpp
 *
 *  Created on: Jun 10, 2015
 *      Author: Rodney Woollett
 */

#include "Style.h"
#include <cstring>
#include <commdlg.h>

namespace App
{
namespace Component
{

Style::Style() :
		hFont(0), hbrBackground(CreateSolidBrush(RGB(0, 0, 0))),
		rgbBackground(RGB(0, 0, 0)), rgbText(RGB(255, 255, 255))
{
	hbrWhite = static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
	hbrGray = static_cast<HBRUSH>(GetStockObject(GRAY_BRUSH));
	std::memset(rgbCustom, -1, sizeof(rgbCustom));
	findDefaultFont();
}

Style::~Style()
{
	if (hFont != 0)
		DeleteObject(hFont);
	if (hbrBackground != 0)
		DeleteObject(hbrBackground);
	DeleteObject(hbrWhite);
	DeleteObject(hbrGray);

}
//
///////////////////////////////////////////////////////////////////////////////
// Public function
///////////////////////////////////////////////////////////////////////////////
//
LRESULT Style::selectFont(HWND owner)
{
	CHOOSEFONT cf =
	{ sizeof(CHOOSEFONT) };
	LOGFONT lf;

	GetObject(hFont, sizeof(LOGFONT), &lf);

	cf.Flags = CF_EFFECTS | CF_INITTOLOGFONTSTRUCT | CF_SCREENFONTS;
	cf.hwndOwner = owner;
	cf.lpLogFont = &lf;
	cf.rgbColors = rgbText;

	if (ChooseFont(&cf))
	{
		HFONT hf = CreateFontIndirect(&lf);
		if (hf)
		{
			if (hFont != 0)
				DeleteObject(hFont);
			hFont = hf;
		}
		else
		{
			MessageBox(owner, "Font creation failed!", "Error",
			MB_OK | MB_ICONEXCLAMATION);
		}

		rgbText = cf.rgbColors;
	}

	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
LRESULT Style::selectColour(HWND owner)
{
	CHOOSECOLOR cc =
	{ sizeof(CHOOSECOLOR) };

	cc.Flags = CC_RGBINIT | CC_FULLOPEN | CC_ANYCOLOR;
	cc.hwndOwner = owner;
	cc.rgbResult = rgbBackground;
	cc.lpCustColors = rgbCustom;

	if (ChooseColor(&cc))
	{
		DeleteObject(hbrBackground);
		rgbBackground = cc.rgbResult;
		hbrBackground = CreateSolidBrush(rgbBackground);
	}
	return (0);

}
//
///////////////////////////////////////////////////////////////////////////////
// Private function
///////////////////////////////////////////////////////////////////////////////
//
BOOL Style::findDefaultFont()
{
	HDC hdc;
	long lfHeight;
	HFONT hDefaultFont;
	// Set the font used
	hdc = GetDC(NULL);
	lfHeight = -MulDiv(11, GetDeviceCaps(hdc, LOGPIXELSY), 72);
	ReleaseDC(NULL, hdc);
	hDefaultFont = CreateFont(lfHeight, 0, 0, 0, FW_SEMIBOLD, 0, 0, 0,
	DEFAULT_CHARSET,
	OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
	DEFAULT_PITCH, "Arial");
	if (hDefaultFont)
	{
		if (hFont != 0)
			DeleteObject(hFont);
		hFont = hDefaultFont;
	}
	else
	{
		hFont = static_cast<HFONT>(GetStockObject(DEFAULT_GUI_FONT));
	}
	return (TRUE);

}

} /* namespace Main */
} /* namespace App */
