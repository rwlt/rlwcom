/*
 * Edit.cpp
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#include "Edit.h"

namespace App
{
namespace Component
{
//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
//
Edit::Edit():controlText("")
{

}
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
//
Edit::~Edit()
{
	binds_iterator iter;
	for (iter = binds.begin(); iter != binds.end(); ++iter)
	{
		delete *iter;
	}
}
//
///////////////////////////////////////////////////////////////////////////////
// Public create
///////////////////////////////////////////////////////////////////////////////
//
BOOL Edit::createControl(HWND hWndParent, HINSTANCE hInst, int controlId)
{
	hWndControl = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "",
			WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE | WS_VSCROLL
					| WS_HSCROLL | ES_MULTILINE |
					ES_AUTOVSCROLL | ES_AUTOHSCROLL, 0, 0, 100, 100,
			hWndParent, (HMENU) controlId, hInst, NULL);
	if (hWndControl == NULL)
	{
		MessageBox(hWndParent, "Could not create edit control.", "Error",
		MB_OK | MB_ICONERROR);
		return (FALSE);
	}
	this->controlId = controlId;
	return (TRUE);

}

//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
void Edit::updateText(const std::string &value)
{
	SendMessage(hWndControl, WM_SETTEXT, 0, (LPARAM) value.c_str());
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
std::string &Edit::readText(){
	controlText = "";
//	int lineCount = SendMessage(hWndControl, EM_GETLINECOUNT, 0, 0);
	int lengthText = GetWindowTextLength(hWndControl);
	if (lengthText < 0)
		return (controlText);

	char *str = (char*) malloc(lengthText + 1);
	SendMessage(hWndControl, WM_GETTEXT, (lengthText + 1), (LPARAM) str);
	controlText.append(str);
	free(str);
	return (controlText);
}

//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
void Edit::accept(Visitor &v)
{
    v.visit(this);
}

} /* namespace Component */
} /* namespace App */
