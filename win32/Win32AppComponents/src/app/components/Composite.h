/*
 * Composite.h
 *
 *  Created on: Jun 10, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_COMPOSITE_H_
#define APP_COMPONENTS_COMPOSITE_H_

#include <vector>
#include "Component.h"


namespace App
{
namespace Component
{

class Composite: public Component
{
public:
	Composite();
	virtual ~Composite();

	void add(Component *c);
	virtual LRESULT paint(HDC hdc, PAINTSTRUCT ps);
	virtual LRESULT style(Style &style);
	virtual LRESULT size();
	virtual HDWP deferSize(HDWP hdwp);
	virtual BOOL checkDialogMessage(MSG* msg);
	virtual void accept(class Visitor &v);

private:
	std::vector<Component*> children;

};

} /* namespace Main */
} /* namespace App */

#endif /* APP_COMPONENTS_COMPOSITE_H_ */
