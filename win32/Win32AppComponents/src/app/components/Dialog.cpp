/*
 * Dialog.cpp
 *
 *  Created on: Jun 24, 2015
 *      Author: Rodney Woollett
 */

#include "Dialog.h"
#include <iostream>
#include <sstream>
#include <locale>

namespace App
{
namespace Component
{
//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
//
Dialog::Dialog():controlText(""), hWndPostMessage(0)
{

}
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
//
Dialog::~Dialog()
{
	if (clientStyle != nullptr){
	  delete clientStyle;
	  clientStyle = nullptr;
	}
	binds_iterator iter;
	for (iter = binds.begin(); iter != binds.end(); ++iter)
	{
		delete *iter;
	}
}
//
///////////////////////////////////////////////////////////////////////////////
// Public create
///////////////////////////////////////////////////////////////////////////////
//
BOOL Dialog::createControl(HWND hWndParent, HINSTANCE hInst, int controlId)//, DLGPROC dialogProc)
{
	// Create a child window based on the available dialog box
	hWndControl = CreateDialogParam(hInst, MAKEINTRESOURCE(controlId),
							   hWndParent, (DLGPROC)DialogWndProc, (LONG_PTR)this);
#ifdef SHOWLAST
	ShowLastError();
#endif
	if (hWndControl == NULL)
	{
		MessageBox(hWndParent, "Could not create dialog.", "Error",
		MB_OK | MB_ICONERROR);
		return (FALSE);
	}
	this->controlId = controlId;
	this->hWndPostMessage = hWndParent;

	// Set up style for dialog windows clients
	Style* style = getStyle();
	if (style == nullptr) {
		//Create a style for the dialog to use
		//this->clientStyle = new Style();
	}
	return (TRUE);

}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
void Dialog::updateText(UINT id, const std::string &value)
{
	HWND handle = GetDlgItem(hWndControl, id);
	std::string classname = className(handle);
	if (classname.compare("EDIT") == 0) {
		if (GetDlgItem(hWndControl, id) != NULL)
		    SendMessage(GetDlgItem(hWndControl, id), WM_SETTEXT, 0, (LPARAM) value.c_str());
	}
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
std::string &Dialog::readText(UINT id){
	controlText = "";
	if (GetDlgItem(hWndControl, id) == NULL)
		return (controlText);

	int lengthText = GetWindowTextLength(GetDlgItem(hWndControl, id));
	if (lengthText < 0)
		return (controlText);

	char *str = (char*) malloc(lengthText + 1);
	SendMessage(GetDlgItem(hWndControl, id), WM_GETTEXT, (lengthText + 1), (LPARAM) str);
	controlText.append(str);
	free(str);
	return (controlText);
}
//
///////////////////////////////////////////////////////////////////////////////
// Private functions
///////////////////////////////////////////////////////////////////////////////
//
void Dialog::okAccept()
{
	// BindToSourceVisitor accepted here to do the bind update to source
	BindToSourceVisitor bindToSourceVisitor;
	accept(bindToSourceVisitor);
}
//
///////////////////////////////////////////////////////////////////////////////
// Dispatch the view message from static window procedure
///////////////////////////////////////////////////////////////////////////////
//
BOOL Dialog::dispatchMessage(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam)
{
	LRESULT returnValue = 0;        // return value
	App::WindowMessage windowMessage;
	App::Parameter paramArgs;
	paramArgs.kind = App::ParameterKind::WPARAMLPARAM;
	paramArgs.args.wParamLParam.wparam = wParam;
	paramArgs.args.wParamLParam.lparma = lParam;
	windowMessage.setParameter(paramArgs);

	switch (Msg)
	{
	case WM_CTLCOLORDLG:
	case WM_CTLCOLORBTN:
	case WM_CTLCOLORSTATIC:
	case WM_CTLCOLOREDIT:
		returnValue = controlBackground(windowMessage);
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			// BindToSourceVisitor accepted here to do the bind update to source
			okAccept();
			returnValue = postMessage(windowMessage);
			break;
		case IDCANCEL:
			returnValue = postMessage(windowMessage);
			break;

		default:
			return (FALSE);
		}
		;
		break;
	default:
		returnValue = FALSE;
		break;
	}
	return (returnValue);
}
////
/////////////////////////////////////////////////////////////////////////////////
//// Main application WinProc for Windows Class of Main Window
/////////////////////////////////////////////////////////////////////////////////
////
BOOL CALLBACK Dialog::DialogWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam)
{
	Dialog *view;
	//Style style;
	// WM_SETFONT is also done before view can be set
	if (Msg == WM_INITDIALOG)  // Non-Client Create
	{
		view = (Dialog*)lParam;
		::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR) view);

		//view->getStyle() = style;
		return (TRUE);
	}
    view = (Dialog *)GetWindowLongPtr(hWnd, GWL_USERDATA);
	if (!view)
		return (FALSE);
	else
		return (view->dispatchMessage(hWnd, Msg, wParam, lParam));

}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
BOOL Dialog::checkDialogMessage(MSG* msg){
	BOOL done = IsDialogMessage(hWndControl, msg);
	return (done);
}
//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
void Dialog::accept(Visitor &v)
{
    v.visit(this);
}
//
///////////////////////////////////////////////////////////////////////////////
// public function
///////////////////////////////////////////////////////////////////////////////
//
LRESULT Dialog::postMessage(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	WPARAM wParam = args.wParamLParam.wparam;
	NMHDR dwhdr;
	int idContents = getControlId();
	dwhdr.code     = LOWORD(wParam);
	dwhdr.hwndFrom = hWndControl;
	dwhdr.idFrom   = idContents;
	std::cout << "Dialog postMessage called.. " << idContents << std::endl;
	return (SendMessage(hWndPostMessage, WM_NOTIFY, idContents, (LPARAM)&dwhdr));
}
//
//---------------------------------------------------------------
// Public function
// Erase background - do the markers on main window
//---------------------------------------------------------------
//
LRESULT Dialog::controlBackground(const App::WindowMessage &message)
{
	App::Parameter::Args args = message.argumments();
	HDC hdc = (HDC) args.wParamLParam.wparam;
	App::Component::Style* style = nullptr;
	// If no style for that use dockwnd style
	if (style == nullptr)
		style = getStyle();

	if (style != nullptr) {
		SetTextColor(hdc, style->rgbText);
		SetBkColor(hdc, style->rgbBackground);
		return ((LONG) style->hbrBackground);
	} else {
		return (0);
	}
}

} /* namespace Component */
} /* namespace App */
