/*
 * Visitor.cpp
 *
 *  Created on: Jul 1, 2015
 *      Author: Rodney Woollett
 */

#include "Visitor.h"
#include "Header.h"
#include "Dialog.h"
#include "Edit.h"
#include "ListView.h"
#include <queue>

namespace App
{
namespace Component
{
//
//------------------------------------------
// Add Item Visitor - does the add event change for BindSource addition
//
// For a bind it gets the bind source item index field name value (string)
// and put the value onto the components control id. The component will have a function
// to do that and the control id is passed to the component (ie: a header component)
//------------------------------------------
//------------------------------------------
// Public method
//------------------------------------------
//
void AddItemVisitor::setSourceIndex(Control::BindSourceBase *source, int index)
{
	sourceBind = source;
	sourceIndex = index;
}
//
//------------------------------------------
// Public method
//------------------------------------------
//
void AddItemVisitor::visit(Header *e)
{
    std::cout << "AddItemVisitor on Header " << e->getTitle() << " " << " with source index at " << sourceIndex << '\n';
    std::cout << "AddItemVisitor on Header " << e->getTitle() << " " << sourceBind << '\n';
	binds_iterator iter;
	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
		// Is binds source the same as visitors
		Control::BindBase* bindptr = *iter;
		if (bindptr->compareSource(sourceBind)) {
			// Let bind item know the bind source index.
			bindptr->setBindRefIndex(sourceIndex);
		    e->updateText(bindptr->getControlId(), bindptr->getValue());
		} // else no action needed.
	}
}
//
//------------------------------------------
// Public method
//------------------------------------------
//
void AddItemVisitor::visit(Dialog *e)
{
    std::cout << "AddItemVisitor on Dialog " << e->getTitle() << " " << " with source index at " << sourceIndex << '\n';
    std::cout << "AddItemVisitor on Dialog " << e->getTitle() << " " << sourceBind << '\n';
	binds_iterator iter;
	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
		// Is binds source the same as visitors
		Control::BindBase* bindptr = *iter;
		if (bindptr->compareSource(sourceBind)) {
			// Let bind item know the bind source index.
			bindptr->setBindRefIndex(sourceIndex);
		    std::cout << "SourceBind:"  << sourceBind << '\n';
		    std::cout << "Control Id " << bindptr->getControlId() << " value bind source item " << bindptr->getValue() << std::endl;
		    e->updateText(bindptr->getControlId(), bindptr->getValue());
		} // else no action needed.
	}
}
//
//------------------------------------------
// Public method
//------------------------------------------
//
void AddItemVisitor::visit(Edit *e)
{
    std::cout << "AddItemVisitor on Edit " << e->getTitle() << " " << " with source index at " << sourceIndex << '\n';
    std::cout << "AddItemVisitor on Edit " << e->getTitle() << " " << sourceBind << '\n';
	binds_iterator iter;
	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
		// Is binds source the same as visitors
		Control::BindBase* bindptr = *iter;
		if (bindptr->compareSource(sourceBind)) {
			// Let bind item know the bind source index.
			bindptr->setBindRefIndex(sourceIndex);
		    std::cout << "SourceBind:"  << sourceBind << '\n';
		    std::cout << "Control Id " << bindptr->getControlId() << " value bind source item " << bindptr->getValue() << std::endl;
		    e->updateText(bindptr->getValue());
		} // else no action needed.
	}
}
//
//------------------------------------------
// Public method
//------------------------------------------
//
void AddItemVisitor::visit(ListView *e)
{
    std::cout << "AddItemVisitor on ListView " << e->getTitle() << " " << " with source index at " << sourceIndex << '\n';
    std::cout << "AddItemVisitor on ListView " << e->getTitle() << " " << sourceBind << '\n';
	binds_iterator iter;


	if (e->binds.size() > 0) {

		// Is binds source the same as visitors? - the bind source should be all the same in binds
		// for listing type of controls
		Control::BindBase* bindptr = e->binds.at(0);
		if (bindptr->compareSource(sourceBind)) {

		    std::vector<std::string> subItems; // Vector of subitems to copy over to e->addListItem

			bindptr->setBindRefIndex(sourceIndex);
			std::cout << "First subitem SourceBind:"  << sourceBind << '\n';
			std::cout << " value bind source item " << bindptr->getValue() << std::endl;
		    // Copy the first string (the label).
			// The first binds is the first subitem for a list view item
			subItems.push_back(bindptr->getValue());

			// Any other binds are the other subitems in the order of the vector
			for (iter = e->binds.begin() + 1; iter != e->binds.end(); ++iter) {
				Control::BindBase* bindptr = *iter;
				if (bindptr->compareSource(sourceBind)) {
					// Let bind item know the bind source index.
					bindptr->setBindRefIndex(sourceIndex);
					std::cout << "Other subitems SourceBind:"  << sourceBind << '\n';
					std::cout << " value bind source item " << bindptr->getValue() << std::endl;
					subItems.push_back(bindptr->getValue());
				}
			}
			e->addListViewItem(subItems, sourceIndex);
		}
	}
}

//
//------------------------------------------
// Bind To Source Visitor - does the add event change for BindSource addition
//
// For a bind it gets the bind source item index field name value (string)
// and put the value onto the components control id. The component will have a function
// to do that and the control id is passed to the component (ie: a header component)
//------------------------------------------
//------------------------------------------
// Public method
//------------------------------------------
//
void BindToSourceVisitor::visit(Header *e)
{
	/* Header don't bind back to source */
}
//
//------------------------------------------
// Public method
//------------------------------------------
//
void BindToSourceVisitor::visit(Dialog *e)
{
    std::cout << "START: BindToSourceVisitor on Dialog " << e->getTitle() << '\n';
	binds_iterator iter;
	std::queue<std::string> values;
	// Read values from controls
	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
		Control::BindBase* bindptr = *iter;
		std::cout << "Dialog field read " << e->readText(bindptr->getControlId()) << std::endl;
	    values.push(e->readText(bindptr->getControlId()));
	}
	// Write saved values to bind source - knowing bind source update item event will execute
	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
		Control::BindBase* bindptr = *iter;
		std::cout << "Source value write " << values.front() << std::endl;
	    bindptr->setValue(values.front());
	    values.pop();
	}
    std::cout << "FINISH: BindToSourceVisitor on Dialog " << e->getTitle() << '\n';
}
//
//------------------------------------------
// Public method
//------------------------------------------
//
void BindToSourceVisitor::visit(ListView *e)
{
    std::cout << "START: BindToSourceVisitor on ListView " << e->getTitle() << '\n';
	binds_iterator iter;
	std::vector<ListItem*>::iterator iter2;
	int index;

	// For the list view list of items loop over each one
	for (iter2 = e->listItems.begin(); iter2 != e->listItems.end(); ++iter2)
	{
		ListItem* itemptr = *iter2;

		// Set up new queue and index to list item for bindings loop
		std::queue<std::string> values;
		index = 0;
		for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
			std::cout << "List item field read " << itemptr->subitems[index] << std::endl;
		    values.push(itemptr->subitems[index]);
		    ++index;
		}
		// TODO:Write saved values to bind source - knowing bind source update item event will execute
	//	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
	//		Control::BindBase* bindptr = *iter;
	//		std::cout << "Source value write " << values.front() << std::endl;
	//	    bindptr->setValue(values.front());
	//	    values.pop();
	//	}

	}
    std::cout << "FINISH: BindToSourceVisitor on ListView " << e->getTitle() << '\n';
}
//
//------------------------------------------
// Public method
//------------------------------------------
//
void BindToSourceVisitor::visit(Edit *e)
{
    std::cout << "BindToSourceVisitor on Edit " << e->getTitle() << '\n';
	binds_iterator iter;
	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
		Control::BindBase* bindptr = *iter;
	    bindptr->setValue(e->readText());
	}
}

//
//------------------------------------------
// Bind UpdateItem Visitor - updates item in bind source item to components bound by index.
//
// For a bind it gets the bind source item index when added event occurred.
// So that index is used in update event for components that use a bind source with one item
// For components that bind to all items in a bind source the events does send the bind source
// index through of the bind source item updated ( via the setitem function in bind source)
//------------------------------------------
//------------------------------------------
// Public method
//------------------------------------------
//
void UpdateItemVisitor::setSourceIndex(Control::BindSourceBase *source, int index)
{
	sourceBind = source;
	sourceIndex = index;
}
//------------------------------------------
// Public method
//------------------------------------------
//
//---------------------------------------
// Header components uses single values for the control id
//---------------------------------------
void UpdateItemVisitor::visit(Header *e)
{
    std::cout << "UpdateItemVisitor on Header " << e->getTitle() << " " << sourceBind << '\n';
	binds_iterator iter;
	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
		Control::BindBase* bindptr = *iter;
		// Is binds source the same as visitors
		if (bindptr->compareSource(sourceBind)) {
			if (bindptr->getBindRefIndex() == sourceIndex)
			    e->updateText(bindptr->getControlId(), bindptr->getValue());
		}
	}
}
//
//------------------------------------------
// Public method
//------------------------------------------
//
//---------------------------------------
// Dialog components uses single values for the control id when edit controls
// The dialog controls can be list items so the item needs finding in the component control
// item list
// TODO: So the controlid needs check before dialog updatetext function as the sourceIndex may
// need to be passed so the dialog component can search for the index in its lists
//---------------------------------------
void UpdateItemVisitor::visit(Dialog *e)
{
    std::cout << "UpdateItemVisitor on Dialog " << e->getTitle() << " " << sourceBind << '\n';
	binds_iterator iter;
	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
		Control::BindBase* bindptr = *iter;
		// Is binds source the same as visitors
		if (bindptr->compareSource(sourceBind)) {
			if (bindptr->getBindRefIndex() == sourceIndex)
			    e->updateText(bindptr->getControlId(), bindptr->getValue());
		}
	}
}
//
//------------------------------------------
// Public method
//------------------------------------------
//
//---------------------------------------
// Edit components uses single values for the control id when edit controls
// The dialog controls can be list items so the item needs finding in the component control
// item list
// TODO: So the controlid needs check before dialog updatetext function as the sourceIndex may
// need to be passed so the dialog component can search for the index in its lists
//---------------------------------------
void UpdateItemVisitor::visit(Edit *e)
{
    std::cout << "UpdateItemVisitor on Edit " << e->getTitle() << " " << sourceBind << '\n';
	binds_iterator iter;
	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
		Control::BindBase* bindptr = *iter;
		// Is binds source the same as visitors
		if (bindptr->compareSource(sourceBind)) {
			if (bindptr->getBindRefIndex() == sourceIndex)
			    e->updateText(bindptr->getValue());
		}
	}
}
//
//------------------------------------------
// Public method
//------------------------------------------
//
//---------------------------------------
// Dialog components uses single values for the control id when edit controls
// The dialog controls can be list items so the item needs finding in the component control
// item list
// TODO: So the controlid needs check before dialog updatetext function as the sourceIndex may
// need to be passed so the dialog component can search for the index in its lists
//---------------------------------------
void UpdateItemVisitor::visit(ListView *e)
{
    std::cout << "UpdateItemVisitor on ListView " << e->getTitle() << " " << sourceBind << '\n';
	binds_iterator iter;
	for (iter = e->binds.begin(); iter != e->binds.end(); ++iter) {
		Control::BindBase* bindptr = *iter;
		// Is binds source the same as visitors
		if (bindptr->compareSource(sourceBind)) {
			//if (bindptr->getBindRefIndex() == sourceIndex)
			    //TODO: e->updateText(bindptr->getControlId(), bindptr->getValue());
		}
	}
}

} /* namespace Component */
} /* namespace App */
