/*
 * DockWnd.h
 *
 *  Created on: May 3, 2015
 *      Author: Rodney Woollett
 */

#ifndef DOCKWND_H_
#define DOCKWND_H_

#include <windows.h>
#include <string>
#include "WindowClient.h"
#include "../WindowMessage.h"
#include "Style.h"
//
//	DockWnd styles
//
// The two define here need to be looked at to be given from main application
#define IDB_APPBUTTONS                          117
#define IDC_CLOSEDOCKBUTTON                     114

#define DWS_BORDERTOP			0x0001	//draw a top    etched border WHEN DOCKED
#define DWS_BORDERBOTTOM		0x0002	//draw a bottom etched border WHEN DOCKED
#define DWS_BORDERLEFT			0x0004	//draw a top    etched border WHEN DOCKED
#define DWS_BORDERRIGHT			0x0008	//draw a bottom etched border WHEN DOCKED
#define DWS_DRAWGRIPPERDOCKED	0x0010	//draw a gripper when docked
#define DWS_DRAWGRIPPERFLOATING	0x0020	//draw a gripper when floating
#define DWS_FORCEDOCK			0x0040
#define DWS_FORCEFLOAT			0x0080
#define DWS_RESIZABLE			0x0100	//is resizable when docked?
#define DWS_NORESIZE			0x0200	//prevent resize when floating

#define DWS_USEBORDERS			0x1000	//use the rcBorders member to define additional border space
#define DWS_NOSETFOCUS			0x2000
#define DWS_NODESTROY			0x4000	//hides the dock-window instead of destroying it

#define DWS_ALLOW_DOCKTABBED   0x08000
#define DWS_ALLOW_DOCKLEFT	   0x10000
#define DWS_ALLOW_DOCKRIGHT	   0x20000
#define DWS_ALLOW_DOCKTOP	   0x40000
#define DWS_ALLOW_DOCKBOTTOM   0x80000

#define DWS_ALLOW_DOCKALL		(DWS_ALLOW_DOCKTABBED | \
		                         DWS_ALLOW_DOCKLEFT   | \
								 DWS_ALLOW_DOCKBOTTOM | \
								 DWS_ALLOW_DOCKRIGHT  | \
								 DWS_ALLOW_DOCKTOP)

#define DWS_DOCKED_NOTDOCKED	0
#define DWS_DOCKED_FLOATING		0
#define DWS_DOCKED_TABBED		0x08000
#define DWS_DOCKED_LEFT			0x10000
#define DWS_DOCKED_RIGHT		0x20000
#define DWS_DOCKED_TOP			0x40000
#define DWS_DOCKED_BOTTOM		0x80000

//
//	DockWnd message notifications...
//
#define DWN_BASE		(0U - 2048U)
#define DWN_HIDDEN		(DWN_BASE - 0)
#define DWN_DOCKED		(DWN_BASE - 1)
#define DWN_UNDOCKED	(DWN_BASE - 2)
#define DWN_CLOSED		(DWN_BASE - 3)
#define DWN_ISDOCKABLE	(DWN_BASE - 4)
#define DWN_TABADD	    (DWN_BASE - 5)
#define DWN_TABREMOVE	(DWN_BASE - 6)

const int MAX_DOCKWNDS = 10;



namespace App
{
class DockWnd;
// Use in DWN_ISDOCKABLE
typedef struct tagNMDOCKWNDQUERY
{
    NMHDR	 hdr;
	HWND     hwndDock;      /* Handle to dock-window */
//    App::DockWnd *pDockWnd;      /* Pointer to the DockWnd structure for that window */
    RECT    *dragrect;      /* Current drag-rectangle */
    POINT   *point;             /* Current mouse point with DockWnd */
	HWND     hwndDockContainer; /* Handle to dock containing window - updated by IsDock message handler */
} NMDOCKWNDQUERY;
//typedef NMDOCKWNDQUERY NMDOCKWNDQUERY;

// Use in DWN_TABCHANGE
typedef struct tagNMDOCKTABCHANGE
{
    NMHDR	 hdr;
	HWND     hwndDock;           /* Handle to dock-window */
	UINT     uTabComponentId;    /* ID of tab container if currently tab docked, otherwise 0 */
} NMDOCKTABCHANGE;

class DockWnd: public App::Component::WindowClient
{
public:
	explicit DockWnd();
	virtual ~DockWnd();

	static LRESULT CALLBACK DockWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
			LPARAM lParam);
	LRESULT dispatchMessage(HWND hWnd, UINT Msg, WPARAM wParam,	LPARAM lParam);

	int createDockWnd(std::string title, HINSTANCE handleInstance, HWND hWndParent, WindowClient *windowClient = nullptr);
	void SetDockContents(HWND hwndContents, HWND hwndParent);
	HWND dockWndHandle();
	void setDockedState(BOOL fDocked, UINT uDockedState, UINT uTabComponentId = 0);
	void setFloatPos(int fx, int fy);
	void setDockWndStyle(DWORD style);

	// Command Messages to DockWnd for message dispatching
	long windowPosChanged(const App::WindowMessage &message);
	long toggleDockingMode(const App::WindowMessage &message);
	long beginDrag(const App::WindowMessage &message);
	long cancelDrag(const App::WindowMessage &message);
	long leftButtonUp(const App::WindowMessage &message);
	long mouseMove(const App::WindowMessage &message);
	long eraseBackground(const App::WindowMessage &message);
	long controlBackground(const App::WindowMessage &message);
	long drawItem(const App::WindowMessage &message);
	long minMaxInfo(const App::WindowMessage &message);
	long settingChange(const App::WindowMessage &message);
	long closeView(const App::WindowMessage &message);
	long close(const App::WindowMessage &message);

	// Container drawing and position of docked windows
	BOOL containerPosition(HWND hwndContainer, HDWP hdwp, RECT *rect, UINT stateWant);

	// Window client virtual functions for dock window decoration
	void setVisible(BOOL newVisible);
	virtual LRESULT style(App::Component::Style &style);
	virtual LRESULT paint(HDC hdc, PAINTSTRUCT ps);
	virtual HDWP deferSize(HDWP hdwp);
	virtual BOOL checkDialogMessage(MSG* msg);
	virtual void accept(App::Component::Visitor &v);

	// Tabbed Resource IDs can be known to callee's
	UINT IdContentWnd();
	UINT IdTabbedWnd();

	static HHOOK	draghook;
	static HWND		g_hwndDockWnd;
	static BOOL		fControl;
	// Static hook procedure
	static LRESULT CALLBACK draghookproc(int code, WPARAM wParam, LPARAM lParam);

	// Statics for list of all dockwnds and mouse dragging of window
	static int getPopupList(HWND hwndMain, HWND hwndList[], int nSize, BOOL fIncMain);
	static int addDockWindow(HWND hWnd);
	static int removeDockWindow(HWND hWnd);
	static HWND getOwner(HWND hwnd);
	static BOOL isOwnedBy(HWND hwndMain, HWND hwnd);
	static int nNumDockWnds;
	static HWND hDockList[MAX_DOCKWNDS];
	static LRESULT HANDLE_NCACTIVATE(HWND hwndMain,
			HWND hwnd, WPARAM wParam, LPARAM lParam);
	static LRESULT HANDLE_ENABLE(HWND hwndMain, HWND hwnd,
			WPARAM wParam, LPARAM lParam);

private:
	DockWnd(const DockWnd& oldDockWnd); // no copying allowed
	DockWnd operator = (const DockWnd& oldDockWnd); // no assignment allowed

	void SetFloatingWinPos(DWORD dwSWFlags);
	void CalcFloatingRect();
	UINT IsDockable(RECT *dragrect, POINT *pt, App::NMDOCKWNDQUERY *dwq);
	UINT TabbedDockChange(UINT code);
	void DrawXorFrame(HWND hwnd, RECT *rect, BOOL fDocked);
	void TogglePopupStyle();
	void DrawGripper(HDC hdc, int x, int y, int height);
	void DrawCloseButton(HDC hdc, int x, int y, int height, DWORD dwSWFlags);
	void addNewDocWnd(HWND hWnd, DockWnd *view);
	HWND GetOwner(HWND hwnd)
	{
		return (GetWindow(hwnd, GW_OWNER));
	}
	//	Send a "fake" WM_SIZE to the specified window
	void Send_WM_SIZE(HWND hwnd)
	{
		RECT rect;
		GetClientRect(hwnd, &rect);
		SendMessage(hwnd, WM_SIZE,  SIZE_RESTORED, MAKELPARAM(rect.right-rect.left,rect.bottom-rect.top));
	}

	DWORD	dwStyle;		//styles..

	int		xpos;			//coordinates of FRAME when floating
	int		ypos;

	int		cxFloating;		//size of CONTENTS when floating
	int		cyFloating;

	int		nDockedSize;	//width/height of window when docked..
	BOOL	fDocked;		//docked/floating?
	UINT	uDockedState;	//left/right/top/bottom when docked?
	UINT	uTabComponentId;//If tab docked what tab component is it at?

	RECT	rcBorderDock;	//border to place around each side of contents
	RECT	rcBorderFloat;	//border to place around each side of contents

	//DWORD	dwUser;			//whatever..

	//
	//	Internal to DockWindow library, do not modify
	//
	int		nFrameWidth;	//window frame width when floating
	int		nFrameHeight;	//(Private)

	HWND	hwndContents;	//handle to the window to host
	HWND	hwnd;			//handle to container (THIS!!!)

	BOOL	fDragging;		//Are we dragging?
	RECT	oldrect;		//
	HBITMAP hbm1;
    HWND    hwndButton;
    WindowClient *wcContents; //we need this for the dockwnd window client component decoration

	//
	// Used for dragging
	// Static is able as only one DockWnd can be dragged in Windows UI
	static BOOL		fOldControl;
	static BOOL		fOldDrawDocked;
	static	BOOL  fMouseMoved;
	static	POINT spt;
	static	RECT  rcStart;
	static	POINT oldpt;

	static int count;


};

} /* namespace App */

#endif /* DOCKWND_H_ */
