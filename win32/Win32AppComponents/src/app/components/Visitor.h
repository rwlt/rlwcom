/*
 * Visitor.h
 *
 *  Created on: Jul 1, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_VISITOR_H_
#define APP_COMPONENTS_VISITOR_H_

#include <iostream>
#include "control/BindSourceBase.h"
#include "control/BindBase.h"
#include <vector>

namespace App
{
namespace Component
{

class Header;
class Dialog;
class Edit;
class ListView;

class Visitor
{
public:
  virtual ~Visitor(){};
  virtual void visit(Header *e) = 0;
  virtual void visit(Dialog *e) = 0;
  virtual void visit(Edit *e) = 0;
  virtual void visit(ListView *e) = 0;
};

typedef std::vector<Control::BindBase*>::iterator binds_iterator;

class AddItemVisitor: public Visitor
{
public:
	AddItemVisitor():sourceIndex(0),sourceBind(nullptr){};
	virtual ~AddItemVisitor(){};
    virtual void visit(Header *e);
    virtual void visit(Dialog *e);
    virtual void visit(Edit *e);
    virtual void visit(ListView *e);

    void setSourceIndex(Control::BindSourceBase *sourceBind, int index);
private:
    int sourceIndex;
    Control::BindSourceBase *sourceBind;

};

class BindToSourceVisitor: public Visitor
{
public:
	virtual ~BindToSourceVisitor(){};
    virtual void visit(Header *e);
    virtual void visit(Dialog *e);
    virtual void visit(Edit *e);
    virtual void visit(ListView *e);

};

class UpdateItemVisitor: public Visitor
{
public:
	virtual ~UpdateItemVisitor(){};
    virtual void visit(Header *e);
    virtual void visit(Dialog *e);
    virtual void visit(Edit *e);
    virtual void visit(ListView *e);

    void setSourceIndex(Control::BindSourceBase *source, int index);
private:
    int sourceIndex;
    Control::BindSourceBase *sourceBind;

};

} /* namespace Component */
} /* namespace App */

#endif /* APP_COMPONENTS_VISITOR_H_ */
