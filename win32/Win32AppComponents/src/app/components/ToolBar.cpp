/*
 * ToolBar.cpp
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#include "ToolBar.h"
#include <commctrl.h>

namespace App
{
namespace Component
{
//
///////////////////////////////////////////////////////////////////////////////
// Constructor
///////////////////////////////////////////////////////////////////////////////
//
ToolBar::ToolBar() :
		hImageList(0)
{

}
//
///////////////////////////////////////////////////////////////////////////////
// Deconstructor
///////////////////////////////////////////////////////////////////////////////
//
ToolBar::~ToolBar()
{
	if (hImageList)
		DeleteObject(hImageList);

}
//
///////////////////////////////////////////////////////////////////////////////
// Public create
///////////////////////////////////////////////////////////////////////////////
//
BOOL ToolBar::createControl(HWND hWndParent, HINSTANCE hInst, int controlId)
{
	// Declare and initialize local constants.
	const int ImageListID = 0;
	const int numButtons = 4;
	const int bitmapSize = 24;

	const DWORD buttonStyles = BTNS_AUTOSIZE; //| TBSTYLE_WRAPABLE ;
	//|TBSTYLE_FLAT;

	// Create the toolbar.
	hWndControl = CreateWindowEx(0, TOOLBARCLASSNAME, NULL,
	WS_CHILD, 0, 0, 0, 0, hWndParent, (HMENU) controlId, hInst,
	NULL);

	if (hWndControl == NULL)
	{
		MessageBox(hWndParent, "Could not create toolbar control.",
				"Error",
				MB_OK | MB_ICONERROR);
		return (FALSE);
	}

	this->controlId = controlId;
	// Create the image list.
	hImageList = ImageList_Create(bitmapSize, bitmapSize, // Dimensions of individual bitmaps.
			ILC_COLOR16 | ILC_MASK,   // Ensures transparent background.
			numButtons, 0);

	// Set the image list.
	SendMessage(hWndControl, TB_SETIMAGELIST, (WPARAM) ImageListID,
			(LPARAM) hImageList);

	// Load the button images.
	SendMessage(hWndControl, TB_LOADIMAGES, (WPARAM) IDB_STD_LARGE_COLOR,
			(LPARAM) HINST_COMMCTRL);

	// Initialize button info.
	TBBUTTON tbButtons[numButtons] =
	{
	{ MAKELONG(STD_FILENEW, ImageListID), IDM_FILE_NEW, TBSTATE_ENABLED,
			buttonStyles,
			{ 0 }, 0, 0 },   //(INT_PTR) L"New" },
			{ MAKELONG(STD_FILEOPEN, ImageListID), IDM_FILE_OPEN,
					TBSTATE_ENABLED, buttonStyles,
					{ 0 }, 0, 0 },   //(INT_PTR) L"Open" },
			{ MAKELONG(STD_FILESAVE, ImageListID), IDM_FILE_SAVE,
					TBSTATE_ENABLED, buttonStyles,
					{ 0 }, 0, 0 }, //(INT_PTR) L"Save" } ,
			{ MAKELONG(STD_PROPERTIES, ImageListID), IDM_FILE_SAVE,
					TBSTATE_ENABLED, buttonStyles,
					{ 0 }, 0, 0 }
	   };

	// Add buttons.
	SendMessage(hWndControl, TB_BUTTONSTRUCTSIZE,
			(WPARAM) sizeof(TBBUTTON), 0);
	SendMessage(hWndControl, TB_ADDBUTTONS, (WPARAM) numButtons,
			(LPARAM) &tbButtons);

	// Resize the toolbar, and then show it.
	SendMessage(hWndControl, TB_AUTOSIZE, 0, 0);
	ShowWindow(hWndControl, TRUE);

	return (TRUE);
}

//
///////////////////////////////////////////////////////////////////////////////
// Public functions
///////////////////////////////////////////////////////////////////////////////
//
LRESULT ToolBar::paint(HDC hdc, PAINTSTRUCT ps)
{
	return (0);
}

} /* namespace Component */
} /* namespace App */
