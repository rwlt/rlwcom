/*
 * ToolBar.h
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_TOOLBAR_H_
#define APP_COMPONENTS_TOOLBAR_H_

#include "WindowClient.h"
#include <commctrl.h>
#define IDM_FILE_NEW                            40000
#define IDM_FILE_OPEN                           40001
#define IDM_FILE_SAVE                           40002

namespace App
{
namespace Component
{

class ToolBar: public WindowClient
{
public:
	ToolBar();
	virtual ~ToolBar();
	BOOL createControl(HWND hWndParent, HINSTANCE hInst, int controlId);
	LRESULT paint(HDC hdc, PAINTSTRUCT ps);

private:
	HIMAGELIST hImageList;

};

} /* namespace Component */
} /* namespace App */

#endif /* APP_COMPONENTS_TOOLBAR_H_ */
