/*
 * Edit.h
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_EDIT_H_
#define APP_COMPONENTS_EDIT_H_

#include "WindowClient.h"
#include "control/Bind.h"
#include <vector>

namespace App
{
namespace Component
{

class Edit: public WindowClient
{
public:
	Edit();
	virtual ~Edit();
	BOOL createControl(HWND hWndParent, HINSTANCE hInst, int controlId);

	void updateText(const std::string &value);
	std::string &readText();

	void addBind(Control::BindSource<std::string> &source, int id)
	{
		if (hWndControl == 0)
			return;
		if (checkValidControl(hWndControl))
		{
			Control::Bind<std::string> * bind =
					new Control::Bind<std::string>(&source, id);
			binds.push_back(bind);
		}
	}
	virtual void accept(Visitor &v);

private:
	std::string controlText;
	std::vector<Control::BindBase*> binds;

	friend class AddItemVisitor;
	friend class BindToSourceVisitor;
	friend class UpdateItemVisitor;

};

} /* namespace Component */
} /* namespace App */

#endif /* APP_COMPONENTS_EDIT_H_ */
