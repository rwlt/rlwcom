/*
 * WindowFramed.h
 *
 *  Created on: Jun 10, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_WINDOWFRAMED_H_
#define APP_COMPONENTS_WINDOWFRAMED_H_

#include "Composite.h"
#include "Style.h"

namespace App
{
namespace Component
{
class WindowClient;
class WindowFramed: public Composite
{
public:
	WindowFramed();
	virtual ~WindowFramed();

	Style &styleFramed() {
		return (frameStyle);
	}
	void addWindowClient(WindowClient *c);

protected:
	HWND hMainWindow;
private:
	Style frameStyle;// Shared by Component derived classes
	WindowFramed(const WindowFramed&);
	WindowFramed operator == (const WindowFramed&);
//friend class WindowClient;
//friend class Edit;
//friend class Header;
};

} /* namespace Main */
} /* namespace App */

#endif /* APP_COMPONENTS_WINDOWFRAMED_H_ */
