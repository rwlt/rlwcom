/*
 * ListView.h
 *
 *  Created on: Jun 11, 2015
 *      Author: Rodney Woollett
 */

#ifndef APP_COMPONENTS_LISTVIEW_H_
#define APP_COMPONENTS_LISTVIEW_H_

#include "WindowClient.h"
#include "control/Bind.h"
#include <vector>

namespace App
{
namespace Component
{

typedef std::vector<std::string>::iterator columns_iterator;

typedef struct ListItem_tag {
	std::vector<std::string> subitems;
	int bindRefIndex;
} ListItem;

class ListView: public WindowClient
{
public:
	ListView();
	virtual ~ListView();
	BOOL createControl(HWND hWndParent, HINSTANCE hInst, int controlId);

	static LRESULT CALLBACK ListViewSubclassProc(HWND hWnd, UINT Msg, WPARAM wParam,
			LPARAM lParam);
	static LRESULT CALLBACK EditSubclassProc(HWND hWnd, UINT Msg, WPARAM wParam,
			LPARAM lParam);
	static BOOL CALLBACK MessageDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam);

	LRESULT paint(HDC hdc, PAINTSTRUCT ps);
	LRESULT style(Style &style);
	virtual void accept(Visitor &v);
	virtual BOOL checkDialogMessage(MSG* msg);

	template<class DataType>
	void addBind(const std::string (DataType::*getPtr)() const,
			     void (DataType::*setPtr)(const std::string &value),
				 std::string name,
			     Control::BindSource<DataType> &source, int id)
	{
		if (hWndControl == 0)
			return;
		if (checkValidControl(hWndControl))
		{
			Control::Bind<DataType> * bind =
					new Control::Bind<DataType>(getPtr, setPtr, &source, id);
			binds.push_back(bind);
			columnNames.push_back(name);
		}
	}
	BOOL initListView(HINSTANCE hInst);
	void acceptChanges(); // Get any changes in list view into the BindSource

private:
	BOOL initListViewColumns(HINSTANCE hInst);
	BOOL initListViewImageLists(HINSTANCE hInst);
	int GetTextMetricsCorrectly( HDC hDC, LPTEXTMETRIC lpTM );
	BOOL addListViewItem(std::vector<std::string> subItems, int refBindIndex);
	void EditDialogPosition();
	RECT getDialogRectFromItemRect(const RECT& itemrect);
	void printRect(const RECT& rect);
	int subItemFromDialogChildWHnd(HWND hWnd);
	void notifyEditText(HWND hWnd);
	void doRedrawItem();
	void editDialogOnListItem(int itemIndex);

	HWND dialogTest;
	int iItem;
	//int iSubItem;

	HWND hWndPostMessage; // Normally parent window where post messages goes to start processing dialog
	std::vector<Control::BindBase*> binds;
	std::vector<ListItem*> listItems;
	std::vector<std::string> columnNames;

	static std::string origWndProc;
	static std::string wndInstance;

	friend class AddItemVisitor;
	friend class BindToSourceVisitor;
	friend class UpdateItemVisitor;

};

} /* namespace Component */
} /* namespace App */

#endif /* APP_COMPONENTS_LISTVIEW_H_ */
