/*
 * WindowMessage.cpp
 *
 *  Created on: May 4, 2015
 *      Author: Rodney Woollett
 */

#include "WindowMessage.h"

namespace App
{

WindowMessage::WindowMessage(): parameterKind(App::ParameterKind::LPARAMETER)
{
	Parameter::Args args;
	args.lParameter.lparam = 0;
	parameterArgs = args;
}

WindowMessage::~WindowMessage()
{
}

Parameter::Args WindowMessage::argumments() const
{
	return (parameterArgs);
}
ParameterKind  WindowMessage::kind() const
{
	return (parameterKind);
}
void  WindowMessage::setParameter(Parameter newParam)
{
	parameterKind = newParam.kind;
	parameterArgs = newParam.args;
}

} /* namespace App */
