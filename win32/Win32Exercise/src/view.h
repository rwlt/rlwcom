/*
 * View.h
 *
 *  Created on: Apr 8, 2015
 *      Author: Rodney Woollett
 */

#ifndef VIEW_H_
#define VIEW_H_

namespace Module
{

class View
{
public:
	View();
	//  PresenterBase Presenter { get; }
	void closeView();
	void showDialogView();
	void showView();
	void setMdiParent(View parent);
	void showChildView(View child);
private:
	int i;
};

}

#endif /* VIEW_H_ */
