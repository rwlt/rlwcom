#include <windows.h>
#include <winuser.h>
#include <commctrl.h>
#include "resource.h"

#include "ViewFactory.h"
#include "model.h"
#include "WindowClass.h"
#include "MainWindow.h"

//------------------------------------------------------------------------------
LRESULT CALLBACK MainWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam);
BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam,
		LPARAM lParam);
BOOL CALLBACK ToolDlgProc(HWND hwnd, UINT Message, WPARAM wParam,
		LPARAM lParam);
INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst,
		LPSTR lpCmdLine, int nCmdShow);
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
INT APIENTRY /*WINAPI*/WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst,
		LPSTR lpCmdLine, int nCmdShow)
{

	MSG Msg;
	const char* className = "Win32App";
	const char* windowName = "Win32 OOP Programming";

	//Initialize the Common controls
	INITCOMMONCONTROLSEX initCtrlEx;
	initCtrlEx.dwSize = sizeof(INITCOMMONCONTROLSEX);
	initCtrlEx.dwICC = ICC_BAR_CLASSES;
	InitCommonControlsEx(&initCtrlEx);

	App::Model model;
	model.hInst = hInstance;

	// Create controller with a Model and View
	// Create the view factory and to the applications Module Controller
    App::ViewFactory viewFactory;
	Module::Controller controller(&viewFactory, &model); //&model, &view);
	controller.loadMainWindow(); //Use a view factory to create a Parent View or main view

	//Initialize the application class
	App::WindowClass windowsClass;
	App::MainWindow mainWindow;

	windowsClass.Create(model.hInst, className, MainWndProc);
	windowsClass.Register();

	//Create the main window
	mainWindow.Create(model.hInst, className, windowName, &controller);
	mainWindow.Show();

	//Process the main windows messages
	while (GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		if (!IsDialogMessage(model.hToolbar, &Msg))
		{
			if (!IsDialogMessage(model.hWndToolbar, &Msg))
			{
				TranslateMessage(&Msg);
				DispatchMessage(&Msg);
			}
		}
	}

	windowsClass.UnRegister();
	return (0);

}
//------------------------------------------------------------------------------
LRESULT CALLBACK MainWndProc(HWND hWnd, UINT Msg, WPARAM wParam,
		LPARAM lParam)
{
	LRESULT returnValue = 0;        // return value
	static Module::Controller *ctrl;
	ctrl = (Module::Controller*) ::GetWindowLongPtr(hWnd, GWLP_USERDATA);

	if (Msg == WM_NCCREATE)  // Non-Client Create
	{
		// WM_NCCREATE message is called before non-client parts(border,
		// titlebar, menu,etc) are created. This message comes with a pointer
		// to CREATESTRUCT in lParam. The lpCreateParams member of CREATESTRUCT
		// actually contains the value of lpPraram of CreateWindowEX().
		// First, retrieve the pointer to the controller specified when
		// MainWin is setup.
		ctrl =
				(Module::Controller*) (((CREATESTRUCT*) lParam)->lpCreateParams);
		ctrl->setHandle(hWnd);

		// Second, store the pointer to the Controller into GWLP_USERDATA,
		// so, other message can be routed to the associated Controller.
		::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR) ctrl);

		return (::DefWindowProc(hWnd, Msg, wParam, lParam));
	}

	// check NULL pointer, because GWLP_USERDATA is initially 0, and
	// we store a valid pointer value when WM_NCCREATE is called.
	if (!ctrl)
		return (::DefWindowProc(hWnd, Msg, wParam, lParam));

	switch (Msg)
	{
	case WM_CREATE:
		returnValue = ctrl->create();
		break;

	case WM_SIZE:
		returnValue = ctrl->size(LOWORD(lParam), HIWORD(lParam),
				(int) wParam);    // width, height, type
		break;

	case WM_PAINT:
		ctrl->paint();
		returnValue = ::DefWindowProc(hWnd, Msg, wParam, lParam);
		break;

	case WM_ERASEBKGND:
		returnValue = ctrl->eraseBackground(wParam);
		break;

	case WM_CTLCOLOREDIT:
		return (ctrl->colorEdit(wParam,lParam));

	case WM_ACTIVATE:
		returnValue = ctrl->activate();
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDM_ABOUT:
		{
			// Show a modal dialog - it implements its own message loop and procedure
			int ret = DialogBox(GetModuleHandle(NULL),
					MAKEINTRESOURCE(IDD_ABOUT), hWnd, AboutDlgProc);
			if (ret == IDOK)
			{
				MessageBox(hWnd, "Dialog exited with IDOK.", "Notice",
				MB_OK | MB_ICONINFORMATION);
			}
			else if (ret == IDCANCEL)
			{
				MessageBox(hWnd, "Dialog exited with IDCANCEL.", "Notice",
				MB_OK | MB_ICONINFORMATION);
			}
			else if (ret == -1)
			{
				MessageBox(hWnd, "Dialog failed!", "Error",
				MB_OK | MB_ICONINFORMATION);
			}
		}
			break;
		default:
			returnValue = ctrl->command(LOWORD(wParam), HIWORD(wParam),
					lParam);   // id, code, msg
			break;
		}
		;
		break;

	case WM_LBUTTONDOWN:
		returnValue = ctrl->leftButtonDown(wParam, LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_DESTROY:
		returnValue = ctrl->destroy();
		break;

	default:
		return (DefWindowProc(hWnd, Msg, wParam, lParam));
	}

	return (returnValue);

}

BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam,
		LPARAM lParam)
{
	LRESULT returnValue = 0;        // return value
	//Caution: use Windows BOOL, not C++ bool
	// Return FALSE for messages you don't handle and TRUE for those you do (If otherwise told not to!
	//  use the Help win32 notes. )
	switch (Message)
	{
	case WM_INITDIALOG: // For initializing dialogs (not WM_CREATE, controls wont be created if you did)
		returnValue = 1L;
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			EndDialog(hwnd, IDOK); //do not call DestroyWindow() for dialogs
			returnValue = 1L;
			break;

		case IDCANCEL:
			EndDialog(hwnd, IDCANCEL); // do not call DestroyWindow() for dialogs
			returnValue = 1L;
			break;
		}
		break;
	default:
		break; // Modal Dialog window procedures don't use DefWindowProc
	}
	return (returnValue);
}

//------------------------------------------------------------------------------
BOOL CALLBACK ToolDlgProc(HWND hwnd, UINT Message, WPARAM wParam,
		LPARAM lParam)
{
	//Caution: use Windows BOOL, not C++ bool
	LRESULT returnValue = 0;        // return value
	// Return FALSE for messages you don't handle and TRUE for those you do (If otherwise told not to!
	//  use the Help win32 notes. )
	// One change is that we don't call EndDialog() for modeless dialogs. We can use
	// DestroyWindow() just like for regular windows
	HWND parent = GetParent(hwnd);
	static Module::Controller *ctrl;
	ctrl = (Module::Controller*) ::GetWindowLongPtr(parent, GWLP_USERDATA);
	switch (Message)
	{
	case WM_COMMAND:
		returnValue = ctrl->command(LOWORD(wParam), HIWORD(wParam),
				lParam);   // id, code, msg
		returnValue = 1L;
		break;
	default:
		break; // Modeless Dialog window procedures don't use DefWindowProc
	}
	return (returnValue);
}
