#ifndef EXERCISE_H
#define EXERCISE_H

#include <windows.h>
#include <winuser.h>
#include <commctrl.h>
#include "resource.h"

#define IDC_MAIN_EDIT	101
extern BOOL CALLBACK ToolDlgProc(HWND hwnd, UINT Message, WPARAM wParam,
		LPARAM lParam);

namespace App
{
//------------------------------------------------------------------------------
class Model
{
public:
	Model();
	HINSTANCE hInst;
	BOOL CreateStandardToolbar(HWND hParent, HINSTANCE hInst);
	HWND hWndToolbar;
	HIMAGELIST hImageList;
	BOOL CreateToolbar(HWND hParent, HINSTANCE hInst);
	HWND hToolbar;
	BOOL CreateEditControl(HWND hParent, HINSTANCE hInst);
	HWND hEditControl;
	HFONT hFont;
	HBRUSH hbrBackground;
	COLORREF rgbBackground;
	COLORREF rgbText;
	BOOL bOpaque;
public:
	void ChangeCurrentCursor(HWND hWnd, const char* cursor);
};
}
#endif
