#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <windows.h>
#include "Controller.h"
#include "MainWindow.h"

namespace App
{
//------------------------------------------------------------------------------
class MainWindow
{
public:
	//We will use a default constructor to declare a window
	MainWindow();
	//The Create() method will be used to initialize a window
	HWND Create(HINSTANCE hinst, const char* clsname, const char* wndname,
			Module::Controller* controller, HWND parent = NULL, DWORD dStyle =
					WS_OVERLAPPEDWINDOW, DWORD dXStyle = 0L, int x =
					CW_USEDEFAULT, int y = CW_USEDEFAULT, int width =
					CW_USEDEFAULT, int height = CW_USEDEFAULT);
	//This method will be used to display the windows
	BOOL Show(int dCmdShow = SW_SHOWNORMAL);

	//Because each window is of type HWND we will need a way
	//to recognize the window handle when used in our application
	operator HWND();

protected:
	//Handle available to this and other windows
	HWND _hwnd;
	Module::Controller* _controller;            // pointer to controller

};
//------------------------------------------------------------------------------
}

#endif
