/*
 * AbstractViewFactory.h
 *
 *  Created on: Apr 8, 2015
 *      Author: Rodney Woollett
 */

#ifndef VIEWLOADER_H_
#define VIEWLOADER_H_

#include "view.h"
namespace Module
{

enum ViewIdentity
{
    MainWindow = 0,
    ResourceLocatorView = 1,
    MessageBoxView = 2,
	TotalViews = 3
};

class ViewLoader
{
public:
	ViewLoader();
	virtual ~ViewLoader();
    virtual void loadParent();


};

class AbstractViewFactory
{
public:
	AbstractViewFactory();
	virtual ~AbstractViewFactory();
	virtual View* createInstance(const ViewLoader *const loader,
			Module::ViewIdentity viewIdentity) = 0;
};

}

#endif /* VIEWLOADER_H_ */
