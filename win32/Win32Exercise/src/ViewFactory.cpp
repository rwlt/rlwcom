/*
 * ViewFactory.cpp
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#include "ViewFactory.h"

namespace App
{

ViewFactory::ViewFactory()
{
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
	{
		moduleView[index] = 0;
	}
}

ViewFactory::~ViewFactory()
{
	for (int index = 0; index < Module::ViewIdentity::TotalViews; ++index)
	{
		if (moduleView[index] != 0)
		{
			delete moduleView[index];
			moduleView[index] = 0;
		}
	}
}

//----------------------------------------------------------------------------------
// createInstance
// Use Module ViewIdentity and the total views available to keep
// an array of views to return to caller
//----------------------------------------------------------------------------------
Module::View* ViewFactory::createInstance(
		const Module::ViewLoader * const loader,
		Module::ViewIdentity viewIdentity)
{
	Module::View* view;

	if (moduleView[viewIdentity] != 0)
	{
		view = moduleView[viewIdentity];
	}
	else
	{
		switch (viewIdentity)
		{
		case Module::ViewIdentity::MainWindow:
			view = new Module::View();
			moduleView[viewIdentity] = view;
			//return new TMMainForm(mainController, m_modelState);
			break;

		default:
			view = new Module::View(); // A default view
			break;
		}
	}
	return (view);
}

}

