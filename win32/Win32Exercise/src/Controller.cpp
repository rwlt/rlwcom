#include "Controller.h"
#include <string>
#include <sstream>

///////////////////////////////////////////////////////////////////////////////
// default constructor
// ModelGL* model, ViewGL* view
// modelGL(model), viewGL(view),
///////////////////////////////////////////////////////////////////////////////
namespace Module
{
Controller::Controller(Module::AbstractViewFactory* abstractViewFactory, App::Model* exo) :
		handle(0), mExo(exo), clientWidth(0), clientHeight(0), hbrWhite(0), hbrGray(
				0), ppt(aptPentagon), cpt(6), aptStar(
		{
		{ 50, 2 },
		{ 2, 98 },
		{ 98, 33 },
		{ 2, 33 },
		{ 98, 98 },
		{ 50, 2 } }), aptTriangle(
		{
		{ 50, 2 },
		{ 98, 86 },
		{ 2, 86 },
		{ 50, 2 } }), aptRectangle(
		{
		{ 2, 2 },
		{ 98, 2 },
		{ 98, 98 },
		{ 2, 98 },
		{ 2, 2 } }), aptPentagon(
		{
		{ 50, 2 },
		{ 98, 35 },
		{ 79, 90 },
		{ 21, 90 },
		{ 2, 35 },
		{ 50, 2 } }), aptHexagon(
		{
		{ 50, 2 },
		{ 93, 25 },
		{ 93, 75 },
		{ 50, 98 },
		{ 7, 75 },
		{ 7, 25 },
		{ 50, 2 } }), loadedView(0), parentView(0), viewFactory(abstractViewFactory)
{
}

Controller::~Controller()
{
	::DestroyWindow(handle);
}

///////////////////////////////////////////////////////////////////////////////
// handle WM_CLOSE
///////////////////////////////////////////////////////////////////////////////
int Controller::close()
{
	// close OpenGL Rendering context
	//viewGL->closeContext(handle);

	::DestroyWindow(handle);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// handle WM_DESTROY
///////////////////////////////////////////////////////////////////////////////
int Controller::destroy()
{
	DestroyWindow(mExo->hWndToolbar);
	DestroyWindow(mExo->hEditControl);
	DeleteObject(mExo->hFont);
	DeleteObject(mExo->hbrBackground);
	DestroyWindow(mExo->hToolbar);
	PostQuitMessage(WM_QUIT);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// handle WM_CREATE
///////////////////////////////////////////////////////////////////////////////
int Controller::create()
{
	mExo->CreateToolbar(handle, mExo->hInst);
	mExo->CreateStandardToolbar(handle, mExo->hInst);
	mExo->CreateEditControl(handle, mExo->hInst);
	hbrWhite = static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH)); //GetStockObject(WHITE_BRUSH);
	hbrGray = static_cast<HBRUSH>(GetStockObject(GRAY_BRUSH)); //GetStockObject(GRAY_BRUSH);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// handle WM_ACTIVATE
///////////////////////////////////////////////////////////////////////////////
int Controller::activate()
{
	static int shown = 0;
	std::stringstream title;
	title << "Rainbow - Titled " << std::to_string(++shown);
	SendMessage(handle, WM_SETTEXT, 0, (LPARAM) title.str().c_str());
	return (0);
}
///////////////////////////////////////////////////////////////////////////////
// handle WM_PAINT
///////////////////////////////////////////////////////////////////////////////
int Controller::paint()
{
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc, rcPort;

	hdc = BeginPaint(handle, &ps);
	GetClientRect(handle, &rc);
	//GetClientRect(Exo.hWndToolbar, &rcToolBar);

	SetMapMode(hdc, MM_ANISOTROPIC);
	SetWindowExtEx(hdc, 100, 100, NULL);
	SetViewportExtEx(hdc, 100, 100, NULL);
	SetRect(&rcPort, 0, 0, 100, 100);
	SetViewportOrgEx(hdc, 0, 30, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, ppt, cpt);

	SetViewportOrgEx(hdc, 100, 30, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, ppt, cpt);

	SetViewportOrgEx(hdc, 0, 130, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, ppt, cpt);

	SetViewportOrgEx(hdc, 100, 130, NULL);
	if (RectVisible(hdc, &rcPort) > 0)
		Polyline(hdc, aptHexagon, 7);

	// Font of edit control previews
	SetMapMode(hdc, MM_TEXT);
	SetViewportOrgEx(hdc, 210, 30, NULL);
	SetRect(&rcPort, 0, 0, 200, 100);
	char szSize[100];
	char szTitle[] = "These are the dimensions of your client area:";

	HFONT hfOld = static_cast<HFONT>(SelectObject(hdc, mExo->hFont));

	SetBkColor(hdc, mExo->rgbBackground);
	SetTextColor(hdc, mExo->rgbText);

	if (mExo->bOpaque)
	{
		SetBkMode(hdc, OPAQUE);
	}
	else
	{
		SetBkMode(hdc, TRANSPARENT);
	}
	FillRect(hdc, &rcPort, mExo->hbrBackground); //(HBRUSH)COLOR_WINDOW	+ 1

	DrawText(hdc, szTitle, -1, &rcPort, DT_WORDBREAK);
	POINT aptPortRect[5] =
	{ 0, 0, 200, 0, 200, 100, 0, 100, 0, 0 };
	Polyline(hdc, aptPortRect, 5);
	wsprintf(szSize, "{%d, %d, %d, %d}", rcPort.left, rcPort.top,
			rcPort.right, rcPort.bottom);
	DrawText(hdc, szSize, -1, &rcPort,
	DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	SelectObject(hdc, hfOld);  //Change back to stored old font

	EndPaint(handle, &ps);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// handle WM_ERASEBKGND
///////////////////////////////////////////////////////////////////////////////
int Controller::eraseBackground(WPARAM hdc)
{
	RECT rc;
	GetClientRect(handle, &rc);
	SetMapMode((HDC) hdc, MM_ANISOTROPIC);
	SetWindowExtEx((HDC) hdc, 100, 100, NULL);
	SetViewportOrgEx((HDC) hdc, 0, 26, NULL);
	SetViewportExtEx((HDC) hdc, rc.right, rc.bottom, NULL);
	FillRect((HDC) hdc, &rc, hbrWhite);

	int i, x, y;
	for (i = 0; i < 13; i++)
	{
		x = (i * 40) % 100;

		y = ((i * 40) / 100) * 20;
		SetRect(&rc, x, y, x + 20, y + 20);
		FillRect((HDC) hdc, &rc, hbrGray);
	}
	return (1L);

}
///////////////////////////////////////////////////////////////////////////////
// handle WM_CTLCOLOREDIT
///////////////////////////////////////////////////////////////////////////////
int Controller::colorEdit(WPARAM wParam, LPARAM lParam)
{
	if (GetDlgCtrlID((HWND) lParam) == IDC_MAIN_EDIT)
	{
		HDC hdcEditControl = (HDC) wParam;
		SetTextColor(hdcEditControl, RGB(255, 255, 255));
		SetBkColor(hdcEditControl, RGB(0, 0, 0));
		return ((LONG) mExo->hbrBackground);
	}
	else
	{
		return (0);
	}
}
///////////////////////////////////////////////////////////////////////////////
// handle WM_COMMAND
///////////////////////////////////////////////////////////////////////////////
int Controller::command(int id, int cmd, LPARAM msg)
{
	switch (id)
	{
	case IDM_FILE_NEW:
		break;

	case IDM_FILE_OPEN:
		break;

	case IDM_FILE_SAVE:
		break;

	case IDM_FILE_SAVEAS:
		break;

	case IDM_FILE_EXIT:
		PostMessage(handle, WM_CLOSE, 0, 0);
		break;

	case IDM_DRAW_ARROW:
		SendMessage(mExo->hWndToolbar, TB_SETSTATE, IDM_DRAW_ARROW,
		TBSTATE_CHECKED | TBSTATE_ENABLED);
		mExo->ChangeCurrentCursor(handle, IDC_ARROW);
		break;

	case IDM_DRAW_FREEHAND:
		SendMessage(mExo->hWndToolbar, TB_SETSTATE, IDM_DRAW_FREEHAND,
		TBSTATE_CHECKED | TBSTATE_ENABLED);
		mExo->ChangeCurrentCursor(handle, MAKEINTRESOURCE(IDC_FREEHAND));
		break;

	case IDM_DIALOG_SHOW:
	case IDC_PRESS:
		ppt = aptHexagon;
		cpt = 7;
		ShowWindow(mExo->hToolbar, SW_SHOW);
		InvalidateRect(handle, NULL, TRUE);
		return (1L);
	case IDM_DIALOG_HIDE:
	case IDC_OTHER:
		ppt = aptRectangle;
		cpt = 5;
		ShowWindow(mExo->hToolbar, SW_HIDE);
		InvalidateRect(handle, NULL, TRUE);
		return (1L);
	}
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// handle WM_SIZE notification
// Note that the input param, width and height is for client area only.
// It excludes non-client area.
///////////////////////////////////////////////////////////////////////////////
int Controller::size(int width, int height, WPARAM type)
{
	HWND hEdit;
	clientWidth = width;
	clientHeight = height;
	hEdit = GetDlgItem(handle, IDC_MAIN_EDIT);
	SetWindowPos(hEdit, NULL, 0, 426, clientWidth, clientHeight - 426,
	SWP_NOZORDER);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// handle WM_LBUTTONDOWN
///////////////////////////////////////////////////////////////////////////////
int Controller::leftButtonDown(WPARAM state, int x, int y) // for WM_LBUTTONDOWN: state, x, y
{
	char szFileName[MAX_PATH];
	HINSTANCE hInstance = GetModuleHandle(NULL);
	GetModuleFileName(hInstance, szFileName, MAX_PATH);
	std::stringstream message;
	message << szFileName << " -  mouse clicked at " << std::to_string(x)
			<< "," << y;
	MessageBox(handle, message.str().c_str(), "This program is:",
	MB_OK | MB_ICONINFORMATION);
	return (0);
}

///////////////////////////////////////////////////////////////////////////////
// handle LoadParent view IViewLoader interface
///////////////////////////////////////////////////////////////////////////////
void Controller::loadMainWindow()
{
	loadedView = viewFactory->createInstance(this,
			Module::ViewIdentity::MainWindow);
	parentView = loadedView;
	loadView(parentView);
}

///////////////////////////////////////////////////////////////////////////////
// private - show a view
///////////////////////////////////////////////////////////////////////////////
void Controller::loadView(View* view)
{
	view->showView();
	//loadedView = view;
}

}
