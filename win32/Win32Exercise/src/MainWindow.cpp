#include "model.h"
#include "Controller.h"
#include "MainWindow.h"

//------------------------------------------------------------------------------
namespace App
{
MainWindow::MainWindow()
{
	// If we declare  window class with a default constructor,
	// we need to reset the window to a nothing
	_hwnd = NULL;
	_controller = NULL;            // pointer to controller

}
//------------------------------------------------------------------------------
HWND MainWindow::Create(HINSTANCE hinst, const char * clsname,
		const char* wndname, Module::Controller* controller, HWND parent,
		DWORD dStyle, DWORD dXStyle, int x, int y, int width, int height)
{
	_controller = controller;
	_hwnd = CreateWindowEx(dXStyle, clsname, wndname, dStyle, x, y, width,
			height, parent, NULL, hinst, (LPVOID) controller); // window creation data
	if (_hwnd != NULL)
		return (_hwnd);
	return (NULL);
}
//------------------------------------------------------------------------------
//This method will be used to display the windows
BOOL MainWindow::Show(int dCmdShow)
{
	// We will display the main window as a regular object and update it
	if (ShowWindow(_hwnd, dCmdShow) && UpdateWindow(_hwnd))
		return (TRUE);
	return (FALSE);
}

//------------------------------------------------------------------------------
//Because each window is of type HWND we will need a way
//to recognize the window handle when used in our application
MainWindow::operator HWND()
{
	//This overloaded operator allows us to use HWND anyway we want
	return (_hwnd);
}

}
