#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDI_RAINDROP                            103
#define IDC_FREEHAND                            105
#define IDR_MAIN_MENU                           110
#define IDB_STANDARD                            113
#define IDM_FILE_NEW                            40000
#define IDM_FILE_OPEN                           40001
#define IDM_FILE_SAVE                           40002
#define IDM_FILE_SAVEAS                         40003
#define IDM_FILE_PRINT                          40005
#define IDM_FILE_EXIT                           40007
#define IDM_DRAW_ARROW                          40010
#define IDM_DRAW_FREEHAND                       40011
#define IDM_ABOUT                               40012
#define IDC_PRESS                               40013
#define IDC_OTHER                               40014
#define IDM_DIALOG_SHOW                         40015
#define IDM_DIALOG_HIDE                         40016

#define IDD_ABOUT								40025
#define IDD_TOOLBAR								40026
