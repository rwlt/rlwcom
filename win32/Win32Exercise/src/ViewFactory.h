/*
 * ViewFactory.h
 *
 *  Created on: Apr 9, 2015
 *      Author: Rodney Woollett
 */

#ifndef VIEWFACTORY_H_
#define VIEWFACTORY_H_

#include "ViewLoader.h"

namespace App
{

class ViewFactory: public Module::AbstractViewFactory
{
public:
	ViewFactory();
	~ViewFactory();
	virtual Module::View* createInstance(
			const Module::ViewLoader * const loader,
			Module::ViewIdentity viewIdentity);
private:
	Module::View* moduleView[Module::ViewIdentity::TotalViews];
};

}
#endif /* VIEWFACTORY_H_ */
