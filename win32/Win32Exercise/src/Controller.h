#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "ViewLoader.h"
#include "model.h"
#include "view.h"

namespace Module
{

class Controller : public ViewLoader
{
public:
	Controller(Module::AbstractViewFactory* abstractViewFactory, App::Model* exo); //ModelGL* model, ViewGL* view); // ctor with params
	~Controller(); // dtor

	void setHandle(HWND handle); // set window handle
	int close(); // close the RC and destroy window
	int command(int id, int cmd, LPARAM msg); // for WM_COMMAND
	int create(); // create RC for
	int activate();
	int destroy();
	int paint();
	int eraseBackground(WPARAM hdc);
	int colorEdit(WPARAM wParam, LPARAM lParam);
	int keyDown(int key, LPARAM lParam);
	int size(int width, int height, WPARAM type);
    int leftButtonDown(WPARAM state, int x, int y);    // for WM_LBUTTONDOWN: state, x, y

    void loadMainWindow(); // Load main window view

private:
	HWND handle; // window handle to map window to controller
	App::Model* mExo; //
	//      ViewGL* viewGL;                             //
	int clientWidth;		// width of client area
	int clientHeight;		// height of client area
	HBRUSH hbrWhite, hbrGray;

	POINT *ppt;
	int cpt;
	POINT aptStar[6];
	POINT aptTriangle[4], aptRectangle[5], aptPentagon[6], aptHexagon[7];

	View* loadedView; // remember what view is active
    View* parentView;
    AbstractViewFactory* viewFactory;
    void loadView(View* view);

};

inline void Controller::setHandle(HWND hwnd)
{
	handle = hwnd;
}

}
#endif
