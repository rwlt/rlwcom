#ifndef WINDOWCLASS_H
#define WINDOWCLASS_H

#include <windows.h>

namespace App {
//-----------------------------------------------------------------------------
class WindowClass
{
public:

  //This constructor will initial the application
  WindowClass();
  void Create(HINSTANCE hInst, const char *ClassName, WNDPROC WndPrc, const char* MenuName = NULL);

  //Class Registration
  void Register();
  void UnRegister();

protected:
  //Global variable that holds the application
  WNDCLASSEX _WndClsEx;
};
}
//-----------------------------------------------------------------------------
#endif
