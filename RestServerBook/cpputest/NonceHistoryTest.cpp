/*
 * NonceHistorytestTest.cpp
 *
 *  Created on: 5/06/2017
 *      Author: rodney
 */
#include "NonceHistory.h"
#include "hashalgs/NonceGenerator.h"
#include <thread>
#include <iostream>

//CppUTest includes should be after your and system includes
#include "CppUTest/TestHarness.h"

//
//-------------------------------------------------------
// NonceHistoryAccessor test group
//-------------------------------------------------------
//
TEST_GROUP(NonceHistoryAccessor) {
	NonceHistory nonceHistory; // defined to remove nonce after expireseconds static of 15 secs
	NonceGenerator ng;

	void setup() {

	}

	void teardown() {
	}
};


TEST(NonceHistoryAccessor, AddNonceAndDateAccessor) {

	DateTimeStamp dts(DateTimeStamp::currentDateTime());
	std::string nonce = ng.generate();
	CHECK(true == dts.acceptedTimeStamp(12));
	nonceHistory.addNonce(nonce, dts); // A verified nonce added will expire with DateTimeStamp check

	DateTimeStamp uts3("Aug301721:51:20 PM");
	CHECK(false == uts3.acceptedTimeStamp(12));
	STRCMP_EQUAL("Aug301721:51:20", uts3.getTimeStamp().c_str());

	LONGS_EQUAL(1, nonceHistory.size());
	//std::this_thread::sleep_for(std::chrono::milliseconds(500));

	time_t now;
	time(&now); /* get current time */
    char       buf[80];
    now-=14;
	struct tm * tm_now = localtime(&now);
    strftime(buf, sizeof(buf), "%b%d%y%T", tm_now);
    std::string timel = std::string(buf);

	DateTimeStamp dtsdue(timel);
	std::cout << " DTS due: " << dtsdue.getTimeStamp() << "\n";
	std::string nonce2 = ng.generate();
	CHECK(true == dts.acceptedTimeStamp(15));
	nonceHistory.addNonce(nonce2, dtsdue); // A verified nonce added will expire with DateTimeStamp check
	LONGS_EQUAL(2, nonceHistory.size());


	DateTimeStamp dts3(DateTimeStamp::currentDateTime());
	std::string nonce3 = ng.generate();
	nonceHistory.addNonce(nonce3, dts3); // A verified nonce added will expire with DateTimeStamp check
	DateTimeStamp dts4(DateTimeStamp::currentDateTime());
	std::string nonce4 = ng.generate();
	nonceHistory.addNonce(nonce4, dts4); // A verified nonce added will expire with DateTimeStamp check
	LONGS_EQUAL(4, nonceHistory.size());

	std::this_thread::sleep_for(std::chrono::milliseconds(1500));

	DateTimeStamp dts5(DateTimeStamp::currentDateTime());
	std::string nonce5 = ng.generate();
	nonceHistory.addNonce(nonce5, dts5); // A verified nonce added will expire with DateTimeStamp check
	LONGS_EQUAL(4, nonceHistory.size());

}

TEST(NonceHistoryAccessor, FindNonceAccessor) {

	DateTimeStamp dts(DateTimeStamp::currentDateTime());
	std::string nonce = ng.generate();
	CHECK(true == dts.acceptedTimeStamp(12));

	nonceHistory.addNonce(nonce, dts);
	CHECK(true == nonceHistory.findNonce(nonce));

	std::string noncenew = ng.generate();
	CHECK(false == nonceHistory.findNonce(noncenew));

	nonceHistory.addNonce(noncenew, dts);
	CHECK(true == nonceHistory.findNonce(noncenew));

	LONGS_EQUAL(2, nonceHistory.size());

	// Can add it again
	nonceHistory.addNonce(noncenew, dts);
	LONGS_EQUAL(2, nonceHistory.size());


}
