CREATE TABLE User (
      UserName     text not null unique,
      Salt         text not null,
      Secret       text not null,
      UserID       INTEGER NOT NULL PRIMARY KEY
);

INSERT INTO "User" (UserID, UserName, Salt, Secret)
VALUES
 (1, 'rod', '213019FF46E0577C6BECEC9966F0426C', 'F72DDA1919D8ECEA6C1684123486CAD94DE783AA43E1C3878C45F38F8E1C1E51');

--User:                                   rod
--Users password:                         pa$$w0rd
--User:                                   user1
--Users password:                         user1pa$$
--User:                                   user2
--Users password:                         user2pa$$
 INSERT INTO "User" (UserID, UserName, Salt, Secret)
VALUES
 (3, 'user2', '25696A674324669759B01B9A0560493F', '51F7201BC225B0A78F35A2FD4C5C3292DE5D36B75CA98635D0422A7ED5B01B2F');
 
SELECT * FROM User-- Where Username = 'rod'
