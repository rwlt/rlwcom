/*
 * Hero.h
 */

#ifndef RESTSERVERUSER_H_
#define RESTSERVERUSER_H_

#include "storage/EntityMap.h"
#include <string>

/// User
class User
{
public:
    /// Constructor
	User()
	{
	}
	/// Deconstructor
    virtual ~User() {};

	ENTITY_INT_ATTRIBUTE(UserID)
	ENTITY_STRING_ATTRIBUTE(UserName)
	ENTITY_STRING_ATTRIBUTE(Salt)
	ENTITY_STRING_ATTRIBUTE(Secret)

};


#endif /* RESTSERVERUSER_H_ */
