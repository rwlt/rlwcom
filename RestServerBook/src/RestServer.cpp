/*
 * RestServer.cpp
 *
 *  Created on: 26/08/2017
 *      Author: rodney
 */

#include "RestServer.h"
#include "RestRequest.h"
#include "hashalgs/NonceGenerator.h"
#include "hashalgs/DateTimeStamp.h"
#include "hashalgs/Base64.h"
//#include "User.h"
//#include "Entity.h"
#include "storage/storage.h"
//#include "storage/ParseDatabaseStatement.h"
//#include "storage/driver/DBSqliteDriver.h"
//#include "storage/RowItem.h"
//#include "json/json.h"

#ifdef DEBUG
#define D(x)
#else
#define D(x) x
#endif

// const char* RestServer::m_db_desc{"Module RestServer 1.0 - September 12, 2017"};
// const char* RestServer::m_xml{"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n"
//     "<module-dbstorage>\n"
//     "  <database>\n"
//     "   <file role=\"live\" driver=\"Sqlite\" name=\"test.db\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
//     "   <file role=\"test\" driver=\"Sqlite\" name=\"test.db\" server=\"localhost\" charset=\"ISO8859_1\"/>\n"
//     "  </database>\n"
// 	"<table entity=\"User\" name=\"User\" key=\"UserID\">\n"
// 	"  <database-statements>\n"
// 	"      <statement type=\"select\">\n"
// 	"        <field name=\"UserID\" type=\"integer\"/>\n"
// 	"        <field name=\"UserName\" type=\"string\"/>\n"
//    	"        <field name=\"Salt\" type=\"string\"/>\n"
//    	"        <field name=\"Secret\" type=\"string\"/>\n"
// 	"      </statement>\n"
// 	"  </database-statements>\n"
// 	"</table>\n"
//     "</module-dbstorage>\n"};

void RestServer::addResource(std::string path, ResourceCall call)
{
  auto pair = std::make_pair(path, call);
  mapCall.insert(pair);
}

bool RestServer::findResourcePath(std::string path)
{
  return mapCall.count(path) > 0;
}

ResourceCallReturn RestServer::doResource(RestRequest *con_info)
{
  json empty;
  empty = json::array({});
  std::string url = con_info->getUrl();
  if (mapCall.count(url) == 0)
  {
    return ResourceCallReturn(false, empty.dump());
  }
  auto result = mapCall[url](&personalModel, "", "");
  bool state;
  std::string item;
  std::tie(state, item) = result;
  return result;
}

long RestServer::get_file_size(const char *filename)
{
  FILE *fp;

  fp = fopen(filename, "rb");
  if (fp)
  {
    long size;

    if ((0 != fseek(fp, 0, SEEK_END)) || (-1 == (size = ftell(fp))))
      size = 0;

    fclose(fp);

    return size;
  }
  else
    return 0;
}

char *RestServer::load_file(const char *filename)
{
  FILE *fp;
  char *buffer;
  long size;

  size = get_file_size(filename);
  if (size == 0)
    return NULL;

  fp = fopen(filename, "rb");
  if (!fp)
    return NULL;

  buffer = (char *)malloc(size + 1);
  if (!buffer)
  {
    fclose(fp);
    return NULL;
  }

  if (size != (long)fread(buffer, 1, size, fp))
  {
    free(buffer);
    buffer = NULL;
  }
  buffer[size] = '\0';
  fclose(fp);
  return buffer;
}

int RestServer::ask_for_authentication(struct MHD_Connection *connection, const char *realm)
{
  int ret;
  struct MHD_Response *response;
  char *headervalue;
  const char *strbase = "Basic realm=";

  response = MHD_create_response_from_buffer(0, NULL,
                                             MHD_RESPMEM_PERSISTENT);
  if (!response)
    return MHD_NO;

  headervalue = (char *)malloc(strlen(strbase) + strlen(realm) + 1);
  if (!headervalue)
    return MHD_NO;

  strcpy(headervalue, strbase);
  strcat(headervalue, realm);

  ret = MHD_add_response_header(response, "WWW-Authenticate", headervalue);
  free(headervalue);
  if (!ret)
  {
    MHD_destroy_response(response);
    return MHD_NO;
  }

  ret = MHD_queue_response(connection, MHD_HTTP_UNAUTHORIZED, response);

  MHD_destroy_response(response);

  return ret;
}

int RestServer::is_authenticated(struct MHD_Connection *connection,
                                 const char *username, const char *password)
{
  const char *headervalue;
  std::string expected_b64, expected;
  const char *strbase = "Basic ";
  int authenticated;

  headervalue =
      MHD_lookup_connection_value(connection, MHD_HEADER_KIND,
                                  "Authorization");
  if (NULL == headervalue)
    return 0;

  authenticated = 0;
  // Basic
  if (0 == strncmp(headervalue, strbase, strlen(strbase)))
  {

    std::stringstream ucat;
    ucat << username << ":" << password;
    expected_b64 = Base64::string_to_base64(ucat.str().c_str(), ucat.str().length());

    authenticated =
        (strcmp(headervalue + strlen(strbase), expected_b64.c_str()) == 0);
  }
  return authenticated;
}

int RestServer::print_out_key(void *cls, enum MHD_ValueKind /*kind*/, const char *key, const char *value)
{
  RestRequest *con_info = (RestRequest *)cls;
  if (0 == strncmp(key, "Authorization", strlen("Authorization")))
  {
    std::string keyValue(value);
    if (keyValue.size() >= 5 && (keyValue.substr(0, 5) == HMAC_TOKEN))
    {
      D(std::cerr << "print_out_key: " << keyValue.substr(5) << "\n";)
      con_info->setAuthorization(keyValue.substr(5));
    }
  }
  if (0 == strncmp(key, "Date", strlen("Date")))
  {
    D(std::cerr << "print_out_key: " << value << "\n";)
    con_info->setDate(value);
  }
  return MHD_YES;
}

int RestServer::on_client_connect(void * /*cls*/, const struct sockaddr *addr, socklen_t /*addrlen*/)
{
  struct sockaddr_in *addr_in = (struct sockaddr_in *)addr;
  char ipstr[INET6_ADDRSTRLEN];

  if (inet_ntop(addr_in->sin_family, &addr_in->sin_addr, ipstr, INET6_ADDRSTRLEN) == NULL)
  {
    perror("inet_ntop");
  }
  else
  {
    printf("IP address 1: %s\n", ipstr);
  }
  return MHD_YES;
}

//
// Processing Post data
//
int RestServer::iterate_post(void *coninfo_cls, enum MHD_ValueKind /*kind*/, const char *key, const char * /*filename*/,
                             const char * /*content_type*/, const char * /*transfer_encoding*/, const char *data, uint64_t /*off*/, size_t size)
{
  std::cerr << "iterate_post " << key << " " << data << "\n";
  RestRequest *con_info = (RestRequest *)coninfo_cls;
  if (0 == strcmp(key, "name"))
  {
    if ((size > 0) && (size <= MAXNAMESIZE))
    {
      con_info->setResult("<html><body><h1>Welcome, " + std::string(data) + "!</center></h1></body></html>");
    }
    else
      con_info->setResult("");

    return MHD_NO;
  }
  return MHD_YES;
}

// Request completed cleanup
void RestServer::request_completed(void * /*cls*/, struct MHD_Connection * /*connection*/, void **con_cls,
                                   enum MHD_RequestTerminationCode /*toe*/)
{
  RestRequest *con_info = (RestRequest *)*con_cls;
  D(std::cerr << "RestServer::request_completed\n";)
  if (NULL == con_info)
    return;
  delete con_info;
  *con_cls = NULL;
}

// Send a page
int RestServer::send_page(struct MHD_Connection *connection, RestRequest *con_info)
{
  int ret = MHD_NO;
  struct MHD_Response *response;

  auto responseID = con_info->getResponseIdentity();
  auto pageInfo = con_info->getResult();
  //std::cerr << "RestServer::send_page: ]" << page << "]\npagelength: " << page.length() << "\n";
  if (pageInfo.first)
  {
    std::cerr << "RestServer::send_page ID: " << responseID << " [" << pageInfo.second << "]\n";
    response = MHD_create_response_from_buffer(pageInfo.first, (void *)pageInfo.second, MHD_RESPMEM_PERSISTENT);
  }
  else
  {
    std::cerr << "RestServer::send_page ID: empty " << responseID << " [" << pageInfo.first << "]\n";
    response = MHD_create_response_from_buffer(0, NULL, MHD_RESPMEM_PERSISTENT);
  }

  for (auto head : con_info->getHeaders())
  {
    std::cerr << head.first << " " << head.second << "\n";
    ret = MHD_add_response_header(response, head.first.c_str(), head.second.c_str());
    if (!ret)
    {
      std::cerr << "headerts?\n";
      MHD_destroy_response(response);
      return ret;
    }
  }

  if (!response)
  {
    return MHD_NO;
  }

  switch (responseID)
  {
  // case ResponseIdentity::Continue:
  //   ret = MHD_queue_response(connection, MHD_HTTP_CONTINUE, response);
  //   break;
  case ResponseIdentity::OK:
    ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
    break;
  case ResponseIdentity::AuthFail:
    ret = MHD_queue_response(connection, MHD_HTTP_UNAUTHORIZED, response);
    break;
  case ResponseIdentity::NotAccepted:
    ret = MHD_queue_response(connection, MHD_HTTP_NOT_ACCEPTABLE, response);
    break;
  case ResponseIdentity::FileNotFound:
    ret = MHD_queue_response(connection, MHD_HTTP_NOT_FOUND, response);
    break;
  case ResponseIdentity::ServerError:
    ret = MHD_queue_response(connection, MHD_HTTP_INTERNAL_SERVER_ERROR, response);
    break;
  default:
    ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
    break;
  }
  MHD_destroy_response(response);

  return ret;
}

// answer with post to process
int RestServer::answer_with_post(void *cls, struct MHD_Connection *connection,
                                 const char *url, const char *method,
                                 const char * /*version*/,
                                 const char *upload_data,
                                 size_t *upload_data_size,
                                 void **con_cls)
{

  if (NULL == *con_cls)
  {
    RestRequest *con_info = new RestRequest(method, url);
    if (con_info == NULL)
      return MHD_NO;
    *con_cls = (void *)con_info; // store pointer for connection
    return MHD_YES;
  }
  std::cerr << "answer_with_post\n";

  RestServer *rServer = (RestServer *)cls;
  RestRequest *con_info = (RestRequest *)*con_cls;
  json errs;
  errs["errors"] = json::array({}); // accumalate errs and set Request object then

  // The upload size is zero and is read by request we can now respond for POST and PUT
  // Check first for not acceptable HTTP verbs
  // The upload size is zero and is read by request we can now respond for POST and PUT
  MHD_get_connection_values(connection, MHD_HEADER_KIND, &print_out_key, con_info);

  std::cerr << "upload: " << *upload_data_size << " " << (*upload_data_size == 0) << "\n";
  if (*upload_data_size == 0 &&
      con_info->isUpLoaded() &&
      (0 == strcmp(method, "POST") || 0 == strcmp(method, "PUT")))
  {
    std::cerr << "Upload at zero and done!\n";
    if (con_info->is_valid_request())
    {
      return send_page(connection, con_info);
    }
    else
    {
      // std::cerr << "Unauthorised done!\n";
      // json msg;
      // msg["message"] = "Unauthorised access";
      // errs["errors"].push_back(msg);
      // con_info->setStatus(ResponseIdentity::AuthFail);
      // con_info->setResult(errs.dump());
      return send_page(connection, con_info);
    }
  }

  // Now check for authenticate request
  // Reads the headers for User, Data Nonce and Hash made with shared secret
  // see foundScret below - apparently stored in a db read later
  MHD_get_connection_values(connection, MHD_HEADER_KIND, &print_out_key, con_info);
  //MHD_get_connection_values(connection, MHD_HEADER_KIND, &print_out_key, con_info);

  RestRequest::RequestTimeStamp timeStamp = con_info->getDateTimeStamp();
  DateTimeStamp dts = std::get<0>(timeStamp);
  auto nonce = std::get<1>(timeStamp);
  //213019FF46E0577C6BECEC9966F0426C api key
  //213019FF46E0577C6BECEC9966F0426C
  UserKeyCallReturn readSecret = rServer->read_user_key(con_info->getUserName());
  std::string secret = std::get<1>(readSecret);
  bool foundSecret = std::get<0>(readSecret);

  // IMPORTANT the next statement is evaluated in the connection request.
  // Expired date timestamp go to error page
  // Find user secret otherwise go to error page
  // Using || left to right condition evaluation so if first is true the next is not
  // evaluated.
  if (false == con_info->is_valid_request() ||
      false == dts.acceptedTimeStamp(12) ||
      true == rServer->m_nonceHistory.findNonce(nonce) ||
      foundSecret == false ||
      con_info->is_hmac_authorized(secret) != true)
  {
    if (0 == strcmp(method, "POST") && 0 == strcmp(url, "/namepost"))
    {
      // Process the Post form url namepost
      MHD_PostProcessor *postprocessor = MHD_create_post_processor(connection, POSTBUFFERSIZE, iterate_post, (void *)con_info);
      if (NULL == postprocessor)
      {
        std::cerr << "Could not create MHD_create_post_processor\n ";
        return MHD_NO;
      }
      con_info->setMHDPostProcessor(postprocessor);
      /* evaluate POST data from a login (Requires HTML url encoded form for MHD*/
      MHD_post_process(con_info->getMHDPostProcessor(), upload_data, *upload_data_size);
      if (0 != *upload_data_size)
      {
        *upload_data_size = 0;
      }
      con_info->setUploaded(true);
      return MHD_YES;
    }
    else if (0 == strcmp(method, "PUT") ||
             0 == strcmp(method, "POST"))
    {
      // if (*upload_data_size != 0) {
      //   con_info->setUploaded(true);
      // }
      json msg;
      msg["message"] = "Unauthorised access";
      errs["errors"].push_back(msg);
      con_info->setStatus(ResponseIdentity::AuthFail);
      con_info->setResult(errs.dump());
      con_info->setUploaded(true);
      *upload_data_size = 0;
      return MHD_YES;
    }
    else
    {
      json msg;
      msg["message"] = "Unauthorised access";
      errs["errors"].push_back(msg);
      con_info->setStatus(ResponseIdentity::AuthFail);
      con_info->setResult(errs.dump());
      return send_page(connection, con_info);
    }
  }

  // It is a valid user and hmac authorised. Can now Store the timestamp nonce
  // as processed at time stamp for valid request. Nonce can only be used once.
  rServer->m_nonceHistory.addNonce(nonce, dts);

  if (0 == strcmp(method, "GET"))
  {
    ResourceCallReturn result = rServer->doResource(con_info);
    if (std::get<0>(result))
    {
      con_info->setStatus(ResponseIdentity::OK);
      con_info->setResult(std::get<1>(result));
    }
    else
    {
      json msg;
      msg["message"] = "Unknown url: " + con_info->getUrl();
      errs["errors"].push_back(msg);
      con_info->setStatus(ResponseIdentity::FileNotFound);
      con_info->setResult(errs.dump());
    }
    return send_page(connection, con_info);
  }
  else if (0 == strcmp(method, "PUT") ||
           0 == strcmp(method, "POST"))
  {

    if (*upload_data_size != 0)
    {
      // Content-Type application/json is the only allowed content here
      std::cerr << "JSON body load " << *upload_data_size << "\n ";
      json root;
      std::vector<std::string> errList;
      bool res = rServer->uploadJson(root, upload_data, *upload_data_size, errList);
      if (!res)
      {
        con_info->setStatus(ResponseIdentity::NotAccepted);
        json errs;
        errs["errors"] = json::array({});
        for (auto err : errList)
        {
          json msg;
          msg["message"] = err;
          errs["errors"].push_back(msg);
        }
        con_info->setResult(errs.dump());
        *upload_data_size = 0;
        con_info->setUploaded(true);
        return MHD_YES;
      }

      //Just sending back what was sent to work
      // Do POST or PUT json into Models
      std::string document = root.dump();
      con_info->setStatus(ResponseIdentity::OK);
      con_info->setResult(document);
      con_info->setUploaded(true);
      *upload_data_size = 0;
      return MHD_YES;
    }
    else
    {
      json msg;
      msg["message"] = "Empty post not allowed: " + std::string(method);
      errs["errors"].push_back(msg);
      con_info->setStatus(ResponseIdentity::NotAccepted);
      con_info->setResult(errs.dump());
      return send_page(connection, con_info);
    }
  }
  else if (0 == strcmp(method, "DELETE"))
  {
    json msg;
    msg["message"] = "Not accepted: " + std::string(method);
    errs["errors"].push_back(msg);
    con_info->setStatus(ResponseIdentity::NotAccepted);
    con_info->setResult(errs.dump());
    return send_page(connection, con_info);
  }

  // Other verbs not processed result in NotAccepted status
  json msg;
  msg["message"] = "Not accepted: " + std::string(method);
  errs["errors"].push_back(msg);
  con_info->setStatus(ResponseIdentity::NotAccepted);
  con_info->setResult(errs.dump());
  return send_page(connection, con_info);
}

UserKeyCallReturn RestServer::read_user_key(std::string /*username*/)
{
  // This is what is stored in db
  std::string secret{"F72DDA1919D8ECEA6C1684123486CAD94DE783AA43E1C3878C45F38F8E1C1E51"};
  //bool found = true;// false;
  //std::string secret{""};
  bool found = true;
  // Storage::DBStorage db(m_db_desc, m_xml);
  // auto result = db.loginUser("TEST", "pass");
  //   if (true == db.isLoggedUser()) {
  //   	std::vector<User> result = db.doRead<User>({{"UserName", username}}, {});
  //       if (1 == result.size()) {
  // 	   found = true;
  // 	   secret = result.at(0).getSecret();
  //       }
  //   }
  return UserKeyCallReturn(found, secret);
}

bool RestServer::uploadJson(json &root,
                            const char *upload_data,
                            size_t upload_data_size,
                            std::vector<std::string> &errs)
{
  std::stringstream parsein;
  parsein.write(upload_data, upload_data_size);
  parsein.put('\0');
  if (!parsein.good())
  {
    errs.push_back("error in upload to stream");
    return false;
  }
  // May need error check TO
  try
  {
    root = json::parse(parsein.str());
  }
  catch (json::exception &e)
  {
    errs.push_back(std::to_string(e.id) + " " + e.what());
    return false;
  }
  //bool ok = Json::parseFromStream(rbuilder, parsein, &root, &errs);
  return true;
}

int RestServer::run()
{

  key_pem = load_file("server.key");
  cert_pem = load_file("server.pem");
  if ((key_pem == NULL) || (cert_pem == NULL))
  {
    printf("The key/certificate files could not be read.\n");
    return 1;
  }

  daemon = MHD_start_daemon(MHD_USE_SELECT_INTERNALLY /* | MHD_USE_SSL*/,
                            PORT, &RestServer::on_client_connect, NULL,
                            &RestServer::answer_with_post, this,
                            /*MHD_OPTION_HTTPS_MEM_KEY, key_pem,
                            MHD_OPTION_HTTPS_MEM_CERT, cert_pem,*/
                            MHD_OPTION_NOTIFY_COMPLETED, &RestServer::request_completed, NULL, MHD_OPTION_END);
  if (NULL == daemon)
  {
    printf("%s\n", cert_pem);
    free(key_pem);
    free(cert_pem);
    return 1;
  }

  std::cout << "Rest Server running\n";
  getchar();
  std::cout << "Rest Server stopped\n";

  MHD_stop_daemon(daemon);
  free(key_pem);
  free(cert_pem);

  return 0;
}
