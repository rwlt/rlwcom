//============================================================================
#include "RestServer.h"
#include "nholmann/json.hpp"
using json = nlohmann::json;
using Module::PersonalModel;
using Module::PA;

#include <stdio.h>

int main(int /*argc*/, char ** /*argv*/)
{
  RestServer restserver;

  ResourceCall pcall = [](Module::PersonalModel *db, const std::string&, const std::string&) -> ResourceCallReturn {
    json root;
    auto list = db->doRead(); 
    root["persons"] = json::array({});
    for (auto& p : list)
    {
      json person;
      auto pID = std::get<0>(p->get(PA.F.ID));
      auto pName = std::get<0>(p->get(PA.F.Name));
      person["PersonID"] = pID;
      person["Name"] = pName;
      root["persons"].push_back(person);
    }
    std::string document = root.dump();
    ResourceCallReturn result(true, document);
    return result;
  };

  restserver.addResource("/persons", pcall);
  return restserver.run();
}
