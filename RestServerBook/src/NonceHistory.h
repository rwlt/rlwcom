/*
 * NonceHistory.h
 *
 *  Created on: 11/09/2017
 *      Author: rodney
 */

#ifndef NONCEHISTORY_H_
#define NONCEHISTORY_H_

#include <stdexcept>
#include <string>
#include <unordered_map>
#include "hashalgs/DateTimeStamp.h"
#include <mutex>

typedef std::unordered_map<std::string, DateTimeStamp> MapNonce;
typedef std::pair<std::string, DateTimeStamp> MapNoncePair;

class NonceHistory {

	MapNonce m_nonces;
	mutable std::mutex m_Mutex{};
	static const int secondexpiry;

public:
	NonceHistory() {
	}
	virtual ~NonceHistory() {
	}

	void addNonce(std::string nonce, DateTimeStamp timestamp);

	inline bool findNonce(std::string nonce) {
		return (m_nonces.count(nonce) > 0);
	}

	std::size_t size() {
		return m_nonces.size();
	}
};


#endif /* NONCEHISTORY_H_ */
