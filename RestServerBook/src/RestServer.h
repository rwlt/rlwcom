/*
 * RestServer.h
 *
 *  Created on: 26/08/2017
 *      Author: rodney
 */

#ifndef SRC_APP_RESTSERVER_H_
#define SRC_APP_RESTSERVER_H_

#include <microhttpd.h>
#include <map>
#include "storage/storage.h"
#include "RestRequest.h"
#include "NonceHistory.h"
#include "PersonalModel.h"
#include "nholmann/json.hpp"

#include <tuple>
#include <functional>
#include <cstring>

#ifdef __cplusplus
extern "C"
{
#endif
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/time.h>
#ifdef __cplusplus
}
#endif

using json = nlohmann::json;
using Module::PA;

#define PORT 8888
//#define FILENAME "picture.png" /* "background.png"*/
//#define MIMETYPE "image/png"
#define MAXNAMESIZE 20
#define POSTBUFFERSIZE 2048

typedef std::tuple<bool, std::string> ResourceCallReturn;

typedef std::tuple<bool, std::string> UserKeyCallReturn;
//std::map resources;
typedef std::function<ResourceCallReturn(Module::PersonalModel *, const std::string &, const std::string &)> ResourceCall;
/// Table map for resource locators urls
typedef std::map<std::string, ResourceCall> ResourceTableMap;
/// Iterator for map of table names
typedef std::map<std::string, ResourceCall>::iterator ResourceTableMap_Iter;
/// Map value type for iter
typedef std::map<std::string, ResourceCall>::value_type ResourceTableMap_Value_type;

class RestServer
{
  Module::PersonalModel personalModel;
  ResourceTableMap mapCall;
  NonceHistory m_nonceHistory;
  char *key_pem;
  char *cert_pem;
  struct MHD_Daemon *daemon;

public:
  RestServer() : key_pem(0), cert_pem(0), daemon(0)
  {
  }

  virtual ~RestServer()
  {
  }

  static long get_file_size(const char *filename);
  static char *load_file(const char *filename);

  static int ask_for_authentication(struct MHD_Connection *connection, const char *realm);
  static int is_authenticated(struct MHD_Connection *connection,
                              const char *username, const char *password);

  void addResource(std::string, ResourceCall);
  bool findResourcePath(std::string);
  ResourceCallReturn doResource(RestRequest *con_info);
  UserKeyCallReturn read_user_key(std::string username);
  bool uploadJson(json &root,
                  const char *upload_data,
                  size_t upload_data_size,
                  std::vector<std::string> &errs);

  int run();

  // Static libmicrohttp specific methods
  static int print_out_key(void *cls, enum MHD_ValueKind kind, const char *key, const char *value);
  static int on_client_connect(void *cls, const struct sockaddr *addr, socklen_t addrlen);
  //
  // Processing Post data
  //
  static int iterate_post(void *coninfo_cls, enum MHD_ValueKind kind, const char *key, const char *filename,
                          const char *content_type, const char *transfer_encoding, const char *data, uint64_t off, size_t size);

  // Request completed cleanup
  static void request_completed(void *cls, struct MHD_Connection *connection, void **con_cls,
                                enum MHD_RequestTerminationCode toe);

  // Send a page
  static int send_page(struct MHD_Connection *connection, RestRequest *con_info);

  // answer with post to process
  static int answer_with_post(void *cls, struct MHD_Connection *connection, const char *url, const char *method,
                              const char *version, const char *upload_data, size_t *upload_data_size, void **con_cls);

  static int answer_with_authentication(void *cls, struct MHD_Connection *connection, const char *url, const char *method,
                                        const char *version, const char *upload_data, size_t *upload_data_size, void **con_cls);
};

#endif /* SRC_APP_RESTSERVER_H_ */
